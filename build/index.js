!function(modules) {
    var installedModules = {};
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) return installedModules[moduleId].exports;
        var module = installedModules[moduleId] = {
            i: moduleId,
            l: !1,
            exports: {}
        };
        return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), 
        module.l = !0, module.exports;
    }
    __webpack_require__.m = modules, __webpack_require__.c = installedModules, __webpack_require__.d = function(exports, name, getter) {
        __webpack_require__.o(exports, name) || Object.defineProperty(exports, name, {
            enumerable: !0,
            get: getter
        });
    }, __webpack_require__.r = function(exports) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(exports, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(exports, "__esModule", {
            value: !0
        });
    }, __webpack_require__.t = function(value, mode) {
        if (1 & mode && (value = __webpack_require__(value)), 8 & mode) return value;
        if (4 & mode && "object" == typeof value && value && value.__esModule) return value;
        var ns = Object.create(null);
        if (__webpack_require__.r(ns), Object.defineProperty(ns, "default", {
            enumerable: !0,
            value
        }), 2 & mode && "string" != typeof value) for (var key in value) __webpack_require__.d(ns, key, function(key) {
            return value[key];
        }.bind(null, key));
        return ns;
    }, __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function getDefault() {
            return module.default;
        } : function getModuleExports() {
            return module;
        };
        return __webpack_require__.d(getter, "a", getter), getter;
    }, __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    }, __webpack_require__.p = "", __webpack_require__(__webpack_require__.s = 131);
}([ function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return __decorate;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return __param;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return __metadata;
    }));
    function __decorate(decorators, target, key, desc) {
        var d, c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc;
        if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function(target, key) {
            decorator(target, key, paramIndex);
        };
    }
    function __metadata(metadataKey, metadataValue) {
        if ("object" == typeof Reflect && "function" == typeof Reflect.metadata) return Reflect.metadata(metadataKey, metadataValue);
    }
    Object.create;
    Object.create;
}, function(module, exports) {
    module.exports = require("typescript-ioc");
}, function(module, exports) {
    module.exports = require("typeorm");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "n", (function() {
        return kfs;
    })), __webpack_require__.d(__webpack_exports__, "o", (function() {
        return last;
    })), __webpack_require__.d(__webpack_exports__, "g", (function() {
        return escTaggedString;
    })), __webpack_require__.d(__webpack_exports__, "f", (function() {
        return escHtml;
    })), __webpack_require__.d(__webpack_exports__, "l", (function() {
        return html;
    })), __webpack_require__.d(__webpack_exports__, "d", (function() {
        return dropHidden;
    })), __webpack_require__.d(__webpack_exports__, "v", (function() {
        return waitResponse;
    })), __webpack_require__.d(__webpack_exports__, "i", (function() {
        return findTypeOf;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return dnsResolver;
    })), __webpack_require__.d(__webpack_exports__, "m", (function() {
        return http;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return comap;
    })), __webpack_require__.d(__webpack_exports__, "t", (function() {
        return toChatId;
    })), __webpack_require__.d(__webpack_exports__, "u", (function() {
        return toSupergroupId;
    })), __webpack_require__.d(__webpack_exports__, "s", (function() {
        return tgLink;
    })), __webpack_require__.d(__webpack_exports__, "r", (function() {
        return split;
    })), __webpack_require__.d(__webpack_exports__, "q", (function() {
        return sortByKey;
    })), __webpack_require__.d(__webpack_exports__, "k", (function() {
        return hashString;
    })), __webpack_require__.d(__webpack_exports__, "j", (function() {
        return hashArray;
    })), __webpack_require__.d(__webpack_exports__, "e", (function() {
        return equalArrays;
    })), __webpack_require__.d(__webpack_exports__, "h", (function() {
        return ffprobe;
    })), __webpack_require__.d(__webpack_exports__, "p", (function() {
        return regTransform;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return asyncMemoizeLast;
    }));
    var cacheable_lookup__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(75), cacheable_lookup__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(cacheable_lookup__WEBPACK_IMPORTED_MODULE_0__), child_process__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(16), crypto__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(50), fast_deep_equal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(76), fast_deep_equal__WEBPACK_IMPORTED_MODULE_3___default = __webpack_require__.n(fast_deep_equal__WEBPACK_IMPORTED_MODULE_3__), got__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(77), got__WEBPACK_IMPORTED_MODULE_4___default = __webpack_require__.n(got__WEBPACK_IMPORTED_MODULE_4__), key_file_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(78), key_file_storage__WEBPACK_IMPORTED_MODULE_5___default = __webpack_require__.n(key_file_storage__WEBPACK_IMPORTED_MODULE_5__), dns__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(79), _utils_data_path__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(8);
    const kfs = key_file_storage__WEBPACK_IMPORTED_MODULE_5___default()(Object(_utils_data_path__WEBPACK_IMPORTED_MODULE_7__.a)("kfs"));
    function last(a) {
        return a && a[a.length - 1];
    }
    const taggedMap = {
        "{": "｛",
        "}": "｝",
        "`": "ᐠ",
        "\\": "⧷",
        "\u2028": "\n",
        "\u2029": "\n"
    };
    function escTaggedString(s) {
        return s ? s.replace(/[\{\}`\\\u{2028}\u{2029}]/gu, m => taggedMap[m]) : "";
    }
    const htmlMap = {
        ...taggedMap,
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;"
    };
    function escHtml(s) {
        return s ? s.replace(/[\{\}`\\\u{2028}\u{2029}&<>]/gu, m => htmlMap[m]) : "";
    }
    async function html(literals, ...placeholders) {
        placeholders = await Promise.all(placeholders);
        let result = escHtml(literals[0]);
        for (let i = 0; i < placeholders.length; i++) result += placeholders[i] || "", result += escHtml(literals[i + 1]);
        return result;
    }
    const NONE_PRINTABLE = /[\u0000-\u0008\u000B-\u001F\u007F-\u009F\u00AD\u0378\u0379\u037F-\u0383\u038B\u038D\u03A2\u0528-\u0530\u0557\u0558\u0560\u0588\u058B-\u058E\u0590\u05C8-\u05CF\u05EB-\u05EF\u05F5-\u0605\u061C\u061D\u06DD\u070E\u070F\u074B\u074C\u07B2-\u07BF\u07FB-\u07FF\u082E\u082F\u083F\u085C\u085D\u085F-\u089F\u08A1\u08AD-\u08E3\u08FF\u0978\u0980\u0984\u098D\u098E\u0991\u0992\u09A9\u09B1\u09B3-\u09B5\u09BA\u09BB\u09C5\u09C6\u09C9\u09CA\u09CF-\u09D6\u09D8-\u09DB\u09DE\u09E4\u09E5\u09FC-\u0A00\u0A04\u0A0B-\u0A0E\u0A11\u0A12\u0A29\u0A31\u0A34\u0A37\u0A3A\u0A3B\u0A3D\u0A43-\u0A46\u0A49\u0A4A\u0A4E-\u0A50\u0A52-\u0A58\u0A5D\u0A5F-\u0A65\u0A76-\u0A80\u0A84\u0A8E\u0A92\u0AA9\u0AB1\u0AB4\u0ABA\u0ABB\u0AC6\u0ACA\u0ACE\u0ACF\u0AD1-\u0ADF\u0AE4\u0AE5\u0AF2-\u0B00\u0B04\u0B0D\u0B0E\u0B11\u0B12\u0B29\u0B31\u0B34\u0B3A\u0B3B\u0B45\u0B46\u0B49\u0B4A\u0B4E-\u0B55\u0B58-\u0B5B\u0B5E\u0B64\u0B65\u0B78-\u0B81\u0B84\u0B8B-\u0B8D\u0B91\u0B96-\u0B98\u0B9B\u0B9D\u0BA0-\u0BA2\u0BA5-\u0BA7\u0BAB-\u0BAD\u0BBA-\u0BBD\u0BC3-\u0BC5\u0BC9\u0BCE\u0BCF\u0BD1-\u0BD6\u0BD8-\u0BE5\u0BFB-\u0C00\u0C04\u0C0D\u0C11\u0C29\u0C34\u0C3A-\u0C3C\u0C45\u0C49\u0C4E-\u0C54\u0C57\u0C5A-\u0C5F\u0C64\u0C65\u0C70-\u0C77\u0C80\u0C81\u0C84\u0C8D\u0C91\u0CA9\u0CB4\u0CBA\u0CBB\u0CC5\u0CC9\u0CCE-\u0CD4\u0CD7-\u0CDD\u0CDF\u0CE4\u0CE5\u0CF0\u0CF3-\u0D01\u0D04\u0D0D\u0D11\u0D3B\u0D3C\u0D45\u0D49\u0D4F-\u0D56\u0D58-\u0D5F\u0D64\u0D65\u0D76-\u0D78\u0D80\u0D81\u0D84\u0D97-\u0D99\u0DB2\u0DBC\u0DBE\u0DBF\u0DC7-\u0DC9\u0DCB-\u0DCE\u0DD5\u0DD7\u0DE0-\u0DF1\u0DF5-\u0E00\u0E3B-\u0E3E\u0E5C-\u0E80\u0E83\u0E85\u0E86\u0E89\u0E8B\u0E8C\u0E8E-\u0E93\u0E98\u0EA0\u0EA4\u0EA6\u0EA8\u0EA9\u0EAC\u0EBA\u0EBE\u0EBF\u0EC5\u0EC7\u0ECE\u0ECF\u0EDA\u0EDB\u0EE0-\u0EFF\u0F48\u0F6D-\u0F70\u0F98\u0FBD\u0FCD\u0FDB-\u0FFF\u10C6\u10C8-\u10CC\u10CE\u10CF\u1249\u124E\u124F\u1257\u1259\u125E\u125F\u1289\u128E\u128F\u12B1\u12B6\u12B7\u12BF\u12C1\u12C6\u12C7\u12D7\u1311\u1316\u1317\u135B\u135C\u137D-\u137F\u139A-\u139F\u13F5-\u13FF\u169D-\u169F\u16F1-\u16FF\u170D\u1715-\u171F\u1737-\u173F\u1754-\u175F\u176D\u1771\u1774-\u177F\u17DE\u17DF\u17EA-\u17EF\u17FA-\u17FF\u180F\u181A-\u181F\u1878-\u187F\u18AB-\u18AF\u18F6-\u18FF\u191D-\u191F\u192C-\u192F\u193C-\u193F\u1941-\u1943\u196E\u196F\u1975-\u197F\u19AC-\u19AF\u19CA-\u19CF\u19DB-\u19DD\u1A1C\u1A1D\u1A5F\u1A7D\u1A7E\u1A8A-\u1A8F\u1A9A-\u1A9F\u1AAE-\u1AFF\u1B4C-\u1B4F\u1B7D-\u1B7F\u1BF4-\u1BFB\u1C38-\u1C3A\u1C4A-\u1C4C\u1C80-\u1CBF\u1CC8-\u1CCF\u1CF7-\u1CFF\u1DE7-\u1DFB\u1F16\u1F17\u1F1E\u1F1F\u1F46\u1F47\u1F4E\u1F4F\u1F58\u1F5A\u1F5C\u1F5E\u1F7E\u1F7F\u1FB5\u1FC5\u1FD4\u1FD5\u1FDC\u1FF0\u1FF1\u1FF5\u1FFF\u200B\u200C\u200E\u200F\u202A-\u202E\u2060-\u206F\u2072\u2073\u208F\u209D-\u209F\u20BB-\u20CF\u20F1-\u20FF\u218A-\u218F\u23F4-\u23F7\u23FB-\u23FF\u2427-\u243F\u244B-\u245F\u2700\u2B4D-\u2B4F\u2B5A-\u2BFF\u2C2F\u2C5F\u2CF4-\u2CF8\u2D26\u2D28-\u2D2C\u2D2E\u2D2F\u2D68-\u2D6E\u2D71-\u2D7E\u2D97-\u2D9F\u2DA7\u2DAF\u2DB7\u2DBF\u2DC7\u2DCF\u2DD7\u2DDF\u2E3C-\u2E7F\u2E9A\u2EF4-\u2EFF\u2FD6-\u2FEF\u2FFC-\u2FFF\u3040\u3097\u3098\u3100-\u3104\u312E-\u3130\u318F\u31BB-\u31BF\u31E4-\u31EF\u321F\u32FF\u4DB6-\u4DBF\u9FCD-\u9FFF\uA48D-\uA48F\uA4C7-\uA4CF\uA62C-\uA63F\uA698-\uA69E\uA6F8-\uA6FF\uA78F\uA794-\uA79F\uA7AB-\uA7F7\uA82C-\uA82F\uA83A-\uA83F\uA878-\uA87F\uA8C5-\uA8CD\uA8DA-\uA8DF\uA8FC-\uA8FF\uA954-\uA95E\uA97D-\uA97F\uA9CE\uA9DA-\uA9DD\uA9E0-\uA9FF\uAA37-\uAA3F\uAA4E\uAA4F\uAA5A\uAA5B\uAA7C-\uAA7F\uAAC3-\uAADA\uAAF7-\uAB00\uAB07\uAB08\uAB0F\uAB10\uAB17-\uAB1F\uAB27\uAB2F-\uABBF\uABEE\uABEF\uABFA-\uABFF\uD7A4-\uD7AF\uD7C7-\uD7CA\uD7FC-\uD83B\uD83F-\uDB3F\uDB41-\uDBFF\uDCFE\uDD46\uDDCC\uDE52-\uDE6F\uDE75-\uDE77\uDE7B-\uDE7F\uDEC6-\uDECA\uDED8-\uDEDF\uDEE6-\uDEE8\uDEEA\uDEED-\uDEEF\uDEF1\uDEF2\uDEFD-\uDEFF\uDF22\uDF23\uDF94\uDF95\uDF98\uDF9C\uDF9D\uDFF1\uDFF2\uDFF6\uE000-\uF8FF\uFA6E\uFA6F\uFADA-\uFAFF\uFB07-\uFB12\uFB18-\uFB1C\uFB37\uFB3D\uFB3F\uFB42\uFB45\uFBC2-\uFBD2\uFD40-\uFD4F\uFD90\uFD91\uFDC8-\uFDEF\uFDFE\uFDFF\uFE0F\uFE1A-\uFE1F\uFE27-\uFE2F\uFE53\uFE67\uFE6C-\uFE6F\uFE75\uFEFD-\uFF00\uFFBF-\uFFC1\uFFC8\uFFC9\uFFD0\uFFD1\uFFD8\uFFD9\uFFDD-\uFFDF\uFFE7\uFFEF-\uFFFB\uFFFE\uFFFF]/g;
    function dropHidden(s) {
        return NONE_PRINTABLE.lastIndex = 0, s ? s.normalize().replace(NONE_PRINTABLE, "") : "";
    }
    function waitResponse(request) {
        return new Promise((resolve, reject) => request.once("close", resolve).once("error", reject).once("response", resolve)).finally(() => {
            request.destroy();
        });
    }
    function findTypeOf(props, a) {
        if (null == a ? void 0 : a.length) for (let p of props) {
            const v = a.find(e => e.type === p);
            if (v) return v;
        }
    }
    const dnsResolver = new dns__WEBPACK_IMPORTED_MODULE_6__.promises.Resolver({
        timeout: 1e4
    }), http = got__WEBPACK_IMPORTED_MODULE_4___default.a.extend({
        method: "GET",
        responseType: "buffer",
        timeout: 3e4,
        maxRedirects: 16,
        dnsCache: new cacheable_lookup__WEBPACK_IMPORTED_MODULE_0___default.a({
            resolver: dnsResolver,
            fallbackDuration: 0,
            lookup: null
        }),
        dnsLookupIpVersion: "ipv4",
        headers: {
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36",
            Referer: "https://vk.com/",
            "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"
        },
        https: {
            rejectUnauthorized: !1
        },
        retry: 0
    });
    function comap(a, map, options, ...args) {
        const breakOnError = !!(null == options ? void 0 : options.breakOnError), filter = (null == options ? void 0 : options.filter) || Boolean, required = Math.min(Math.max(0, null == options ? void 0 : options.required) || a.length, a.length);
        let concurrency = Math.min(Math.max(0, null == options ? void 0 : options.concurrency) || required, required);
        let i = 0;
        const s = new Set;
        function next() {
            if (s.size < required && i < a.length) {
                const p = async function _map(v) {
                    return map(v, ...args);
                }(a[i++]);
                return s.add(p), p.then(r => {
                    filter(r) || s.delete(p), next();
                }, breakOnError ? _ => {
                    i = a.length;
                } : next);
            }
        }
        const threads = [];
        for (;concurrency--; ) threads.push(next());
        return Promise.allSettled(threads).then(_ => Promise.all(s));
    }
    function toChatId(supergroup_id) {
        return +("-100" + supergroup_id);
    }
    function toSupergroupId(chat_id) {
        return +chat_id.toString().replace(/^-100/, "");
    }
    function tgLink(chat_id, message_id = 1048576) {
        return `tg://privatepost?channel=${toSupergroupId(chat_id)}&post=${message_id / 1048576}`;
    }
    function* split(s, size) {
        for (let i = 0; i < s.length; i += size) yield s.slice(i, i + size);
    }
    function sortByKey(a, key) {
        return a.sort((a, b) => a[key].localeCompare(b[key]));
    }
    function hashString(s) {
        return Object(crypto__WEBPACK_IMPORTED_MODULE_2__.createHash)("sha1").update(s, "utf-8").digest("base64url");
    }
    function hashArray(a, ...keys) {
        return hashString(a.map(v => keys.map(k => v[k]).join("?")).sort().join("|"));
    }
    function equalArrays(a, b, key) {
        return a.map(v => v[key]).sort().join("|") === b.map(v => v[key]).sort().join("|");
    }
    function ffprobe(path) {
        return new Promise(resolve => {
            Object(child_process__WEBPACK_IMPORTED_MODULE_1__.execFile)("ffprobe", [ "-hide_banner", "-show_streams", "-print_format", "json", path ], (err, stdout, stderr) => {
                try {
                    if (err) throw "ENOENT" === err.code ? err : stderr;
                    return resolve(JSON.parse(stdout));
                } catch (e) {
                    console.error.bind(console, "(./src/utils.ts:199)")(e);
                }
                resolve(null);
            });
        });
    }
    const regTransform = (() => {
        const lgroups = [ "а - aя - aо - o0 ѳөы - y", "б - b ", "в - vb ѵ", "г - g ԍ", "д - d ԁ", "ж - zj ", "з - z3 ӡ", "и - i ӏіĭй - j ѝӣе - e ҽё - oэ - e", "к - k", "л - l ᴫ", "м - m ", "н - nh", "п - pn", "р - rp", "с - sc", "т - t", "у - uyю - u", "ф - f", "х - hx", "ц - ct", "ч - c", "шщ - s", "q - кԛ", "h - һ", "w - вԝѡ", "j - ј", "x - к", "@" ], LETTERS = Array.from(new Set(lgroups.join("").replace(/[\-\s]+/g, ""))).join(""), LR = Object.create(null);
        for (const l of LETTERS) LR[l] = Array.from(new Set(lgroups.filter(g => g.includes(l)).join("").replace(/[\-\s]+/g, ""))).join("");
        LR[" "] = " ";
        const NOT_LETTERS = new RegExp("[^" + LETTERS + "]+", "ugis");
        return (s, notFist) => {
            if (s instanceof Function) return s(notFist);
            if (s instanceof RegExp) return [ dropHidden(s.source) ];
            if (s.constructor === String) {
                s = dropHidden(s).replace(NOT_LETTERS, " ").trim().toLowerCase();
                const a = Array.from(s, l => LR[l]).filter((l, i, a) => a[i - 1] != l).map((l, i, a) => " " === l ? [] : [ `[^${l}]{${a[i - 1] && " " !== a[i - 1] ? "0,3" : "1,6"}}`, `[${l}]+` ]).flat();
                return notFist ? a : a.slice(1);
            }
            return s ? [ ".*" ] : [];
        };
    })();
    function asyncMemoizeLast(fn) {
        let prevArgs, lastResult;
        return (...args) => (fast_deep_equal__WEBPACK_IMPORTED_MODULE_3___default()(args, prevArgs) || (prevArgs = void 0, 
        lastResult = fn(...args), lastResult.catch(() => prevArgs = void 0), prevArgs = args), 
        lastResult);
    }
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return Access;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return crud_v;
    }));
    __webpack_require__(113);
    var external_typeorm_ = __webpack_require__(2), types = __webpack_require__(12), string_tools_ = __webpack_require__(5), external_fastest_validator_ = __webpack_require__(70), external_fastest_validator_default = __webpack_require__.n(external_fastest_validator_), array_ = __webpack_require__(71), array_default = __webpack_require__.n(array_), string_ = __webpack_require__(46), string_default = __webpack_require__.n(string_);
    const validator_helpers_v = new external_fastest_validator_default.a({
        messages: {
            validRegex: "The '{field}' {actual}"
        }
    });
    validator_helpers_v.add("regex", (function({messages}) {
        return {
            source: `\n            try {\n                new RegExp(value);\n            } catch(err) {\n                ${this.makeError({
                type: "validRegex",
                actual: "err.message",
                messages
            })}\n            }\n            return value;\n        `
        };
    })), validator_helpers_v.add("javascript", (function({messages}) {
        return {
            source: `\n            try {\n                new Function(value);\n            } catch(err) {\n                ${this.makeError({
                type: "validRegex",
                actual: "err.message",
                messages
            })}\n            }\n            return value;\n        `
        };
    })), validator_helpers_v.add("wysiwyg", string_default.a), validator_helpers_v.add("link", string_default.a), 
    validator_helpers_v.add("md-tgclient", string_default.a), validator_helpers_v.add("md-tgbotlegacy", string_default.a), 
    validator_helpers_v.add("files", array_default.a);
    const compile = validator_helpers_v.compile.bind(validator_helpers_v);
    function getFieldSchema(target, options, propertyName) {
        const {nullable, unsigned, array, zerofill, primary} = options;
        let type;
        try {
            type = options.type.name.toLowerCase();
        } catch {
            type = options.type.toString();
        }
        let vo = target.__validatorInfo && target.__validatorInfo[propertyName];
        const _enum = options.enum || vo && vo.enum;
        let enumPairs = void 0, enumValues = void 0;
        const generated = Object(external_typeorm_.getMetadataArgsStorage)().generations.some(v => v.target === target && v.propertyName === propertyName);
        if (Array.isArray(_enum)) enumValues = _enum, enumPairs = enumValues.map(v => [ v, v ]); else if (_enum) {
            const keys = new Set(Object.keys(_enum));
            for (;keys.size > 0; ) {
                const {value} = keys.values().next();
                let k = value, v = _enum[k];
                !Object(string_tools_.isValidVar)(k) || _enum[v] === k && Object(string_tools_.isValidVar)(v) && v.toUpperCase() == v && k.toUpperCase() !== k ? keys.delete(k) : (Array.isArray(enumPairs) || (enumPairs = [], 
                enumValues = []), keys.delete(v.toString()), keys.delete(k), enumPairs.push([ k, v ]), 
                enumValues.push(v));
            }
        }
        let integer = void 0;
        if (type.includes("int")) type = "number", integer = !0; else if (type.includes("date") || type.includes("time")) type = "date"; else if (type.includes("string") || type.includes("char") || type.includes("clob") || type.includes("text")) type = "string"; else if (type.includes("bool")) type = "boolean"; else {
            if (type.includes("blob") || type.includes("binary")) throw "Validator not implemented";
            "simple-array" === type || "simple-json" === type || "array" === type || array ? type = "array" : "enum" === type || (type = "number");
        }
        (!Array.isArray(enumValues) || enumValues.length < 1) && (enumValues = enumPairs = void 0);
        let state = vo ? 0 | vo.state : 0;
        ("array" === type || "enum" === type || enumValues) && (8 == (8 & state) || !enumValues || vo && vo.min > 1 ? (enumValues = null, 
        type = "array") : type = "enum", vo && (vo = {
            ...vo
        }, delete vo.enum));
        const isNumber = "number" === type, o = {
            type,
            enumPairs,
            enum: enumValues,
            values: enumValues,
            integer,
            min: isNumber && (unsigned || zerofill || vo && vo.positive) ? 0 : null,
            max: isNumber && vo && vo.negative ? 0 : null,
            optional: !generated && (nullable || null != options.default),
            default: zerofill && isNumber && null == options.default ? 0 : options.default,
            state
        }, r = function nullPrune(o) {
            for (let k in o) null == o[k] && delete o[k];
            return o;
        }(vo ? Object.assign(o, vo) : o);
        return "link" === r.type && (r.state = 34 | r.state), primary && (r.state = 18 | r.state | (generated ? 98 : 0)), 
        r;
    }
    function validate(o) {
        const r = this(o);
        if (!0 !== r) throw r;
    }
    function Access(allowedFor, displayInfo, hooks) {
        return entity => {
            const vInsert = Object.create(null), vUpdate = Object.create(null);
            for (let {propertyName, options} of function* getColumns(entity) {
                if (entity && entity !== Function.prototype) {
                    const selfColumns = Object(external_typeorm_.getMetadataArgsStorage)().filterColumns(entity);
                    yield* getColumns(Object.getPrototypeOf(entity));
                    for (let column of selfColumns) yield column;
                }
            }(entity)) {
                const schema = getFieldSchema(entity, options, propertyName);
                Object(types.a)(schema, 34) || (vInsert[propertyName] = schema), vUpdate[propertyName] = schema;
            }
            allowedFor = allowedFor || Object.create(null), allowedFor._ !== +allowedFor._ && (allowedFor._ = -1), 
            Object.freeze(allowedFor), Object.defineProperty(entity, "checkAccess", {
                value(action, user) {
                    if (!user) throw 401;
                    let t = allowedFor[action];
                    if (null == t && (t = allowedFor._), (t & user.role) !== t) throw 403;
                    return !0;
                }
            });
            for (const _ in vInsert) {
                Object.defineProperty(entity, "insertValidate", {
                    value: validate.bind(compile(vInsert))
                });
                break;
            }
            for (const _ in vUpdate) {
                Object.defineProperty(entity, "updateValidate", {
                    value: validate.bind(compile(vUpdate))
                });
                break;
            }
            return displayInfo || (displayInfo = Object.create(null)), displayInfo.display || (displayInfo.display = entity.asyncProvider ? "single" : "table"), 
            Object.defineProperty(entity, "rtti", {
                value: Object.freeze({
                    props: Object.freeze(vUpdate),
                    displayInfo: Object.freeze(displayInfo)
                })
            }), Object.defineProperty(entity, "hooks", {
                value: hooks || Object.create(null)
            }), delete entity.__validatorInfo, entity;
        };
    }
    function crud_v(validatorProps) {
        return ({constructor}, prop) => {
            (constructor.__validatorInfo || (constructor.__validatorInfo = Object.create(null)))[prop] = Object.freeze(validatorProps);
        };
    }
}, function(module, exports) {
    module.exports = require("@taraflex/string-tools");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "g", (function() {
        return REAL_HOSTNAME;
    })), __webpack_require__.d(__webpack_exports__, "d", (function() {
        return HOSTNAME;
    })), __webpack_require__.d(__webpack_exports__, "e", (function() {
        return PEM;
    })), __webpack_require__.d(__webpack_exports__, "f", (function() {
        return PORT;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return ADMIN;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return API;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return HEALTH;
    })), __webpack_require__.d(__webpack_exports__, "h", (function() {
        return STATIC;
    }));
    var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7), path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(11), net__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(28);
    const regulus = require(__dirname + "/../package.json").regulus || {}, REAL_HOSTNAME = regulus.hostname || "localhost", HOSTNAME = process.argv.includes("--local") && !Object(net__WEBPACK_IMPORTED_MODULE_2__.isIP)(REAL_HOSTNAME) && "localhost" != REAL_HOSTNAME ? "dev." + REAL_HOSTNAME : REAL_HOSTNAME, PEM = REAL_HOSTNAME !== HOSTNAME && Object(fs__WEBPACK_IMPORTED_MODULE_0__.existsSync)(__dirname + "/../REGULUS.pem") && Object(path__WEBPACK_IMPORTED_MODULE_1__.resolve)(__dirname + "/../REGULUS.pem") || "", PORT = REAL_HOSTNAME !== HOSTNAME ? regulus.devport || 7813 : regulus.port || 7812, routes = Object.assign({
        admin: "/adm",
        api: "/api",
        health: "/health"
    }, regulus.routes), ADMIN = routes.admin, API = routes.api, HEALTH = routes.health, STATIC = "/static";
}, function(module, exports) {
    module.exports = require("fs");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    var appdirectory__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74), appdirectory__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(appdirectory__WEBPACK_IMPORTED_MODULE_0__), make_dir__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(30), path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(11), _utils_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6);
    let cwd = process.cwd();
    const path = function createDataDir(path) {
        try {
            return Object(make_dir__WEBPACK_IMPORTED_MODULE_1__.sync)(path), path;
        } catch (err) {
            return console.error.bind(console, "(./src/core/utils/data-path.ts:14)")(err), path.startsWith(cwd) ? (path = path.slice(cwd.length), 
            cwd = Object(path__WEBPACK_IMPORTED_MODULE_2__.join)(cwd, "./../"), createDataDir(Object(path__WEBPACK_IMPORTED_MODULE_2__.join)(cwd, "/", path))) : path;
        }
    }(Object(path__WEBPACK_IMPORTED_MODULE_2__.normalize)(new appdirectory__WEBPACK_IMPORTED_MODULE_0___default.a({
        appName: "ali-forward-" + _utils_config__WEBPACK_IMPORTED_MODULE_3__.f,
        useRoaming: !0
    }).userData()));
    console.log("App directory: " + path), __webpack_exports__.a = function(...pathParts) {
        return pathParts.length > 0 ? Object(path__WEBPACK_IMPORTED_MODULE_2__.join)(path, "/", ...pathParts) : path;
    };
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "Settings", (function() {
        return entities_Settings_Settings;
    })), __webpack_require__.d(__webpack_exports__, "SettingsRepository", (function() {
        return entities_Settings_SettingsRepository;
    }));
    var tslib_es6 = __webpack_require__(0), external_typeorm_ = __webpack_require__(2), ioc = __webpack_require__(10), rnd = __webpack_require__(51);
    let Settings_Settings = class Settings {};
    Object(tslib_es6.a)([ Object(external_typeorm_.PrimaryGeneratedColumn)(), Object(tslib_es6.b)("design:type", Number) ], Settings_Settings.prototype, "id", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: Object(rnd.b)()
    }), Object(tslib_es6.b)("design:type", String) ], Settings_Settings.prototype, "secret", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: !1
    }), Object(tslib_es6.b)("design:type", Boolean) ], Settings_Settings.prototype, "installed", void 0), 
    Settings_Settings = Object(tslib_es6.a)([ Object(ioc.a)(null, !0) ], Settings_Settings);
    external_typeorm_.Repository;
    var utils = __webpack_require__(3);
    let entities_Settings_Settings = class Settings extends Settings_Settings {
        get supergroup_chat() {
            return Object(utils.t)(this.supergroup_id);
        }
        setToken(name, access_token, expires_in) {
            this[name].token = access_token, this[name].expires_at = Math.floor((Date.now() + 900 * expires_in) / 1e3);
        }
        getToken(name) {
            const {token, expires_at} = this[name];
            return expires_at < Date.now() / 1e3 ? "" : token || "";
        }
    };
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: 0,
        type: "bigint"
    }), Object(tslib_es6.b)("design:type", Number) ], entities_Settings_Settings.prototype, "supergroup_id", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: 0,
        type: "bigint"
    }), Object(tslib_es6.b)("design:type", Number) ], entities_Settings_Settings.prototype, "admin_id", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: "{}",
        type: "simple-json"
    }), Object(tslib_es6.b)("design:type", Object) ], entities_Settings_Settings.prototype, "vk", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: "{}",
        type: "simple-json"
    }), Object(tslib_es6.b)("design:type", Object) ], entities_Settings_Settings.prototype, "ae", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: "{}",
        type: "simple-json"
    }), Object(tslib_es6.b)("design:type", Object) ], entities_Settings_Settings.prototype, "admitad", void 0), 
    entities_Settings_Settings = Object(tslib_es6.a)([ Object(ioc.a)() ], entities_Settings_Settings);
    class entities_Settings_SettingsRepository extends external_typeorm_.Repository {}
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return SingletonEntity;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return SingletonRepository;
    }));
    var typeorm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2), typescript_ioc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1), _ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(43);
    function SingletonEntity(options, disable) {
        if (disable) return function(target) {
            return target;
        };
        {
            const ewrap = Object(typeorm__WEBPACK_IMPORTED_MODULE_0__.Entity)(options);
            return function(target) {
                if (ewrap(target), !target.asyncProvider) {
                    const provider = Object(_ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__.b)(async connection => {
                        const repository = connection.getRepository(target);
                        let instance = await repository.findOne();
                        return instance || await repository.insert(instance = new target), instance;
                    });
                    target.asyncProvider = provider, Object(typescript_ioc__WEBPACK_IMPORTED_MODULE_1__.Provided)(provider)(target);
                }
                return target;
            };
        }
    }
    function SingletonRepository(e) {
        return function(target) {
            return target.provider || Object(typescript_ioc__WEBPACK_IMPORTED_MODULE_1__.Provided)(target.provider = Object(_ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__.a)(connection => connection.getRepository(e)))(target), 
            target;
        };
    }
}, function(module, exports) {
    module.exports = require("path");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    function has(item, state) {
        return (item.state & state) === state;
    }
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return has;
    }));
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "ClientSettings", (function() {
        return ClientSettings;
    })), __webpack_require__.d(__webpack_exports__, "ClientSettingsRepository", (function() {
        return ClientSettingsRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4), _ioc__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(10), _utils_tg_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(18);
    const TIMEZONE_OFFSET = (new Date).getTimezoneOffset() / 60;
    let ClientSettings = class ClientSettings {
        get timezoneOffset() {
            return this.timezone + TIMEZONE_OFFSET;
        }
        get removeAfterSeconds() {
            return 86400 * this.removeAfter;
        }
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        notEqual: 0,
        description: "Telegram Api id [взять тут](https://my.telegram.org/apps)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "apiId", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: _utils_tg_utils__WEBPACK_IMPORTED_MODULE_4__.a,
        description: "Telegram Api hash [взять там же](https://my.telegram.org/apps)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "",
        length: 32
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "apiHash", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 5,
        description: "Tg телефон"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "phone", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: _utils_tg_utils__WEBPACK_IMPORTED_MODULE_4__.b,
        description: "Токен телеграм бота, получить у [@BotFather](tg://resolve?domain=BotFather)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "token", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 5,
        description: "VK телефон"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "vkPhone", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        empty: !1,
        description: "VK пароль"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "vkPassword", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 18,
        description: "Пауза между запросами к VK в секундах (если у нас будет **x** источников, то полная проверка произойдет не быстрее чем за _{ x * пауза }_ секунд + дополнительное время на скачивание картинок)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 18
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "vkRequestTimeout", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 0,
        max: 24,
        description: "В каком часовом поясе отображать дату публикации поста"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 3,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "timezone", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 100,
        description: "Максимальное число последних постов подгружаемых из VK групп"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 3,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "vkPostCount", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 100,
        description: "Максимальное число последних постов подгружаемых из VK групп после рестарта"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 20,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "vkPostCountAfterRestart", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 255,
        description: "Максимальное число попыток парсинга поста"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 4,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "maxParseTries", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 86400,
        description: "Перезапускать бота после **x** секунд без новых постов в канале админки"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 9e3,
        type: "int"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "restartWithoutPostsAfter", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Часы, в которые требуется проверять бота на перезапуск (UTC+0)",
        enum: Array(24).fill(0).map((_, i) => i),
        state: 8,
        type: "array"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "[0,3,12,15,18,21]",
        type: "simple-json"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], ClientSettings.prototype, "checkRestartAt", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Таймаут получения превью товара с aliexpress"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 25
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "maxParseAliPreviewTime", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 255,
        description: "Максимальное число фото в коллаже"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 25,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "maxPhotosCount", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 255,
        description: "Максимальное число параллельно загружаемых фото"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 32,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "maxPhotosConcurrency", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Таймаут скачивания одного фото"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 30
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "maxPhotoDownloadTime", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Таймаут скачивания одного видео/анимации"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 120
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "maxMediaDownloadTime", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 100,
        description: "Качество генерируемого превью в формате jpeg"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 85,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "previewQuality", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Таймаут добавления/обновления тг источника (не забывайте сначала подписываться на источник). **0** - ждать бесконечно"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 60
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "maxTgSourceAddTime", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 8,
        description: "Максимальное число управляющих кнопок в строке у постов в группе-админке"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 3,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "buttonsPerRow", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 255,
        description: "Удалять из базы посты/медиа старше **х** дней. Проверка запускается раз в день. **0** - отключить"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 7,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "removeAfter", void 0), 
    ClientSettings = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)({
        GET: 0,
        PATCH: 0
    }, {
        display: "single",
        icon: "cog",
        order: 1
    }, {
        PATCH: "tgclient_save"
    }), Object(_ioc__WEBPACK_IMPORTED_MODULE_3__.a)() ], ClientSettings);
    class ClientSettingsRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "User", (function() {
        return User;
    })), __webpack_require__.d(__webpack_exports__, "UserRepository", (function() {
        return UserRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);
    let User = class User {};
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], User.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /\S+@\S+/
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unique: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], User.prototype, "email", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 5,
        max: 70
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        length: 60
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], User.prototype, "password", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 2147483647
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], User.prototype, "role", void 0), 
    User = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)(), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Entity)() ], User);
    class UserRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, exports, __webpack_require__) {
    "use strict";
    var pug_has_own_property = Object.prototype.hasOwnProperty;
    function pug_classes(val, escaping) {
        return Array.isArray(val) ? function pug_classes_array(val, escaping) {
            for (var className, classString = "", padding = "", escapeEnabled = Array.isArray(escaping), i = 0; i < val.length; i++) className = pug_classes(val[i]), 
            className && (escapeEnabled && escaping[i] && (className = pug_escape(className)), 
            classString = classString + padding + className, padding = " ");
            return classString;
        }(val, escaping) : val && "object" == typeof val ? function pug_classes_object(val) {
            var classString = "", padding = "";
            for (var key in val) key && val[key] && pug_has_own_property.call(val, key) && (classString = classString + padding + key, 
            padding = " ");
            return classString;
        }(val) : val || "";
    }
    function pug_style(val) {
        if (!val) return "";
        if ("object" == typeof val) {
            var out = "";
            for (var style in val) pug_has_own_property.call(val, style) && (out = out + style + ":" + val[style] + ";");
            return out;
        }
        return val + "";
    }
    function pug_attr(key, val, escaped, terse) {
        if (!1 === val || null == val || !val && ("class" === key || "style" === key)) return "";
        if (!0 === val) return " " + (terse ? key : key + '="' + key + '"');
        var type = typeof val;
        return "object" !== type && "function" !== type || "function" != typeof val.toJSON || (val = val.toJSON()), 
        "string" == typeof val || (val = JSON.stringify(val), escaped || -1 === val.indexOf('"')) ? (escaped && (val = pug_escape(val)), 
        " " + key + '="' + val + '"') : " " + key + "='" + val.replace(/'/g, "&#39;") + "'";
    }
    exports.merge = function pug_merge(a, b) {
        if (1 === arguments.length) {
            for (var attrs = a[0], i = 1; i < a.length; i++) attrs = pug_merge(attrs, a[i]);
            return attrs;
        }
        for (var key in b) if ("class" === key) {
            var valA = a[key] || [];
            a[key] = (Array.isArray(valA) ? valA : [ valA ]).concat(b[key] || []);
        } else if ("style" === key) {
            valA = pug_style(a[key]);
            valA = valA && ";" !== valA[valA.length - 1] ? valA + ";" : valA;
            var valB = pug_style(b[key]);
            valB = valB && ";" !== valB[valB.length - 1] ? valB + ";" : valB, a[key] = valA + valB;
        } else a[key] = b[key];
        return a;
    }, exports.classes = pug_classes, exports.style = pug_style, exports.attr = pug_attr, 
    exports.attrs = function pug_attrs(obj, terse) {
        var attrs = "";
        for (var key in obj) if (pug_has_own_property.call(obj, key)) {
            var val = obj[key];
            if ("class" === key) {
                val = pug_classes(val), attrs = pug_attr(key, val, !1, terse) + attrs;
                continue;
            }
            "style" === key && (val = pug_style(val)), attrs += pug_attr(key, val, !1, terse);
        }
        return attrs;
    };
    var pug_match_html = /["&<>]/;
    function pug_escape(_html) {
        var html = "" + _html, regexResult = pug_match_html.exec(html);
        if (!regexResult) return _html;
        var i, lastIndex, escape, result = "";
        for (i = regexResult.index, lastIndex = 0; i < html.length; i++) {
            switch (html.charCodeAt(i)) {
              case 34:
                escape = "&quot;";
                break;

              case 38:
                escape = "&amp;";
                break;

              case 60:
                escape = "&lt;";
                break;

              case 62:
                escape = "&gt;";
                break;

              default:
                continue;
            }
            lastIndex !== i && (result += html.substring(lastIndex, i)), lastIndex = i + 1, 
            result += escape;
        }
        return lastIndex !== i ? result + html.substring(lastIndex, i) : result;
    }
    exports.escape = pug_escape, exports.rethrow = function pug_rethrow(err, filename, lineno, str) {
        if (!(err instanceof Error)) throw err;
        if (!("undefined" == typeof window && filename || str)) throw err.message += " on line " + lineno, 
        err;
        try {
            str = str || __webpack_require__(7).readFileSync(filename, "utf8");
        } catch (ex) {
            pug_rethrow(err, null, lineno);
        }
        var context = 3, lines = str.split("\n"), start = Math.max(lineno - context, 0), end = Math.min(lines.length, lineno + context);
        context = lines.slice(start, end).map((function(line, i) {
            var curr = i + start + 1;
            return (curr == lineno ? "  > " : "    ") + curr + "| " + line;
        })).join("\n");
        throw err.path = filename, err.message = (filename || "Pug") + ":" + lineno + "\n" + context + "\n\n" + err.message, 
        err;
    };
}, function(module, exports) {
    module.exports = require("child_process");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "includes", (function() {
        return includes;
    })), __webpack_require__.d(__webpack_exports__, "Post", (function() {
        return Post;
    })), __webpack_require__.d(__webpack_exports__, "PostRepository", (function() {
        return PostRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), last_match__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(33), last_match__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(last_match__WEBPACK_IMPORTED_MODULE_1__), typeorm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2), _photos_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(45), _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3);
    function includes(s) {
        return new RegExp(Object(_utils__WEBPACK_IMPORTED_MODULE_4__.p)(s, 0).join(), "ugis").test(this);
    }
    let Post = class Post {
        constructor() {
            this.source_photos = [], this.ali_photos = [], this.video = {}, this.channels = {};
        }
        static id(message) {
            return -message.chat_id + "_" + message.id;
        }
        get message_id() {
            return +Object(_utils__WEBPACK_IMPORTED_MODULE_4__.o)(this.id.split("_"));
        }
        get tg() {
            return this.owner_id < 0;
        }
        get sourceTag() {
            return this.tg ? "tg" + Object(_utils__WEBPACK_IMPORTED_MODULE_4__.u)(this.owner_id) : "vk" + this.owner_id;
        }
        get is_owner() {
            return this.from_id === this.owner_id;
        }
        async concatPreviews(picture, settings) {
            if ("None" !== picture) for (let {photos, type} of "Ali" === picture ? [ {
                type: "Ali",
                photos: this.ali_photos
            }, {
                type: "Source",
                photos: this.source_photos
            } ] : [ {
                type: "Source",
                photos: this.source_photos
            }, {
                type: "Ali",
                photos: this.ali_photos
            } ]) {
                const url = await Object(_photos_utils__WEBPACK_IMPORTED_MODULE_3__.a)(photos, settings, !(!this.video._ && !this.video.src));
                if (url) return [ type, url ];
            }
            return [ "None", "" ];
        }
        async makeCollage(picture, settings) {
            const [type, url] = await this.concatPreviews(picture, settings);
            try {
                return this.type != type || this.preview != url;
            } finally {
                this.type = type, this.preview = url;
            }
        }
        get canDisplayPreview() {
            return !("None" === this.type || !this.preview);
        }
        get previewsCount() {
            return this.preview ? 0 | +last_match__WEBPACK_IMPORTED_MODULE_1___default()(this.preview, /\/(\d+)\$/) || 1 : 0;
        }
        get previewLocal() {
            return this.preview.replace(/^.+?\/static\/files\//, __dirname + "/static/files/");
        }
        get url() {
            return this.tg ? Object(_utils__WEBPACK_IMPORTED_MODULE_4__.s)(this.owner_id, this.message_id) : "https://vk.com/wall" + this.id;
        }
        getChannelMessageId(channel) {
            const i = Math.abs(this.channels[channel]);
            return i > 1 ? i : 0;
        }
        changeChannelState(channel, messageIdOrState) {
            return this.channels[channel] !== messageIdOrState && (this.channels[channel] = messageIdOrState, 
            !0);
        }
        setError(loc, channel, err) {
            if (err) {
                if ("MESSAGE_NOT_MODIFIED" === err.message) return this.changeChannelState(channel, this.getChannelMessageId(channel));
                if ("MESSAGE_ID_INVALID" === err.message) return this.changeChannelState(channel, 1);
            }
            return console.error(loc, Object(_utils__WEBPACK_IMPORTED_MODULE_4__.s)(Object(_utils__WEBPACK_IMPORTED_MODULE_4__.t)(channel), this.getChannelMessageId(channel)), err), 
            this.changeChannelState(channel, -this.getChannelMessageId(channel) || -1);
        }
        get idCaption() {
            return {
                _: "formattedText",
                text: this.id,
                entities: [ {
                    _: "textEntity",
                    offset: 0,
                    length: this.id.length,
                    type: {
                        _: "textEntityTypeTextUrl",
                        url: this.url
                    }
                } ]
            };
        }
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.PrimaryColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Post.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Index)(), Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: !1
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Boolean) ], Post.prototype, "actual", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Index)(), Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: 0,
        type: "tinyint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Post.prototype, "fails", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: !1
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Boolean) ], Post.prototype, "had_tags", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Post.prototype, "text", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: 0,
        type: "bigint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Post.prototype, "from_id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: 0,
        type: "bigint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Post.prototype, "owner_id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: 0,
        type: "bigint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Post.prototype, "date", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: 0,
        type: "bigint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Post.prototype, "edited", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: "Source",
        type: "varchar",
        length: 4
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Post.prototype, "type", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: "[]",
        type: "simple-json"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], Post.prototype, "source_photos", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: "[]",
        type: "simple-json"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], Post.prototype, "ali_photos", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Post.prototype, "preview", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Boolean) ], Post.prototype, "db", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: "{}",
        type: "simple-json"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Object) ], Post.prototype, "video", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Column)({
        default: "{}",
        type: "simple-json"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Object) ], Post.prototype, "channels", void 0), 
    Post = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_2__.Entity)() ], Post);
    class PostRepository extends typeorm__WEBPACK_IMPORTED_MODULE_2__.Repository {}
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "d", (function() {
        return getBotInfo;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return escMdForce;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return BOT_TOKEN_RE;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return API_HASH_RE;
    }));
    var _taraflex_http_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(29);
    __webpack_require__(35);
    (function getCommandParser(cmd) {
        const re = new RegExp("^\\/" + cmd + "(\\s+|$)", "i");
        return text => re.test(text) ? text.substr(cmd.length + 2).trim() : null;
    })("start");
    async function getBotInfo(token) {
        const {body} = await Object(_taraflex_http_client__WEBPACK_IMPORTED_MODULE_0__.get)(`https://api.telegram.org/bot${token}/getMe`), bot = JSON.parse(body).result;
        if (!bot.is_bot) throw "Invalid token: " + token;
        return bot;
    }
    const escMdMapForce = {
        "*": "✶",
        "#": "♯",
        "[": "［",
        "]": "］",
        _: "＿",
        "\\": "﹨",
        "`": "’",
        "~": "～",
        "@": "@‍",
        "/": "/‍"
    };
    function escMdForce(s) {
        return s ? s.replace(/[~@\/\*\[\]\\_`#]/g, m => escMdMapForce[m]) : "";
    }
    const BOT_TOKEN_RE = /^\d{9,}:[\w-]{35}$/, API_HASH_RE = /^[0-9abcdef]{32}$/;
}, function(module, exports) {
    module.exports = require("koa-body");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "AdPlatforms", (function() {
        return AdPlatforms;
    })), __webpack_require__.d(__webpack_exports__, "LinksSettings", (function() {
        return LinksSettings;
    })), __webpack_require__.d(__webpack_exports__, "ShortsSettingsRepository", (function() {
        return ShortsSettingsRepository;
    }));
    var AdPlatforms, tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4), _ioc__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(10);
    !function(AdPlatforms) {
        AdPlatforms.None = "None", AdPlatforms.Admitad = "Admitad", AdPlatforms.AEPlatform = "AEPlatform", 
        AdPlatforms.Backit = "Backit";
    }(AdPlatforms || (AdPlatforms = {}));
    let LinksSettings = class LinksSettings {};
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], LinksSettings.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Рекламная платформа",
        enum: [ AdPlatforms.Admitad, AdPlatforms.AEPlatform, AdPlatforms.Backit, AdPlatforms.None ]
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: AdPlatforms.None
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], LinksSettings.prototype, "adplatform", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /^https?:\/\/.+[^/]$/,
        description: "Url сокращалки"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "http://alik.cf"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], LinksSettings.prototype, "shorbyUrl", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Таймаут сокращения ссылки (ссылки в посте обрабатываются ПОСЛЕДОВАТЕЛЬНО)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 10
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], LinksSettings.prototype, "maxLinkShortTime", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Таймаут получения оригинальной ссылки из сокращенной (ссылки в посте обрабатываются ПАРАЛЛЕЛЬНО)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        default: 25
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], LinksSettings.prototype, "maxLinkRestoreTime", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "**AEPlatforms** user id [взять тут](https://console.aliexpress.com/)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        type: "int",
        default: 611
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], LinksSettings.prototype, "aeUserId", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "**AEPlatforms** client id [взять там же](https://aeplatform.ru/ru/profile/clients)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unsigned: !0,
        type: "int",
        default: 123645
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], LinksSettings.prototype, "aeClientId", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /[a-zA-Z0-9]+/,
        description: "**AEPlatforms** client secret [взять там же](https://aeplatform.ru/ru/profile/clients)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "4qkT23MQoaI1UrEizBPOE0WIsEkV4hRD643aHGRiJ4EAtblhK888io0ayou54KGUMIXzmjuJFlc1lF5N6qshRP"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], LinksSettings.prototype, "aeClientSecret", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /^https?:\/\/.+[^/]$/,
        description: "**Admitad** ссылка [(скопировать после подключения программы по клику на иконку скрепки)](https://store.admitad.com/ru/catalog/)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "https://alitems.co/g/vv3q4oey1vbe13cbb72db6d1781017"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], LinksSettings.prototype, "admitad", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Разрешенные url параметры",
        state: 8
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "simple-array",
        default: "wh_pid,pagePath,shopId,srcSns,sellerAdminSeq,sellerId,image"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], LinksSettings.prototype, "allowedQueryStrings", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Домены (включая поддомены), которые требуют поиска ссылки редиректа в теле страницы",
        state: 8
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "simple-array",
        default: "go9.ru,69v.ru"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], LinksSettings.prototype, "hostsFindLinkInBody", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Домены (включая поддомены), которые не требуют развертывания редиректов",
        state: 8
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "simple-array",
        default: "time100.ru"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], LinksSettings.prototype, "hostsWhiteList", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Домены (включая поддомены), которые точно не указывают на aliexpress",
        state: 8
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "simple-array",
        default: "prfl.me,1sota.ru,220-volt.ru,asos.com,beeline.ru,bit.ly,castorama.ru,citilink.ru,divizion.com,dixis-ul.ru,dochkisinochki.ru,drovosek-profi.ru,eldorado.ru,epicgames.com,euroset.ru,frontime.ru,gloria-jeans.ru,holodilnik.ru,honor.ru,instagram.com,kazanexpress.ru,kcentr.ru,kfc.ru,kuvalda.ru,lamoda.ru,leran.pro,leroymerlin.ru,matrix12.ru,maxidom.ru,megastroy.com,mi.com,multivarka.pro,mvideo.ru,obi.ru,onlinetrade.ru,ozon.ru,rbt.ru,re-store.ru,reebok.ru,sbermegamarket.ru,sc-store.ru,huawei.ru,megafon.ru,mts.ru,samsung.com,tele2.ru,skillbox.ru,store.sony.ru,svyaznoy.ru,t.me,technopark.ru,techport.ru,techprom.ru,tpko.ru,twitter.com,vseinstrumenti.ru,wildberries.ru,ya.cc,yandex.ru,youtu.be,youtube.com,zeon18.ru,zurmarket.ru,auchan.ru,galamart.ru,adidas.ru,tinkoff.promo,akusherstvo.ru,sbermarket.ru,letu.ru,metro-cc.ru,delivery-club.ru"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], LinksSettings.prototype, "hostsBlackList", void 0), 
    LinksSettings = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)({
        GET: 0,
        PATCH: 0
    }, {
        display: "single",
        icon: "external-link-square-alt",
        order: 3
    }), Object(_ioc__WEBPACK_IMPORTED_MODULE_3__.a)() ], LinksSettings);
    class ShortsSettingsRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, , function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "Source", (function() {
        return Source;
    })), __webpack_require__.d(__webpack_exports__, "SourceRepository", (function() {
        return SourceRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4), _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3);
    const PROXY = new Proxy(globalThis, {
        has: (_, key) => key.constructor === String && key.startsWith("$"),
        get: () => 0
    }), POST_PROPS = __webpack_require__(114);
    let Source = class Source {
        get tg() {
            return this.id < 0;
        }
        get url() {
            return this.tg ? this.domain ? "tg://resolve?domain=" + this.domain : Object(_utils__WEBPACK_IMPORTED_MODULE_3__.s)(this.id) : this.domain ? "https://vk.com/" + this.domain : "https://vk.com/public" + this.id;
        }
        buildMasker(channels) {
            this.masker || (this.masker = new Function("__proxy__", `with(__proxy__) return function({ ${POST_PROPS}, is_owner, tg }, includes) { let channels = 0; ${channels.map(c => c.dtypes).join(";")}; ${this.weightCalcScript};return channels | 0; }`)(PROXY));
        }
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 98
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryColumn)({
        readonly: !0,
        type: "bigint",
        default: 0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Source.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 98
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Source.prototype, "domain", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        enum: [ "Source", "Ali" ],
        description: "Предпочитаемое превью"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "Source",
        type: "varchar",
        length: Math.max("Source".length, "Ali".length)
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Source.prototype, "picture", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 34,
        description: "Название"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Source.prototype, "name", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Js скрипт расставления весов [доступные переменные](/static/docs/classes/_src_entities_post_.post.html) (_CTRL+D_ - автоформатирование кода)",
        type: "javascript",
        state: 4
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Source.prototype, "weightCalcScript", void 0), 
    Source = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)({
        GET: 0,
        DELETE: 0,
        PATCH: 0
    }, {
        display: "table",
        icon: "_vk",
        order: 7
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Entity)() ], Source);
    class SourceRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, exports) {
    module.exports = require("http");
}, function(module, exports) {
    module.exports = require("ws");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "Channel", (function() {
        return Channel;
    })), __webpack_require__.d(__webpack_exports__, "ChannelRepository", (function() {
        return ChannelRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4), _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3);
    let Channel = class Channel {
        get slug() {
            return "$" + this.title.replace(/[^\p{L}\p{Nl}$\p{Mn}\p{Mc}\p{Nd}\p{Pc}]+/gu, "_");
        }
        get dtypes() {
            return `const ${this.slug} = ${this.mask}`;
        }
        get url() {
            return Object(_utils__WEBPACK_IMPORTED_MODULE_3__.s)(Object(_utils__WEBPACK_IMPORTED_MODULE_3__.t)(this.supergroup_id));
        }
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Название"
    }), Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 34
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Channel.prototype, "title", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Порядок кнопок"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: 0,
        type: "int"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Channel.prototype, "order", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 98
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryColumn)({
        type: "bigint"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Channel.prototype, "supergroup_id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 98
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: 0,
        type: "int",
        unique: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Channel.prototype, "mask", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /^\w{1,31}$/,
        description: "Tracking ID [отсюда](https://portals.aliexpress.com/trackIdManage.htm)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "aligod_xyz_ru"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Channel.prototype, "trackingId", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Повторять сообщение каждые"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: 0,
        type: "int",
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Channel.prototype, "repeate_message_after", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        type: "md-tgbotlegacy",
        state: 4,
        description: "Повторяемое сообщение (markdown текст)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Channel.prototype, "additional_text", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        empty: !1,
        state: 4,
        description: "Шаблон преобразования ссылок (для обновления в старых постах требуется перепубликация). Пример _https://clck.ru/--?url=${encodeURIComponent(link)}_"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "${link}"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Channel.prototype, "urlTemplate", void 0), 
    Channel = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)({
        GET: 0,
        DELETE: 0,
        PATCH: 0
    }, {
        display: "table",
        icon: "_telegram-plane",
        order: 5
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Entity)() ], Channel);
    class ChannelRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, exports) {
    module.exports = require("koa-router");
}, function(module, exports) {
    module.exports = require("bcryptjs");
}, function(module, exports) {
    module.exports = require("net");
}, function(module, exports) {
    module.exports = require("@taraflex/http-client");
}, function(module, exports) {
    module.exports = require("make-dir");
}, function(module, exports) {
    module.exports = require("koa-passport");
}, , function(module, exports) {
    module.exports = require("last-match");
}, function(module, exports) {
    module.exports = require("msgpack-lite");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_exports__.a = () => Math.floor(9007199254740991 * Math.random());
}, function(module, exports) {
    module.exports = require("util");
}, function(module, exports) {
    module.exports = require("emittery");
}, function(module, exports) {
    module.exports = require("node-mdbx");
}, function(module, exports) {
    module.exports = require("param-case");
}, function(module, exports) {
    module.exports = require("memoize-one");
}, , function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "Patch", (function() {
        return Patch_Patch;
    })), __webpack_require__.d(__webpack_exports__, "PatchRepository", (function() {
        return Patch_PatchRepository;
    }));
    var tslib_es6 = __webpack_require__(0), external_typeorm_ = __webpack_require__(2), crud = __webpack_require__(4), ioc = __webpack_require__(10), utils = __webpack_require__(3);
    function oneof(...s) {
        return notFist => [ "(", ...s.map(s => Object(utils.p)(s, notFist).concat("|")).flat().slice(0, -1), ")" ];
    }
    function maybeOneof(...s) {
        return notFist => [ "(", ...s.map(s => Object(utils.p)(s, notFist).concat("|")).flat().slice(0, -1), ")?" ];
    }
    function maybe(s) {
        return notFist => [ "(", ...Object(utils.p)(s, notFist), ")?" ];
    }
    let Patch_Patch = class Patch {
        run() {
            const patches = [];
            function replace(...s) {
                const re = new RegExp(s.map(utils.p).flat().join(""), "ugis");
                return (literals, ...placeholders) => {
                    let arg = literals.raw[0];
                    for (let i = 0; i < placeholders.length; i++) arg += placeholders[i], arg += literals.raw[i + 1];
                    patches.push({
                        re,
                        arg
                    });
                };
            }
            return new Function("maybeOneof", "oneof", "maybe", "replace", "del", "delToEnd", "append", "prepend", this.script)(maybeOneof, oneof, maybe, replace, (function del(...s) {
                replace(...s)``;
            }), (function delToEnd(...s) {
                replace(...s, !0)``;
            }), (function append(arg) {
                patches.push({
                    re: /$/,
                    arg
                });
            }), (function prepend(arg) {
                patches.push({
                    re: /^/,
                    arg
                });
            })), patches;
        }
    };
    Object(tslib_es6.a)([ Object(external_typeorm_.PrimaryGeneratedColumn)(), Object(tslib_es6.b)("design:type", Number) ], Patch_Patch.prototype, "id", void 0), 
    Object(tslib_es6.a)([ Object(crud.b)({
        description: "Emoji для удаления"
    }), Object(external_typeorm_.Column)({
        default: "♠️💙💚🔥❤️✅❗️💥↩️↪️⚡⁠🌎"
    }), Object(tslib_es6.b)("design:type", String) ], Patch_Patch.prototype, "emoji", void 0), 
    Object(tslib_es6.a)([ Object(crud.b)({
        description: "Js скрипт патчей (_CTRL+D_ - автоформатирование кода) [документация](/static/docs/modules/_src_patch_utils_.html) (_CTRL+D_ - автоформатирование кода)",
        type: "javascript"
    }), Object(external_typeorm_.Column)({
        default: ""
    }), Object(tslib_es6.b)("design:type", String) ], Patch_Patch.prototype, "script", void 0), 
    Patch_Patch = Object(tslib_es6.a)([ Object(crud.a)({
        GET: 0,
        PATCH: 0
    }, {
        display: "single",
        icon: "tools",
        order: 6
    }), Object(ioc.a)() ], Patch_Patch);
    class Patch_PatchRepository extends external_typeorm_.Repository {}
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    function createAsync(initCb) {
        let instance = null;
        return {
            init: async (...args) => instance || (instance = await initCb(...args)),
            get: () => instance
        };
    }
    function create(initCb) {
        let instance;
        return {
            init: (...args) => instance || (instance = initCb(...args)),
            get: () => instance
        };
    }
    __webpack_require__.d(__webpack_exports__, "b", (function() {
        return createAsync;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return create;
    }));
}, function(module, exports) {
    module.exports = require("dayjs");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "b", (function() {
        return loadPhotos;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return concatPhotos;
    }));
    var child_process__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(16), fs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7), image_size__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(58), path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(11), path__WEBPACK_IMPORTED_MODULE_3___default = __webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_3__), stream__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(80), _sindresorhus_slugify__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(81), _sindresorhus_slugify__WEBPACK_IMPORTED_MODULE_5___default = __webpack_require__.n(_sindresorhus_slugify__WEBPACK_IMPORTED_MODULE_5__), _utils_config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6), _utils__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3);
    function fileSize(filename) {
        try {
            return 0 | Object(fs__WEBPACK_IMPORTED_MODULE_1__.statSync)(filename).size;
        } catch {}
        return 0;
    }
    function shuffle(array) {
        let temporaryValue, randomIndex, currentIndex = array.length;
        for (;0 !== currentIndex; ) randomIndex = Math.floor(Math.random() * currentIndex), 
        currentIndex -= 1, temporaryValue = array[currentIndex], array[currentIndex] = array[randomIndex], 
        array[randomIndex] = temporaryValue;
        return array;
    }
    const braces = s => ` ( ${s} ) `;
    function prow(photo, rowHeight) {
        return braces(`"${photo.local}" -resize "${photo.cellWidth}x${rowHeight}^" -gravity center -extent ${photo.cellWidth}x${rowHeight} -strokewidth 4 -fill none -stroke white -draw "rectangle 0,0 ${photo.cellWidth},${rowHeight}"`);
    }
    const CWD = __dirname + "/static/files/";
    function loadPhotos(photos, settings) {
        const minS = function maxLayout(length) {
            const [rowsCount, rowSize] = ll(length);
            return Math.max(Math.floor(800 / rowSize), 800 / rowsCount | 0);
        }(photos.length);
        return Object(_utils__WEBPACK_IMPORTED_MODULE_7__.b)(photos, async photo => {
            let {url, local, size, width, height} = photo;
            try {
                if (!(size = fileSize(local))) {
                    if (!url) return;
                    const U = new URL(url);
                    local = CWD + (U.hostname.endsWith("alicdn.com") ? _sindresorhus_slugify__WEBPACK_IMPORTED_MODULE_5___default()(U.pathname.slice(1), {
                        lowercase: !1,
                        decamelize: !1
                    }).replace(/-(jpe?g|png|webp|gif)$/i, ".$1") : Object(_utils__WEBPACK_IMPORTED_MODULE_7__.k)(url));
                    let body = null;
                    if (url.includes(".alicdn.com/")) for (let s of [ 100, 120, 200, 250, 300, 350, 640 ]) if (s >= minS) try {
                        body = (await Object(_utils__WEBPACK_IMPORTED_MODULE_7__.m)(url + `_${s}x${s}q100.jpg_.webp`, {
                            timeout: 1e3 * settings.maxPhotoDownloadTime
                        })).rawBody;
                        break;
                    } catch {}
                    body || (body = (await Object(_utils__WEBPACK_IMPORTED_MODULE_7__.m)(url, {
                        timeout: 1e3 * settings.maxPhotoDownloadTime
                    })).rawBody);
                    const is = Object(image_size__WEBPACK_IMPORTED_MODULE_2__.imageSize)(body);
                    if (!(null == is ? void 0 : is.type)) throw "Unsuppotred type";
                    is && (width = is.width, height = is.height, local = path__WEBPACK_IMPORTED_MODULE_3___default.a.format({
                        ...path__WEBPACK_IMPORTED_MODULE_3___default.a.parse(local),
                        base: void 0,
                        ext: "." + is.type
                    })), size = body.length, await fs__WEBPACK_IMPORTED_MODULE_1__.promises.writeFile(local, body);
                }
                const isize = width && height ? {
                    width,
                    height
                } : Object(image_size__WEBPACK_IMPORTED_MODULE_2__.imageSize)(local);
                return Object.assign(photo, {
                    size,
                    width: isize.width,
                    height: isize.height,
                    local,
                    aspect: isize.width / isize.height
                });
            } catch (e) {
                console.error.bind(console, "(./src/photos-utils.ts:94)")(url || local, e);
            }
        }, {
            required: settings.maxPhotosCount,
            concurrency: settings.maxPhotosConcurrency
        });
    }
    const smallLayouts = [ 1, 1, 2, 2, 2, 2, 2, 3, 3 ];
    function ll(length) {
        const rowsCount = smallLayouts[length] || 0 | Math.sqrt(length);
        return [ rowsCount, length / rowsCount | 0 ];
    }
    const MAGICK_BIN = "win32" === process.platform ? "magick -script -" : "magick-script -";
    async function concatPhotos(photos, settings, requestLocal) {
        if (!(null == photos ? void 0 : photos.length)) return null;
        if (1 === photos.length && !requestLocal && photos[0].url) return photos[0].url;
        const locals = await loadPhotos(photos, settings);
        if (!locals.length) return null;
        const filename = locals.length + "$" + Object(_utils__WEBPACK_IMPORTED_MODULE_7__.j)(locals, "local", "size") + (1 === locals.length ? path__WEBPACK_IMPORTED_MODULE_3___default.a.extname(locals[0].local) : ".jpg");
        if (!fileSize(CWD + filename)) if (1 === locals.length) await fs__WEBPACK_IMPORTED_MODULE_1__.promises.symlink(locals[0].local.replace(/[\\\/]files_unprotect[\\\/]/, "/files/"), CWD + filename), 
        await fs__WEBPACK_IMPORTED_MODULE_1__.promises.chmod(CWD + filename, 420); else {
            const table = function calcLayout(ps) {
                const [rowsCount, rowSize] = ll(ps.length), bigRowCounts = ps.length % rowsCount, c = bigRowCounts * (rowSize + 1);
                return [ ...Object(_utils__WEBPACK_IMPORTED_MODULE_7__.r)(shuffle(ps.slice(0, c)), rowSize + 1), ...Object(_utils__WEBPACK_IMPORTED_MODULE_7__.r)(shuffle(ps.slice(c)), rowSize) ];
            }(locals.sort((a, b) => a.aspect - b.aspect)), rowHeight = 800 / table.length | 0, command = braces(table.map(row => {
                const sa = row.reduce((s, p) => s + p.aspect, 0);
                return row.forEach(p => p.cellWidth = 800 * p.aspect / sa | 0), row[row.length - 1].cellWidth += 800 - row.reduce((s, p) => s + p.cellWidth, 0), 
                1 === row.length ? prow(row[0], rowHeight) : braces(row.map(p => prow(p, rowHeight)).join("") + " +append");
            }).join("")) + `-append -alpha Deactivate -define webp:image-hint=photo -define webp:alpha-compression=0 -quality ${settings.previewQuality} -write "./${filename}"`, {stderr} = await new Promise((resolve, reject) => {
                const cp = Object(child_process__WEBPACK_IMPORTED_MODULE_0__.exec)(MAGICK_BIN, {
                    cwd: CWD,
                    windowsHide: !0,
                    encoding: "utf-8"
                }, (err, _, stderr) => {
                    err ? reject({
                        err,
                        stderr
                    }) : resolve({
                        stderr
                    });
                });
                stream__WEBPACK_IMPORTED_MODULE_4__.Readable.from([ command ]).pipe(cp.stdin);
            });
            if (!fileSize(CWD + filename)) throw `Fail generate ${filename}: ` + stderr;
        }
        return "https://" + _utils_config__WEBPACK_IMPORTED_MODULE_6__.d + "/static/files/" + filename;
    }
}, function(module, exports) {
    module.exports = require("fastest-validator/lib/rules/string");
}, function(module, exports) {
    module.exports = require("timers");
}, function(module, exports) {
    module.exports = require("url");
}, function(module, exports) {
    module.exports = require("async-mutex");
}, function(module, exports) {
    module.exports = require("crypto");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return rndString;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return seedRndString;
    }));
    var base32_encode__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(59), base32_encode__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(base32_encode__WEBPACK_IMPORTED_MODULE_0__), crypto__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(50), node_machine_id__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(82), random_bytes_seed__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(83), random_bytes_seed__WEBPACK_IMPORTED_MODULE_3___default = __webpack_require__.n(random_bytes_seed__WEBPACK_IMPORTED_MODULE_3__), random_seed__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(84), util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(36), _config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6);
    const rndBytes = Object(util__WEBPACK_IMPORTED_MODULE_5__.promisify)(crypto__WEBPACK_IMPORTED_MODULE_1__.randomBytes), STRING_SEED = "ali-forward" + _config__WEBPACK_IMPORTED_MODULE_6__.f + Object(node_machine_id__WEBPACK_IMPORTED_MODULE_2__.machineIdSync)(!0);
    async function rndString() {
        return base32_encode__WEBPACK_IMPORTED_MODULE_0___default()(await rndBytes(23 + ~~(7 * Math.random())), "Crockford").toLowerCase();
    }
    function seedRndString(seed = STRING_SEED) {
        const b = random_bytes_seed__WEBPACK_IMPORTED_MODULE_3___default()(seed)(Object(random_seed__WEBPACK_IMPORTED_MODULE_4__.create)(seed).intBetween(23, 30));
        return base32_encode__WEBPACK_IMPORTED_MODULE_0___default()(b, "Crockford").toLowerCase();
    }
}, function(module, exports) {
    module.exports = require("pluralize");
}, function(module, exports) {
    module.exports = require("etag");
}, function(module, exports) {
    module.exports = require("got/dist/source");
}, function(module, exports) {
    module.exports = require("sentence-case");
}, function(module, exports) {
    module.exports = require("marked");
}, function(module, exports) {
    module.exports = require("replicator");
}, function(module, exports) {
    module.exports = require("image-size");
}, function(module, exports) {
    module.exports = require("base32-encode");
}, function(module, exports) {
    module.exports = require("cheerio");
}, function(module, exports) {
    module.exports = require("qs");
}, function(module, exports) {
    module.exports = require("playwright-firefox");
}, function(module, exports) {
    module.exports = require("set-utils");
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, Object, SAVE, can, ctx, isEditableField, isExtendableArrayField, isVisibleField, props) {
            const {sentenceCase} = __webpack_require__(55);
            let marked = __webpack_require__(56);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>";
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode, fullscreen) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("fullscreen", fullscreen, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.component = pug_interp = function(v, i, row, fullscreen) {
                this && this.block, this && this.attributes;
                switch (row = row || "item", v.type) {
                  case "string":
                    pug_mixins.input(v, row, i);
                    break;

                  case "array":
                    pug_mixins.array(v, row, i);
                    break;

                  case "files":
                    pug_mixins.files(v, row, i);
                    break;

                  case "date":
                    pug_mixins.date(v, row, i);
                    break;

                  case "number":
                    pug_mixins.number(v, row, i);
                    break;

                  case "enum":
                    pug_mixins.enum(v, row, i);
                    break;

                  case "boolean":
                    pug_mixins.boolean(v, row, i);
                    break;

                  case "wysiwyg":
                    pug_mixins.wysiwyg(v, row, i);
                    break;

                  case "md-tgclient":
                    pug_mixins.code(v, row, i, "md-tgclient");
                    break;

                  case "md-tgbotlegacy":
                    pug_mixins.code(v, row, i, "md-tgbotlegacy");
                    break;

                  case "regex":
                    pug_mixins.code(v, row, i, "regex");
                    break;

                  case "javascript":
                    pug_mixins.mn(v, row, i, "javascript", fullscreen);
                    break;

                  case "link":
                    pug_mixins.link(v, row, i);
                    break;

                  default:
                    pug_html += "<span>Unsupported type</span>";
                }
            }, pug_html += '<div class="rg-data_single" v-if="!!values[0]">';
            let i = 0, oneVisible = 1 === Object.keys(props).reduce((sum, k) => sum + isVisibleField(props[k]), 0);
            (function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isVisibleField(v) && (oneVisible ? (pug_html = pug_html + "<p>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</p>", 
                    pug_mixins.component(v, i, "values[0]", oneVisible)) : (pug_html = pug_html + "<div><p>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</p>", 
                    pug_mixins.component(v, i, "values[0]", oneVisible), pug_html += "</div>")), ++i;
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isVisibleField(v) && (oneVisible ? (pug_html = pug_html + "<p>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</p>", 
                        pug_mixins.component(v, i, "values[0]", oneVisible)) : (pug_html = pug_html + "<div><p>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</p>", 
                        pug_mixins.component(v, i, "values[0]", oneVisible), pug_html += "</div>")), ++i;
                    }
                }
            }).call(this), can.PATCH && (pug_html = pug_html + '<p> <el-button @click="save(values[0])" type="primary"' + pug.attr("round", !0, !0, !0) + ' icon="el-icon-check"' + pug.attr(":loading", "values[0].state==" + SAVE, !0, !0) + ">Save</el-button></p>"), 
            pug_html += "</div>";
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "Object" in locals_for_with ? locals_for_with.Object : "undefined" != typeof Object ? Object : void 0, "SAVE" in locals_for_with ? locals_for_with.SAVE : "undefined" != typeof SAVE ? SAVE : void 0, "can" in locals_for_with ? locals_for_with.can : "undefined" != typeof can ? can : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0, "isVisibleField" in locals_for_with ? locals_for_with.isVisibleField : "undefined" != typeof isVisibleField ? isVisibleField : void 0, "props" in locals_for_with ? locals_for_with.props : "undefined" != typeof props ? props : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, NONE, Object, REMOVE, SAVE, can, ctx, findPrimaryIndex, isEditableField, isExtendableArrayField, isFullwidthField, isNormalField, props) {
            const {sentenceCase} = __webpack_require__(55);
            let marked = __webpack_require__(56);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, $s = state => `(item.state==${state})`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            let display = null;
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                "table" === display ? (pug_html = pug_html + "<td" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></td>") : (pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>");
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode, fullscreen) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("fullscreen", fullscreen, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.component = pug_interp = function(v, i, row, fullscreen) {
                this && this.block, this && this.attributes;
                switch (row = row || "item", v.type) {
                  case "string":
                    pug_mixins.input(v, row, i);
                    break;

                  case "array":
                    pug_mixins.array(v, row, i);
                    break;

                  case "files":
                    pug_mixins.files(v, row, i);
                    break;

                  case "date":
                    pug_mixins.date(v, row, i);
                    break;

                  case "number":
                    pug_mixins.number(v, row, i);
                    break;

                  case "enum":
                    pug_mixins.enum(v, row, i);
                    break;

                  case "boolean":
                    pug_mixins.boolean(v, row, i);
                    break;

                  case "wysiwyg":
                    pug_mixins.wysiwyg(v, row, i);
                    break;

                  case "md-tgclient":
                    pug_mixins.code(v, row, i, "md-tgclient");
                    break;

                  case "md-tgbotlegacy":
                    pug_mixins.code(v, row, i, "md-tgbotlegacy");
                    break;

                  case "regex":
                    pug_mixins.code(v, row, i, "regex");
                    break;

                  case "javascript":
                    pug_mixins.mn(v, row, i, "javascript", fullscreen);
                    break;

                  case "link":
                    pug_mixins.link(v, row, i);
                    break;

                  default:
                    pug_html += "<span>Unsupported type</span>";
                }
            }, pug_mixins.controlls = pug_interp = function() {
                var state;
                this && this.block, this && this.attributes;
                (can.CHANGE || can.DELETE) && (pug_html += '<td class="rg-data__small-cell"><el-button-group>', 
                can.CHANGE && (pug_html += "<keep-alive>", can.PATCH && (pug_html = pug_html + "<el-button" + pug.attr("v-if", $s(NONE), !0, !0) + ' @click="edit(item)" type="primary" icon="el-icon-edit"' + pug.attr(":disabled", $s(REMOVE), !0, !0) + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + ">                           </el-button>"), 
                pug_html = pug_html + "<el-button" + pug.attr("v-if", (state = NONE, `(item.state!=${state})`), !0, !0) + ' @click="save(item)" type="primary" icon="el-icon-check"' + pug.attr(":loading", $s(SAVE), !0, !0) + pug.attr(":disabled", $s(REMOVE), !0, !0) + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + "></el-button></keep-alive>"), 
                can.DELETE && (pug_html = pug_html + '<el-button @click="remove(item, index)" type="danger" icon="el-icon-delete"' + pug.attr(":disabled", $s(SAVE), !0, !0) + pug.attr(":loading", $s(REMOVE), !0, !0) + pug.attr("circle", can.CHANGE, !0, !0) + pug.attr("round", !can.CHANGE, !0, !0) + "></el-button>"), 
                pug_html += "</el-button-group></td>");
            }, pug_html += '<div class="rg-data" v-loading="!inited" ref="table"><draggable :options="draggableOptions" :noTransitionOnDrag="true" @update="sort" element="table">';
            const hasNormalFields = Object.values(props).some(isNormalField), hasFullWidth = Object.values(props).some(isFullwidthField), hasNormalRow = can.CHANGE || can.DELETE || hasNormalFields;
            let i = 0;
            hasNormalRow && !hasFullWidth ? (pug_html = pug_html + '<tbody><tr class="rg-draggable" v-for="(item, index) in values"' + pug.attr(":key", "item[" + findPrimaryIndex(props) + "]", !0, !0) + ">       ", 
            display = "table", function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var v = $$obj[pug_index2];
                    isNormalField(v) && pug_mixins.component(v, i), ++i;
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        v = $$obj[pug_index2];
                        isNormalField(v) && pug_mixins.component(v, i), ++i;
                    }
                }
            }.call(this), pug_mixins.controlls(), pug_html += "</tr></tbody>") : (pug_html = pug_html + '<tbody class="rg-draggable" v-for="(item, index) in values"' + pug.attr(":key", "item[" + findPrimaryIndex(props) + "]", !0, !0) + "> ", 
            hasNormalRow && (pug_html += "<tr>       ", display = "table", i = 0, function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var v = $$obj[pug_index3];
                    isNormalField(v) && pug_mixins.component(v, i), ++i;
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        v = $$obj[pug_index3];
                        isNormalField(v) && pug_mixins.component(v, i), ++i;
                    }
                }
            }.call(this), pug_mixins.controlls(), pug_html += "</tr>"), hasFullWidth && (display = "single", 
            i = 0, function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isFullwidthField(v) && (pug_html = pug_html + '<tr><td colspan="999"><div class="rg-subtitle">' + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</div>", 
                    pug_mixins.component(v, i), pug_html += "</td></tr>"), ++i;
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isFullwidthField(v) && (pug_html = pug_html + '<tr><td colspan="999"><div class="rg-subtitle">' + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</div>", 
                        pug_mixins.component(v, i), pug_html += "</td></tr>"), ++i;
                    }
                }
            }.call(this)), pug_html += "</tbody>"), hasNormalFields && (pug_html += '<thead ref="tableHeader"><tr>', 
            function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isNormalField(v) && (pug_html = pug_html + "<th>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</th>");
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isNormalField(v) && (pug_html = pug_html + "<th>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</th>");
                    }
                }
            }.call(this), (can.CHANGE || can.DELETE) && (pug_html += '<td class="rg-data__small-cell"><el-button-group v-if="values.length&gt;1">', 
            can.CHANGE && (pug_html = pug_html + '<el-button :disabled="!hasUnsaved" @click="saveAll" type="primary" icon="el-icon-check"' + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + "></el-button>"), 
            can.DELETE && (pug_html = pug_html + '<el-button @click="removeAll" type="danger" icon="el-icon-delete"' + pug.attr("circle", can.CHANGE, !0, !0) + pug.attr("round", !can.CHANGE, !0, !0) + "></el-button>"), 
            pug_html += "</el-button-group></td>"), pug_html += "</tr></thead>"), pug_html += '</draggable><div class="rg-spacer"></div>', 
            can.PUT && (pug_html += '<div class="rg-controlls-panel"><div class="rg-spacer"></div><el-button @click="add" type="primary" round icon="el-icon-plus">New</el-button></div>'), 
            pug_html += "</div>";
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "NONE" in locals_for_with ? locals_for_with.NONE : "undefined" != typeof NONE ? NONE : void 0, "Object" in locals_for_with ? locals_for_with.Object : "undefined" != typeof Object ? Object : void 0, "REMOVE" in locals_for_with ? locals_for_with.REMOVE : "undefined" != typeof REMOVE ? REMOVE : void 0, "SAVE" in locals_for_with ? locals_for_with.SAVE : "undefined" != typeof SAVE ? SAVE : void 0, "can" in locals_for_with ? locals_for_with.can : "undefined" != typeof can ? can : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "findPrimaryIndex" in locals_for_with ? locals_for_with.findPrimaryIndex : "undefined" != typeof findPrimaryIndex ? findPrimaryIndex : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0, "isFullwidthField" in locals_for_with ? locals_for_with.isFullwidthField : "undefined" != typeof isFullwidthField ? isFullwidthField : void 0, "isNormalField" in locals_for_with ? locals_for_with.isNormalField : "undefined" != typeof isNormalField ? isNormalField : void 0, "props" in locals_for_with ? locals_for_with.props : "undefined" != typeof props ? props : void 0), 
        pug_html;
    };
}, function(module, exports) {
    module.exports = require("global-agent");
}, function(module, exports) {
    module.exports = require("koa-mount");
}, function(module, exports) {
    module.exports = require("koa-send");
}, function(module, exports) {
    module.exports = require("os");
}, function(module, exports) {
    module.exports = require("fastest-validator");
}, function(module, exports) {
    module.exports = require("fastest-validator/lib/rules/array");
}, function(module, exports) {
    module.exports = require("raw-body");
}, function(module, exports) {
    module.exports = require("@pm2/io");
}, function(module, exports) {
    module.exports = require("appdirectory");
}, function(module, exports) {
    module.exports = require("cacheable-lookup");
}, function(module, exports) {
    module.exports = require("fast-deep-equal");
}, function(module, exports) {
    module.exports = require("got");
}, function(module, exports) {
    module.exports = require("key-file-storage");
}, function(module, exports) {
    module.exports = require("dns");
}, function(module, exports) {
    module.exports = require("stream");
}, function(module, exports) {
    module.exports = require("@sindresorhus/slugify");
}, function(module, exports) {
    module.exports = require("node-machine-id");
}, function(module, exports) {
    module.exports = require("random-bytes-seed");
}, function(module, exports) {
    module.exports = require("random-seed");
}, function(module, exports) {
    module.exports = require("sanitize-filename");
}, function(module, exports) {
    module.exports = require("tdl");
}, function(module, exports) {
    module.exports = require("tdl-tdlib-ffi");
}, function(module, exports) {
    module.exports = require("emoji-regex/es2015");
}, function(module, exports) {
    module.exports = require("emoji-regex/es2015/RGI_Emoji");
}, function(module, exports) {
    module.exports = require("@taraflex/cancelation-token");
}, function(module, exports) {
    module.exports = require("crypto-random-string");
}, function(module, exports) {
    module.exports = require("escape-string-regexp");
}, function(module, exports) {
    module.exports = require("tough-cookie");
}, function(module, exports) {
    module.exports = require("vk-io");
}, function(module, exports) {
    module.exports = require("xxhash-wasm");
}, function(module, exports) {
    module.exports = require("@repeaterjs/repeater");
}, function(module, exports) {
    module.exports = require("fs-cp");
}, function(module, exports) {
    module.exports = require("koa-compress");
}, function(module, exports) {
    module.exports = require("path-to-regexp");
}, function(module, exports) {
    module.exports = require("koa-compose");
}, function(module, exports) {
    module.exports = require("koa-session");
}, function(module, exports) {
    module.exports = require("passport-local");
}, function(module, exports) {
    module.exports = require("koa");
}, function(module, exports, __webpack_require__) {
    var H = __webpack_require__(128);
    module.exports = function() {
        var T = new H.Template({
            code: function(c, p, i) {
                var t = this;
                return t.b(i = i || ""), t.b("window.Paths = Object.freeze({"), t.b("\n" + i), t.s(t.f("Paths", c, p, 1), c, p, 0, 45, 78, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                    t.b("    '"), t.b(t.t(t.f("name", c, p, 0))), t.b("':"), t.b(t.t(t.f("url", c, p, 0))), 
                    t.b(","), t.b("\n" + i);
                })), c.pop()), t.b("});"), t.b("\n" + i), t.b("window.UserInfo = Object.freeze({email:'"), 
                t.b(t.t(t.f("email", c, p, 0))), t.b("'});"), t.b("\n" + i), t.b("window.Flash = Object.freeze("), 
                t.b(t.t(t.f("Flash", c, p, 0))), t.b(");"), t.b("\n" + i), t.b("window.EDITOR_WORKER = '"), 
                t.b(t.t(t.f("EDITOR_WORKER", c, p, 0))), t.b("';"), t.b("\n" + i), t.b("window.TYPESCRIPT_WORKER = '"), 
                t.b(t.t(t.f("TYPESCRIPT_WORKER", c, p, 0))), t.b("';"), t.b("\n" + i), t.b("window.HasPty = "), 
                t.b(t.t(t.f("HasPty", c, p, 0))), t.b(";"), t.b("\n" + i), t.b("window.EntitiesFactory = function(mixins, debounce) {"), 
                t.b("\n" + i), t.b("    return ["), t.b("\n" + i), t.s(t.f("components", c, p, 1), c, p, 0, 404, 1317, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                    t.b("Object.assign({"), t.b("\n" + i), t.b("    name:'"), t.b(t.t(t.f("name", c, p, 0))), 
                    t.b("',"), t.b("\n" + i), t.b("    icon:'"), t.b(t.t(t.f("icon", c, p, 0))), t.b("',"), 
                    t.b("\n" + i), t.b("    order:"), t.b(t.t(t.f("order", c, p, 0))), t.b(","), t.b("\n" + i), 
                    t.b("    mixins:mixins,"), t.b("\n" + i), t.b("    data:function(){return{"), t.b("\n" + i), 
                    t.b("        draggableOptions:Object.freeze({disabled:!"), t.b(t.t(t.f("sortable", c, p, 0))), 
                    t.b(",draggable:'.rg-draggable',ghostClass:'rg-drag-ghost',animation:0}),"), t.b("\n" + i), 
                    t.b("        values:[],"), t.b("\n" + i), t.s(t.f("remote", c, p, 1), c, p, 0, 708, 755, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                        t.b("        "), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), t.b(t.t(t.f("method", c, p, 0))), 
                        t.b("_data:[],"), t.b("\n" + i);
                    })), c.pop()), t.b("        inited:false"), t.b("\n" + i), t.b("    }},"), t.b("\n" + i), 
                    t.b("    beforeCreate:function(){this.endPoint='"), t.b(t.t(t.f("endPoint", c, p, 0))), 
                    t.b("';this.hooks=Object.freeze("), t.b(t.t(t.f("hooks", c, p, 0))), t.b(");this.can=Object.freeze("), 
                    t.b(t.t(t.f("can", c, p, 0))), t.b(");this.rtti=Object.freeze("), t.b(t.t(t.f("rtti", c, p, 0))), 
                    t.b(");},"), t.b("\n" + i), t.b("    methods:{"), t.b("\n" + i), t.s(t.f("remote", c, p, 1), c, p, 0, 999, 1281, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                        t.b("        'call_"), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), t.b(t.t(t.f("method", c, p, 0))), 
                        t.b("':debounce(function(){"), t.b("\n" + i), t.b("            var self = this;"), 
                        t.b("\n" + i), t.b("            this.$rpc.remote('"), t.b(t.t(t.f("c", c, p, 0))), 
                        t.b("')."), t.b(t.t(t.f("method", c, p, 0))), t.b(".apply(this.$rpc,arguments).then(function(data){"), 
                        t.b("\n" + i), t.b("                self."), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), 
                        t.b(t.t(t.f("method", c, p, 0))), t.b("_data=data;"), t.b("\n" + i), t.b("            });"), 
                        t.b("\n" + i), t.b("        }, 500),"), t.b("\n" + i);
                    })), c.pop()), t.b("    }"), t.b("\n" + i), t.b("}, "), t.b(t.t(t.f("render", c, p, 0))), 
                    t.b("),"), t.b("\n" + i);
                })), c.pop()), t.b("    ]"), t.b("\n" + i), t.b("};"), t.fl();
            },
            partials: {},
            subs: {}
        });
        return T.render.apply(T, arguments);
    };
}, function(module, exports) {
    module.exports = require("vue-template-compiler");
}, function(module, exports) {
    module.exports = require("vue-template-es2015-compiler");
}, function(module, exports) {
    module.exports = require("alphanum-sort");
}, function(module, exports) {
    module.exports = require("fast-glob");
}, function(module, exports) {
    module.exports = require("time-stamp");
}, , function(module, exports) {
    module.exports = require("pretty-error");
}, function(module, exports) {
    module.exports = require("node-pty");
}, function(module, exports) {
    module.exports = require("reflect-metadata");
}, function(module, exports) {
    module.exports = "actual,ali_photos,date,edited,fails,from_id,had_tags,owner_id,preview,source_photos,text,type";
}, function(module, exports, __webpack_require__) {
    var map = {
        "./base.pug": 116,
        "./baseform.pug": 117,
        "./checkpass.pug": 118,
        "./components/mixins.pug": 119,
        "./components/single.pug": 64,
        "./components/table.pug": 65,
        "./index.pug": 120,
        "./install.pug": 121,
        "./login.pug": 122,
        "./newpassword.pug": 123,
        "./resetemail.pug": 124,
        "./resetpass-sended.pug": 125,
        "./resetpass.pug": 126,
        "./submitform.pug": 127
    };
    function webpackContext(req) {
        var id = webpackContextResolve(req);
        return __webpack_require__(id);
    }
    function webpackContextResolve(req) {
        if (!__webpack_require__.o(map, req)) {
            var e = new Error("Cannot find module '" + req + "'");
            throw e.code = "MODULE_NOT_FOUND", e;
        }
        return map[req];
    }
    webpackContext.keys = function webpackContextKeys() {
        return Object.keys(map);
    }, webpackContext.resolve = webpackContextResolve, module.exports = webpackContext, 
    webpackContext.id = 115;
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(assetsUrl, favicon, scripts, styles, title) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html += "</head><body>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var s = $$obj[pug_index1];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index1];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, title, warning) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            title && (pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>"), 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html += "</form></div></div></div></div>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Repeate password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.passwordField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, ctx, isEditableField, isExtendableArrayField) {
            const {sentenceCase} = __webpack_require__(55);
            let marked = __webpack_require__(56);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>";
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode, fullscreen) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("fullscreen", fullscreen, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            };
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(assetsUrl, favicon, inlineScript, scripts, styles, title) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html += '</head><body><div id="app"></div>', inlineScript && (pug_html = pug_html + "<script>" + (null == (pug_interp = inlineScript) ? "" : pug_interp) + "<\/script>"), 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var s = $$obj[pug_index1];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index1];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "inlineScript" in locals_for_with ? locals_for_with.inlineScript : "undefined" != typeof inlineScript ? inlineScript : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Installing";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField("Your email address (for password recovery)"), 
            pug_mixins.passwordField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = "Login") ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = "Login") ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = "Login".toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField(), pug_mixins.passwordField(), pug_html += '<div class="form-group"><a href="resetpass" style="width:100%;text-align:center;display:inline-block;text-decoration:none;">Forgot password?</a></div><br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "New password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.passwordField("New password (5-70 symbols)"), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "New email";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField("New email"), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, service, styles, warning) {
            pug_html += "<!DOCTYPE html><html>";
            const title = "Reset password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html = pug_html + '<div class="form-group">Письмо с инструкциями по изменению пароля отправлено на ' + pug.escape(null == (pug_interp = email) ? "" : pug_interp) + "</div>", 
            service && (pug_html = pug_html + '<div class="form-group"><a class="btn btn-block btn-primary btn-lg"' + pug.attr("href", service.url, !0, !0) + ">Открыть " + pug.escape(null == (pug_interp = service.title) ? "" : pug_interp) + "</a></div>"), 
            pug_html += "</form></div></div></div></div>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "service" in locals_for_with ? locals_for_with.service : "undefined" != typeof service ? service : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Reset password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(15);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, title, warning) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            title && (pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>"), 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports) {
    module.exports = require("hogan.js");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_exports__.default = 'declare type PatchParam = string | Function | RegExp | true;\ndeclare function oneof(...s: readonly PatchParam[]): (notFist: number | boolean) => any[];\ndeclare function maybeOneof(...s: readonly PatchParam[]): (notFist: number | boolean) => any[];\ndeclare function maybe(s: string | RegExp): (notFist: number | boolean) => any[];\ndeclare function replace(...s: readonly PatchParam[]): (literals: TemplateStringsArray, ...placeholders: any[]) => void;\ndeclare function del(...s: readonly PatchParam[]): void;\ndeclare function delToEnd(...s: readonly PatchParam[]): void;\ndeclare function append(s: string): void;\ndeclare function prepend(s: string): void;\ndeclare const enum Picture {\n    NONE_OR_VIDEO = "None",\n    SOURCE = "Source",\n    ALI = "Ali"\n}\ndeclare const enum ChannelPostState {\n    Error = -1,\n    Deleted = 1\n}\ndeclare function includes(s: string | RegExp): boolean;\ndeclare type LocalPhoto = {\n    readonly size: number;\n    readonly width: number;\n    readonly height: number;\n    readonly local: string;\n    readonly url?: string;\n};\ndeclare type ResizedLocalPhoto = {\n    readonly aspect: number;\n    cellWidth?: number;\n} & LocalPhoto;\ndeclare type Photo = {\n    size?: number;\n    width?: number;\n    height?: number;\n    local?: string;\n    readonly url?: string;\n};\n';
}, function(module, exports) {
    module.exports = "\n/** состояние поста в чате-админке НЕ требует обновления */\ndeclare const actual:boolean;\n\n/** ali фотографии */\ndeclare const ali_photos:undefined;\n\n/** время публикации записи в формате unixtime. */\ndeclare const date:number;\n\n/** время изменения записи в формате unixtime. */\ndeclare const edited:number;\n\n/** число раз парсинга поста с ошибками ссылок */\ndeclare const fails:number;\n\n/** идентификатор автора записи (от чьего имени опубликована запись). */\ndeclare const from_id:number;\n\n/** были ли у записи хештеги */\ndeclare const had_tags:boolean;\n\n/** идентификатор владельца стены, на которой размещена запись. */\ndeclare const owner_id:number;\n\n/** ссылка на коллаж */\ndeclare const preview:string;\n\n/** фотографии c источника парсинга */\ndeclare const source_photos:undefined;\n\n/** текст записи. */\ndeclare const text:string;\n\n/** тип активного фото */\ndeclare const type:Picture;\n\n/** запись опубликована владельцем стены */\ndeclare const is_owner:boolean;\n\n/** источник - телеграм канал */\ndeclare const tg:boolean;\n\n/** каналы, в которые будет опубликован пост */\ndeclare let channels:number;";
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    var tslib_es6 = __webpack_require__(0), external_child_process_ = __webpack_require__(16), external_child_process_default = __webpack_require__.n(external_child_process_), external_global_agent_ = __webpack_require__(66), config = __webpack_require__(6);
    const DEV_NULL = process.platform.startsWith("win") ? "nul" : "/dev/null", cmd = "grep -i '^Port\\|^BasicAuth' /etc/tinyproxy/tinyproxy.conf";
    if ("object" != typeof v8debug && !/--debug|--inspect/.test(process.execArgv.join(" "))) {
        const PrettyError = __webpack_require__(111), pe = new PrettyError;
        pe.appendStyle({
            "pretty-error > trace > item > header > pointer": {
                display: "none"
            }
        }), pe.skipNodeFiles(), pe.withoutColors(), pe.skip(traceLine => (null == traceLine ? void 0 : traceLine.dir) && (traceLine.dir.startsWith("node:internal/") || traceLine.dir.startsWith("internal/"))), 
        pe.start();
    }
    if (config.e) {
        global.SSH_TUNNEL = Object(external_child_process_.spawn)("ssh", [ "-o", "UserKnownHostsFile=" + DEV_NULL, "-o", "StrictHostKeyChecking=no", "-o", "BatchMode=yes", "-gnNT", "-R", config.f + ":localhost:" + config.f, "-i", config.e, "root@" + config.g ], {
            windowsHide: !0,
            stdio: "inherit"
        }).once("exit", code => process.exit(code));
        const proxy = function(remote) {
            const {Port, BasicAuth} = JSON.parse("{" + Object(external_child_process_.execSync)(remote ? `ssh -o UserKnownHostsFile=${DEV_NULL} -o StrictHostKeyChecking=no -o BatchMode=yes -i "${config.e}" root@${config.g} "${cmd}"` : cmd, {
                encoding: "utf-8",
                windowsHide: !0
            }).trim().split("\n").map(v => v.replace(/\s+/, ":").replace(/([^:]+)/g, '"$1"')) + "}");
            return `${BasicAuth.replace(/\s+/, ":")}@${config.g}:${Port}`;
        }(!0);
        proxy && (Object(external_global_agent_.bootstrap)(), global.GLOBAL_AGENT.NO_PROXY = "localhost,127.0.0.1,oauth.vk.com,ipecho.net,ident.me,icanhazip.com", 
        console.log("Active proxy: " + (global.GLOBAL_AGENT.HTTP_PROXY = `http://${proxy}/`)));
    }
    var external_http_ = __webpack_require__(23), external_koa_mount_ = __webpack_require__(67), external_koa_mount_default = __webpack_require__.n(external_koa_mount_), external_koa_send_ = __webpack_require__(68), external_koa_send_default = __webpack_require__.n(external_koa_send_), external_path_ = __webpack_require__(11), external_timers_ = __webpack_require__(47), external_typescript_ioc_ = __webpack_require__(1), external_ws_ = __webpack_require__(24), external_ws_default = __webpack_require__.n(external_ws_), string_tools_ = __webpack_require__(5), external_os_ = __webpack_require__(69);
    const shell = Object(external_os_.platform)().includes("win") ? "cmd.exe" : function hasTmux() {
        try {
            return Object(external_child_process_.execSync)("command -v tmux", {
                windowsHide: !0
            }), !0;
        } catch {
            return !1;
        }
    }() ? "tmux" : "sh";
    let spawn = null;
    try {
        spawn = __webpack_require__(112).spawn;
    } catch {}
    const AVALIBLE = !!spawn;
    var external_msgpack_lite_ = __webpack_require__(34), external_replicator_ = __webpack_require__(57), external_replicator_default = __webpack_require__.n(external_replicator_);
    const opts = {
        codec: Object(external_msgpack_lite_.createCodec)({
            preset: !0,
            safe: !1,
            useraw: !1,
            int64: !1,
            binarraybuffer: !0,
            uint8array: !1,
            usemap: !1
        })
    }, ropts = {
        codec: Object(external_msgpack_lite_.createCodec)({
            preset: !0,
            safe: !1,
            useraw: !1,
            int64: !1,
            binarraybuffer: !0,
            uint8array: !0,
            usemap: !1
        })
    };
    external_replicator_default.a.prototype.removeTransforms = function(transforms) {
        this.transforms = this.transforms.filter(t => !transforms.includes(t.type));
        for (var i = 0; i < transforms.length; i++) delete this.transformsMap[transforms[i]];
        return this;
    };
    const replicator = new external_replicator_default.a({
        serialize: o => Object(external_msgpack_lite_.encode)(o, ropts),
        deserialize: data => Object(external_msgpack_lite_.decode)(data, ropts)
    });
    function decode(data) {
        return Object(external_msgpack_lite_.decode)(data, opts);
    }
    function encode(o) {
        return Object(external_msgpack_lite_.encode)(o, opts);
    }
    replicator.removeTransforms([ "[[Date]]", "[[RegExp]]", "[[ArrayBuffer]]", "[[TypedArray]]" ]);
    var rnd_id = __webpack_require__(35);
    class RPC_AnswerChain extends class Chain {
        write(o) {
            this.readResolve && (this.readResolve(o), this.readResolve = null);
        }
        read() {
            return new Promise(resolve => this.readResolve = resolve);
        }
    } {
        constructor(i, timeout) {
            super(), this.i = i, this.timeout = timeout, this.created = Date.now();
        }
    }
    const CALLBACK_SOURCES = new Map, CALLS = new Map, chainDropInterval = setInterval(() => {
        const now = Date.now();
        CALLS.forEach(c => {
            c.timeout && now - c.created > c.timeout && c.write({
                i: c.i,
                e: "RPC call timeout"
            });
        });
    }, 1e4);
    chainDropInterval.unref();
    class RPC_RemoteHandler {
        constructor(key, rpc, timeout) {
            this.key = key, this.rpc = rpc, this.timeout = timeout;
        }
        get(_, f) {
            return (...a) => {
                let i = 0;
                do {
                    i = Object(rnd_id.a)();
                } while (CALLS.has(i));
                const h = new RPC_AnswerChain(i, 1e3 * this.timeout);
                try {
                    CALLS.set(i, h), this.rpc.send(replicator.encode({
                        i,
                        c: this.key,
                        f,
                        a
                    }));
                } catch (e) {
                    throw CALLS.delete(i), e;
                }
                return h.read().then(answer => {
                    if (null != answer.e) throw answer.e;
                    return answer.r;
                }).finally(() => CALLS.delete(i));
            };
        }
    }
    const EMPTY_OBJECT = Object.freeze(Object.create(null));
    class RPC_RPC {
        static register(key, o) {
            CALLBACK_SOURCES.set(key, o);
        }
        static unregister(key) {
            CALLBACK_SOURCES.delete(key);
        }
        remote(key, timeout) {
            return new Proxy(EMPTY_OBJECT, new RPC_RemoteHandler(key, this, timeout || 0));
        }
        async process(message) {
            if (message) if (message.constructor === String) this.term && this.term.write(message); else {
                const data = replicator.decode(new Uint8Array(message, 0));
                if (data.f) {
                    const {i, f, c, a} = data;
                    try {
                        if ("RPC" === c) {
                            if ("enableTerminal" !== f) throw `Deny method [${f}] called on RPC`;
                            this.send(replicator.encode({
                                i,
                                r: this.enableTerminal(a[0])
                            }));
                        } else this.send(replicator.encode({
                            i,
                            r: await this.rcall(CALLBACK_SOURCES.get(c), f, a)
                        }));
                    } catch (e) {
                        this.send(replicator.encode({
                            i,
                            e
                        }));
                    }
                } else CALLS.get(data.i).write(data), CALLS.delete(data.i);
            }
        }
    }
    const RPCServers = new Map;
    function pingServers(timeout) {
        RPCServers.forEach(m => m.forEach(rpc => rpc.ping(timeout)));
    }
    function fromCtx(ctx) {
        return RPCServers.get(ctx.state.user.id).get(parseInt(ctx.headers["x-tab"]));
    }
    class ServerRPCHandler {
        constructor(server) {
            this.server = server;
        }
        get(target, prop) {
            return "__rpc" == prop ? this.server : target[prop];
        }
    }
    class ServerRPC_ServerRPC extends RPC_RPC {
        constructor(socket, tab, browserId, user, terminalSize) {
            if (super(), this.socket = socket, this.tab = tab, this.browserId = browserId, this.user = user, 
            this.lastActive = Date.now(), this.onWSMessage = message => {
                this.lastActive = Date.now(), this.process(message).catch(console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:49)"));
            }, this.onWSClose = () => {
                try {
                    this.dispose();
                } catch (err) {
                    console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:56)")(err);
                }
            }, this.onWSPong = () => {
                this.lastActive = Date.now();
            }, tab !== +tab || !tab) throw "Invalid websocket tab: " + Object(string_tools_.stringify)(tab);
            let remoteTabs = RPCServers.get(user.id);
            remoteTabs || RPCServers.set(user.id, remoteTabs = new Map), remoteTabs.set(tab, this), 
            socket.on("close", this.onWSClose), socket.on("pong", this.onWSPong), socket.on("message", this.onWSMessage), 
            terminalSize && this.enableTerminal(terminalSize);
        }
        getActual() {
            const u = RPCServers.get(this.user.id), r = u && u.get(this.tab);
            return r !== this && this.dispose(), r;
        }
        send(data) {
            this.getActual().socket.send(data);
        }
        enableTerminal(terminalSize) {
            if (this.term) this.term.resize(terminalSize.cols, terminalSize.rows); else {
                const term = function create(options) {
                    return AVALIBLE ? spawn(shell, "tmux" === shell ? [ "new", "-A", "-s", "ali-forward-" + config.f ] : [], {
                        name: "xterm-color",
                        cwd: process.env.HOME,
                        env: process.env,
                        ...options
                    }) : null;
                }(terminalSize);
                term && (this.term = term, this.socket.send(String.fromCharCode(27) + "c"), term.on("data", data => {
                    try {
                        this.socket.send(data);
                    } catch (err) {
                        console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:117)")(err);
                    }
                }));
            }
        }
        rcall(o, fname, args) {
            return o[fname].apply(new Proxy(o, new ServerRPCHandler(this)), args);
        }
        dispose() {
            this.term && (this.term.kill(), this.term = void 0);
            const c = RPCServers.get(this.user.id);
            c && c.get(this.tab) === this && c.delete(this.tab), this.socket.readyState != external_ws_default.a.CLOSING && this.socket.readyState != external_ws_default.a.CLOSED && this.socket.close();
        }
        ping(timeout) {
            Date.now() - this.lastActive >= timeout && this.socket.readyState === external_ws_default.a.OPEN && this.socket.ping();
        }
    }
    var external_net_ = __webpack_require__(28), http_client_ = __webpack_require__(29), get_my_ip = async function() {
        let ip = "";
        try {
            const {body} = await Object(http_client_.get)("http://ipecho.net/plain");
            if (ip = body.toString().trim(), !Object(external_net_.isIP)(ip)) throw null;
        } catch {
            try {
                const {body} = await Object(http_client_.get)("http://ident.me");
                if (ip = body.toString().trim(), !Object(external_net_.isIP)(ip)) throw null;
            } catch {
                const {body} = await Object(http_client_.get)("http://icanhazip.com");
                if (ip = body.toString().trim(), !Object(external_net_.isIP)(ip)) throw "Invalid ip: " + ip;
            }
        }
        return ip;
    }, external_make_dir_ = __webpack_require__(30), external_make_dir_default = __webpack_require__.n(external_make_dir_), external_util_ = __webpack_require__(36), types = __webpack_require__(12), ClientSettings = __webpack_require__(13), external_raw_body_ = __webpack_require__(72), external_raw_body_default = __webpack_require__.n(external_raw_body_);
    var body_parse_msgpack = async (ctx, next) => {
        const {method} = ctx.request;
        if ("PATCH" === method || "PUT" === method || "POST" === method) {
            const body = await external_raw_body_default()(ctx.req, {
                limit: 5242880
            });
            ctx.request.body = Object.assign(body && body.length > 0 ? decode(body) : null, ctx.request.query);
        }
        return next();
    }, is_ajax = function(ctx) {
        return "XMLHttpRequest" === ctx.request.headers["x-requested-with"];
    }, check_entity_access = ({checkAccess}) => function(ctx, next) {
        if (!is_ajax(ctx)) throw 403;
        return checkAccess(ctx.request.method, ctx.state.user), next();
    }, external_url_ = __webpack_require__(48), resolve_url = function(ctx, target, localhost) {
        return Object(external_url_.resolve)(ctx.protocol + "://" + (localhost && !ctx.secure ? "127.0.0.1:" + config.f : ctx.host), target.replace(/[\/\\]{2,}/g, "/"));
    };
    function setNoStoreHeader(ctx) {
        ctx.set("Cache-Control", "no-store, no-cache, max-age=0");
    }
    var no_store = function(ctx, next) {
        return setNoStoreHeader(ctx), next();
    };
    const routsCache = Object.create(null), secureRoutsCache = Object.create(null);
    var smart_redirect = (ctx, next) => {
        const rcache = ctx.secure ? secureRoutsCache : routsCache;
        return ctx.resolve = (routeName, params) => !params && ctx.router.stack.find(r => r.name === routeName).paramNames.length < 1 ? rcache[routeName] || (rcache[routeName] = resolve_url(ctx, ctx.router.url(routeName, null))) : resolve_url(ctx, ctx.router.url(routeName, Object.assign({}, ctx.params, params))), 
        ctx.namedRedirect = (routName, delay, params) => {
            setNoStoreHeader(ctx);
            const target = ctx.resolve(routName || "root", params);
            delay > 0 ? (ctx.type = "html", ctx.body = `<html><head><meta http-equiv="refresh" content="${delay};url=${target}" /></head></html>`) : ctx.redirect(target);
        }, next();
    }, io_ = __webpack_require__(73), io_default = __webpack_require__.n(io_), data_path = __webpack_require__(8), User = __webpack_require__(14);
    async function sendEmail(email, subject, message) {
        try {
            const {body} = await Object(http_client_.post)({
                url: "https://script.google.com/macros/s/AKfycbyau7ecNID-Y9M6s067panGIAdkcUS31BCtoa73K4GURJh_76Q/exec",
                form: {
                    email,
                    body: message,
                    subject
                }
            });
            if ("ok" !== JSON.parse(body).success) throw null;
        } catch (err) {
            throw err && console.error.bind(console, "(./src/core/utils/send-email.ts:22)")(err), 
            "Email sending error. Try again later.";
        }
    }
    async function sendErrorEmail(message) {
        try {
            const ur = external_typescript_ioc_.Container.get(User.UserRepository);
            await sendEmail((await ur.findOne()).email, "Ошибка в работе бота http://" + config.d, message);
        } catch {}
    }
    var external_async_mutex_ = __webpack_require__(49), external_dayjs_ = __webpack_require__(44), external_dayjs_default = __webpack_require__.n(external_dayjs_), external_last_match_ = __webpack_require__(33), external_last_match_default = __webpack_require__.n(external_last_match_), Channel = __webpack_require__(25), Post = __webpack_require__(17), Settings = __webpack_require__(9), Source = __webpack_require__(22), tg_utils = __webpack_require__(18), external_emittery_ = __webpack_require__(37), external_emittery_default = __webpack_require__.n(external_emittery_), external_fs_ = __webpack_require__(7), external_sanitize_filename_ = __webpack_require__(85), external_sanitize_filename_default = __webpack_require__.n(external_sanitize_filename_), external_tdl_ = __webpack_require__(86), external_tdl_tdlib_ffi_ = __webpack_require__(87);
    class ExtendableError extends Error {
        constructor(message) {
            super(message), this.name = this.constructor.name, Error.captureStackTrace(this, this.constructor);
        }
    }
    class Deffered_DefferTimeout extends ExtendableError {}
    function makeDeffer(timeout) {
        const o = {
            createTime: Date.now(),
            promise: null,
            fulfill: null,
            reject: null
        };
        return o.promise = new Promise((fullfil, reject) => {
            o.fulfill = fullfil, o.reject = reject;
        }), timeout && setTimeout(o.reject, timeout, new Deffered_DefferTimeout).unref(), 
        o;
    }
    class DeferredMap {
        constructor() {
            this.map = Object.create(null);
        }
        get empty() {
            for (const _ in this.map) return !1;
            return !0;
        }
        keys() {
            return Object.keys(this.map);
        }
        createdAt(id) {
            return this.map[id].createTime;
        }
        make(id, timeout, cb) {
            if (!this.map[id]) {
                const d = this.map[id] = makeDeffer(timeout);
                if (cb) try {
                    cb(id).catch(e => d.reject(e));
                } catch (e) {
                    d.reject(e);
                }
            }
            return this.map[id].promise;
        }
        has(id) {
            return null != this.map[id];
        }
        fulfill(id, v) {
            var _a;
            null === (_a = this.map[id]) || void 0 === _a || _a.fulfill(v), delete this.map[id];
        }
        reject(id, err) {
            var _a;
            null === (_a = this.map[id]) || void 0 === _a || _a.reject(err), delete this.map[id];
        }
        rejectAll(err) {
            for (let id of this.keys()) this.reject(id, err);
        }
    }
    const exec = Object(external_util_.promisify)(external_child_process_default.a.exec), tdlibOptions = {
        online: !1,
        prefer_ipv6: !1,
        use_storage_optimizer: !0,
        always_parse_markdown: !1,
        ignore_inline_thumbnails: !0
    }, tdlibPostOptions = {
        disable_top_chats: !0,
        disable_time_adjustment_protection: !0,
        disable_persistent_network_statistics: !0,
        ignore_sensitive_content_restrictions: !0,
        disable_sent_scheduled_message_notifications: !0
    };
    class BaseTelegramClient_BaseTelegramClient extends external_emittery_default.a {
        constructor() {
            super(...arguments), this.messagePendings = new DeferredMap, this._id = 0, this.tdl = new external_tdl_tdlib_ffi_.TDLib("./libtdjson"), 
            this.tdParams = {
                _: "tdlibParameters",
                use_secret_chats: !1,
                device_model: "ali-forward " + config.f,
                enable_storage_optimizer: !0,
                use_file_database: !0,
                use_chat_info_database: !0,
                use_message_database: !0,
                ignore_file_names: !1,
                use_test_dc: !1
            };
        }
        get id() {
            return this._id;
        }
        getChat(chat_id) {
            return this.client.invoke({
                _: "getChat",
                chat_id
            });
        }
        async getSupergroupDomain(supergroup_id) {
            const group = await this.client.invoke({
                _: "getSupergroup",
                supergroup_id
            });
            return (null == group ? void 0 : group.username) || "";
        }
        deleteMessage(id, chat_id) {
            return this.client.invoke({
                _: "deleteMessages",
                message_ids: [ id ],
                chat_id,
                revoke: !0
            });
        }
        sendErrorEmail() {
            return sendErrorEmail("Сбой в авторизации телеграм клиента. Пересохраните настройки для получения нового авторизационного кода.");
        }
        async update(authInfo, ctx) {
            var _a;
            const {type, apiHash, apiId} = authInfo;
            if (!apiId) throw "Invalid apiId: " + apiId;
            if ("user" != type && "bot" != type) throw "Invalid tdlib client type: " + type;
            if (!tg_utils.a.test(apiHash)) throw "Invalid apiHash: " + apiHash;
            if ("user" === authInfo.type && !authInfo.phone) throw "Invalid phone: " + authInfo.phone;
            if ("bot" === authInfo.type && !tg_utils.b.test(authInfo.token)) throw "Invalid bot token: " + authInfo.token;
            if (!this.client || authInfo.apiHash != this.apiHash || authInfo.apiId != this.apiId || "user" === authInfo.type && authInfo.phone != this.phoneOrToken || "bot" === authInfo.type && authInfo.token != this.phoneOrToken) {
                const phoneOrToken = "bot" === authInfo.type ? authInfo.token : authInfo.phone, profile = external_sanitize_filename_default()("bot" === type ? (await Object(tg_utils.d)(phoneOrToken)).id.toString() : phoneOrToken);
                try {
                    this.dispose();
                    const filesDir = Object(data_path.a)(profile + "/files"), client = this.client = new external_tdl_.Client(this.tdl, {
                        apiId,
                        apiHash,
                        databaseDirectory: Object(data_path.a)(profile + "/database"),
                        filesDirectory: filesDir,
                        verbosityLevel: 1,
                        skipOldUpdates: !1,
                        useTestDc: !1,
                        useMutableRename: !0,
                        tdlibParameters: this.tdParams
                    });
                    Object(external_fs_.existsSync)(filesDir) || await external_make_dir_default()(filesDir), 
                    "win32" === process.platform ? await external_fs_.promises.symlink(filesDir, filesDir + "_unprotect", "junction").catch(Boolean) : (Object(external_fs_.existsSync)(filesDir + "_unprotect") || await external_make_dir_default()(filesDir + "_unprotect"), 
                    await exec(`mountpoint -q "${filesDir}_unprotect" || mount --bind "${filesDir}" "${filesDir}_unprotect"`)), 
                    this.onUpdate && client.on("update", async u => {
                        try {
                            "updateMessageSendSucceeded" === u._ ? this.messagePendings.fulfill(u.message.chat_id + "_" + u.old_message_id, u.message) : "updateMessageSendFailed" === u._ ? this.messagePendings.reject(u.message.chat_id + "_" + u.old_message_id, {
                                code: u.error_code,
                                message: u.error_message
                            }) : await this.onUpdate(u);
                        } catch (err) {
                            console.error.bind(console, "(./src/BaseTelegramClient.ts:174)")(null == u ? void 0 : u._, err);
                        }
                    }), client.on("error", console.error.bind(console, "(./src/BaseTelegramClient.ts:178)")), 
                    await client.connect();
                    const logsDir = Object(data_path.a)(profile + "/logs");
                    await external_make_dir_default()(logsDir), await client.invoke({
                        _: "setLogStream",
                        log_stream: {
                            _: "logStreamFile",
                            path: logsDir + "/error.log",
                            max_file_size: 10485760
                        }
                    }), await client.invoke({
                        _: "setLogVerbosityLevel",
                        new_verbosity_level: 1
                    });
                    for (let name in tdlibOptions) await client.invoke({
                        _: "setOption",
                        name,
                        value: {
                            _: "optionValueBoolean",
                            value: tdlibOptions[name]
                        }
                    });
                    this.apiHash = apiHash, this.apiId = apiId, this.phoneOrToken = phoneOrToken, await client.login(() => "bot" === type ? {
                        type: "bot",
                        getToken: async () => phoneOrToken
                    } : {
                        type: "user",
                        getPassword: async (passwordHint, retry) => {
                            const message = "Введите пароль двухэтапной аутентификации телеграм" + (passwordHint ? "\nподсказка: ```\n" + passwordHint + "\n```" : "");
                            if (ctx) {
                                const code = await fromCtx(ctx).remote("ElMessageBox").prompt(message, {
                                    showCancelButton: !1,
                                    roundButton: !0
                                });
                                return code.value || "";
                            }
                            return this.passwordHandler(message, retry);
                        },
                        getPhoneNumber: async _ => phoneOrToken,
                        getAuthCode: async retry => {
                            const message = "Введите код авторизации телеграм клиента (придет в sms или в уже авторизованный клиент):";
                            if (ctx) {
                                const code = await fromCtx(ctx).remote("ElMessageBox").prompt(message, {
                                    showCancelButton: !1,
                                    inputType: "number",
                                    roundButton: !0
                                });
                                return code.value || "";
                            }
                            return this.authHandler(message, retry);
                        }
                    });
                    const u = await this.getMe();
                    if (this._id = u.id, "userTypeBot" !== u.type._) for (let name in tdlibPostOptions) try {
                        await this.client.invoke({
                            _: "setOption",
                            name,
                            value: {
                                _: "optionValueBoolean",
                                value: tdlibPostOptions[name]
                            }
                        });
                    } catch (err) {
                        console.warn.bind(console, "(./src/BaseTelegramClient.ts:249)")(err);
                    }
                    return await (null === (_a = this.afterUpdate) || void 0 === _a ? void 0 : _a.call(this)), 
                    !0;
                } catch (err) {
                    throw this.dispose(), err;
                }
            }
            return !1;
        }
        async authHandler(text, retry) {
            throw this.dispose(), retry || await this.sendErrorEmail(), "Telegram client auth required. " + text;
        }
        async passwordHandler(text, retry) {
            throw this.dispose(), retry || await this.sendErrorEmail(), "Telegram client password required. " + text;
        }
        getMe() {
            return this.client ? this.client.invoke({
                _: "getMe"
            }) : null;
        }
        dispose() {
            this.client && (this.client.destroy(), this.client = null, this.phoneOrToken = null, 
            this.apiHash = null, this.apiId = null, this._id = 0);
        }
        async logout() {
            this.client && await this.client.invoke({
                _: "logOut"
            }), this.dispose();
        }
        async sendMessage(chat_id, content, reply_markup, disable_notification) {
            const message = await this.client.invoke({
                _: "sendMessage",
                chat_id,
                reply_markup,
                options: disable_notification ? {
                    _: "messageSendOptions",
                    disable_notification
                } : void 0,
                input_message_content: content.constructor === String ? {
                    _: "inputMessageText",
                    text: this.client.execute({
                        _: "parseTextEntities",
                        text: content,
                        parse_mode: {
                            _: "textParseModeMarkdown"
                        }
                    }),
                    disable_web_page_preview: !0
                } : content
            });
            return this.messagePendings.make(message.chat_id + "_" + message.id);
        }
        async downloadFile(f, signal, timeout) {
            var _a;
            return f = (null === (_a = f.local) || void 0 === _a ? void 0 : _a.path) ? f : await new Promise((resolve, reject) => {
                const cancel = e => {
                    var _a;
                    reject(e.type || e), (null === (_a = f.local) || void 0 === _a ? void 0 : _a.is_downloading_active) && this.client.invoke({
                        _: "cancelDownloadFile",
                        file_id: f.id
                    }).catch(console.error.bind(console, "(./src/BaseTelegramClient.ts:334)"));
                };
                if (signal) {
                    if (signal.aborted) return reject("abort");
                    signal.addEventListener("abort", cancel);
                }
                const p = this.client.invoke({
                    _: "downloadFile",
                    file_id: f.id,
                    priority: 1,
                    synchronous: !0
                }).then(resolve, reject);
                if (timeout) {
                    const t = setTimeout(cancel, timeout, "timeout").unref();
                    p.finally(() => {
                        null == signal || signal.removeEventListener("abort", cancel), clearTimeout(t);
                    });
                } else signal && p.finally(() => signal.removeEventListener("abort", cancel));
            }), f.local.path = f.local.path.replace(/[\\\/]files[\\\/]/, "/files_unprotect/"), 
            f;
        }
        async copyFile(source, remote, timeout) {
            return remote ? {
                _: "inputFileRemote",
                id: source.remote.id
            } : {
                _: "inputFileLocal",
                path: (await this.downloadFile(source, null, timeout)).local.path
            };
        }
        async messageToMediaInput(message, timeout, caption) {
            const {thumbnail, width, height, duration, video, supports_streaming, animation} = message.content.video || message.content.animation, [media, thumb] = await Promise.all([ this.copyFile(video || animation, message.can_be_forwarded, timeout), thumbnail && this.copyFile(thumbnail.photo || thumbnail.file, message.can_be_forwarded, timeout) ]);
            return {
                _: video ? "inputMessageVideo" : "inputMessageAnimation",
                video: video ? media : void 0,
                animation: animation ? media : void 0,
                thumbnail: thumbnail ? {
                    _: "inputThumbnail",
                    thumbnail: thumb,
                    width: thumbnail.width,
                    height: thumbnail.height
                } : void 0,
                duration,
                width,
                height,
                supports_streaming,
                caption
            };
        }
        optimizeStorate(seconds) {
            var _a;
            return null === (_a = this.client) || void 0 === _a ? void 0 : _a.invoke({
                _: "optimizeStorage",
                size: -1,
                ttl: seconds,
                count: -1,
                immunity_delay: -1
            });
        }
    }
    var es2015_ = __webpack_require__(88), es2015_default = __webpack_require__.n(es2015_), RGI_Emoji_ = __webpack_require__(89), RGI_Emoji_default = __webpack_require__.n(RGI_Emoji_), external_memoize_one_ = __webpack_require__(40), external_memoize_one_default = __webpack_require__.n(external_memoize_one_), external_typeorm_ = __webpack_require__(2), Patch = __webpack_require__(42);
    class AsyncUniqQueue {
        constructor() {
            this.objects = [], this.resolvers = [];
        }
        [Symbol.asyncIterator]() {
            return this;
        }
        get size() {
            return this.objects.length;
        }
        add(value, first) {
            this.resolvers.length > 0 ? this.resolvers.shift()({
                done: !1,
                value
            }) : first ? (this.delete(value), this.objects.unshift(value)) : this.objects.includes(value) || this.objects.push(value);
        }
        delete(value) {
            const i = this.objects.indexOf(value);
            i > -1 && this.objects.splice(i, 1);
        }
        next() {
            return new Promise(resolve => {
                this.objects.length > 0 ? resolve({
                    done: !1,
                    value: this.objects.shift()
                }) : this.resolvers.push(resolve);
            });
        }
    }
    var cancelation_token_ = __webpack_require__(90);
    function timeFn(connection, resolve) {
        connection.dispose(), resolve();
    }
    var utils_delay = function(time, cancelationToken) {
        return new Promise(cancelationToken ? (resolve, reject) => {
            const connection = cancelationToken.connect(() => {
                clearTimeout(timeout), reject(new cancelation_token_.CancelationTokenError);
            }), timeout = setTimeout(timeFn, time, connection, resolve);
        } : resolve => {
            setTimeout(resolve, time);
        });
    };
    function fit2size(m, s) {
        let o = m.size - s;
        if (o > .3 * s) for (let k of m.keys()) if (m.delete(k), !--o) break;
    }
    var LinksSettings = __webpack_require__(20), external_node_mdbx_ = __webpack_require__(38), external_node_mdbx_default = __webpack_require__.n(external_node_mdbx_), utils = __webpack_require__(3);
    let AEplatform_AEPlatform = class AEPlatform {
        constructor(settings, coreSettings, coreSettingsRepository) {
            this.settings = settings, this.coreSettings = coreSettings, this.coreSettingsRepository = coreSettingsRepository, 
            this.cache = new external_node_mdbx_default.a({
                path: Object(data_path.a)("caches/aeplatform"),
                maxDbs: 16,
                syncMode: "safeNoSync",
                valueMode: "string"
            });
        }
        async api(url, json) {
            const {errors, data} = await Object(utils.m)(url, {
                method: json ? "POST" : "GET",
                responseType: "json",
                resolveBodyOnly: !0,
                throwHttpErrors: !1,
                timeout: 1e3 * this.settings.maxLinkShortTime,
                headers: {
                    "Content-Type": "application/json",
                    "X-Client-ID": this.settings.aeClientId.toString(),
                    Authorization: this.coreSettings.ae.token
                },
                json
            });
            if (errors) {
                const exists = errors.find(e => 409002 === e.code);
                if (exists) {
                    const {creative_id} = exists.meta;
                    return this.api(url + "/" + creative_id);
                }
                throw errors;
            }
            return data.attributes;
        }
        process(tracking_id, url) {
            return this.cache.asyncTransact(async txn => {
                const cache = txn.getDbi(this.settings.aeClientId.toString(32));
                let link = cache.get(url);
                if (!link) try {
                    let token = this.coreSettings.getToken("ae");
                    if (!token) {
                        const {error, access_token, expires_in} = await utils.m.post("https://oauth2.aeplatform.ru/token", {
                            responseType: "json",
                            resolveBodyOnly: !0,
                            timeout: 1e3 * this.settings.maxLinkShortTime,
                            form: {
                                grant_type: "client_credentials",
                                client_id: this.settings.aeClientId,
                                client_secret: this.settings.aeClientSecret
                            }
                        });
                        if (error) throw error;
                        this.coreSettings.setToken("ae", token = access_token, expires_in), await this.coreSettingsRepository.save(this.coreSettings);
                    }
                    const {targetLink} = await this.api(`https://api.aeplatform.ru/api/v1/users/${this.settings.aeUserId}/creatives`, {
                        title: "Ali forward",
                        link: url
                    });
                    (link = targetLink) && cache.put(url, link);
                } catch (err) {
                    console.error.bind(console, "(./src/AEplatform.ts:87)")(url, err);
                }
                return link && (link += "?sub=" + tracking_id), link;
            });
        }
    };
    AEplatform_AEPlatform = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ LinksSettings.LinksSettings, Settings.Settings, Settings.SettingsRepository ]) ], AEplatform_AEPlatform);
    class LinkShortener_TUrl {
        constructor(_shortable, url, title) {
            this._shortable = _shortable, this.url = url, this.title = title;
        }
        get shortable() {
            return this.url && this._shortable;
        }
        toString() {
            let {title, url} = this;
            return url ? (url = Object(utils.f)(url), `<a href="${url}">${title ? Object(utils.f)(title) : url}</a>`) : "";
        }
    }
    function wrapLink(url, title) {
        return new LinkShortener_TUrl(!1, url, title);
    }
    const patchLink = Object(utils.a)(async link => {
        const u = new URL(link);
        if (!Object(external_net_.isIP)(u.hostname)) {
            let ip = utils.n[u.hostname];
            if (!ip) try {
                utils.n[u.hostname] = ip = (await utils.c.resolve4(u.hostname)).find(Boolean);
            } catch (e) {
                console.error.bind(console, "(./src/LinkShortener.ts:40)")(e);
            }
            ip && (u.hostname = ip);
        }
        return u.toString();
    }), DUMMY_OBJECT = Object.create(null), ALI_RE = /^https?:\/\/(?:[a-z0-9\-]+\.)*aliexpress\.(?:ru|com)\//i;
    let LinkShortener_LinkShortener = class LinkShortener {
        constructor(settings, aeplatform) {
            this.settings = settings, this.aeplatform = aeplatform, this.scache = new Map, this.waitAll = async (literals, ...placeholders) => {
                const pp = await Promise.all(placeholders), shortable = pp.filter(p => p.shortable).map(p => p.url), processed = shortable.length ? await this.shorby(shortable) : DUMMY_OBJECT;
                let result = literals[0];
                for (let i = 0; i < pp.length; i++) {
                    const p = pp[i];
                    if (p instanceof LinkShortener_TUrl) {
                        const n = processed[p.url];
                        n && (p.url = this.settings.shorbyUrl + "/" + n);
                    }
                    result += p.toString(), result += literals[i + 1];
                }
                return result;
            };
        }
        async shorby(urls) {
            const result = Object.create(null);
            try {
                const requestedUrls = [];
                for (const u of urls) {
                    const s = this.scache.get(u);
                    s ? result[u] = s : requestedUrls.push(u);
                }
                if (requestedUrls.length) {
                    const s = await patchLink(this.settings.shorbyUrl);
                    Object.assign(result, await utils.m.put(s, {
                        responseType: "json",
                        resolveBodyOnly: !0,
                        json: requestedUrls,
                        timeout: 1e3 * this.settings.maxLinkShortTime
                    }));
                }
            } catch (e) {
                console.error.bind(console, "(./src/LinkShortener.ts:85)")(e);
            }
            for (let url in result) this.scache.set(url, result[url]);
            return fit2size(this.scache, 800), result;
        }
        admitad(track, url) {
            return this.settings.admitad + `/?ulp=${encodeURIComponent(url)}&subid=` + encodeURIComponent(track);
        }
        async a(track, urlTpl, url, title) {
            return ALI_RE.test(url) && (track || urlTpl) ? new LinkShortener_TUrl(!0, track && (this.settings.adplatform === LinksSettings.AdPlatforms.AEPlatform && await this.aeplatform.process(track, url) || this.settings.adplatform === LinksSettings.AdPlatforms.Admitad && this.admitad(track, url)) || urlTpl && new Function("link", "return `" + urlTpl + "`")(url), title) : wrapLink(url, title);
        }
        async processLinks(content, channel) {
            const a = channel && this.settings.adplatform !== LinksSettings.AdPlatforms.None ? this.a.bind(this, channel.trackingId, channel.urlTemplate) : wrapLink;
            return await new Function("waitAll", "a", "return waitAll`" + content + "`")(this.waitAll, a);
        }
    };
    function rshift(r, from, dist) {
        let start = r.offset, end = r.offset + r.length;
        return start >= from && (start = Math.max(from, start + dist)), end >= from && (end = Math.max(from, end + dist)), 
        r.length = end - start, r.offset = start, r.length > 0;
    }
    LinkShortener_LinkShortener = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ LinksSettings.LinksSettings, AEplatform_AEPlatform ]) ], LinkShortener_LinkShortener);
    class MultiRange {
        constructor(text, entities) {
            this.text = text, this.entities = entities;
        }
        prepend(text, entity) {
            this.insert(0, text.length), this.text = text + this.text, this.entities.unshift(entity);
        }
        prependTag(hashtag, suffix = "\n") {
            this.prepend("#" + hashtag + suffix, {
                _: "textEntity",
                type: {
                    _: "textEntityTypeHashtag"
                },
                offset: 0,
                length: hashtag.length + 1
            });
        }
        applyPatch(re, arg) {
            re.lastIndex = 0, this.text = this.text.replace(re, (whatReplace, ...groups) => {
                const offset = groups[groups.length - 2];
                return whatReplace.length && this.remove(offset, whatReplace.length), arg.length && this.insert(offset, arg.length), 
                arg;
            });
        }
        cut(offset, length) {
            this.text = function cut(s, offset, length) {
                return s.substr(0, offset) + s.substr(offset + length);
            }(this.text, offset, length), this.remove(offset, length);
        }
        remove(offset, length) {
            this.entities.splice(0, this.entities.length, ...this.entities.filter(r => rshift(r, offset, -length)));
        }
        insert(offset, length) {
            this.entities.every(r => rshift(r, offset, length));
        }
        get size() {
            return this.text.length;
        }
        toText(size) {
            const text = this.text.length > size ? function trimTo(text, length) {
                return text.slice(0, text.lastIndexOf(" ", length - 1)).trimEnd();
            }(this.text, size) : this.text;
            return {
                _: "formattedText",
                text,
                entities: this.entities.filter(r => rshift(r, text.length, -2147483647))
            };
        }
    }
    class BotClient_PromtCancelError extends ExtendableError {}
    const EMOJI_RE = es2015_default()(), genEmojiRe = external_memoize_one_default()(s => {
        const m = s && s.match(RGI_Emoji_default()());
        return Array.isArray(m) ? new RegExp(m.join("|"), "ug") : null;
    });
    function* chunk(it, size) {
        const rows = Math.ceil(it.length / size), minSize = it.length / rows | 0;
        let over = it.length - minSize * rows;
        for (;it.length; ) yield it.splice(0, over > 0 ? minSize + 1 : minSize), --over;
    }
    function b64(c, ...args) {
        return Buffer.from(JSON.stringify([ c, ...args ])).toString("base64");
    }
    function f64(data) {
        return JSON.parse(Buffer.from(data, "base64").toString());
    }
    function filterButtons(...buttons) {
        return buttons = buttons.filter(Boolean), buttons.length > 1 ? buttons : null;
    }
    function filterRows(...rows) {
        return rows.filter(Boolean);
    }
    const BAD_ENTITIES = new Set([ "textEntityTypeMention", "textEntityTypeHashtag", "textEntityTypeCashtag", "textEntityTypeBotCommand", "textEntityTypeMentionName", "textEntityTypeUrl" ]);
    let BotClient_BotClient = class BotClient extends BaseTelegramClient_BaseTelegramClient {
        constructor(linkShortener, coreSettings, coreSettingsRepository, settings, channelRepository, postRepository, sourceRepository, patch) {
            super(), this.linkShortener = linkShortener, this.coreSettings = coreSettings, this.coreSettingsRepository = coreSettingsRepository, 
            this.settings = settings, this.channelRepository = channelRepository, this.postRepository = postRepository, 
            this.sourceRepository = sourceRepository, this.patch = patch, this.chain = new AsyncUniqQueue, 
            this.cache = new Map, this.patches = [], this.channels = [], this.sources = new Map, 
            this.commands = {
                refresh5: [ "🔄 5 постов с ошибками", () => this.refreshFailedPosts(5) ],
                refresh25: [ "🔄 25 постов с ошибками", () => this.refreshFailedPosts(25) ],
                refresh50: [ "🔄 50 постов с ошибками", () => this.refreshFailedPosts(50) ],
                refresh_all: [ "🔄 все посты с ошибками", () => this.refreshFailedPosts() ],
                restart: [ "Перезапустить бота", () => this.emit("restart", "Bot") ]
            }, this.threadPromise = null, this.afterUpdate = async () => {
                await this.loadChannels(), await Promise.allSettled([ ...this.channels, this.coreSettings ].map(s => s.supergroup_id && this.getSupergroupDomain(s.supergroup_id))), 
                this.coreSettings.admin_id && await this.getChat(this.coreSettings.admin_id), this.threadPromise || (this.threadPromise = this.thread().catch(console.error.bind(console, "(./src/BotClient.ts:171)")));
            }, this.promtMarkup = {
                _: "replyMarkupInlineKeyboard",
                rows: [ ...chunk(Array.from("789456123🆑0↩", d => ({
                    _: "inlineKeyboardButton",
                    type: {
                        _: "inlineKeyboardButtonTypeCallback",
                        data: b64("t", d)
                    },
                    text: d
                })), 3) ]
            }, this.updateMarkupMutex = new external_async_mutex_.Mutex, this.activeCommands = new Set, 
            this.onUpdate = async update => {
                var _a, _b, _c;
                if ("updateNewCallbackQuery" === update._ && this.threadPromise) {
                    const {payload, chat_id, message_id} = update;
                    if ("callbackQueryPayloadData" === payload._) if (chat_id === this.coreSettings.supergroup_chat) {
                        const [cmd, post_id, arg] = f64(payload.data), hash = cmd + post_id;
                        if (!this.activeCommands.has(hash)) try {
                            this.activeCommands.add(hash), await this[cmd](await this.getPost(post_id), arg);
                        } catch (err) {
                            console.error.bind(console, "(./src/BotClient.ts:298)")(cmd, post_id, err);
                        } finally {
                            this.activeCommands.delete(hash);
                        }
                    } else if (chat_id === this.coreSettings.admin_id && message_id === (null === (_a = this.promtReply) || void 0 === _a ? void 0 : _a.id)) {
                        const [cmd, key] = f64(payload.data);
                        if ("t" === cmd) switch (key) {
                          case "↩":
                            return this.client.invoke({
                                _: "editMessageReplyMarkup",
                                chat_id,
                                message_id,
                                reply_markup: {
                                    _: "replyMarkupInlineKeyboard",
                                    rows: []
                                }
                            }).catch(Boolean), this.promtReply.fulfill(parseInt(this.promtReply.accumulator.join("").replace(/\D+/g, "")).toString());

                          case "🆑":
                            return void (this.promtReply.accumulator.length && (this.promtReply.accumulator.length = 0, 
                            this.client.invoke({
                                _: "editMessageReplyMarkup",
                                chat_id,
                                message_id,
                                reply_markup: this.updatePromtMarkup()
                            }).catch(Boolean)));

                          default:
                            this.promtReply.accumulator.push(key), this.client.invoke({
                                _: "editMessageReplyMarkup",
                                chat_id,
                                message_id,
                                reply_markup: this.updatePromtMarkup()
                            }).catch(Boolean);
                        }
                    }
                } else if ("updateNewMessage" === update._ && !update.message.is_outgoing) if (update.message.chat_id === this.coreSettings.supergroup_chat) this.processSlashCommands(update.message); else if (update.message.chat_id === this.coreSettings.admin_id) {
                    const {content} = update.message;
                    if ("messageText" === content._) {
                        if (update.message.reply_to_message_id === (null === (_b = this.promtReply) || void 0 === _b ? void 0 : _b.id)) return this.promtReply.fulfill(content.text.text.trim());
                        this.processSlashCommands(update.message);
                    } else if ("messageVideo" === content._ || "messageAnimation" === content._) {
                        const id = null === (_c = content.caption) || void 0 === _c ? void 0 : _c.text;
                        if (id) {
                            const post = await this.getPost(id);
                            post.actual = !1, await post.makeCollage("None", this.settings);
                            const src = post.video.src;
                            post.video = await this.messageToMediaInput(update.message, this.settings.maxMediaDownloadTime), 
                            src && (post.video.src = src), await this.savePost(post, !0);
                        }
                    }
                }
            }, this.refreshing = !1, this.loadPatches();
        }
        async getPost(post_id, p) {
            return p = this.cache.get(post_id) || p || await this.postRepository.findOneOrFail(post_id), 
            this.cache.delete(post_id), this.cache.set(post_id, p), fit2size(this.cache, 80), 
            p;
        }
        async findPost(id) {
            return this.cache.get(id) || await this.postRepository.findOne(id);
        }
        async savePost(post, finalAction) {
            await this.postRepository.save(post, {
                reload: !finalAction,
                listeners: !1
            }), finalAction && this.chain.add(!0);
        }
        async setCommands() {
            const commands = Object.keys(this.commands).map(command => ({
                _: "botCommand",
                command,
                description: this.commands[command][0]
            }));
            return await this.client.invoke({
                _: "deleteCommands"
            }), Promise.all([ this.client.invoke({
                _: "setCommands",
                scope: {
                    _: "botCommandScopeChat",
                    chat_id: this.coreSettings.admin_id
                },
                commands
            }), this.client.invoke({
                _: "setCommands",
                scope: {
                    _: "botCommandScopeChatAdministrators",
                    chat_id: this.coreSettings.supergroup_chat
                },
                commands
            }) ]);
        }
        calcMask(post) {
            var _a;
            try {
                return 0 | (null === (_a = this.sources.get(post.owner_id)) || void 0 === _a ? void 0 : _a.masker(post, Post.includes.bind(post.text)));
            } catch (err) {
                console.error.bind(console, "(./src/BotClient.ts:178)")(post.url, err);
            }
            return 0;
        }
        updatePromtMarkup() {
            return this.promtMarkup.rows[3][2].text = "[" + this.promtReply.accumulator.join("").padEnd(5, "●") + "] ↩", 
            this.promtMarkup;
        }
        async promtInternal(text, markup) {
            var _a;
            null === (_a = this.promtReply) || void 0 === _a || _a.reject(new BotClient_PromtCancelError), 
            this.promtReply = makeDeffer(), this.promtReply.accumulator = [];
            const {id} = await this.sendMessage(this.coreSettings.admin_id, text, markup);
            return this.promtReply.id = id, this.promtReply.promise.finally(() => {
                this.deleteMessage(id, this.coreSettings.admin_id).catch(console.error.bind(console, "(./src/BotClient.ts:205)"));
            });
        }
        promtNum(text) {
            return this.promtMarkup.rows[3][2].text = "↩", this.promtInternal(text + "\n`Используйте только инлайн кнопки,` [причина](https://github.com/tdlib/td/issues/451#issuecomment-519343382)\n`Вводите код достаточно медленно для обновления введенных цифр на кнопке ↩`", this.promtMarkup);
        }
        promt(text) {
            return this.promtInternal(text, {
                _: "replyMarkupForceReply"
            });
        }
        async updateMarkup(post) {
            const message_id = post.getChannelMessageId(this.coreSettings.supergroup_id);
            if (message_id) {
                const unlock = await this.updateMarkupMutex.acquire(), chat_id = this.coreSettings.supergroup_chat;
                try {
                    await this.client.invoke({
                        _: "getMessage",
                        chat_id,
                        message_id
                    }), await this.client.invoke({
                        _: "editMessageReplyMarkup",
                        chat_id,
                        message_id,
                        reply_markup: this.markup(post, this.calcMask(post))
                    });
                } catch (err) {
                    console.error.bind(console, "(./src/BotClient.ts:233)")(Object(utils.s)(chat_id, message_id), err);
                } finally {
                    unlock();
                }
            }
        }
        async i(post, pic) {
            if (await post.makeCollage(pic, this.settings)) return (await Object(utils.b)(Object.keys(post.channels), async channel => {
                const c = +channel;
                try {
                    post.getChannelMessageId(c) && c != this.coreSettings.supergroup_id && await this.publish(post, c);
                } catch (err) {
                    return post.setError("(./src/BotClient.ts:249)", c, err);
                }
            })).length > 0 && await this.savePost(post), await this.publish(post, this.coreSettings.supergroup_id, this.calcMask(post));
            await this.updateMarkup(post);
        }
        async d(post, channel) {
            const c = post.getChannelMessageId(channel);
            c && await this.deleteMessage(c, Object(utils.t)(channel)).then(() => post.channels[channel] = 1, err => post.setError("(./src/BotClient.ts:263)", channel, err)) && await this.savePost(post), 
            await this.updateMarkup(post);
        }
        async p(post, channel) {
            await this.publish(post, channel).catch(err => post.setError("(./src/BotClient.ts:271)", channel, err)) && await this.savePost(post), 
            await this.updateMarkup(post);
        }
        r(post) {
            return this.emit("refresh_tg", post);
        }
        v(post) {
            return this.emit("refresh_vk", post);
        }
        processSlashCommands(message) {
            var _a;
            if ("messageText" === message.content._ && this.threadPromise) for (const e of message.content.text.entities) if ("textEntityTypeBotCommand" === e.type._) {
                const cmd = message.content.text.text.substr(e.offset + 1, e.length - 1).split("@", 1)[0];
                null === (_a = this.commands[cmd]) || void 0 === _a || _a[1](), message.can_be_deleted_for_all_users && this.deleteMessage(message.id, message.chat_id).catch(console.error.bind(console, "(./src/BotClient.ts:369)"));
            }
        }
        async thread() {
            for await (let _ of this.chain) {
                let post = await this.postRepository.findOne({
                    actual: !1
                });
                if (!post) continue;
                this.chain.add(!0);
                let wait = 0, updated = !1;
                try {
                    post = await this.getPost(post.id, post);
                    const mask = this.calcMask(post);
                    for (const c of this.channels) try {
                        if (mask & c.mask && 1 != post.channels[c.supergroup_id] || post.getChannelMessageId(c.supergroup_id)) {
                            const {id, is_new, chat_id} = await this.publish(post, c.supergroup_id);
                            updated = !0, is_new && c.repeate_message_after > 0 && id % (c.repeate_message_after + 1) == 0 && c.additional_text && await this.sendMessage(chat_id, c.additional_text);
                        }
                    } catch (err) {
                        post.setError("(./src/BotClient.ts:406)", c.supergroup_id, err) && (updated = !0);
                    }
                    await this.publish(post, this.coreSettings.supergroup_id, mask), post.actual = updated = !0;
                } catch (err) {
                    429 === (null == err ? void 0 : err.code) && (wait = 0 | parseInt(external_last_match_default()(err.message, /\d+$/))), 
                    post.setError("(./src/BotClient.ts:417)", this.coreSettings.supergroup_id, err) && (updated = !0), 
                    (post.actual = !!post.getChannelMessageId(this.coreSettings.supergroup_id)) && (updated = !0);
                } finally {
                    updated && await this.savePost(post).catch(err => console.error.bind(console, "(./src/BotClient.ts:424)")(post.url, err)), 
                    wait && await utils_delay(1e3 * wait);
                }
            }
        }
        async loadSources() {
            const c = await this.sourceRepository.find();
            this.sources = new Map(c.map(s => (s.buildMasker(this.channels), [ s.id, s ])));
        }
        async loadChannels() {
            this.channels = (await this.channelRepository.find()).sort((a, b) => a.order - b.order), 
            await this.loadSources();
        }
        loadPatches() {
            this.patches = this.patch.run();
        }
        async getContent(post, channel, forceRemoveEmoji) {
            const maxLength = post.video._ ? 1024 : 4096, eRe = forceRemoveEmoji ? EMOJI_RE : genEmojiRe(this.patch.emoji), data = this.client.execute({
                _: "parseTextEntities",
                text: await this.linkShortener.processLinks(eRe ? post.text.replace((eRe.lastIndex = 0, 
                eRe), "") : post.text, channel),
                parse_mode: {
                    _: "textParseModeHTML"
                }
            });
            data.entities = data.entities.filter(e => !BAD_ENTITIES.has(e.type._));
            const m = new MultiRange(data.text, data.entities);
            if (channel) for (const {offset, length, type} of data.entities.slice()) "fail" === type.language && length > 0 && m.cut(offset, length);
            for (const {re, arg} of this.patches) m.applyPatch(re, arg);
            return channel || (post.fails && m.prependTag("error", " " + post.fails + "\n"), 
            m.prependTag(post.sourceTag)), !forceRemoveEmoji && m.size >= maxLength ? this.getContent(post, channel, !0) : (post.canDisplayPreview && m.prepend(" ", {
                _: "textEntity",
                type: {
                    _: "textEntityTypeTextUrl",
                    url: post.preview
                },
                offset: 0,
                length: 1
            }), m.toText(maxLength));
        }
        markup(post, mask) {
            const source = this.sources.get(post.owner_id), url = post.tg && (null == source ? void 0 : source.domain) ? source.url + "&post=" + post.message_id / 1048576 : post.url;
            return {
                _: "replyMarkupInlineKeyboard",
                rows: filterRows([ {
                    _: "inlineKeyboardButton",
                    type: {
                        _: "inlineKeyboardButtonTypeUrl",
                        url
                    },
                    text: (post.is_owner ? "😎" : "") + (post.had_tags ? "#️⃣" : "") + (post.fails ? "🔴" : "") + ((null == source ? void 0 : source.name) || url)
                }, {
                    _: "inlineKeyboardButton",
                    type: {
                        _: "inlineKeyboardButtonTypeCallback",
                        data: b64(post.tg ? "r" : "v", post.id, Object(rnd_id.a)())
                    },
                    text: external_dayjs_default()(1e3 * post.date).add(this.settings.timezoneOffset, "hour").format("DD.MM H:mm") + (post.edited && post.edited != post.date ? "│" + external_dayjs_default()(1e3 * post.edited).add(this.settings.timezoneOffset, "hour").format("DD.MM H:mm") : "") + " 🔄"
                } ], filterButtons({
                    _: "inlineKeyboardButton",
                    type: {
                        _: "inlineKeyboardButtonTypeCallback",
                        data: b64("i", post.id, "None")
                    },
                    text: ("None" === post.type ? "🔘" : "") + (post.video._ ? "🎥" : "🚫")
                }, post.source_photos.length && {
                    _: "inlineKeyboardButton",
                    type: {
                        _: "inlineKeyboardButtonTypeCallback",
                        data: b64("i", post.id, "Source")
                    },
                    text: "Source" === post.type ? `🔘Source [${post.previewsCount} из ${post.source_photos.length}]` : `Source [${post.source_photos.length}]`
                }, post.ali_photos.length && {
                    _: "inlineKeyboardButton",
                    type: {
                        _: "inlineKeyboardButtonTypeCallback",
                        data: b64("i", post.id, "Ali")
                    },
                    text: "Ali" === post.type ? `🔘Ali [${post.previewsCount} из ${post.ali_photos.length}]` : `Ali [${post.ali_photos.length}]`
                }), ...chunk(this.channels.map(c => {
                    const state = post.channels[c.supergroup_id];
                    return {
                        _: "inlineKeyboardButton",
                        type: {
                            _: "inlineKeyboardButtonTypeCallback",
                            data: b64(post.getChannelMessageId(c.supergroup_id) ? "d" : "p", post.id, c.supergroup_id)
                        },
                        text: (state > 1 ? "🟢" : -1 === state ? "🔴" : state < -1 ? "🟠" : "") + (mask & c.mask ? c.title : "🔸" + c.title)
                    };
                }), this.settings.buttonsPerRow))
            };
        }
        async publish(post, supergroup_id, mask = 0) {
            const isAdminChannel = supergroup_id === this.coreSettings.supergroup_id, unlock = isAdminChannel ? await this.updateMarkupMutex.acquire() : null;
            try {
                const caption = await this.getContent(post, isAdminChannel ? null : this.channels.find(c => c.supergroup_id === supergroup_id)), reply_markup = isAdminChannel ? this.markup(post, mask) : void 0, chat_id = Object(utils.t)(supergroup_id), message_id = post.getChannelMessageId(supergroup_id);
                if (message_id) return await this.client.invoke({
                    _: "getMessage",
                    chat_id,
                    message_id
                }), post.video._ ? this.client.invoke({
                    _: "editMessageMedia",
                    chat_id,
                    message_id,
                    reply_markup,
                    input_message_content: "None" === post.type ? {
                        ...post.video,
                        caption
                    } : {
                        _: "inputMessagePhoto",
                        caption,
                        photo: {
                            _: "inputFileLocal",
                            path: post.previewLocal
                        }
                    }
                }) : await this.client.invoke({
                    _: "editMessageText",
                    chat_id,
                    message_id,
                    reply_markup,
                    input_message_content: {
                        _: "inputMessageText",
                        disable_web_page_preview: !post.canDisplayPreview,
                        text: caption
                    }
                });
                {
                    const m = post.video._ ? await this.sendMessage(chat_id, "None" === post.type ? {
                        ...post.video,
                        caption
                    } : {
                        _: "inputMessagePhoto",
                        caption,
                        photo: {
                            _: "inputFileLocal",
                            path: post.previewLocal
                        }
                    }, reply_markup) : await this.sendMessage(chat_id, {
                        _: "inputMessageText",
                        text: caption,
                        clear_draft: !1,
                        disable_web_page_preview: !post.canDisplayPreview
                    }, reply_markup);
                    return post.channels[supergroup_id] = m.id, isAdminChannel && (utils.n.last_post_published_at = new Date, 
                    this.refreshFailedPosts(2)), Object.assign(m, {
                        is_new: !0
                    });
                }
            } finally {
                null == unlock || unlock();
            }
        }
        async refreshFailedPosts(take) {
            try {
                if (this.refreshing) return;
                this.refreshing = !0;
                const posts = await this.postRepository.find({
                    select: [ "id", "owner_id", "fails" ],
                    where: {
                        fails: Object(external_typeorm_.Between)(1, this.settings.maxParseTries - 1)
                    },
                    order: {
                        date: "DESC"
                    },
                    take
                });
                for (const post of posts) post.tg ? await this.emit("refresh_tg", post) : await this.emit("refresh_vk", post);
            } catch (err) {
                console.error.bind(console, "(./src/BotClient.ts:661)")(err);
            } finally {
                this.refreshing = !1;
            }
        }
        incPostFails(id, fails) {
            return this.postRepository.update({
                id
            }, {
                fails: fails >= this.settings.maxParseTries ? fails + 1 : this.settings.maxParseTries
            });
        }
        dropOldPosts() {
            return this.settings.removeAfter > 0 && this.postRepository.delete({
                edited: Object(external_typeorm_.LessThan)(Math.round(Date.now() / 1e3) - this.settings.removeAfterSeconds)
            }).catch(console.error.bind(console, "(./src/BotClient.ts:677)"));
        }
    };
    BotClient_BotClient = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.c)(6, external_typescript_ioc_.Inject), Object(tslib_es6.c)(7, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ LinkShortener_LinkShortener, Settings.Settings, Settings.SettingsRepository, ClientSettings.ClientSettings, Channel.ChannelRepository, Post.PostRepository, Source.SourceRepository, Patch.Patch ]) ], BotClient_BotClient);
    class MultiMap_MultiMap extends Map {
        constructor(cb) {
            super(), this.cb = cb, this.cache = new external_node_mdbx_default.a({
                path: Object(data_path.a)("caches/tg_albums"),
                maxDbs: 1
            });
        }
        push(key, v) {
            this.cache.transact(txn => {
                const dbi = txn.getDbi();
                if (dbi.has(key)) {
                    const a = decode(dbi.get(key));
                    a.includes(v) || (a.push(v), dbi.put(key, encode(a)));
                } else dbi.put(key, encode([ v ]));
            }), this.has(key) || this.set(key, setTimeout(() => this.cb(key, this.pop(key)), 4e3).unref());
        }
        pop(key) {
            return clearTimeout(this.get(key)), this.delete(key), this.cache.transact(txn => {
                const buf = txn.getDbi().get(key);
                return buf && decode(buf) || [];
            });
        }
    }
    var external_cheerio_ = __webpack_require__(60), external_crypto_random_string_ = __webpack_require__(91), external_crypto_random_string_default = __webpack_require__.n(external_crypto_random_string_), external_escape_string_regexp_ = __webpack_require__(92), external_escape_string_regexp_default = __webpack_require__.n(external_escape_string_regexp_), source_ = __webpack_require__(54), external_qs_ = __webpack_require__(61), external_qs_default = __webpack_require__.n(external_qs_), external_tough_cookie_ = __webpack_require__(93);
    const SUPPORTED_ENTITIES = new Set([ "textEntityTypeHashtag", "textEntityTypeBold", "textEntityTypeItalic", "textEntityTypeUnderline", "textEntityTypeStrikethrough", "textEntityTypeCode", "textEntityTypePreCode", "textEntityTypePre", "textEntityTypeUrl", "textEntityTypeTextUrl" ]);
    class ETree_ETree {
        constructor(source, start, length, type) {
            if (this.source = source, this.start = start, this.length = length, this.type = type, 
            this.children = [], this.end = this.start + this.length, type) switch (type._) {
              case "textEntityTypeHashtag":
                break;

              case "textEntityTypeBold":
                this.begin = '${"<b>"}', this.finish = '${"</b>"}';
                break;

              case "textEntityTypeItalic":
                this.begin = '${"<i>"}', this.finish = '${"</i>"}';
                break;

              case "textEntityTypeUnderline":
                this.begin = '${"<u>"}', this.finish = '${"</u>"}';
                break;

              case "textEntityTypeStrikethrough":
                this.begin = '${"<s>"}', this.finish = '${"</s>"}';
                break;

              case "textEntityTypeCode":
                this.begin = '${"<code>"}', this.finish = '${"</code>"}';
                break;

              case "textEntityTypePreCode":
              case "textEntityTypePre":
                this.begin = '${"<pre>"}', this.finish = '${"</pre>"}';
                break;

              case "textEntityTypeUrl":
                this.begin = "${resolve(`" + source.substr(start, length) + "`)}";
                break;

              case "textEntityTypeTextUrl":
                {
                    const title = Object(utils.d)(source.substr(start, length)).trim();
                    title && (this.begin = "${resolve(`" + type.url + "`, `" + title + "`)}");
                    break;
                }
            }
        }
        includes(l) {
            return l != this && this.parent != l && l.start >= this.start && l.end <= this.end;
        }
        * content(from) {
            if (from != this.start && (yield this.source.substring(from, this.start), from = this.start), 
            this.begin && (yield this.begin), this.finish || !this.type) {
                for (const c of this.children.sort((a, b) => a.start - b.start)) yield* c.content(from), 
                from = c.end;
                from != this.end && (yield this.source.substring(from, this.end));
            }
            this.finish && (yield this.finish);
        }
    }
    const HTTPS_RE = /^https?:\/\//i, ALI_HOST_RE = /^(?:[a-z0-9\-]+\.)*aliexpress\.(?:ru|com)$/i, ALI_SCLICK_HOST_RE = /^s\.click\.aliexpress\.(?:ru|com)$/i, ALI_LOGIN_HOST_RE = /^login\.(?:[a-z0-9\-]+\.)*aliexpress\.(?:ru|com)$/i, getHostsChecker = (...hosts) => {
        const r = new RegExp("^" + hosts.map(host => "(?:[a-z0-9\\-]+\\.)*" + external_escape_string_regexp_default()(host)).join("|") + "$");
        return r.test.bind(r);
    }, getHostsCheckerWhite = external_memoize_one_default()(getHostsChecker), getHostsCheckerBlack = external_memoize_one_default()(getHostsChecker), getHostsCheckerBody = external_memoize_one_default()(getHostsChecker);
    class PostTransformer_PromiseCookieJar extends external_tough_cookie_.CookieJar {
        setCookie(cookieOrString, currentUrl, options) {
            return options || (options = {}), options.ignoreError = !0, super.setCookie(cookieOrString, currentUrl, options);
        }
    }
    const r32hexs = external_crypto_random_string_default.a.bind(null, {
        length: 32,
        type: "hex"
    });
    let PostTransformer_PostTransformer = class PostTransformer {
        constructor(bot, postRepository, settings, linksSettings) {
            this.bot = bot, this.postRepository = postRepository, this.settings = settings, 
            this.linksSettings = linksSettings, this.mdbx = new external_node_mdbx_default.a({
                path: Object(data_path.a)("caches/resolves"),
                maxDbs: 2,
                syncMode: "safeNoSync",
                pageSize: 16384,
                valueMode: "string"
            });
        }
        async processLink(pathname) {
            var _a, _b;
            let data;
            try {
                const html = await utils.m.get(`https://login.aliexpress.ru/sync_cookie_write.htm?acs_random_token=${r32hexs()}&xman_goto=` + encodeURIComponent("https://m.aliexpress.ru" + pathname), {
                    resolveBodyOnly: !0,
                    responseType: "text",
                    cookieJar: new PostTransformer_PromiseCookieJar,
                    timeout: 1e3 * this.settings.maxParseAliPreviewTime,
                    headers: {
                        "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
                        Accept: "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                        "Upgrade-Insecure-Requests": "1",
                        "User-Agent": "Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36"
                    }
                }), script = Object(external_cheerio_.load)(html, {
                    decodeEntities: !1
                })('script[type="application/json"]').first().html();
                data = JSON.parse(script);
                const prev = null === (_b = null === (_a = function wScan(widgets) {
                    let w = widgets.find(w => w.widgetId.startsWith("bx/SnowProductContextWidget/"));
                    if (!w) for (const child of widgets) if (w = child.children && wScan(child.children)) break;
                    return w;
                }(data.widgets)) || void 0 === _a ? void 0 : _a.props.gallery) || void 0 === _b ? void 0 : _b.find(v => v.imageUrl);
                return null == prev ? void 0 : prev.imageUrl;
            } catch (e) {
                console.error.bind(console, "(./src/PostTransformer.ts:100)")("https://m.aliexpress.ru" + pathname, Object(string_tools_.stringify)(e)), 
                data && (utils.n[pathname] = data);
            }
            return null;
        }
        cleanUrl(link, ru) {
            const url = new URL(link), {hostname, pathname, searchParams} = url;
            if (pathname.startsWith("/redirect/cpa/o/") && searchParams.has("to") || "aliclick.shop" === hostname && searchParams.has("to") || "vk.com" === hostname && "/away.php" === pathname) return this.cleanUrl(searchParams.get("to"));
            if (/^(?:alitems\.[a-z]{2,}|ad\.admitad\.com)$/.test(hostname) && searchParams.has("ulp")) return this.cleanUrl(searchParams.get("ulp"));
            if (!ALI_HOST_RE.test(hostname)) return url;
            if (ALI_LOGIN_HOST_RE.test(hostname)) {
                if (searchParams.has("return_url")) return this.cleanUrl(searchParams.get("return_url"));
            } else if (ALI_SCLICK_HOST_RE.test(hostname)) {
                if ("/deep_link.htm" === pathname) return this.cleanUrl(searchParams.get("dl_target_url"));
                const afref = searchParams.get("afref");
                if (afref) return this.cleanUrl(afref);
            } else /^\/(?:item|store)\/(?!v3\/)/.test(pathname) ? (ru && (url.hostname = url.hostname.replace(/^www\./, "").replace(/\.[a-z]{2,}$/, ".ru")), 
            url.search = "") : url.search = external_qs_default.a.stringify(external_qs_default.a.parse(url.search.slice(1)), {
                filter: this.linksSettings.allowedQueryStrings
            });
            return url;
        }
        async compileText(ftext, post) {
            let s = Object(utils.g)(ftext.text), had_tags = !1, has_ali = !1, fail = !1, has_links = !1;
            const leafs = ftext.entities.filter(e => SUPPORTED_ENTITIES.has(e.type._)).map(e => ("textEntityTypeHashtag" === e.type._ ? had_tags = !0 : "textEntityTypeUrl" !== e.type._ && "textEntityTypeTextUrl" !== e.type._ || (has_links = !0), 
            new ETree_ETree(s, e.offset, e.length, e.type)));
            if (!has_links) return {
                has_ali,
                fail,
                had_tags
            };
            leafs.push(new ETree_ETree(s, 0, s.length)), leafs.sort((x, y) => {
                var _a;
                let diff = x.length - y.length;
                return !diff && x.start === y.start && (null === (_a = x.type) || void 0 === _a ? void 0 : _a._.endsWith("Url")) && (diff = 1), 
                diff;
            });
            for (const leaf of leafs) for (const l of leafs) if (l.includes(leaf)) {
                leaf.parent = l, l.children.push(leaf);
                break;
            }
            s = Object(utils.d)(Array.from(Object(utils.o)(leafs).content(0)).join(""));
            const blackRe = getHostsCheckerBlack(...this.linksSettings.hostsBlackList), whiteRe = getHostsCheckerWhite(...this.linksSettings.hostsWhiteList), bodyLinksRe = getHostsCheckerBody(...this.linksSettings.hostsFindLinkInBody), previews = new Set(post.ali_photos.map(p => p.url));
            let tpl = await this.mdbx.asyncTransact(txn => new Function("html", "resolve", "return html`" + s + "`;")(utils.l, async (_source, title) => {
                var _a;
                let source = _source.trim();
                const mayBeNotLink = !HTTPS_RE.test(source);
                mayBeNotLink && (source = "http://" + source), title && title.trim().toLowerCase().replace(HTTPS_RE, "") === source.toLowerCase().replace(HTTPS_RE, "") && (title = null);
                let redirects = [ source ];
                try {
                    let {hostname, href} = this.cleanUrl(source);
                    if (whiteRe(hostname)) return title ? "${a(`" + href + "`, `" + title + "`)}" : "${a(`" + href + "`)}";
                    if (blackRe(hostname)) return;
                    href != source && redirects.push(href + "#sanitized");
                    const links = txn.getDbi("links"), cachedUrl = links.get(href);
                    let url = cachedUrl && new URL(cachedUrl);
                    if (!url) {
                        if (bodyLinksRe(hostname)) {
                            if (href = Object(external_cheerio_.load)(await utils.m.get(href, {
                                timeout: 1e3 * this.linksSettings.maxLinkRestoreTime,
                                resolveBodyOnly: !0
                            }), {
                                decodeEntities: !1
                            })("a").attr("href"), !href) return;
                            redirects.push(href + "#extracted");
                            const u = this.cleanUrl(href);
                            if (blackRe(hostname = u.hostname)) return;
                            href != u.href && (href = u.href, redirects.push(href + "#sanitized"));
                        }
                        const request = utils.m.stream(href, {
                            method: ALI_HOST_RE.test(hostname) ? "HEAD" : "GET",
                            cookieJar: new PostTransformer_PromiseCookieJar,
                            throwHttpErrors: !1,
                            ignoreInvalidCookies: !0,
                            timeout: 1e3 * this.linksSettings.maxLinkRestoreTime,
                            hooks: {
                                beforeRedirect: [ options => {
                                    const prev = options.url.href;
                                    redirects.push(prev), options.url = this.cleanUrl(prev), prev != options.url.href && redirects.push(options.url.href + "#sanitized"), 
                                    ALI_HOST_RE.test(options.url.hostname) ? options.method = "HEAD" : blackRe(options.url.hostname) && request.destroy();
                                } ]
                            }
                        }), response = await Object(utils.v)(request);
                        if (!response) return;
                        const code = response.statusCode >= 500 && response.url.startsWith("https://aliexpress.ru/item/") ? 200 : response.statusCode;
                        if (uncachedCode(code)) throw new source_.HTTPError(response);
                        url = this.cleanUrl(response.url, !0), url.hash = "code=" + code, url.href != cachedUrl && links.put(href, url.href);
                    }
                    const statusCode = 0 | +new URLSearchParams(url.hash.slice(1)).get("code");
                    if (statusCode >= 400) throw new source_.HTTPError({
                        statusCode,
                        statusMessage: external_http_.STATUS_CODES[statusCode]
                    });
                    if (ALI_HOST_RE.test(url.hostname)) {
                        url.hash = "", has_ali = !0;
                        const {pathname} = url;
                        if (pathname.startsWith("/item/")) {
                            const pcache = txn.getDbi("previews");
                            let preview = pcache.get(pathname);
                            !preview && previews.size < this.settings.maxPhotosCount + (previews.has("") ? 1 : 0) && (preview = await this.processLink(pathname)) && pcache.put(pathname, preview), 
                            previews.add(preview || "");
                        }
                        return title ? "${a(`" + url.href + "`, `" + title + "`)}" : "${a(`" + url.href + "`)}";
                    }
                } catch (err) {
                    return mayBeNotLink && err instanceof source_.RequestError ? Object(utils.f)(_source) : (fail || (fail = uncachedCode(0 | (null === (_a = null == err ? void 0 : err.response) || void 0 === _a ? void 0 : _a.statusCode))), 
                    err = Object(string_tools_.stringify)(err), fail && console.error.bind(console, "(./src/PostTransformer.ts:299)")(post.url, redirects, err), 
                    `<pre><code class="language-fail">{${Object(utils.f)(err)}}</code></pre>`);
                }
                return `<pre><code class="language-fail">{${new URL(Object(utils.o)(redirects)).hostname}}</code></pre>`;
            }));
            return previews.has("") && (previews.delete(""), previews.size < this.settings.maxPhotosCount && (fail = !0)), 
            {
                has_ali,
                had_tags,
                tpl: tpl.trim(),
                previews,
                fail
            };
        }
        async process(post, ftext, picture, picSource, transform) {
            const ac = new AbortController, source_photos_promise = Object(utils.b)(picSource, transform, {
                required: this.settings.maxPhotosCount,
                concurrency: this.settings.maxPhotosConcurrency,
                breakOnError: !0
            }, ac.signal);
            try {
                const {tpl, previews, had_tags, fail, has_ali} = await this.compileText(ftext, post);
                if (post.tg ? has_ali : has_ali || fail) {
                    if (tpl && tpl != post.text || previews.size > post.ali_photos.length || !fail && post.fails || !Object(utils.e)(post.source_photos, await source_photos_promise, post.tg ? "local" : "url")) return tpl && (post.text = tpl), 
                    post.had_tags || (post.had_tags = had_tags), post.actual = !has_ali, post.fails = fail ? 1 + (0 | post.fails) : 0, 
                    post.ali_photos = Array.from(previews, url => ({
                        ...post.ali_photos.find(s => s.url === url),
                        url
                    })), post.source_photos = await source_photos_promise, post.type && "None" !== post.type ? picture = post.type : post.video._ || 0 === this.bot.calcMask(post) ? picture = "None" : "Source" === picture && post.source_photos.length < 2 && (picture = "Ali"), 
                    await post.makeCollage(picture, this.settings), !0;
                    post.db && (fail || post.fails) && (post.fails = fail ? 1 + (0 | post.fails) : 0, 
                    has_ali || (post.actual = !0), await this.postRepository.update({
                        id: post.id
                    }, {
                        fails: post.fails,
                        actual: post.actual
                    }));
                }
            } finally {
                ac.abort(), await source_photos_promise;
            }
            return !1;
        }
    };
    function uncachedCode(code) {
        return 0 === code || code >= 400 && 401 !== code && 403 !== code && 405 !== code;
    }
    PostTransformer_PostTransformer = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ BotClient_BotClient, Post.PostRepository, ClientSettings.ClientSettings, LinksSettings.LinksSettings ]) ], PostTransformer_PostTransformer);
    var AriaClient_1, external_playwright_firefox_ = __webpack_require__(62), external_playwright_firefox_default = __webpack_require__.n(external_playwright_firefox_), external_vk_io_ = __webpack_require__(94), external_xxhash_wasm_ = __webpack_require__(95), external_xxhash_wasm_default = __webpack_require__.n(external_xxhash_wasm_), repeater_ = __webpack_require__(96);
    let xhash, AriaClient_AriaClient = AriaClient_1 = class AriaClient {
        constructor() {
            this.thread();
        }
        async download(url) {
            try {
                xhash || (xhash = await external_xxhash_wasm_default()());
                const {pathname, searchParams} = new URL(url), out = (Object(external_path_.basename)(pathname, ".mp4") || searchParams.get("id")) + ".mp4", gid = xhash.h64ToString(url), dir = __dirname + "/static/files/";
                return await AriaClient_1.pendings.make(gid, void 0, () => utils.m.post("http://localhost:6800/jsonrpc", {
                    dnsCache: !1,
                    json: {
                        jsonrpc: "2.0",
                        id: gid,
                        method: "aria2.addUri",
                        params: [ [ url ], {
                            gid,
                            out,
                            dir,
                            "user-agent": AriaClient_1.userAgent,
                            "load-cookies": AriaClient_1.cookiePath,
                            "save-cookies": AriaClient_1.cookiePath,
                            "force-save": "true",
                            "remove-control-file": "false",
                            "allow-overwrite": "true",
                            continue: "true",
                            header: [ "Origin: https://vk.com", "Referer: https://vk.com/", "User-Agent: " + AriaClient_1.userAgent ]
                        } ]
                    }
                })) ? dir + out : null;
            } catch (e) {
                return console.error.bind(console, "(./src/AriaClient.ts:56)")(url, e), null;
            }
        }
        checkOld(socket) {
            AriaClient_1.pendings.empty || socket.readyState !== external_ws_default.a.OPEN || socket.send(JSON.stringify({
                jsonrpc: "2.0",
                id: Date.now(),
                method: "system.multicall",
                params: [ AriaClient_1.pendings.keys().map(gid => ({
                    methodName: "aria2.tellStatus",
                    params: [ gid, [ "gid", "status" ] ]
                })) ]
            }), err => err && console.error.bind(console, "(./src/AriaClient.ts:70)")(err));
        }
        async thread() {
            for (;;) try {
                for await (const {method, params, result} of new repeater_.Repeater(async (push, stop) => {
                    const socket = new external_ws_default.a("ws://localhost:6800/jsonrpc");
                    socket.on("message", e => push(JSON.parse(e))), socket.once("error", stop), socket.once("stop", stop), 
                    socket.once("open", () => this.checkOld(socket)), await stop, socket.close();
                })) if (result) for (const {status, gid} of result) "error" === status || "removed" === status ? AriaClient_1.pendings.fulfill(gid, !1) : "complete" === status && AriaClient_1.pendings.fulfill(gid, !0); else switch (method) {
                  case "aria2.onDownloadComplete":
                    params.forEach(({gid}) => AriaClient_1.pendings.fulfill(gid, !0));
                    break;

                  case "aria2.onDownloadStop":
                  case "aria2.onDownloadError":
                    params.forEach(({gid}) => AriaClient_1.pendings.fulfill(gid, !1));
                }
            } catch (err) {
                console.error(err), console.error.bind(console, "(./src/AriaClient.ts:113)")(err);
            }
        }
    };
    AriaClient_AriaClient.userAgent = "Dalvik/1.4.0 (Linux; U; Android 2.3.5; HTC Desire HD A9191 Build/GRJ90)", 
    AriaClient_AriaClient.cookiePath = Object(data_path.a)("cookies.txt"), AriaClient_AriaClient.pendings = new DeferredMap, 
    AriaClient_AriaClient = AriaClient_1 = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.b)("design:paramtypes", []) ], AriaClient_AriaClient);
    async function loadCookies(path) {
        return function(filename) {
            try {
                return Object(external_fs_.statSync)(filename).size > 0;
            } catch {}
            return !1;
        }(path) ? function fromNetscapeCookies(cookies) {
            return cookies.split("\n").filter(s => s && !s.startsWith("#")).map(s => {
                const [domain, , path, secure, expires, name, value] = s.split("\t");
                return {
                    name,
                    value,
                    domain,
                    path,
                    expires: 0 | +expires,
                    httpOnly: !1,
                    secure: "TRUE" === secure,
                    sameSite: "Lax"
                };
            });
        }(await external_fs_.promises.readFile(path, "utf-8")) : [];
    }
    var photos_utils = __webpack_require__(45);
    const officialAppCredentials_android = {
        clientId: "2274003",
        clientSecret: "hHbZxrka2uZ6jB1inYsH"
    };
    function merge(post, v) {
        const url = (null == v ? void 0 : v.url) || (null == v ? void 0 : v.src);
        return url && {
            width: v.width,
            height: v.height,
            ...post.source_photos.find(s => s.url === url),
            url
        };
    }
    function areaCompare(a, b) {
        return a._area || (a._area = a.width * a.height), b._area || (b._area = b.width * b.height), 
        a._area - b._area;
    }
    function isValidVideoPreview(p) {
        return p && p.size && /\.jpe?g$/i.test(p.local) && p.width <= 320 && p.height <= 320 && p.size < 204800;
    }
    class VKClient_VKAuthError extends ExtendableError {}
    const PREVIEW_SIZES = [ "y", "x", "m", "s" ], NAV_OPTIONS = {
        timeout: 1e4,
        waitUntil: "domcontentloaded"
    }, VK_LINK_RE = /\b(?:https?:\/{2})?(?:[a-z\d][a-z\d-]*\.)+[a-z]{2,}(?::\d{2,5})?(?:\/[-\w.~:/?[\]@!$&'()*+,;=%]*\w\/?)?/g, VK_NAMED_LINK_RE = new RegExp("\\[" + VK_LINK_RE.source + "\\|[^\\]]+\\]", VK_LINK_RE.flags);
    async function processPage(page, phone, password, twoFaHandler) {
        const $phone = await page.$('input[name="email"]');
        $phone && await $phone.fill(phone);
        const $password = await page.$('input[name="pass"]');
        $password && await $password.fill(password);
        const $code = await page.$('input[name="code"]');
        if ($code) {
            const code = await twoFaHandler("```\n" + await page.innerText(".fi_row") + "\n```");
            await $code.fill(code);
        } else if (!$password || !$phone) throw new VKClient_VKAuthError(await page.innerText("body"));
        await Promise.all([ page.waitForNavigation(NAV_OPTIONS), page.click('input[type="submit"]') ]);
    }
    let VKClient_VKClient = class VKClient extends external_emittery_default.a {
        constructor(ptransformer, aria, bot, coreSettings, settings, coreSettingsRepository) {
            super(), this.ptransformer = ptransformer, this.aria = aria, this.bot = bot, this.coreSettings = coreSettings, 
            this.settings = settings, this.coreSettingsRepository = coreSettingsRepository, 
            this.queue = new AsyncUniqQueue, this.threadPromise = null;
        }
        dispose() {
            var _a;
            this.bot.clearListeners("refresh_vk"), null === (_a = this.client) || void 0 === _a || _a.updates.stop(), 
            this.client = null;
        }
        remove(id) {
            this.queue.delete(id);
        }
        add(id) {
            id && this.queue.add(id, !0);
        }
        async getGroupInfo(screen_name) {
            var _a;
            screen_name = screen_name.replace(/^(https?:\/\/)?(m\.)?vk\.com\//i, "");
            const {object_id} = await this.client.api.utils.resolveScreenName({
                screen_name
            });
            return null === (_a = await this.client.api.groups.getById({
                group_id: object_id.toString()
            })) || void 0 === _a ? void 0 : _a[0];
        }
        fillQueue() {
            if (this.client) for (const v of this.bot.sources.values()) v.tg || this.queue.add(v.id);
        }
        async processMessageById({id, fails}) {
            try {
                const [message] = await this.client.api.wall.getById({
                    posts: id
                });
                message ? await this.processMessage(message, !0) : await this.bot.incPostFails(id, fails);
            } catch (err) {
                console.error.bind(console, "(./src/VKClient.ts:240)")("https://vk.com/wall" + id, err);
            }
        }
        async processMessage(message, force, picture) {
            var _a;
            if ((message.edited || message.date) + this.settings.removeAfterSeconds < Date.now() / 1e3) return;
            const id = message.owner_id.toString() + "_" + message.id.toString();
            try {
                let post = await this.bot.findPost(id);
                if (!post || force || message.edited > post.edited || post.fails && post.fails < this.settings.maxParseTries) {
                    post || (post = new Post.Post), post.id = id, post.date = message.date, post.owner_id = -message.owner_id, 
                    post.from_id = -message.from_id, VK_NAMED_LINK_RE.lastIndex = 0, message.text = message.text.replace(VK_NAMED_LINK_RE, "").replace(/#[\p{L}\p{N}_]+/giu, () => (post.had_tags = !0, 
                    ""));
                    let protectedVideos = new Map, videos = [];
                    if (await this.ptransformer.process(post, function toFormattedText(text) {
                        return VK_LINK_RE.lastIndex = 0, {
                            _: "formattedText",
                            text,
                            entities: Array.from(text.matchAll(VK_LINK_RE), ({0: s, index}) => ({
                                _: "textEntity",
                                offset: index,
                                length: s.length,
                                type: {
                                    _: "textEntityTypeUrl"
                                }
                            }))
                        };
                    }(message.text), picture || post.type, message.attachments || [], att => {
                        var _a;
                        switch (null == att ? void 0 : att.type) {
                          case "photo":
                            return merge(post, Object(utils.i)(PREVIEW_SIZES, att.photo.sizes));

                          case "doc":
                            {
                                const {type, preview, url} = att.doc, photo = preview.photo;
                                switch (type) {
                                  case 4:
                                    return merge(post, Object(utils.i)(PREVIEW_SIZES, photo.sizes) || {
                                        url
                                    });

                                  case 6:
                                    return videos.unshift({
                                        url
                                    }), null;

                                  case 3:
                                    return videos.unshift({
                                        url: (null === (_a = preview.video) || void 0 === _a ? void 0 : _a.src) || url,
                                        poster: merge(post, Object(utils.i)([ "q", "p", "m", "s" ], photo.sizes))
                                    }), merge(post, Object(utils.i)(PREVIEW_SIZES, photo.sizes));
                                }
                            }

                          case "video":
                            {
                                const images = att.video.image.filter(i => !i.with_padding).sort(areaCompare);
                                return protectedVideos.set(att.video.owner_id + "_" + att.video.id, merge(post, images.reverse().find(i => i.width <= 320 && i.height <= 320))), 
                                merge(post, images.find(i => i.width >= 800 || i.height >= 800) || Object(utils.o)(images));
                            }
                        }
                    })) {
                        if (post.edited = message.edited || message.date, videos.length || protectedVideos.size) {
                            const vs = await this.client.api.video.get({
                                videos: Array.from(protectedVideos.keys()),
                                count: protectedVideos.size
                            });
                            for (const item of vs.items) {
                                if (item.live) continue;
                                const {files, converting} = item;
                                let url = null == files ? void 0 : files.mp4_720;
                                if (converting && !url) return;
                                (url || (url = files && (files.mp4_480 || files.mp4_360 || files.mp4_240))) && videos.push({
                                    url,
                                    poster: protectedVideos.get(item.owner_id + "_" + item.id)
                                });
                            }
                            videos.some(v => v.url === post.video.src) && (videos.length = 0);
                            for (let v of Object(utils.q)(videos, "url")) {
                                const path = await this.aria.download(v.url);
                                if (path) {
                                    const info = await Object(utils.h)(path), videoParams = null === (_a = null == info ? void 0 : info.streams) || void 0 === _a ? void 0 : _a.find(s => "video" === s.codec_type);
                                    if (!videoParams) continue;
                                    post.video.src = v.url, post.actual = !0;
                                    const {width, height} = videoParams, hasAudio = info.streams.some(s => "audio" === s.codec_type), thumbnail = await this.loadPoster(post, v.poster);
                                    return await this.bot.savePost(post), void await this.emit("media", {
                                        _: hasAudio ? "inputMessageVideo" : "inputMessageAnimation",
                                        supports_streaming: !!hasAudio || void 0,
                                        video: hasAudio ? {
                                            _: "inputFileLocal",
                                            path
                                        } : void 0,
                                        animation: hasAudio ? void 0 : {
                                            _: "inputFileLocal",
                                            path
                                        },
                                        thumbnail,
                                        width,
                                        height,
                                        caption: post.idCaption
                                    });
                                }
                            }
                        }
                        return await this.bot.savePost(post, !0);
                    }
                }
                force && (null == post ? void 0 : post.db) && await this.bot.updateMarkup(post);
            } catch (err) {
                console.error.bind(console, "(./src/VKClient.ts:372)")("https://vk.com/wall" + id, err);
            }
        }
        async thread() {
            this.fillQueue();
            const needExtendedCheck = new Set(Array.from(this.bot.sources.values(), v => v.tg ? 0 : v.id));
            needExtendedCheck.delete(0);
            for await (let id of this.queue) {
                const source = this.bot.sources.get(id);
                try {
                    if (!source) continue;
                    const {items} = await this.client.api.wall.get({
                        owner_id: -id,
                        offset: 0,
                        count: needExtendedCheck.has(id) ? this.settings.vkPostCountAfterRestart : this.settings.vkPostCount
                    });
                    needExtendedCheck.delete(id);
                    for (let message of items) message.marked_as_ads || await this.processMessage(message, !1, source.picture);
                } catch (err) {
                    console.error.bind(console, "(./src/VKClient.ts:400)")(source.url, err);
                } finally {
                    await utils_delay(1e3 * this.settings.vkRequestTimeout), this.queue.size < 1 && this.fillQueue();
                }
            }
        }
        async loadPoster(post, poster) {
            return poster && (poster = (await Object(photos_utils.b)([ poster ], this.settings)).find(isValidVideoPreview)), 
            poster || (poster = (await Object(photos_utils.b)(post.source_photos, this.settings)).find(isValidVideoPreview)), 
            poster ? {
                _: "inputThumbnail",
                thumbnail: {
                    _: "inputFileLocal",
                    path: poster.local
                },
                width: poster.width,
                height: poster.height
            } : void 0;
        }
        async getToken(phone, password, ctx) {
            if (this.coreSettings.vk.token && phone === this.coreSettings.vk.phone && password === this.coreSettings.vk.password) return this.coreSettings.vk.token;
            const twoFaHandler = ctx ? async payload => {
                const {value} = await fromCtx(ctx).remote("ElMessageBox").prompt(payload, {
                    showCancelButton: !1,
                    roundButton: !0
                }, "Vk Auth");
                return value;
            } : payload => this.bot.promt("*Vk Auth*\n" + payload), browser = await external_playwright_firefox_default.a.firefox.launch(), context = await browser.newContext({
                ...external_playwright_firefox_default.a.devices["Pixel 2"],
                userAgent: AriaClient_AriaClient.userAgent,
                isMobile: !1,
                javaScriptEnabled: !1,
                locale: "ru-RU",
                ignoreHTTPSErrors: !0,
                permissions: []
            });
            try {
                await context.addCookies(await loadCookies(AriaClient_AriaClient.cookiePath)), await context.route("**", (route, request) => {
                    const t = request.resourceType();
                    "document" !== t && "script" !== t ? route.fulfill({
                        status: 201
                    }) : route.continue();
                });
                const page = await context.newPage();
                for (await page.goto("https://m.vk.com/docs", NAV_OPTIONS); page.url().startsWith("https://m.vk.com/login"); ) await processPage(page, phone, password, twoFaHandler);
                const token = await async function getAccess(phone, password, page, twoFaHandler) {
                    let lastError = "";
                    for (const params = {
                        grant_type: "password",
                        client_id: officialAppCredentials_android.clientId,
                        client_secret: officialAppCredentials_android.clientSecret,
                        username: phone,
                        password,
                        scope: 466964..toString(),
                        v: "5.126",
                        "2fa_supported": "1"
                    }; ;) {
                        const resp = await page.goto("https://oauth.vk.com/token?" + new URLSearchParams(params).toString());
                        delete params.code;
                        let {access_token, error, redirect_uri, validation_type, phone_mask, error_description} = await resp.json();
                        if ("need_validation" === error && validation_type) {
                            const code = await twoFaHandler((lastError && "```\n" + lastError + "\n```\n") + "Пожалуйста, введите код двуфакторной авторизации из " + ("2fa_app" === validation_type ? "личного сообщения от Администрации или из приложения для генерации кодов или воспользуйтесь резервными кодами.\nДля принудительной отправки кода по sms вместо приложения введите /sms (не рекомендуется, так как по sms коды часто не доходят)" : `sms (отправлено на номер \`${phone_mask}\`) или воспользуйтесь резервными кодами. Рекомендуется включить в [настройках безопасности](https://vk.com/settings?act=security) вход через приложение для генерации кодов, так как по sms коды часто не доходят.`));
                            "/sms" === code ? params.force_sms = "1" : params.code = code;
                        } else if ("need_validation" === error) {
                            for (await page.goto(redirect_uri, NAV_OPTIONS); !page.url().includes("success=1"); ) if (await processPage(page, phone, password, twoFaHandler), 
                            page.url().includes("fail=1")) throw new VKClient_VKAuthError(await page.innerText("body"));
                            access_token = new URLSearchParams(new external_url_.URL(page.url()).hash.slice(1)).get("access_token");
                        } else error && (lastError = `[${error}]: ` + error_description);
                        if (access_token) return access_token;
                    }
                }(phone, password, page, twoFaHandler);
                return this.coreSettings.vk = {
                    phone,
                    password,
                    token
                }, await this.coreSettingsRepository.save(this.coreSettings), token;
            } finally {
                await function saveCookies(cookies, path) {
                    return external_fs_.promises.writeFile(path, function toNetscapeCookies(cookies) {
                        return cookies.map(c => [ c.domain, c.domain.startsWith(".") ? "TRUE" : "FALSE", c.path, c.secure ? "TRUE" : "FALSE", c.expires, c.name, c.value ].join("\t")).join("\n");
                    }(cookies));
                }(await context.cookies(), AriaClient_AriaClient.cookiePath), browser.close().catch(console.error.bind(console, "(./src/VKClient.ts:475)"));
            }
        }
        async update(settings, ctx) {
            if (!this.client || !this.coreSettings.vk.token || settings.vkPhone !== this.coreSettings.vk.phone || settings.vkPassword !== this.coreSettings.vk.password) {
                const token = await this.getToken(settings.vkPhone, settings.vkPassword, ctx);
                this.dispose(), this.client = new external_vk_io_.VK({
                    token,
                    apiHeaders: {
                        "User-Agent": AriaClient_AriaClient.userAgent
                    }
                }), this.threadPromise || (this.threadPromise = this.thread().catch(console.error.bind(console, "(./src/VKClient.ts:498)"))), 
                this.bot.on("refresh_vk", id => this.processMessageById(id));
            }
        }
    };
    VKClient_VKClient = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ PostTransformer_PostTransformer, AriaClient_AriaClient, BotClient_BotClient, Settings.Settings, ClientSettings.ClientSettings, Settings.SettingsRepository ]) ], VKClient_VKClient);
    let AdminClient_AdminClient = class AdminClient extends BaseTelegramClient_BaseTelegramClient {
        constructor(ptransformer, bot, vk, settings, coreSettings, coreSettingsRepository, postRepository, sourceRepository, channelRepository) {
            super(), this.ptransformer = ptransformer, this.bot = bot, this.vk = vk, this.settings = settings, 
            this.coreSettings = coreSettings, this.coreSettingsRepository = coreSettingsRepository, 
            this.postRepository = postRepository, this.sourceRepository = sourceRepository, 
            this.channelRepository = channelRepository, this.queue = new MultiMap_MultiMap(async (key, message_ids) => {
                try {
                    const chat_id = +key.split("_", 1)[0];
                    let {messages} = await this.client.invoke({
                        _: "getChatHistory",
                        chat_id,
                        from_message_id: Math.max(...message_ids),
                        offset: -1,
                        limit: message_ids.length
                    });
                    messages && (messages = messages.filter(m => message_ids.includes(m.id)), messages.length && this.processContent(messages));
                } catch (err) {
                    console.error.bind(console, "(./src/AdminClient.ts:49)")(key, err);
                }
            }), this.initialized = makeDeffer(), this.requestedChats = new Set, this.afterUpdate = async () => {
                const description = "Restart at " + external_dayjs_default()().add(this.settings.timezoneOffset, "hour").format("DD.MM H:mm:ss") + `\n[${utils.n.terminated_by || ""}]`;
                await this.client.invoke({
                    _: "searchPublicChat",
                    username: (await this.bot.getMe()).username
                }), await this.client.invoke({
                    _: "createPrivateChat",
                    user_id: this.bot.id
                }), this.requestedChats.clear();
                const lastPosts = new Map;
                for (const s of this.bot.sources.values()) if (s.tg) {
                    this.requestedChats.add(s.id);
                    const m = await this.postRepository.findOne({
                        owner_id: s.id
                    }, {
                        select: [ "id" ],
                        order: {
                            date: "DESC"
                        }
                    });
                    m && lastPosts.set(s.id, m.message_id);
                }
                await this.syncChats(0);
                try {
                    const {supergroup_id} = this.coreSettings;
                    if (!supergroup_id) throw "system_supergroup_id required";
                    await this.client.invoke({
                        _: "createSupergroupChat",
                        supergroup_id
                    }), await this.client.invoke({
                        _: "setChatDescription",
                        chat_id: this.coreSettings.supergroup_chat,
                        description
                    });
                } catch (err) {
                    console.warn.bind(console, "(./src/AdminClient.ts:198)")(err);
                    const c = await this.searchGroup("Ali forward bot channel") || await this.createGroup("Ali forward bot channel", description);
                    this.coreSettings.supergroup_id = Object(utils.u)(c.id), await this.addAdminToSupergroup(this.bot.id, c.id);
                }
                this.coreSettings.admin_id = this.id, await this.coreSettingsRepository.save(this.coreSettings), 
                await this.bot.setCommands(), this.bot.on("refresh_tg", post => this.processMessageById({
                    fails: post.fails,
                    chat_id: post.owner_id,
                    message_id: post.message_id
                })), this.vk.on("media", i => this.sendMessage(this.bot.id, i));
                for (const [chat_id, message_id] of lastPosts) await this.processMessageById({
                    chat_id,
                    message_id
                }, !0);
                this.initialized.fulfill(), this.bot.chain.add(!0);
            }, this.sourceMutex = new external_async_mutex_.Mutex, this.onUpdate = async update => {
                var _a, _b;
                if ("updateNewChat" === update._) this.requestedChats.delete(update.chat.id); else if ("updateMessageEdited" === update._) await this.initialized.promise, 
                await this.processMessageById(update); else if ("updateNewMessage" === update._) {
                    await this.initialized.promise;
                    const {message} = update;
                    if (this.bot.sources.has(message.chat_id)) await this.processMessage(message); else if (message.is_outgoing) {
                        let markForRemove = !1;
                        if (message.chat_id === this.bot.id || message.chat_id === this.coreSettings.supergroup_chat) {
                            if ("messageForwardOriginChannel" === (null === (_b = null === (_a = message.forward_info) || void 0 === _a ? void 0 : _a.origin) || void 0 === _b ? void 0 : _b._)) await this.addSource(Object(utils.u)(message.forward_info.origin.chat_id)), 
                            markForRemove = !0; else if ("messageText" === message.content._) {
                                const ft = message.content.text;
                                for (const link of ft.entities.filter(e => "textEntityTypeUrl" === e.type._).map(e => ft.text.substr(e.offset, e.length))) {
                                    const username = external_last_match_default()(link, /^https:\/\/t\.me\/(c\/\d+|\w+)(?:$|\/\d+)/);
                                    if (username) {
                                        let supergroup_id = username.startsWith("c/") ? parseInt(username.substr(2)) : Object(utils.u)((await this.client.invoke({
                                            _: "searchPublicChat",
                                            username
                                        })).id);
                                        await this.addSource(supergroup_id), markForRemove = !0;
                                    } else {
                                        const group = external_last_match_default()(link, /^https:\/\/vk\.com\/(\w+)(?:\/|$)/);
                                        if (group) {
                                            const info = await this.vk.getGroupInfo(group);
                                            if (info) {
                                                const s = this.bot.sources.get(info.id) || new Source.Source;
                                                s.id = info.id, s.name = info.name || "", s.domain = info.screen_name || "", await this.sourceRepository.save(s), 
                                                markForRemove = !0;
                                            }
                                        }
                                    }
                                }
                            }
                        } else if (message.is_channel_post && "messageText" === message.content._ && /^\s*register[_\s-]channel\s*$/.test(message.content.text.text)) {
                            const chat = await this.getChat(message.chat_id);
                            if ("chatTypeSupergroup" === chat.type._) {
                                const {supergroup_id} = chat.type, c = this.bot.channels.find(c => c.supergroup_id === supergroup_id) || await this.channelRepository.findOne(supergroup_id) || new Channel.Channel;
                                if (c.supergroup_id = supergroup_id, c.title = chat.title, !c.mask) {
                                    const bitPos = this.bot.channels.reduce((m, c) => m ^ c.mask, 2147483647).toString(2).lastIndexOf("1");
                                    bitPos < 0 ? await this.bot.sendMessage(this.coreSettings.supergroup_chat, `Не удалось добавить канал [${Object(tg_utils.c)(c.title)}](${c.url})\nВозможно добавить не более 31 канала.`) : c.mask = 2 ** (30 - bitPos), 
                                    c.order = 1 + Math.max(...this.bot.channels.map(c => c.order));
                                }
                                c.mask && (await Promise.all([ this.channelRepository.save(c), this.addAdminToSupergroup(this.bot.id, chat.id, !0) ]), 
                                await Promise.all([ this.bot.getSupergroupDomain(supergroup_id), this.bot.sendMessage(this.coreSettings.supergroup_chat, `Канал обновлен [${Object(tg_utils.c)(c.title)}](${c.url})`) ]).catch(console.error.bind(console, "(./src/AdminClient.ts:330)"))), 
                                markForRemove = !0;
                            }
                        }
                        markForRemove && this.deleteMessage(message.id, message.chat_id).catch(console.error.bind(console, "(./src/AdminClient.ts:336)"));
                    }
                }
            }, this.downloadPhoto = async (m, signal) => {
                var _a, _b, _c, _d, _e, _f;
                try {
                    let size = Object(utils.i)([ "x", "m", "s" ], "photo" === (null === (_a = m.content.web_page) || void 0 === _a ? void 0 : _a.type) && (null === (_c = null === (_b = m.content.web_page) || void 0 === _b ? void 0 : _b.photo) || void 0 === _c ? void 0 : _c.sizes) || (null === (_d = m.content.photo) || void 0 === _d ? void 0 : _d.sizes)) || (null === (_e = m.content.animation) || void 0 === _e ? void 0 : _e.thumbnail) || (null === (_f = m.content.video) || void 0 === _f ? void 0 : _f.thumbnail);
                    if (size) {
                        const f = await this.downloadFile(size.photo || size.file, signal, 1e3 * this.settings.maxPhotoDownloadTime);
                        return {
                            size: f.local.downloaded_size,
                            width: size.width,
                            height: size.height,
                            local: f.local.path
                        };
                    }
                } catch (e) {
                    "abort" !== e && console.error.bind(console, "(./src/AdminClient.ts:417)")(Object(utils.s)(m.chat_id, m.id), e);
                }
            };
        }
        dispose() {
            Promise.allSettled([ this.initialized.promise, this.initialized.reject(void 0) ]), 
            this.initialized = makeDeffer(), this.vk.clearListeners("media"), this.bot.clearListeners("refresh_tg"), 
            this.requestedChats.clear(), super.dispose();
        }
        authHandler(text, _) {
            return this.bot.promtNum(text);
        }
        passwordHandler(text, _) {
            return this.bot.promt(text);
        }
        async addAdminToSupergroup(user_id, chat_id, isChannel) {
            isChannel || await this.client.invoke({
                _: "addChatMember",
                user_id,
                chat_id
            });
            const t = await this.client.invoke({
                _: "searchChatMembers",
                chat_id,
                query: "",
                filter: {
                    _: "chatMembersFilterAdministrators"
                }
            });
            t.members.some(u => u.member_id.user_id === this.bot.id) || await this.client.invoke({
                _: "setChatMemberStatus",
                chat_id,
                member_id: {
                    _: "messageSenderUser",
                    user_id
                },
                status: {
                    _: "chatMemberStatusAdministrator",
                    rights: {
                        _: "chatAdministratorRights",
                        can_delete_messages: !0,
                        can_edit_messages: !0,
                        can_post_messages: !0
                    }
                }
            });
        }
        async searchGroup(query) {
            for (const command of [ "searchChats", "searchChatsOnServer" ]) {
                const {chat_ids} = await this.client.invoke({
                    _: command,
                    query,
                    limit: 1
                });
                for (const id of chat_ids) {
                    const c = await this.getChat(id);
                    if ((null == c ? void 0 : c.title) === query && "chatTypeSupergroup" === c.type._ && !c.type.is_channel) return c;
                }
            }
        }
        async createGroup(title, description) {
            const c = await this.client.invoke({
                _: "createNewSupergroupChat",
                title,
                is_channel: !1,
                description
            });
            return await this.client.invoke({
                _: "setChatPhoto",
                chat_id: c.id,
                photo: {
                    _: "inputChatPhotoStatic",
                    photo: {
                        _: "inputFileLocal",
                        path: Object(external_path_.resolve)(__dirname + "/../chat_photo.png")
                    }
                }
            }), c;
        }
        async syncChats(timeout) {
            for (const c of this.requestedChats) await this.getChat(c).catch(_ => !1) && this.requestedChats.delete(c);
            timeout *= 1e3;
            const startAt = Date.now();
            for (;this.requestedChats.size; ) try {
                if (await this.client.invoke({
                    _: "loadChats",
                    limit: 2147483647
                }), timeout > 0 && startAt + timeout > Date.now()) break;
            } catch (e) {
                if (404 === (null == e ? void 0 : e.code)) break;
                console.error.bind(console, "(./src/AdminClient.ts:160)")(e);
            }
        }
        async addSource(supergroup_id) {
            const unlock = await this.sourceMutex.acquire();
            try {
                if (this.bot.channels.some(c => c.supergroup_id === supergroup_id)) throw "Нельзя добавлять в качестве источника целевой канал";
                this.requestedChats.clear(), this.requestedChats.add(Object(utils.t)(supergroup_id)), 
                await this.syncChats(this.settings.maxTgSourceAddTime);
                const chat = await this.client.invoke({
                    _: "createSupergroupChat",
                    supergroup_id
                }), s = this.bot.sources.get(chat.id) || new Source.Source;
                s.id = chat.id, s.name = chat.title, await this.sourceRepository.save(s), this.bot.sendMessage(this.coreSettings.supergroup_chat, `Источник обновлен [${Object(tg_utils.c)(s.name)}](${s.url})`).catch(console.error.bind(console, "(./src/AdminClient.ts:240)"));
            } catch (err) {
                console.error.bind(console, "(./src/AdminClient.ts:242)")(err), this.bot.sendMessage(this.coreSettings.supergroup_chat, `Ошибка обновления источника [${supergroup_id}](${Object(utils.s)(Object(utils.t)(supergroup_id))})\n${Object(tg_utils.c)(Object(string_tools_.stringify)(err))}`).catch(console.error.bind(console, "(./src/AdminClient.ts:243)"));
            } finally {
                unlock();
            }
        }
        async processMessageById({chat_id, message_id, fails}, loadHistory) {
            try {
                if (this.bot.sources.has(chat_id)) {
                    const {messages} = await this.client.invoke({
                        _: "getChatHistory",
                        chat_id,
                        from_message_id: message_id,
                        offset: loadHistory ? -99 : -1,
                        limit: loadHistory ? 99 : 1
                    });
                    if (loadHistory) {
                        if (null == messages ? void 0 : messages.length) {
                            const errors = [], ids = new Set((await this.postRepository.createQueryBuilder().select("id").whereInIds(messages.map(m => -chat_id + "_" + m.id)).getRawMany()).map(v => +Object(utils.o)(v.id.split("_"))));
                            ids.add(message_id);
                            for (const m of messages.reverse()) ids.has(m.id) || await this.processMessage(m).catch(e => errors.push(e));
                            switch (errors.length) {
                              case 0:
                                break;

                              case 1:
                                throw errors[0];

                              default:
                                throw errors;
                            }
                        }
                    } else {
                        const m = null == messages ? void 0 : messages[0];
                        if (m && m.id != message_id) throw await this.bot.incPostFails(Post.Post.id({
                            chat_id,
                            id: message_id
                        }), fails), "Got invalid message from history " + Object(utils.s)(chat_id, m.id);
                        await this.processMessage(m);
                    }
                }
            } catch (err) {
                console.error.bind(console, "(./src/AdminClient.ts:388)")(Object(utils.s)(chat_id, message_id), err);
            }
        }
        async processContent(messages) {
            var _a, _b, _c, _d;
            const source = this.bot.sources.get(messages[0].chat_id);
            if (!source) return;
            const message = messages.find(m => {
                var _a;
                return null === (_a = m.content.caption) || void 0 === _a ? void 0 : _a.text;
            }) || messages[0], id = Post.Post.id(message);
            let post = await this.bot.findPost(id);
            const ftext = message.content.text || message.content.caption;
            if ((null == ftext ? void 0 : ftext.text) && (post || (post = new Post.Post), post.id = id, 
            post.date = message.date, post.from_id = (null === (_a = message.forward_info) || void 0 === _a ? void 0 : _a.from_chat_id) || (null === (_b = message.forward_info) || void 0 === _b ? void 0 : _b.origin.sender_user_id) || (null === (_c = message.forward_info) || void 0 === _c ? void 0 : _c.origin.sender_chat_id) || (null === (_d = message.forward_info) || void 0 === _d ? void 0 : _d.origin.chat_id) || message.sender_id.chat_id || message.sender_id.user_id, 
            post.owner_id = message.chat_id, await this.ptransformer.process(post, ftext, source.picture, messages, this.downloadPhoto))) {
                post.edited = message.edit_date || message.date;
                const mediaMessage = messages.find(m => "messageVideo" === m.content._ || "messageAnimation" === m.content._);
                if (mediaMessage) {
                    const media = await this.messageToMediaInput(mediaMessage, this.settings.maxMediaDownloadTime, post.idCaption);
                    return post.actual = !0, await this.bot.savePost(post), void await this.sendMessage(this.bot.id, media);
                }
                return await this.bot.savePost(post, !0);
            }
            (null == post ? void 0 : post.db) && await this.bot.updateMarkup(post);
        }
        processMessage(message) {
            if ("messageText" === message.content._ || "messagePhoto" === message.content._ || "messageVideo" === message.content._ || "messageAnimation" === message.content._) {
                if (!+message.media_album_id) return this.processContent([ message ]);
                this.queue.push(message.chat_id + "_" + message.media_album_id.toString(), message.id);
            }
        }
    };
    AdminClient_AdminClient = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.c)(6, external_typescript_ioc_.Inject), Object(tslib_es6.c)(7, external_typescript_ioc_.Inject), Object(tslib_es6.c)(8, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ PostTransformer_PostTransformer, BotClient_BotClient, VKClient_VKClient, ClientSettings.ClientSettings, Settings.Settings, Settings.SettingsRepository, Post.PostRepository, Source.SourceRepository, Channel.ChannelRepository ]) ], AdminClient_AdminClient);
    var external_fs_cp_ = __webpack_require__(97), external_fs_cp_default = __webpack_require__.n(external_fs_cp_), external_koa_compress_ = __webpack_require__(98), external_koa_compress_default = __webpack_require__.n(external_koa_compress_), external_koa_router_ = __webpack_require__(26), external_koa_router_default = __webpack_require__.n(external_koa_router_), external_param_case_ = __webpack_require__(39), external_path_to_regexp_ = __webpack_require__(99), external_pluralize_ = __webpack_require__(52), external_pluralize_default = __webpack_require__.n(external_pluralize_), external_set_utils_ = __webpack_require__(63), external_set_utils_default = __webpack_require__.n(external_set_utils_), ioc = __webpack_require__(10);
    const entities = [], repositories = [], storage = Object(external_typeorm_.getMetadataArgsStorage)(), {Settings: generated_Settings, SettingsRepository} = __webpack_require__(9);
    generated_Settings && storage.filterTables(generated_Settings).length > 0 && (entities.push(generated_Settings), 
    SettingsRepository && repositories.push(Object(ioc.b)(generated_Settings)(SettingsRepository)));
    const {User: generated_User, UserRepository} = __webpack_require__(14);
    generated_User && storage.filterTables(generated_User).length > 0 && (entities.push(generated_User), 
    UserRepository && repositories.push(Object(ioc.b)(generated_User)(UserRepository)));
    const {Channel: generated_Channel, ChannelRepository} = __webpack_require__(25);
    generated_Channel && storage.filterTables(generated_Channel).length > 0 && (entities.push(generated_Channel), 
    ChannelRepository && repositories.push(Object(ioc.b)(generated_Channel)(ChannelRepository)));
    const {ClientSettings: generated_ClientSettings, ClientSettingsRepository} = __webpack_require__(13);
    generated_ClientSettings && storage.filterTables(generated_ClientSettings).length > 0 && (entities.push(generated_ClientSettings), 
    ClientSettingsRepository && repositories.push(Object(ioc.b)(generated_ClientSettings)(ClientSettingsRepository)));
    const {LinksSettings: generated_LinksSettings, LinksSettingsRepository} = __webpack_require__(20);
    generated_LinksSettings && storage.filterTables(generated_LinksSettings).length > 0 && (entities.push(generated_LinksSettings), 
    LinksSettingsRepository && repositories.push(Object(ioc.b)(generated_LinksSettings)(LinksSettingsRepository)));
    const {Patch: generated_Patch, PatchRepository} = __webpack_require__(42);
    generated_Patch && storage.filterTables(generated_Patch).length > 0 && (entities.push(generated_Patch), 
    PatchRepository && repositories.push(Object(ioc.b)(generated_Patch)(PatchRepository)));
    const {Post: generated_Post, PostRepository} = __webpack_require__(17);
    generated_Post && storage.filterTables(generated_Post).length > 0 && (entities.push(generated_Post), 
    PostRepository && repositories.push(Object(ioc.b)(generated_Post)(PostRepository)));
    const {Source: generated_Source, SourceRepository} = __webpack_require__(22);
    generated_Source && storage.filterTables(generated_Source).length > 0 && (entities.push(generated_Source), 
    SourceRepository && repositories.push(Object(ioc.b)(generated_Source)(SourceRepository)));
    const entities_entities = entities, entities_repositories = repositories;
    var check_auth = function(ctx, next) {
        if (ctx.isAuthenticated()) return next();
        throw is_ajax(ctx) ? 401 : "Login required";
    };
    const DEFAULT_COOKIE_OPTIONS = Object.freeze({
        maxAge: 18e4,
        secure: !1,
        httpOnly: !0,
        sameSite: "lax",
        signed: !1,
        overwrite: !0
    }), DELETE_COOKIE_OPTIONS = Object.freeze({
        ...DEFAULT_COOKIE_OPTIONS,
        maxAge: void 0,
        expires: new Date(-1),
        signed: !1
    });
    function deleteCookie(name, ctx, signed) {
        ctx.cookies.set(name, "", DELETE_COOKIE_OPTIONS), signed && ctx.cookies.set(name + ".sig", "", DELETE_COOKIE_OPTIONS);
    }
    var flash = async (ctx, next) => {
        const t = function getCookie(name, ctx) {
            try {
                const v = ctx.cookies.get(name);
                return v ? JSON.parse(decodeURIComponent(v)) : null;
            } catch (err) {
                console.error.bind(console, "(./src/core/utils/server-cookie.ts:36)")(name, err), 
                deleteCookie(name, ctx);
            }
            return null;
        }("_f", ctx);
        let a = [ [], [], [] ];
        try {
            t && (ctx.flash = {
                messages: t[2],
                warnings: t[1],
                errors: t[0]
            }), ctx.addFlash = (message, level) => {
                a[0 | level].push(message);
            }, await next();
        } finally {
            a.some(v => v.length > 0) ? function setCookie(name, ctx, data) {
                ctx.cookies.set(name, encodeURIComponent(JSON.stringify(data)), DEFAULT_COOKIE_OPTIONS);
            }("_f", ctx, a) : t && deleteCookie("_f", ctx);
        }
    };
    let root = "", rootSlash = "";
    var normalize_traling_slashes = function(ctx, next) {
        if ("GET" === ctx.method) {
            const {path} = ctx;
            if (root || (root = new URL(ctx.resolve("root")).pathname.replace(/\/$/, ""), rootSlash = root + "/"), 
            "/" !== path) {
                if (root === path) return ctx.status = 301, ctx.redirect(rootSlash + (ctx.querystring ? "?" + ctx.querystring : ""));
                if (path.endsWith("/") && path !== rootSlash) return ctx.status = 301, ctx.redirect(path.slice(0, -1) + (ctx.querystring ? "?" + ctx.querystring : ""));
            }
        }
        return next();
    }, external_etag_ = __webpack_require__(53), external_etag_default = __webpack_require__.n(external_etag_);
    const assetsUrls = new Array(2);
    var pug = function(ctx, next) {
        return ctx.pug = (file, locals, noCommon) => {
            ctx.type = "html";
            const template = "function" == typeof file ? file : __webpack_require__(115)("./" + file + ".pug"), r = Object.assign({}, ...[ !noCommon && "common" ].concat(null == locals ? void 0 : locals.entries).map(e => e && require(__dirname + "/" + e + "-assets.json"))), styles = Object.values(r).map(r => r.css).filter(Boolean), scripts = Object.values(r).map(r => r.js).filter(Boolean), body = template({
                title: "Ali forward",
                assetsUrl: assetsUrls[+ctx.secure] || (assetsUrls[+ctx.secure] = resolve_url(ctx, config.h, !ctx.secure && config.d != config.g)),
                favicon: "favicon.ico",
                ...ctx.flash,
                ...locals,
                styles,
                scripts
            });
            ctx.set("Cache-Control", "no-cache, max-age=31536000");
            const et = external_etag_default()(body);
            ctx.set("ETag", et), ctx.headers["if-none-match"] && ctx.headers["if-none-match"].endsWith(et) ? ctx.status = 304 : ctx.body = body;
        }, next();
    }, external_bcryptjs_ = __webpack_require__(27), external_koa_compose_ = __webpack_require__(100), external_koa_compose_default = __webpack_require__.n(external_koa_compose_), external_koa_passport_ = __webpack_require__(31), external_koa_passport_default = __webpack_require__.n(external_koa_passport_), external_koa_session_ = __webpack_require__(101), external_koa_session_default = __webpack_require__.n(external_koa_session_), external_passport_local_ = __webpack_require__(102), external_koa_ = __webpack_require__(103), external_koa_default = __webpack_require__.n(external_koa_);
    let Koa_Koa = class Koa extends external_koa_default.a {
        constructor(settings) {
            super(), this.proxy = !0, this.keys = [ settings.secret ];
        }
    };
    Koa_Koa = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ Settings.Settings ]) ], Koa_Koa);
    function sessionSig(ctx) {
        return ctx.cookies.get("_s.sig");
    }
    function destroySession(ctx) {
        const id = ctx.state.user && ctx.state.user.id, bId = sessionSig(ctx);
        id && ctx.res.once("finish", () => function destroyByUserId(id, browserId) {
            const m = RPCServers.get(id);
            m && (browserId ? m.forEach(v => v.browserId == browserId && v.dispose()) : m.forEach(v => v.dispose()));
        }(id, bId)), ctx.session = null;
    }
    let AuthRegister_AuthRegister = class AuthRegister {
        constructor(userRepository, app) {
            external_koa_passport_default.a.serializeUser((user, done) => {
                done(null, user.id);
            }), external_koa_passport_default.a.deserializeUser((id, done) => {
                userRepository.findOneOrFail(id).then(user => done(null, user)).catch(err => done(err, !1));
            }), external_koa_passport_default.a.use(new external_passport_local_.Strategy({
                usernameField: "email",
                passwordField: "password"
            }, async (email, password, done) => {
                try {
                    email = Object(string_tools_.trim)(email), password = Object(string_tools_.trim)(password);
                    const user = await userRepository.findOneOrFail({
                        email
                    });
                    await Object(external_bcryptjs_.compare)(password, user.password) ? done(null, user) : done(null, !1);
                } catch (err) {
                    done(err, !1);
                }
            })), this.session = external_koa_session_default()({
                ...DEFAULT_COOKIE_OPTIONS,
                key: "_s",
                maxAge: 31536e6,
                renew: !0,
                signed: !0
            }, app), this.passport = external_koa_passport_default.a.initialize(), this.passportSession = external_koa_passport_default.a.session(), 
            this.passportComposed = external_koa_compose_default()([ this.session, this.passport, this.passportSession ]);
        }
    };
    AuthRegister_AuthRegister = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ User.UserRepository, Koa_Koa ]) ], AuthRegister_AuthRegister);
    var redirect_after_error = redirectRouteName => async function(ctx, next) {
        if (is_ajax(ctx)) await next(); else try {
            await next();
        } catch (err) {
            err instanceof Error ? (console.error.bind(console, "(./src/core/middlewares/redirect-after-error.ts:16)")(err), 
            ctx.status = 500, destroySession(ctx), ctx.namedRedirect(redirectRouteName, 1)) : (err && ctx.addFlash && ("function" == typeof err.forEach ? err.forEach(e => ctx.addFlash(Object(string_tools_.stringify)(e))) : ctx.addFlash(Object(string_tools_.stringify)(err))), 
            ctx.namedRedirect(redirectRouteName));
        }
    }, index_mustache = __webpack_require__(104), index_mustache_default = __webpack_require__.n(index_mustache);
    function route(method, params, ...middlewares) {
        return (target, prop) => {
            (target.constructor.__routes_info__ || (target.constructor.__routes_info__ = [])).push({
                method,
                name: params && params.name,
                path: params && params.path || "/" + Object(external_param_case_.paramCase)(prop),
                prop,
                middlewares
            });
        };
    }
    function applyRoutes(router, routes) {
        for (let {method, name, path, prop, middlewares} of routes.constructor.__routes_info__) "function" == typeof path && (path = path.call(routes)), 
        name ? router[method](name, path, ...middlewares, routes[prop].bind(routes)) : router[method](path, ...middlewares, routes[prop].bind(routes));
        return router;
    }
    const get = route.bind(null, "get"), routes_helpers_post = (route.bind(null, "head"), 
    route.bind(null, "post"));
    route.bind(null, "put"), route.bind(null, "delete"), route.bind(null, "patch"), 
    route.bind(null, "all");
    var external_vue_template_compiler_ = __webpack_require__(105), external_vue_template_es2015_compiler_ = __webpack_require__(106), external_vue_template_es2015_compiler_default = __webpack_require__.n(external_vue_template_es2015_compiler_);
    const templates = {
        single: __webpack_require__(64),
        table: __webpack_require__(65)
    };
    function isExtendableArrayField(v) {
        return Object(types.a)(v, 8);
    }
    function isVisibleField(v) {
        return !Object(types.a)(v, 98);
    }
    function isEditableField(v) {
        return !Object(types.a)(v, 34);
    }
    function isNormalField(v) {
        return !Object(types.a)(v, 98) && !Object(types.a)(v, 4);
    }
    function isFullwidthField(v) {
        return !Object(types.a)(v, 98) && Object(types.a)(v, 4);
    }
    function findPrimaryIndex(props) {
        let i = 0;
        for (let k in props) {
            if (Object(types.a)(props[k], 18)) return i;
            ++i;
        }
        throw new Error("Can't find primary field.");
    }
    function toFunction(code) {
        return `(function(){${code}})`;
    }
    var rtti_to_vue_component = ({props, displayInfo}, can, ctx) => {
        const vueTemplate = templates[displayInfo.display]({
            props,
            EDIT: 1,
            NONE: 0,
            REMOVE: 3,
            SAVE: 2,
            can,
            ctx,
            isExtendableArrayField,
            findPrimaryIndex,
            isVisibleField,
            isEditableField,
            isNormalField,
            isFullwidthField
        }), {render, staticRenderFns} = Object(external_vue_template_compiler_.compile)(vueTemplate, {
            preserveWhitespace: !1
        });
        return external_vue_template_es2015_compiler_default()(`(function(){\nreturn {staticRenderFns:[${staticRenderFns.map(toFunction)}],render:${toFunction(render)}};\n})()`);
    }, external_alphanum_sort_ = __webpack_require__(107), external_alphanum_sort_default = __webpack_require__.n(external_alphanum_sort_), external_fast_glob_ = __webpack_require__(108), external_fast_glob_default = __webpack_require__.n(external_fast_glob_), external_time_stamp_ = __webpack_require__(109), external_time_stamp_default = __webpack_require__.n(external_time_stamp_), singleton_providers_factory = __webpack_require__(43);
    let ValidateSubscriber = class ValidateSubscriber {
        async beforeInsert({entity}) {
            const f = entity.constructor.insertValidate;
            f && await f(entity);
        }
        async beforeUpdate({entity}) {
            const f = entity.constructor.updateValidate;
            f && await f(entity);
        }
    };
    ValidateSubscriber = Object(tslib_es6.a)([ Object(external_typeorm_.EventSubscriber)() ], ValidateSubscriber);
    let ChannelSubscriber_ChannelSubscriber = class ChannelSubscriber {
        listenTo() {
            return Channel.Channel;
        }
        afterRemove(_) {
            return external_typescript_ioc_.Container.get(BotClient_BotClient).loadChannels();
        }
        afterUpdate(_) {
            return external_typescript_ioc_.Container.get(BotClient_BotClient).loadChannels();
        }
        afterInsert(_) {
            return external_typescript_ioc_.Container.get(BotClient_BotClient).loadChannels();
        }
    };
    ChannelSubscriber_ChannelSubscriber = Object(tslib_es6.a)([ Object(external_typeorm_.EventSubscriber)() ], ChannelSubscriber_ChannelSubscriber);
    let PatchSubscriber_PatchSubscriber = class PatchSubscriber {
        listenTo() {
            return Patch.Patch;
        }
        afterUpdate(_) {
            return external_typescript_ioc_.Container.get(BotClient_BotClient).loadPatches();
        }
        afterInsert(_) {
            return external_typescript_ioc_.Container.get(BotClient_BotClient).loadPatches();
        }
    };
    PatchSubscriber_PatchSubscriber = Object(tslib_es6.a)([ Object(external_typeorm_.EventSubscriber)() ], PatchSubscriber_PatchSubscriber);
    let SourceSubscriber_SourceSubscriber = class SourceSubscriber {
        listenTo() {
            return Source.Source;
        }
        beforeRemove({entityId}) {
            const vk = external_typescript_ioc_.Container.get(VKClient_VKClient);
            Array.isArray(entityId) ? entityId.forEach(id => vk.remove(id)) : entityId && vk.remove(entityId);
        }
        async beforeInsert({entity}) {
            entity.tg && (entity.domain = await external_typescript_ioc_.Container.get(AdminClient_AdminClient).getSupergroupDomain(Object(utils.u)(entity.id)));
        }
        async beforeUpdate({entity}) {
            if (entity.tg) {
                const admin = external_typescript_ioc_.Container.get(AdminClient_AdminClient);
                entity.name = (await admin.getChat(entity.id)).title, entity.domain = await admin.getSupergroupDomain(Object(utils.u)(entity.id));
            } else {
                const info = await external_typescript_ioc_.Container.get(VKClient_VKClient).getGroupInfo("https://vk.com/public" + entity.id);
                info && (entity.name = info.name || entity.name, entity.domain = info.screen_name || "");
            }
        }
        afterInsert({entity}) {
            return entity.tg || external_typescript_ioc_.Container.get(VKClient_VKClient).add(entity.id), 
            external_typescript_ioc_.Container.get(BotClient_BotClient).loadSources();
        }
        afterUpdate(e) {
            return this.afterInsert(e);
        }
        afterRemove(_) {
            return external_typescript_ioc_.Container.get(BotClient_BotClient).loadSources();
        }
    };
    SourceSubscriber_SourceSubscriber = Object(tslib_es6.a)([ Object(external_typeorm_.EventSubscriber)() ], SourceSubscriber_SourceSubscriber);
    var generated = [ ValidateSubscriber, ChannelSubscriber_ChannelSubscriber, PatchSubscriber_PatchSubscriber, SourceSubscriber_SourceSubscriber ], subscribers = generated;
    function createDBFilename() {
        return "ali-forward." + external_time_stamp_default()("YYYY.MM.DD[HH.mm.ss]") + ".sqlite3";
    }
    async function getConnection(database, migrations) {
        const hasMigrations = Array.isArray(migrations) && migrations.length > 0, connection = await Object(external_typeorm_.createConnection)({
            type: "sqlite",
            database,
            synchronize: !hasMigrations && !Object(external_fs_.existsSync)(database),
            logging: [ "error" ],
            entities: entities_entities,
            subscribers,
            migrationsRun: hasMigrations,
            migrations
        });
        return Object.defineProperty(connection, "filename", {
            value: database
        }), connection;
    }
    let sqliteFilename = null;
    const provider = Object(singleton_providers_factory.b)(async () => {
        const alldb = external_alphanum_sort_default()(await external_fast_glob_default()("ali-forward.*].sqlite3", {
            cwd: Object(data_path.a)(),
            absolute: !0,
            extglob: !1,
            braceExpansion: !1,
            caseSensitiveMatch: !1,
            baseNameMatch: !0,
            globstar: !1
        })).reverse();
        sqliteFilename = alldb.find(f => {
            const stat = Object(external_fs_.statSync)(f);
            return stat.size > 4096;
        }) || Object(data_path.a)(createDBFilename());
        try {
            await Promise.all(alldb.map(f => f !== sqliteFilename && external_fs_.promises.unlink(f)));
        } catch (err) {
            console.error.bind(console, "(./src/core/DBConnection.ts:82)")(err);
        }
        let connection = await getConnection(sqliteFilename);
        const migrations = await async function generateMigrations(connection) {
            const {upQueries, downQueries} = await connection.driver.createSchemaBuilder().log();
            if (upQueries.length > 0) {
                const migration = new Function(`return function _${Date.now()}() { }`)(), s = upQueries.reduce((s, q) => s + +q.query.startsWith("DROP INDEX "), 0);
                if (s !== upQueries.length / 2 || s !== upQueries.reduce((s, q) => s + +q.query.startsWith("CREATE INDEX "), 0)) return migration.prototype.up = async function(queryRunner) {
                    console.log("Start migration:");
                    for (let {query} of upQueries) console.log(query), await queryRunner.query(query);
                }, migration.prototype.down = async function(queryRunner) {
                    console.log("Rollback migration:");
                    for (let {query} of downQueries) console.log(query), await queryRunner.query(query);
                }, [ migration ];
            }
            return [];
        }(connection);
        migrations.length > 0 && (await connection.close(), connection = await getConnection(sqliteFilename, migrations)), 
        entities_repositories.forEach(r => r.provider.init(connection));
        for (let e of entities_entities) e.asyncProvider && await e.asyncProvider.init(connection);
        return connection;
    }), dbInit = provider.init;
    let DBConnection_DBConnection = class DBConnection extends external_typeorm_.Connection {};
    function convertError(err) {
        return Object(string_tools_.isJsonable)(err) ? err : Object(string_tools_.stringify)(err);
    }
    DBConnection_DBConnection = Object(tslib_es6.a)([ Object(external_typescript_ioc_.Provided)(provider) ], DBConnection_DBConnection);
    var catch_error = async (ctx, next) => {
        try {
            await next();
        } catch (err) {
            const ajax = is_ajax(ctx);
            if (ctx.type = ajax ? "json" : "text", Array.isArray(err)) ctx.status = 400, ctx.body = ajax ? JSON.stringify({
                errors: err.map(convertError)
            }) : err.map(string_tools_.stringify).join("\n"); else {
                if (external_http_.STATUS_CODES[err]) ctx.status = +err, err = external_http_.STATUS_CODES[err]; else {
                    const s = 0 | +err.status || 500;
                    ctx.status = s, s >= 500 ? (console.error.bind(console, "(./src/core/middlewares/catch-error.ts:30)")(err), 
                    err = external_http_.STATUS_CODES[s] || external_http_.STATUS_CODES[500]) : delete err.status;
                }
                ctx.body = ajax ? JSON.stringify({
                    errors: [ convertError(err) ]
                }) : Object(string_tools_.stringify)(err);
            }
        }
    };
    let MainRouter_MainRouter = class MainRouter extends external_koa_router_default.a {
        constructor() {
            super(), this.use(catch_error).get("health", config.c, no_store, ctx => {
                ctx.type = "json", ctx.body = '{"success":"ok"}';
            }).get("index", "/", no_store, ctx => {
                ctx.status = 200, ctx.body = "";
            });
        }
    };
    MainRouter_MainRouter = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.b)("design:paramtypes", []) ], MainRouter_MainRouter);
    var external_koa_body_ = __webpack_require__(19), external_koa_body_default = __webpack_require__.n(external_koa_body_);
    let checkpass_CheckPassRoutes = class CheckPassRoutes {
        checkpass(ctx) {
            const {action} = ctx.params;
            ctx.pug("checkpass", {
                action: ctx.resolve("checkpass", {
                    action
                })
            });
        }
        async checkpassPost(ctx) {
            ctx.session.confirmed = 0;
            let {password} = ctx.request.body;
            if (!await Object(external_bcryptjs_.compare)(Object(string_tools_.trim)(password), ctx.state.user.password)) throw "Wrong password";
            ctx.session.confirmed = Date.now(), ctx.namedRedirect(ctx.params.action);
        }
    };
    Object(tslib_es6.a)([ get({
        name: "checkpass",
        path: "/checkpass/:action"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], checkpass_CheckPassRoutes.prototype, "checkpass", null), 
    Object(tslib_es6.a)([ routes_helpers_post({
        path: "/checkpass/:action"
    }, redirect_after_error("checkpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], checkpass_CheckPassRoutes.prototype, "checkpassPost", null), 
    checkpass_CheckPassRoutes = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton ], checkpass_CheckPassRoutes);
    let install_InstallRoutes = class InstallRoutes {
        constructor(settings, userRepository, settingsRepository) {
            this.settings = settings, this.userRepository = userRepository, this.settingsRepository = settingsRepository;
        }
        install(ctx) {
            this.settings.installed ? ctx.isAuthenticated() ? ctx.namedRedirect("root") : (ctx.addFlash("Already installed. Login please.", 1), 
            ctx.namedRedirect("login")) : ctx.pug("install");
        }
        async installPost(ctx) {
            if (this.settings.installed) ctx.namedRedirect("install"); else {
                let {email, password} = ctx.request.body;
                email = Object(string_tools_.trim)(email), password = Object(string_tools_.trim)(password), 
                User.User.insertValidate({
                    email,
                    password
                });
                let user = await this.userRepository.findOne({
                    email
                });
                if (user) throw "Пользователь с таким email уже существует.";
                user = new User.User, user.email = email, user.password = await Object(external_bcryptjs_.hash)(password, 10), 
                await this.userRepository.insert(user), this.settings.installed = !0, await this.settingsRepository.save(this.settings), 
                await ctx.login(user), ctx.namedRedirect("root");
            }
        }
    };
    Object(tslib_es6.a)([ get({
        name: "install"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], install_InstallRoutes.prototype, "install", null), 
    Object(tslib_es6.a)([ routes_helpers_post({
        path: "/install"
    }, redirect_after_error("install"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], install_InstallRoutes.prototype, "installPost", null), 
    install_InstallRoutes = Object(tslib_es6.a)([ Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ Settings.Settings, User.UserRepository, Settings.SettingsRepository ]) ], install_InstallRoutes);
    class login_LoginRoutes {
        login(ctx) {
            ctx.pug("login");
        }
        loginPost(ctx, next) {
            return external_koa_passport_default.a.authenticate("local", async (_, user, info) => {
                user ? (await ctx.login(user), ctx.namedRedirect("root")) : (ctx.addFlash(info && info.message || "Invalid login data"), 
                ctx.namedRedirect("login"));
            })(ctx, next);
        }
    }
    Object(tslib_es6.a)([ get({
        name: "login"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], login_LoginRoutes.prototype, "login", null), 
    Object(tslib_es6.a)([ routes_helpers_post({
        path: "/login"
    }, external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object, Function ]), Object(tslib_es6.b)("design:returntype", void 0) ], login_LoginRoutes.prototype, "loginPost", null);
    let resetemail_ResetEmailRoutes = class ResetEmailRoutes {
        constructor(userRepository) {
            this.userRepository = userRepository;
        }
        resetemail(ctx) {
            Date.now() - (ctx.session.confirmed || 0) < 12e4 ? ctx.pug("resetemail") : ctx.namedRedirect("checkpass", 0, {
                action: "resetemail"
            });
        }
        async resetemailPost(ctx) {
            let {email} = ctx.request.body;
            email = Object(string_tools_.trim)(email);
            const user = {
                ...ctx.state.user,
                email
            };
            User.User.updateValidate(user), await this.userRepository.save(user), await ctx.login(user), 
            ctx.namedRedirect("root");
        }
    };
    Object(tslib_es6.a)([ get({
        name: "resetemail"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], resetemail_ResetEmailRoutes.prototype, "resetemail", null), 
    Object(tslib_es6.a)([ routes_helpers_post({
        path: "/resetemail"
    }, redirect_after_error("resetemail"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], resetemail_ResetEmailRoutes.prototype, "resetemailPost", null), 
    resetemail_ResetEmailRoutes = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ User.UserRepository ]) ], resetemail_ResetEmailRoutes);
    const services = Object.freeze({
        "mail.ru": Object.freeze({
            title: "почту Mail.Ru",
            url: "https://e.mail.ru/"
        }),
        "bk.ru": Object.freeze({
            title: "почту Mail.Ru (bk.ru)",
            url: "https://e.mail.ru/"
        }),
        "list.ru": Object.freeze({
            title: "почту Mail.Ru (list.ru)",
            url: "https://e.mail.ru/"
        }),
        "inbox.ru": Object.freeze({
            title: "почту Mail.Ru (inbox.ru)",
            url: "https://e.mail.ru/"
        }),
        "yandex.ru": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ru/"
        }),
        "ya.ru": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ru/"
        }),
        "yandex.ua": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ua/"
        }),
        "yandex.by": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.by/"
        }),
        "yandex.kz": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.kz/"
        }),
        "yandex.com": Object.freeze({
            title: "Yandex.Mail",
            url: "https://mail.yandex.com/"
        }),
        "gmail.com": Object.freeze({
            title: "Gmail",
            url: "https://mail.google.com/"
        }),
        "googlemail.com": Object.freeze({
            title: "Gmail",
            url: "https://mail.google.com/"
        }),
        "outlook.com": Object.freeze({
            title: "Outlook.com",
            url: "https://mail.live.com/"
        }),
        "hotmail.com": Object.freeze({
            title: "Outlook.com (Hotmail)",
            url: "https://mail.live.com/"
        }),
        "live.ru": Object.freeze({
            title: "Outlook.com (live.ru)",
            url: "https://mail.live.com/"
        }),
        "live.com": Object.freeze({
            title: "Outlook.com (live.com)",
            url: "https://mail.live.com/"
        }),
        "me.com": Object.freeze({
            title: "iCloud Mail",
            url: "https://www.icloud.com/"
        }),
        "icloud.com": Object.freeze({
            title: "iCloud Mail",
            url: "https://www.icloud.com/"
        }),
        "rambler.ru": Object.freeze({
            title: "Рамблер/почта",
            url: "https://mail.rambler.ru/"
        }),
        "yahoo.com": Object.freeze({
            title: "Yahoo! Mail",
            url: "https://mail.yahoo.com/"
        }),
        "ukr.net": Object.freeze({
            title: "почту ukr.net",
            url: "https://mail.ukr.net/"
        }),
        "i.ua": Object.freeze({
            title: "почту I.UA",
            url: "http://mail.i.ua/"
        }),
        "bigmir.net": Object.freeze({
            title: "почту Bigmir.net",
            url: "http://mail.bigmir.net/"
        }),
        "tut.by": Object.freeze({
            title: "почту tut.by",
            url: "https://mail.tut.by/"
        }),
        "inbox.lv": Object.freeze({
            title: "Inbox.lv",
            url: "https://www.inbox.lv/"
        }),
        "mail.kz": Object.freeze({
            title: "почту mail.kz",
            url: "http://mail.kz/"
        })
    });
    var detect_email_service = email => {
        const m = email.split("@"), domain = m && m.length > 1 ? m[m.length - 1] : null;
        return services[domain] || "";
    }, rnd = __webpack_require__(51);
    let resetpass_ResetPassRoutes = class ResetPassRoutes {
        constructor(userRepository) {
            this.userRepository = userRepository, this.resetPasswordTokens = Object.create(null), 
            Object(external_timers_.setInterval)(() => {
                const NOW = Date.now();
                for (let token in this.resetPasswordTokens) NOW - this.resetPasswordTokens[token].time > 12e5 && delete this.resetPasswordTokens[token];
            }, 12e5).unref();
        }
        validateToken(token) {
            let tokenInfo = this.resetPasswordTokens[token];
            if (!tokenInfo) throw "Invalid or expiried recovery url";
            const {time} = tokenInfo;
            if (Date.now() - time > 12e5) throw delete this.resetPasswordTokens[token], "Expiried recovery url";
            return tokenInfo;
        }
        resetpass(ctx) {
            ctx.pug("resetpass", {
                email: ctx.isAuthenticated() ? ctx.state.user.email : ""
            });
        }
        async resetpassPost(ctx) {
            let {email} = ctx.request.body;
            email = Object(string_tools_.trim)(email);
            const user = await this.userRepository.findOne({
                email
            });
            if (!user) throw "User not found";
            const {id} = user;
            for (let t in this.resetPasswordTokens) this.resetPasswordTokens[t].id === id && delete this.resetPasswordTokens[t];
            const token = await Object(rnd.a)();
            this.resetPasswordTokens[token] = {
                id,
                time: Date.now()
            }, await sendEmail(email, "Изменение пароля " + ctx.resolve("root"), "Для продолжения смены пароля перейдите на " + ctx.resolve("newpassword", {
                token
            })), ctx.pug("resetpass-sended", {
                email,
                service: detect_email_service(email)
            });
        }
        newpassword(ctx) {
            const {token} = ctx.params;
            this.validateToken(token), ctx.pug("newpassword", {
                action: ctx.resolve("newpassword", {
                    token
                })
            });
        }
        async newpasswordPost(ctx) {
            const {token} = ctx.params, {id} = this.validateToken(token);
            delete this.resetPasswordTokens[token];
            const user = id && await this.userRepository.findOne(id);
            if (!user) throw "User not found. Попробуйте другой аккаунт.";
            let {password} = ctx.request.body;
            password = Object(string_tools_.trim)(password), User.User.insertValidate({
                email: user.email,
                password
            }), user.password = await Object(external_bcryptjs_.hash)(password, 10), await this.userRepository.save(user), 
            await ctx.login(user), ctx.namedRedirect("root");
        }
    };
    Object(tslib_es6.a)([ get({
        name: "resetpass"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], resetpass_ResetPassRoutes.prototype, "resetpass", null), 
    Object(tslib_es6.a)([ routes_helpers_post({
        path: "/resetpass"
    }, redirect_after_error("resetpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], resetpass_ResetPassRoutes.prototype, "resetpassPost", null), 
    Object(tslib_es6.a)([ get({
        name: "newpassword",
        path: "/newpassword/:token"
    }, redirect_after_error("resetpass")), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], resetpass_ResetPassRoutes.prototype, "newpassword", null), 
    Object(tslib_es6.a)([ routes_helpers_post({
        path: "/newpassword/:token"
    }, redirect_after_error("resetpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], resetpass_ResetPassRoutes.prototype, "newpasswordPost", null), 
    resetpass_ResetPassRoutes = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ User.UserRepository ]) ], resetpass_ResetPassRoutes);
    const EXT_COMPS = new Set([ "wysiwyg", "javascript" ]), WORKER_INFO = require(__dirname + "/workers-assets.json"), COMP_ENT_MAP = {
        javascript: "monaco",
        wysiwyg: "wysiwyg"
    };
    let AdminRouter_AdminRouter = class AdminRouter extends external_koa_router_default.a {
        constructor(settings, dbConnection, installRoutes, loginRoutes, resetPassRoutes, checkPassRoutes, resetEmailRoutes, {session, passport, passportSession}) {
            super(), this.events = new external_emittery_default.a, this.restart = ctx => {
                const {path} = ctx;
                ctx.type = "json", ctx.body = '{"success":"ok"}', ctx.res.once("finish", () => this.events.emit("restart", path));
            }, this.use(flash, smart_redirect, normalize_traling_slashes, redirect_after_error("login"), session, passport, passportSession, pug), 
            this.put("logout", "/logout", ctx => {
                ctx.status = 204, destroySession(ctx);
            }), applyRoutes(this, installRoutes), settings.installed || this.use((settings => function(ctx, next) {
                if (settings.installed) return next();
                ctx.namedRedirect("install");
            })(settings)), applyRoutes(this, loginRoutes), applyRoutes(this, resetPassRoutes), 
            this.use(check_auth), applyRoutes(this, checkPassRoutes), applyRoutes(this, resetEmailRoutes);
            const componentsCache = {};
            this.get("root", "/", ctx => {
                const user = ctx.state.user;
                if (!componentsCache[user.role]) {
                    const components = [], validators = new Set;
                    for (let entity of entities_entities) {
                        let {name, rtti, checkAccess, hooks} = entity;
                        name = external_pluralize_default()(name);
                        const can = Object.create(null);
                        for (let action of [ "GET", "PATCH", "PUT", "DELETE" ]) try {
                            can[action] = checkAccess(action, ctx.state.user);
                        } catch {}
                        if (can.GET && rtti.displayInfo.display) {
                            can.CHANGE = can.PUT || can.PATCH;
                            const render = rtti_to_vue_component(rtti, can, ctx), remote = Object.values(rtti.props).filter(v => v.remote).map(v => {
                                const i = v.remote.lastIndexOf("."), c = v.remote.slice(0, i), method = v.remote.slice(i + 1);
                                if (!Object(string_tools_.isValidVar)(c) || !Object(string_tools_.isValidVar)(method) || method == v.remote) throw "Invalid property " + Object(string_tools_.tosource)(v);
                                return {
                                    c,
                                    method
                                };
                            });
                            for (const p in rtti.props) validators.add(rtti.props[p].type);
                            components.push({
                                name,
                                sortable: !(!can.PATCH || !rtti.displayInfo.sortable || "single" === rtti.displayInfo.display),
                                order: 0 | rtti.displayInfo.order,
                                icon: rtti.displayInfo.icon,
                                render,
                                can: Object(string_tools_.tosource)(can, null, ""),
                                rtti: Object(string_tools_.tosource)(rtti, null, ""),
                                hooks: Object(string_tools_.tosource)(hooks, null, ""),
                                endPoint: Object(external_param_case_.paramCase)(name),
                                remote
                            });
                        }
                    }
                    const classes = [], m = external_typescript_ioc_.Scope.Singleton.constructor.instances;
                    m.forEach((v, k) => classes.push({
                        name: k.name,
                        props: Object.keys(v).filter(p => !/^[_#]/.test(p))
                    }));
                    const {stack} = external_typescript_ioc_.Container.get(MainRouter_MainRouter);
                    componentsCache[user.role] = {
                        components,
                        Paths: stack.filter(l => l.name).map(l => {
                            if (l.paramNames.length > 0) {
                                const tokens = Object(external_path_to_regexp_.parse)(l.path);
                                return {
                                    name: l.name,
                                    url: `function (${l.paramNames.map(p => p.name).join(", ")}) { return ${tokens.map(p => p.constructor == String ? JSON.stringify(p) : p.name).join(' + "/" + ')}}`
                                };
                            }
                            return {
                                name: l.name,
                                url: JSON.stringify(l.path)
                            };
                        }),
                        entries: Array.from(external_set_utils_default.a.map(external_set_utils_default.a.intersect(validators, EXT_COMPS), c => COMP_ENT_MAP[c])).concat("index"),
                        HasPty: AVALIBLE,
                        EDITOR_WORKER: WORKER_INFO["editor.worker"].js,
                        TYPESCRIPT_WORKER: WORKER_INFO["typescript.worker"].js
                    };
                }
                ctx.pug("index", {
                    entries: componentsCache[user.role].entries,
                    inlineScript: index_mustache_default()({
                        ...componentsCache[user.role],
                        email: user.email,
                        Flash: JSON.stringify(ctx.flash)
                    })
                }, !0);
            }), this.get("backup", "/backup", no_store, external_koa_compress_default()(), ctx => {
                ctx.attachment(createDBFilename()), ctx.body = Object(external_fs_.createReadStream)(dbConnection.filename);
            }), this.put("restore", "/restore", async ctx => {
                await external_fs_cp_default()(ctx.req, Object(data_path.a)(createDBFilename())), 
                this.restart(ctx);
            }), this.put("restart", "/restart", this.restart);
        }
    };
    async function data2Model(ctx, model, repository) {
        const {body} = ctx.request, {props} = model.constructor.rtti;
        for (let k in body) {
            const p = props[k];
            p && !Object(types.a)(p, 34) && (model[k] = body[k]);
        }
        model.beforeFilter && await model.beforeFilter();
        const o = await repository.save(model);
        return o.afterFilter && await o.afterFilter(), o;
    }
    function msgpack(ctx, data) {
        ctx.type = "application/x-msgpack";
        const body = Array.isArray(data) ? data.length > 0 ? encode({
            fields: Object.keys(data[0]),
            values: data.map(v => Object.values(v))
        }) : null : null != data ? encode(data) : null;
        if ("GET" === ctx.method) {
            const et = body && external_etag_default()(body);
            if (et) {
                if (ctx.set("Cache-Control", "no-cache, max-age=31536000"), ctx.set("ETag", et), 
                ctx.headers["if-none-match"] && ctx.headers["if-none-match"].endsWith(et)) return void (ctx.status = 304);
            } else setNoStoreHeader(ctx);
        }
        ctx.body = body;
    }
    AdminRouter_AdminRouter = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.c)(6, external_typescript_ioc_.Inject), Object(tslib_es6.c)(7, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ Settings.Settings, DBConnection_DBConnection, install_InstallRoutes, login_LoginRoutes, resetpass_ResetPassRoutes, checkpass_CheckPassRoutes, resetemail_ResetEmailRoutes, AuthRegister_AuthRegister ]) ], AdminRouter_AdminRouter);
    let ApiRouter_ApiRouter = class ApiRouter extends external_koa_router_default.a {
        constructor(connection, {session, passport, passportSession}) {
            super(), this.use(session, passport, passportSession);
            for (let e of entities_entities) {
                const repository = connection.getRepository(e), access = check_entity_access(e), name = Object(external_param_case_.paramCase)(external_pluralize_default()(e.name)), singltonInstance = e.asyncProvider ? external_typescript_ioc_.Container.get(e) : null, saveModel = singltonInstance ? async (ctx, model) => {
                    const o = await data2Model(ctx, model, repository), {props} = e.rtti;
                    for (let p in props) singltonInstance[p] = o[p], Object(types.a)(props[p], 2) || delete o[p];
                    ctx.status = 201, msgpack(ctx, o);
                } : async (ctx, model) => {
                    const o = await data2Model(ctx, model, repository), {props} = e.rtti;
                    for (let p in props) Object(types.a)(props[p], 2) || delete o[p];
                    ctx.status = 201, msgpack(ctx, o);
                };
                this.get(name, "/" + name, access, async ctx => {
                    msgpack(ctx, await repository.find());
                }).get(`/${name}/:id`, access, async ctx => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) throw 404;
                    msgpack(ctx, model);
                }).put("/" + name, access, body_parse_msgpack, ctx => saveModel(ctx, new e)).patch(`/${name}/:id`, access, body_parse_msgpack, async ctx => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) throw 404;
                    await saveModel(ctx, model);
                }).delete("/" + name, access, async ctx => {
                    await repository.clear(), ctx.body = null;
                }).delete(`/${name}/:id`, access, async ctx => {
                    await repository.delete(ctx.params.id), ctx.body = null;
                });
            }
        }
    };
    ApiRouter_ApiRouter = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ DBConnection_DBConnection, AuthRegister_AuthRegister ]) ], ApiRouter_ApiRouter);
    const execAsync = Object(external_util_.promisify)(external_child_process_.exec);
    external_make_dir_default.a.sync(Object(data_path.a)("caches"));
    let App_App = class App {
        constructor(bot, admin, vkClient, settings, db, adminRouter, settingsRepository, apiRouter) {
            this.bot = bot, this.admin = admin, this.vkClient = vkClient, this.settings = settings, 
            this.db = db, this.dispose = signal => {
                console.log.bind(console, "(./src/App.ts:158)")("Terminate requested by " + signal);
                try {
                    utils.n.terminated_by = signal;
                } catch {}
                this.admin.dispose(), this.vkClient.dispose(), this.bot.dispose(), this.db.close().catch(console.error.bind(console, "(./src/App.ts:163)")).finally(() => process.exit(2));
            }, adminRouter.events.on("restart", this.dispose), bot.on("restart", this.dispose), 
            process.on("SIGINT", this.dispose), utils.n.last_post_published_at || (utils.n.last_post_published_at = new Date), 
            utils.n.restart_without_posts_after || (utils.n.restart_without_posts_after = settings.restartWithoutPostsAfter), 
            utils.n.check_restart_at || (utils.n.check_restart_at = settings.checkRestartAt), 
            RPC_RPC.register("channels", {
                dtypes: () => [ __webpack_require__(129).default, __webpack_require__(130), ...bot.channels.map(c => "declare " + c.dtypes) ].join(";")
            }), apiRouter.patch("tgclient_save", "/tgclient-save/:id", check_entity_access(ClientSettings.ClientSettings), body_parse_msgpack, smart_redirect, async ctx => {
                const body = ctx.request.body;
                ClientSettings.ClientSettings.insertValidate(body);
                try {
                    await this.bot.update({
                        ...body,
                        type: "bot"
                    }, ctx);
                } catch (err) {
                    throw console.error.bind(console, "(./src/App.ts:73)")(body, err), [ {
                        field: "token",
                        message: Object(string_tools_.stringify)(err)
                    } ];
                }
                try {
                    await this.admin.update({
                        ...body,
                        type: "user"
                    }, ctx);
                } catch (err) {
                    throw console.error.bind(console, "(./src/App.ts:82)")(body, err), [ {
                        field: "phone",
                        message: Object(string_tools_.stringify)(err)
                    } ];
                }
                const {vkPhone, vkPassword} = body;
                try {
                    await this.vkClient.update(body, ctx);
                } catch (err) {
                    console.error.bind(console, "(./src/App.ts:92)")(vkPhone, vkPassword, err);
                    const message = Object(string_tools_.stringify)(err);
                    throw [ {
                        field: "vkPhone",
                        message
                    }, {
                        field: "vkPassword",
                        message
                    } ];
                }
                const {props} = ClientSettings.ClientSettings.rtti;
                for (let k in body) {
                    const p = props[k];
                    p && !Object(types.a)(p, 98) && (settings[k] = body[k]);
                }
                await settingsRepository.save(settings), utils.n.restart_without_posts_after = body.restartWithoutPostsAfter, 
                utils.n.check_restart_at = body.checkRestartAt, ctx.status = 201, msgpack(ctx, settings);
            });
        }
        async init() {
            const {settings} = this;
            if (settings.phone && settings.token) {
                try {
                    await this.bot.update({
                        type: "bot",
                        ...settings
                    }), await this.admin.update({
                        type: "user",
                        ...settings
                    });
                } catch (err) {
                    console.error.bind(console, "(./src/App.ts:127)")(err), sendErrorEmail("Ошибка запуска telegram бота");
                } finally {
                    delete utils.n.terminated_by;
                }
                if (settings.vkPassword && settings.vkPhone) try {
                    await this.vkClient.update(settings);
                } catch (err) {
                    console.error.bind(console, "(./src/App.ts:137)")(err), sendErrorEmail("Ошибка запуска vk клиента. Проверьте телефон и пароль. По возможности НЕ используйте двухфакторную авторизацию.");
                }
                io_default.a.action("cleanup", cb => {
                    this.clean().then(_ => cb({
                        _
                    }));
                });
            }
        }
        clean() {
            return Promise.allSettled(this.settings.removeAfter ? [ this.admin.optimizeStorate(this.settings.removeAfterSeconds), this.bot.optimizeStorate(this.settings.removeAfterSeconds), this.bot.dropOldPosts(), "win32" !== process.platform && execAsync(`find ${__dirname}/static/files/* -ctime +${this.settings.removeAfter} -exec rm -rf {} \\;`) ] : []);
        }
    };
    App_App = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.c)(6, external_typescript_ioc_.Inject), Object(tslib_es6.c)(7, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ BotClient_BotClient, AdminClient_AdminClient, VKClient_VKClient, ClientSettings.ClientSettings, DBConnection_DBConnection, AdminRouter_AdminRouter, ClientSettings.ClientSettingsRepository, ApiRouter_ApiRouter ]) ], App_App);
    let core_Main = class Main {
        constructor(baseApp, authRegister, app, router, apiRouter, adminRouter) {
            this.baseApp = baseApp, this.authRegister = authRegister, this.app = app, this.router = router, 
            this.apiRouter = apiRouter, this.adminRouter = adminRouter;
        }
        async main() {
            var _a;
            const {router, apiRouter, adminRouter, app, baseApp} = this;
            await baseApp.init(), router.use(config.b, apiRouter.routes(), apiRouter.allowedMethods()), 
            router.use(config.a, adminRouter.routes(), adminRouter.allowedMethods()), app.use(router.routes()).use(router.allowedMethods()).use(external_koa_mount_default()(config.h, ctx => external_koa_send_default()(ctx, ctx.path, {
                root: Object(external_path_.normalize)(__dirname + "/static/"),
                maxage: 31536e6,
                gzip: !0,
                brotli: !1,
                setHeaders(res) {
                    res.setHeader("Access-Control-Allow-Origin", "http://" + config.d), res.setHeader("Cache-Control", "no-store, no-cache, max-age=0");
                }
            })));
            const server = Object(external_http_.createServer)(app.callback()), {passportComposed} = this.authRegister, wss = new external_ws_.Server({
                server,
                verifyClient: (info, callback) => {
                    try {
                        const ctx = app.createContext(info.req, null);
                        passportComposed(ctx, () => (callback(!!ctx.state.user), null)).catch(console.error);
                    } catch (err) {
                        console.error.bind(console, "(./src/core/index.ts:77)")(err);
                    }
                }
            });
            wss.on("connection", async (socket, request) => {
                try {
                    const ctx = app.createContext(request, null);
                    await passportComposed(ctx, () => {
                        let terminalSize = ctx.query.cols && ctx.query.rows ? {
                            cols: +ctx.query.cols || 100,
                            rows: +ctx.query.rows || 40
                        } : null;
                        return new ServerRPC_ServerRPC(socket, parseInt(ctx.query.tab), sessionSig(ctx), ctx.state.user, terminalSize), 
                        null;
                    });
                } catch (err) {
                    console.error.bind(console, "(./src/core/index.ts:100)")(err), socket.close();
                }
            }), Object(external_timers_.setInterval)(pingServers, 1e4, 8e3).unref();
            const [currentIp] = await Promise.all([ await get_my_ip(), new Promise(resolve => server.listen(config.f, resolve)) ]);
            console.log(`Current ip: ${currentIp} \nHost: ${config.g} []\nAdmin url: http://${config.d}` + config.a), 
            null === (_a = process.send) || void 0 === _a || _a.call(process, "ready");
        }
    };
    core_Main = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ App_App, AuthRegister_AuthRegister, Koa_Koa, MainRouter_MainRouter, ApiRouter_ApiRouter, AdminRouter_AdminRouter ]) ], core_Main), 
    dbInit().then(() => external_typescript_ioc_.Container.get(core_Main).main()).catch(err => {
        console.error.bind(console, "(./src/core/index.ts:123)")(err), process.exit(1);
    });
} ]);