//@ts-nocheck

const { name, regulus } = require('./package.json')

module.exports = {
    apps: [
        {
            "name": name + "-" + (regulus?.port || 7812),
            "namespace": regulus?.hostname + (regulus?.routes?.admin || "/adm"),
            "script": "./build/index.js",
            "watch": false,
            "source_map_support": false,
            "log_date_format": "YYYY.MM.DD HH:mm:ss",
            "combine_logs": true,
            "log_type": "json",
            "min_uptime": 60000,
            "exp_backoff_restart_delay": 1000,
            "vizion": false,
            "no_color": true
        }
    ]
}