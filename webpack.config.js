'use strict';

module.exports = require('./src/core/webpack.core')({}, {
    module: {
        rules: [{
            test: /\.shell$/,
            loader: 'raw-loader'
        }]
    }
});