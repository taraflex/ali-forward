if [ $(pm2 pid ali-forward-7812) ]; then
    cd /root/.local/share/ali-forward-7812/kfs
    WITHOUT=$(cat ./restart_without_posts_after)
    LAST=$(cat ./last_post_published_at | tr -d '"')
    if [ $(date -d "now - $WITHOUT second" +%s) -gt $(date -d $LAST +%s) ]; then
        jq -e "index( $(date +%H) )" ./check_restart_at && pm2 restart ali-forward-7812
    fi
fi