import { regTransform } from './utils';

export type PatchParam = string | Function | RegExp | true;

export function oneof(...s: readonly PatchParam[]) {
    return (notFist: number | boolean) => ['(', ...s.map(s => regTransform(s, notFist).concat('|')).flat().slice(0, -1), ')'];
}

export function maybeOneof(...s: readonly PatchParam[]) {
    return (notFist: number | boolean) => ['(', ...s.map(s => regTransform(s, notFist).concat('|')).flat().slice(0, -1), ')?'];
}

export function maybe(s: string | RegExp) {
    return (notFist: number | boolean) => ['(', ...regTransform(s, notFist), ')?'];
}

/**
 * Заменить подстроку в посте (фонетическое сравнение)
 */
export declare function replace(...s: readonly PatchParam[]): (literals: TemplateStringsArray, ...placeholders: any[]) => void
/**
 * Удалить подстроку из поста (фонетическое сравнение)
 */
export declare function del(...s: readonly PatchParam[]): void;
/**
 * Удалить подстроку из поста (фонетическое сравнение) и все что после
 */
export declare function delToEnd(...s: readonly PatchParam[]): void;
/**
 * Добавить строку в конец поста
 * 
 * @param {string} s что добавляем
 */
export declare function append(s: string): void;
/**
 * Добавить строку в начало поста
 * 
 * @param {string} s что добавляем
 */
export declare function prepend(s: string): void;