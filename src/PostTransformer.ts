import { load } from 'cheerio';
import rndString from 'crypto-random-string';
import escReg from 'escape-string-regexp';
import { HTTPError, RequestError } from 'got/dist/source';
import { STATUS_CODES } from 'http';
import memoizeLast from 'memoize-one';
import MDBX from 'node-mdbx';
import qs from 'qs';
import { Cookie, CookieJar } from 'tough-cookie';
import { Inject, Singleton } from 'typescript-ioc';

import { ClientSettings } from '@entities/ClientSettings';
import { LinksSettings } from '@entities/LinksSettings';
import { Photo, Picture, Post, PostRepository } from '@entities/Post';
import { stringify } from '@taraflex/string-tools';
import dataPath from '@utils/data-path';

import { BotClient } from './BotClient';
import { ETree, SUPPORTED_ENTITIES } from './ETree';
import {
    comap, dropHidden, equalArrays, escHtml, escTaggedString, html, http, kfs, last, waitResponse
} from './utils';

import type { formattedText } from 'tdlib-types';

const HTTPS_RE = /^https?:\/\//i;
const ALI_HOST_RE = /^(?:[a-z0-9\-]+\.)*aliexpress\.(?:ru|com)$/i;
const ALI_SCLICK_HOST_RE = /^s\.click\.aliexpress\.(?:ru|com)$/i;
const ALI_LOGIN_HOST_RE = /^login\.(?:[a-z0-9\-]+\.)*aliexpress\.(?:ru|com)$/i;

const getHostsChecker = (...hosts: string[]) => {
    const r = new RegExp('^' + hosts.map(host => '(?:[a-z0-9\\-]+\\.)*' + escReg(host)).join('|') + '$');
    return r.test.bind(r);
};
const getHostsCheckerWhite = memoizeLast(getHostsChecker);
const getHostsCheckerBlack = memoizeLast(getHostsChecker);
const getHostsCheckerBody = memoizeLast(getHostsChecker);

class PromiseCookieJar extends CookieJar {
    //@ts-ignore
    setCookie(cookieOrString: Cookie | string, currentUrl: string, options?: CookieJar.SetCookieOptions): Promise<Cookie> {
        options ||= {};
        options.ignoreError = true;
        return super.setCookie(cookieOrString, currentUrl, options);
    }
}

const r32hexs = rndString.bind(null, { length: 32, type: 'hex' });

type Widget = { widgetId: string, children?: readonly Widget[], props: { gallery?: { imageUrl?: string }[] } }
function wScan(widgets: readonly Widget[]) {
    let w = widgets.find(w => w.widgetId.startsWith('bx/SnowProductContextWidget/'))
    if (!w) {
        for (const child of widgets) {
            if (w = child.children && wScan(child.children)) {
                break;
            }
        }
    }
    return w;
}

@Singleton
export class PostTransformer {
    protected readonly mdbx = new MDBX({
        path: dataPath('caches/resolves'),
        maxDbs: 2,
        syncMode: 'safeNoSync',
        pageSize: 16 * 1024,
        valueMode: 'string',
    });

    constructor(
        @Inject protected readonly bot: BotClient,
        @Inject protected readonly postRepository: PostRepository,
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly linksSettings: LinksSettings,
    ) { }

    protected async processLink(pathname: string) {
        let data: any;
        try {
            const html = await http.get(`https://login.aliexpress.ru/sync_cookie_write.htm?acs_random_token=${r32hexs()}&xman_goto=` + encodeURIComponent('https://m.aliexpress.ru' + pathname), {
                resolveBodyOnly: true,
                responseType: 'text',
                cookieJar: new PromiseCookieJar(),
                timeout: this.settings.maxParseAliPreviewTime * 1000,
                headers: {
                    'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                    'Upgrade-Insecure-Requests': '1',
                    'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36'
                },
            });
            const script = load(html, { decodeEntities: false })('script[type="application/json"]').first().html();
            data = JSON.parse(script);
            const prev = wScan(data.widgets)?.props.gallery?.find(v => v.imageUrl)
            return prev?.imageUrl;
        } catch (e) {
            LOG_ERROR('https://m.aliexpress.ru' + pathname, stringify(e));
            if (data) {
                kfs[pathname] = data;
            }
        }
        return null;
    }

    protected cleanUrl(link: string, ru?: boolean): URL {
        const url = new URL(link);
        const { hostname, pathname, searchParams } = url;

        if (
            (pathname.startsWith('/redirect/cpa/o/') && searchParams.has('to')) || //backit.me - домены очень разные
            (hostname === 'aliclick.shop' && searchParams.has('to')) ||
            (hostname === 'vk.com' && pathname === '/away.php')
        ) {
            return this.cleanUrl(searchParams.get('to'));
        }
        if (/^(?:alitems\.[a-z]{2,}|ad\.admitad\.com)$/.test(hostname) && searchParams.has('ulp')) {
            return this.cleanUrl(searchParams.get('ulp'));
        }

        if (!ALI_HOST_RE.test(hostname)) {
            return url;
        }
        if (ALI_LOGIN_HOST_RE.test(hostname)) {
            if (searchParams.has('return_url')) {
                return this.cleanUrl(searchParams.get('return_url') /*|| searchParams.get('xman_goto')*/);//xman_goto вызывает бесконечный редирект - нельзя перепрыгивать
            }
        } else if (ALI_SCLICK_HOST_RE.test(hostname)) {
            if (pathname === '/deep_link.htm') {
                return this.cleanUrl(searchParams.get('dl_target_url'));
            }
            const afref = searchParams.get('afref')
            if (afref) {
                return this.cleanUrl(afref);
            }
        } else if (/^\/(?:item|store)\/(?!v3\/)/.test(pathname)) {
            if (ru) {
                url.hostname = url.hostname.replace(/^www\./, '').replace(/\.[a-z]{2,}$/, '.ru');
            }
            url.search = ''
        } else {
            //мобильные версии и страницы с купонами
            url.search = qs.stringify(qs.parse(url.search.slice(1)), { filter: this.linksSettings.allowedQueryStrings });
        }
        return url;
    }

    protected async compileText(ftext: formattedText, post: Post) {

        let s = escTaggedString(ftext.text);

        let had_tags = false;
        let has_ali = false;
        let fail = false;

        let has_links = false;
        const leafs = ftext.entities.filter(e => SUPPORTED_ENTITIES.has(e.type._)).map(e => {
            if (e.type._ === 'textEntityTypeHashtag') {
                had_tags = true;
            } else if (e.type._ === 'textEntityTypeUrl' || e.type._ === 'textEntityTypeTextUrl') {
                has_links = true;
            }
            return new ETree(s, e.offset, e.length, e.type);
        });

        if (!has_links) return { has_ali, fail, had_tags };

        leafs.push(new ETree(s, 0, s.length));
        leafs.sort((x, y) => {
            let diff = x.length - y.length;
            if (!diff && x.start === y.start && x.type?._.endsWith('Url')) diff = 1;
            return diff;
        });
        for (const leaf of leafs) {
            for (const l of leafs) {
                if (l.includes(leaf)) {
                    leaf.parent = l;
                    l.children.push(leaf);
                    break;
                }
            }
        }

        s = dropHidden(Array.from(last(leafs).content(0)).join(''));

        const blackRe = getHostsCheckerBlack(...this.linksSettings.hostsBlackList);
        const whiteRe = getHostsCheckerWhite(...this.linksSettings.hostsWhiteList);
        const bodyLinksRe = getHostsCheckerBody(...this.linksSettings.hostsFindLinkInBody);

        const previews = new Set<string>(post.ali_photos.map(p => p.url));

        let tpl: string = await this.mdbx.asyncTransact(txn => new Function('html', 'resolve', 'return html`' + s + '`;')(html, /*resolve*/ async (_source: string, title?: string) => {
            let source = _source.trim();
            const mayBeNotLink = !HTTPS_RE.test(source);
            if (mayBeNotLink) {
                source = 'http://' + source;
            }
            if (title && title.trim().toLowerCase().replace(HTTPS_RE, '') === source.toLowerCase().replace(HTTPS_RE, '')) {
                title = null;
            }
            let redirects = [source];
            try {
                let { hostname, href } = this.cleanUrl(source);
                if (whiteRe(hostname)) {
                    return title ? '${a(`' + href + '`, `' + title + '`)}' : '${a(`' + href + '`)}';
                }
                if (blackRe(hostname)) {
                    return;
                }
                if (href != source) {
                    redirects.push(href + '#sanitized');
                }
                const links = txn.getDbi('links');
                const cachedUrl = links.get(href);
                let url = cachedUrl && new URL(cachedUrl);
                if (!url) {

                    if (bodyLinksRe(hostname)) {
                        href = load(await http.get(href, { timeout: this.linksSettings.maxLinkRestoreTime * 1000, resolveBodyOnly: true }), { decodeEntities: false })('a').attr('href');
                        if (!href) {
                            return;
                        }
                        redirects.push(href + '#extracted');
                        const u = this.cleanUrl(href);
                        if (blackRe(hostname = u.hostname)) {
                            return;
                        }
                        if (href != u.href) {
                            href = u.href;
                            redirects.push(href + '#sanitized');
                        }
                    }

                    const request = http.stream(href, {
                        method: ALI_HOST_RE.test(hostname) ? 'HEAD' : 'GET',
                        cookieJar: new PromiseCookieJar(),
                        throwHttpErrors: false,
                        ignoreInvalidCookies: true,
                        timeout: this.linksSettings.maxLinkRestoreTime * 1000,
                        hooks: {
                            beforeRedirect: [options => {
                                const prev = options.url.href;
                                redirects.push(prev);
                                options.url = this.cleanUrl(prev) as any;
                                if (prev != options.url.href) {
                                    redirects.push(options.url.href + '#sanitized');
                                }
                                if (ALI_HOST_RE.test(options.url.hostname)) {
                                    options.method = 'HEAD';
                                } else if (blackRe(options.url.hostname)) {
                                    request.destroy();
                                }
                            }]
                        }
                    });
                    const response = await waitResponse(request);
                    if (!response) {
                        return;
                    }
                    const code = response.statusCode >= 500 && response.url.startsWith('https://aliexpress.ru/item/') ? 200 : response.statusCode;
                    if (uncachedCode(code)) {
                        throw new HTTPError(response);
                    }
                    url = this.cleanUrl(response.url, true);
                    url.hash = 'code=' + code;
                    if (url.href != cachedUrl) {
                        links.put(href, url.href);
                    }
                }
                const statusCode = +new URLSearchParams(url.hash.slice(1)).get('code') | 0;
                if (statusCode >= 400) {
                    throw new HTTPError({ statusCode, statusMessage: STATUS_CODES[statusCode] } as any);
                }
                if (ALI_HOST_RE.test(url.hostname)) {
                    url.hash = '';
                    has_ali = true;
                    const { pathname } = url;
                    if (pathname.startsWith('/item/')) {
                        const pcache = txn.getDbi('previews');
                        let preview = pcache.get(pathname);
                        if (!preview && previews.size < this.settings.maxPhotosCount + (previews.has('') ? 1 : 0)) {
                            if (preview = await this.processLink(pathname)) {
                                pcache.put(pathname, preview);
                            }
                        }
                        previews.add(preview || '');
                    }
                    return title ? '${a(`' + url.href + '`, `' + title + '`)}' : '${a(`' + url.href + '`)}';
                }
            } catch (err) {
                if (mayBeNotLink && err instanceof RequestError) {
                    return escHtml(_source);
                }
                fail ||= uncachedCode(err?.response?.statusCode | 0);
                err = stringify(err);
                if (fail) {
                    LOG_ERROR(post.url, redirects, err);
                }
                return `<pre><code class="language-fail">{${escHtml(err)}}</code></pre>`;
            }
            return `<pre><code class="language-fail">{${new URL(last(redirects)).hostname}}</code></pre>`;
        }));
        if (previews.has('')) {
            previews.delete('');
            if (previews.size < this.settings.maxPhotosCount) {
                fail = true;//todo не выставлять флаг если фото с али не нужно
            }
        }
        return { has_ali, had_tags, tpl: tpl.trim(), previews, fail };
    }

    async process<T>(post: Post, ftext: formattedText, picture: Picture, picSource: readonly T[], transform: (v: T, signal: AbortSignal) => Promise<Photo> | Photo) {
        const ac = new AbortController();
        const source_photos_promise = comap(picSource, transform, { required: this.settings.maxPhotosCount, concurrency: this.settings.maxPhotosConcurrency, breakOnError: true }, ac.signal);
        try {
            //todo сбрасывать post.fails после редактирования оригинала
            //нужно быть аккуратным с обновлением post.edited в базе, чтобы не вызвать бесконечную проверку сломанного поста
            const { tpl, previews, had_tags, fail, has_ali } = await this.compileText(ftext, post);

            if (post.tg ? has_ali : has_ali || fail) {

                if (
                    (tpl && tpl != post.text) ||
                    (previews.size > post.ali_photos.length) ||
                    (!fail && post.fails) ||
                    !equalArrays(post.source_photos, await source_photos_promise, post.tg ? 'local' : 'url')
                ) {
                    if (tpl) {
                        post.text = tpl;
                    }
                    post.had_tags ||= had_tags;
                    post.actual = !has_ali;
                    post.fails = fail ? (post.fails | 0) + 1 : 0;

                    post.ali_photos = Array.from(previews, url => ({ ...post.ali_photos.find(s => s.url === url), url }));
                    post.source_photos = await source_photos_promise;

                    if (post.type && post.type !== Picture.NONE_OR_VIDEO) {
                        picture = post.type;
                    } else if (post.video._ || this.bot.calcMask(post) === 0) {
                        picture = Picture.NONE_OR_VIDEO;
                    } else if (picture === Picture.SOURCE && post.source_photos.length < 2) {
                        picture = Picture.ALI;
                    }
                    await post.makeCollage(picture, this.settings);

                    return true;
                } else if (post.db && (fail || post.fails)) {
                    post.fails = fail ? (post.fails | 0) + 1 : 0;
                    if (!has_ali) {
                        post.actual = true;
                    }
                    await this.postRepository.update(
                        { id: post.id },
                        { fails: post.fails, actual: post.actual }
                    );
                }
            }
        } finally {
            ac.abort();
            await source_photos_promise;
        }
        return false;
    }
}

function uncachedCode(code: number) {
    return code === 0 || (code >= 400 && code !== 401 && code !== 403 && code !== 405);
}
