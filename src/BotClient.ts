import { Mutex } from 'async-mutex';
import dayjs from 'dayjs';
import emojiRegexFull from 'emoji-regex/es2015';
import emojiRegex from 'emoji-regex/es2015/RGI_Emoji';
import lastMatch from 'last-match';
import memoizeLast from 'memoize-one';
import { Between, LessThan } from 'typeorm';
import { Inject, Singleton } from 'typescript-ioc';

import { Channel, ChannelRepository } from '@entities/Channel';
import { ClientSettings } from '@entities/ClientSettings';
import { Patch, PatchInfo } from '@entities/Patch';
import { ChannelPostState, includes, Picture, Post, PostRepository } from '@entities/Post';
import { Settings, SettingsRepository } from '@entities/Settings';
import { Source, SourceRepository } from '@entities/Source';
import { AsyncUniqQueue } from '@utils/AsyncUniqQueue';
import delay from '@utils/delay';
import { ExtendableError } from '@utils/extendable-error';
import rndId from '@utils/rnd-id';
import { fit2size } from '@utils/trim-map2size';

import { BaseTelegramClient } from './BaseTelegramClient';
import { IDeffered, makeDeffer } from './Deffered';
import { LinkShortener } from './LinkShortener';
import { MultiRange } from './MultiRange';
import { comap, kfs, tgLink, toChatId } from './utils';

import type { formattedText, inlineKeyboardButton, inputMessageVideo$Input, message, ReplyMarkup, ReplyMarkup$Input, replyMarkupInlineKeyboard, Update } from 'tdlib-types';

class PromtCancelError extends ExtendableError { }

const EMOJI_RE = emojiRegexFull();

const genEmojiRe = memoizeLast((s: string) => {
    const m = s && s.match(emojiRegex());
    return Array.isArray(m) ? new RegExp(m.join('|'), 'ug') : null;
});

function* chunk<T>(it: T[], size: number) {
    const rows = Math.ceil(it.length / size);
    const minSize = it.length / rows | 0;
    let over = it.length - minSize * rows;
    while (it.length) {
        yield it.splice(0, over > 0 ? minSize + 1 : minSize);
        --over;
    }
}

function b64(c: Command, ...args: (number | string)[]) {
    return Buffer.from(JSON.stringify([c, ...args])).toString('base64');
}

function f64(data: string): [Command, string, ...any[]] {
    return JSON.parse(Buffer.from(data, 'base64').toString())
}

function filterButtons(...buttons: (inlineKeyboardButton | null)[]): inlineKeyboardButton[] | null {
    buttons = buttons.filter(Boolean);
    return buttons.length > 1 ? buttons : null;
}

function filterRows(...rows: (inlineKeyboardButton[] | null)[]): inlineKeyboardButton[][] {
    return rows.filter(Boolean);
}

const enum Icons {
    RED = '🔴',
    ORANGE = '🟠',
    GREEN = '🟢'
}

const enum Command {
    remove = 'd',
    publish = 'p',
    updateImage = 'i',
    refresh_tg = 'r',
    refresh_vk = 'v',
    digit = 't'
}

const BAD_ENTITIES = new Set(['textEntityTypeMention', 'textEntityTypeHashtag', 'textEntityTypeCashtag', 'textEntityTypeBotCommand', 'textEntityTypeMentionName', 'textEntityTypeUrl']);

@Singleton
export class BotClient extends BaseTelegramClient<{
    restart: string,
    refresh_vk: { id: string, fails: number },
    refresh_tg: { owner_id: number, message_id: number, fails: number }
}> {
    readonly chain = new AsyncUniqQueue<true>();
    private readonly cache: Map<string, Post> = new Map();
    protected patches: PatchInfo[] = [];

    channels: ReadonlyArray<Channel> = [];
    sources: Map<number, Source> = new Map();

    constructor(
        @Inject protected readonly linkShortener: LinkShortener,
        @Inject protected readonly coreSettings: Settings,
        @Inject protected readonly coreSettingsRepository: SettingsRepository,
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly channelRepository: ChannelRepository,
        @Inject protected readonly postRepository: PostRepository,
        @Inject protected readonly sourceRepository: SourceRepository,
        @Inject protected readonly patch: Patch,
    ) {
        super();
        this.loadPatches();
    }

    protected async getPost(post_id: string, p?: Post) {
        p = this.cache.get(post_id) || p || await this.postRepository.findOneOrFail(post_id);
        this.cache.delete(post_id);
        this.cache.set(post_id, p);
        fit2size(this.cache, 80);
        return p;
    }

    async findPost(id: string) {
        return this.cache.get(id) || await this.postRepository.findOne(id);
    }

    async savePost(post: Post, finalAction?: boolean) {
        await this.postRepository.save(post, { reload: !finalAction, listeners: false });
        if (finalAction) {
            this.chain.add(true);
        }
    }

    protected commands: Record<string, [string, () => void]> = {
        refresh5: ['🔄 5 постов с ошибками', () => this.refreshFailedPosts(5)],
        refresh25: ['🔄 25 постов с ошибками', () => this.refreshFailedPosts(25)],
        refresh50: ['🔄 50 постов с ошибками', () => this.refreshFailedPosts(50)],
        refresh_all: ['🔄 все посты с ошибками', () => this.refreshFailedPosts()],
        restart: ['Перезапустить бота', () => this.emit('restart', 'Bot')]
    };

    async setCommands() {
        const commands = Object.keys(this.commands).map(command => ({
            _: 'botCommand' as 'botCommand',
            command,
            description: this.commands[command][0]
        }))
        await this.client.invoke({ _: 'deleteCommands' })
        return Promise.all([
            this.client.invoke({
                _: 'setCommands',
                scope: {
                    _: 'botCommandScopeChat',
                    chat_id: this.coreSettings.admin_id
                },
                commands
            }),
            this.client.invoke({
                _: 'setCommands',
                scope: {
                    _: 'botCommandScopeChatAdministrators',
                    chat_id: this.coreSettings.supergroup_chat
                },
                commands
            })
        ]);
    }

    protected threadPromise = null;
    protected readonly afterUpdate = async () => {
        await this.loadChannels();
        await Promise.allSettled([...this.channels, this.coreSettings].map(s => s.supergroup_id && this.getSupergroupDomain(s.supergroup_id)));
        if (this.coreSettings.admin_id) {
            await this.getChat(this.coreSettings.admin_id);
        }
        this.threadPromise ||= this.thread().catch(LOG_ERROR);
    };

    calcMask(post: Readonly<Post>): number {
        try {
            return this.sources.get(post.owner_id)?.masker(post, includes.bind(post.text)) | 0
        } catch (err) {
            LOG_ERROR(post.url, err);
        }
        return 0;
    }

    protected promtReply: IDeffered<string> & { accumulator?: string[], id?: number };
    protected readonly promtMarkup: replyMarkupInlineKeyboard = {
        _: 'replyMarkupInlineKeyboard',
        rows: [...chunk<inlineKeyboardButton>(Array.from('789456123🆑0↩', d => ({
            _: 'inlineKeyboardButton',
            type: { _: 'inlineKeyboardButtonTypeCallback', data: b64(Command.digit, d) },
            text: d
        })), 3)]
    };

    protected updatePromtMarkup() {
        this.promtMarkup.rows[3][2].text = '[' + this.promtReply.accumulator.join('').padEnd(5, '●') + '] ↩';
        return this.promtMarkup;
    }

    protected async promtInternal(text: string, markup: ReplyMarkup$Input): Promise<string> {
        this.promtReply?.reject(new PromtCancelError());
        this.promtReply = makeDeffer();
        this.promtReply.accumulator = [];
        const { id } = await this.sendMessage(this.coreSettings.admin_id, text, markup);
        this.promtReply.id = id;
        return this.promtReply.promise.finally(() => {
            this.deleteMessage(id, this.coreSettings.admin_id).catch(LOG_ERROR);
        });
    }

    promtNum(text: string): Promise<string> {
        this.promtMarkup.rows[3][2].text = '↩';
        return this.promtInternal(text + '\n`Используйте только инлайн кнопки,` [причина](https://github.com/tdlib/td/issues/451#issuecomment-519343382)\n`Вводите код достаточно медленно для обновления введенных цифр на кнопке ↩`', this.promtMarkup);
    }

    promt(text: string): Promise<string> {
        return this.promtInternal(text, { _: 'replyMarkupForceReply' });
    }

    private readonly updateMarkupMutex = new Mutex();
    async updateMarkup(post: Post) {
        const message_id = post.getChannelMessageId(this.coreSettings.supergroup_id);
        if (message_id) {
            const unlock = await this.updateMarkupMutex.acquire();
            const chat_id = this.coreSettings.supergroup_chat;
            try {
                await this.client.invoke({ _: 'getMessage', chat_id, message_id });
                await this.client.invoke({
                    _: 'editMessageReplyMarkup',
                    chat_id,
                    message_id,
                    reply_markup: this.markup(post, this.calcMask(post))
                });
            } catch (err) {
                LOG_ERROR(tgLink(chat_id, message_id), err);
            } finally {
                unlock();
            }
        }
    }

    protected async[Command.updateImage](post: Post, pic: Picture) {
        if (await post.makeCollage(pic, this.settings)) {
            if ((await comap(Object.keys(post.channels), async channel => {
                const c = +channel;
                try {
                    if (post.getChannelMessageId(c) && c != this.coreSettings.supergroup_id) {
                        await this.publish(post, c);
                    }
                } catch (err) {
                    return post.setError('(__FILE__:__LINE__)', c, err);
                }
            })).length > 0) {
                await this.savePost(post);
            }
            return await this.publish(post, this.coreSettings.supergroup_id, this.calcMask(post));
        }
        await this.updateMarkup(post);
    }

    protected async[Command.remove](post: Post, channel: number) {
        const c = post.getChannelMessageId(channel);
        if (c && await this.deleteMessage(c, toChatId(channel)).then(
            () => post.channels[channel] = ChannelPostState.Deleted,
            err => post.setError('(__FILE__:__LINE__)', channel, err)
        )) {
            await this.savePost(post);
        }
        await this.updateMarkup(post);
    }

    protected async[Command.publish](post: Post, channel: number) {
        if (await this.publish(post, channel).catch(err => post.setError('(__FILE__:__LINE__)', channel, err))) {
            await this.savePost(post);
        }
        await this.updateMarkup(post);
    }

    protected [Command.refresh_tg](post: Post) {
        return this.emit('refresh_tg', post);
    }
    protected [Command.refresh_vk](post: Post) {
        return this.emit('refresh_vk', post);
    }

    protected readonly activeCommands = new Set<string>()

    protected readonly onUpdate = async (update: Update) => {
        if (update._ === 'updateNewCallbackQuery' && this.threadPromise) {
            const { payload, chat_id, message_id } = update;
            if (payload._ === 'callbackQueryPayloadData') {
                if (chat_id === this.coreSettings.supergroup_chat) {
                    const [cmd, post_id, arg] = f64(payload.data);
                    const hash = cmd + post_id;
                    if (!this.activeCommands.has(hash)) {
                        try {
                            this.activeCommands.add(hash);
                            await this[cmd](await this.getPost(post_id), arg);
                        } catch (err) {
                            LOG_ERROR(cmd, post_id, err);
                        } finally {
                            this.activeCommands.delete(hash);
                        }
                    }
                } else if (chat_id === this.coreSettings.admin_id && message_id === this.promtReply?.id) {
                    const [cmd, key] = f64(payload.data);
                    if (cmd === Command.digit) {
                        switch (key) {
                            case '↩': {
                                this.client.invoke({
                                    _: 'editMessageReplyMarkup', chat_id, message_id,
                                    reply_markup: { _: 'replyMarkupInlineKeyboard', rows: [] }
                                }).catch(Boolean);
                                return this.promtReply.fulfill(parseInt(this.promtReply.accumulator.join('').replace(/\D+/g, '')).toString());
                            }
                            case '🆑':
                                if (this.promtReply.accumulator.length) {
                                    this.promtReply.accumulator.length = 0;
                                    this.client.invoke({
                                        _: 'editMessageReplyMarkup', chat_id, message_id,
                                        reply_markup: this.updatePromtMarkup()
                                    }).catch(Boolean);
                                }
                                return;
                            default: {
                                this.promtReply.accumulator.push(key);
                                this.client.invoke({
                                    _: 'editMessageReplyMarkup', chat_id, message_id,
                                    reply_markup: this.updatePromtMarkup()
                                }).catch(Boolean);
                            }
                        }
                    }
                }
            }
        } else if (update._ === 'updateNewMessage' && !update.message.is_outgoing) {
            if (update.message.chat_id === this.coreSettings.supergroup_chat) {
                this.processSlashCommands(update.message);
            } else if (update.message.chat_id === this.coreSettings.admin_id) {
                const { content } = update.message;
                if (content._ === 'messageText') {
                    if (update.message.reply_to_message_id === this.promtReply?.id) {
                        return this.promtReply.fulfill(content.text.text.trim());
                    }
                    this.processSlashCommands(update.message);
                } else if (content._ === 'messageVideo' || content._ === 'messageAnimation') {
                    const id = content.caption?.text;
                    if (id) {
                        const post = await this.getPost(id);
                        post.actual = false;
                        await post.makeCollage(Picture.NONE_OR_VIDEO, this.settings);
                        const src = post.video.src;
                        post.video = await this.messageToMediaInput(update.message, this.settings.maxMediaDownloadTime);
                        if (src) {
                            post.video.src = src;
                        }
                        await this.savePost(post, true);
                    }
                }
            }
        }
    }

    protected processSlashCommands(message: message) {
        if (message.content._ === 'messageText' && this.threadPromise) {
            for (const e of message.content.text.entities) {
                if (e.type._ === 'textEntityTypeBotCommand') {
                    const cmd = message.content.text.text.substr(e.offset + 1, e.length - 1).split('@', 1)[0];
                    this.commands[cmd]?.[1]()
                    if (message.can_be_deleted_for_all_users) {
                        this.deleteMessage(message.id, message.chat_id).catch(LOG_ERROR);
                    }
                }
            }
        }
    }

    protected async thread() {
        for await (let _ of this.chain) {
            let post = await this.postRepository.findOne({ actual: false });
            if (!post) {
                continue;
            }
            this.chain.add(true);
            let wait = 0;
            let updated = false;
            try {
                post = await this.getPost(post.id, post);
                const mask = this.calcMask(post);
                for (const c of this.channels) {
                    try {
                        if (
                            ((mask & c.mask) && post.channels[c.supergroup_id] != ChannelPostState.Deleted) ||
                            post.getChannelMessageId(c.supergroup_id)
                        ) {
                            const { id, is_new, chat_id } = await this.publish(post, c.supergroup_id);
                            updated = true;
                            if (
                                is_new &&
                                c.repeate_message_after > 0 &&
                                (id % (c.repeate_message_after + 1)) === 0 &&
                                c.additional_text
                            ) {
                                await this.sendMessage(chat_id, c.additional_text);
                            }
                        }
                    } catch (err) {
                        if (post.setError('(__FILE__:__LINE__)', c.supergroup_id, err)) {
                            updated = true;
                        }
                    }
                }
                await this.publish(post, this.coreSettings.supergroup_id, mask);
                post.actual = updated = true;
            } catch (err) {
                if (err?.code === 429) {
                    wait = parseInt(lastMatch(err.message, /\d+$/)) | 0;
                }
                if (post.setError('(__FILE__:__LINE__)', this.coreSettings.supergroup_id, err)) {
                    updated = true;
                }
                if (post.actual = !!post.getChannelMessageId(this.coreSettings.supergroup_id)) {
                    updated = true;
                }
            } finally {
                updated && await this.savePost(post).catch(err => LOG_ERROR(post.url, err));
                if (wait) {
                    await delay(wait * 1000)
                }
            }
        }
    }

    async loadSources() {
        const c = await this.sourceRepository.find();
        this.sources = new Map(c.map(s => (s.buildMasker(this.channels), [s.id, s])));
    }

    async loadChannels() {
        this.channels = (await this.channelRepository.find()).sort((a, b) => a.order - b.order);
        await this.loadSources();
    }

    loadPatches() {
        this.patches = this.patch.run();
    }

    protected async getContent(post: Post, channel: Channel, forceRemoveEmoji?: boolean): Promise<formattedText> {

        const maxLength = post.video._ ? 1024 : 4096;

        const eRe = forceRemoveEmoji ? EMOJI_RE : genEmojiRe(this.patch.emoji);

        const data = this.client.execute({
            _: 'parseTextEntities',
            text: await this.linkShortener.processLinks(eRe ? post.text.replace((eRe.lastIndex = 0, eRe), '') : post.text, channel),
            parse_mode: { _: 'textParseModeHTML' }
        }) as formattedText;

        data.entities = data.entities.filter(e => !BAD_ENTITIES.has(e.type._));

        const m = new MultiRange(data.text, data.entities);

        if (channel) {
            for (const { offset, length, type } of data.entities.slice()) {
                if (type['language'] === 'fail' && length > 0) {
                    m.cut(offset, length);
                }
            }
        }

        for (const { re, arg } of this.patches) {
            m.applyPatch(re, arg);
        }

        if (!channel) {
            if (post.fails) {
                m.prependTag('error', ' ' + post.fails + '\n');
            }
            m.prependTag(post.sourceTag);
        }

        if (!forceRemoveEmoji && m.size >= maxLength) {
            return this.getContent(post, channel, true);
        }

        if (post.canDisplayPreview) {
            m.prepend('\u00A0', {
                _: 'textEntity',
                type: {
                    _: 'textEntityTypeTextUrl',
                    url: post.preview
                },
                offset: 0,
                length: 1
            });
        }

        return m.toText(maxLength);
    }

    protected markup(post: Post, mask: number): ReplyMarkup {
        const source = this.sources.get(post.owner_id);
        const url = post.tg && source?.domain ? source.url + '&post=' + post.message_id / 1048576 : post.url;
        return {
            _: 'replyMarkupInlineKeyboard',
            rows: filterRows(
                [{
                    _: 'inlineKeyboardButton',
                    type: { _: 'inlineKeyboardButtonTypeUrl', url },
                    text: (post.is_owner ? '😎' : '') +
                        (post.had_tags ? '#️⃣' : '') +
                        (post.fails ? Icons.RED : '') +
                        (source?.name || url)
                }, {
                    _: 'inlineKeyboardButton',
                    type: {
                        _: 'inlineKeyboardButtonTypeCallback',
                        // rndId() - позволяется всегда обновлять клавиатуру чтобы снять значок часов
                        data: b64(post.tg ? Command.refresh_tg : Command.refresh_vk, post.id, rndId())
                    },
                    text: dayjs(post.date * 1000).add(this.settings.timezoneOffset, 'hour').format('DD.MM H:mm') + (post.edited && post.edited != post.date ? '│' + dayjs(post.edited * 1000).add(this.settings.timezoneOffset, 'hour').format('DD.MM H:mm') : '') + ' 🔄'
                }],
                filterButtons(
                    {
                        _: 'inlineKeyboardButton',
                        type: {
                            _: 'inlineKeyboardButtonTypeCallback',
                            data: b64(Command.updateImage, post.id, Picture.NONE_OR_VIDEO)
                        },
                        text: (post.type === Picture.NONE_OR_VIDEO ? '🔘' : '') + (post.video._ ? '🎥' : '🚫')
                    },
                    post.source_photos.length && {
                        _: 'inlineKeyboardButton',
                        type: {
                            _: 'inlineKeyboardButtonTypeCallback',
                            data: b64(Command.updateImage, post.id, Picture.SOURCE)
                        },
                        text: post.type === Picture.SOURCE ?
                            `🔘${Picture.SOURCE} [${post.previewsCount} из ${post.source_photos.length}]` :
                            `${Picture.SOURCE} [${post.source_photos.length}]`,
                    },
                    post.ali_photos.length && {
                        _: 'inlineKeyboardButton',
                        type: {
                            _: 'inlineKeyboardButtonTypeCallback',
                            data: b64(Command.updateImage, post.id, Picture.ALI)
                        },
                        text: post.type === Picture.ALI ?
                            `🔘${Picture.ALI} [${post.previewsCount} из ${post.ali_photos.length}]` :
                            `${Picture.ALI} [${post.ali_photos.length}]`
                    }
                ),
                ...chunk<inlineKeyboardButton>(this.channels.map(c => {
                    const state = post.channels[c.supergroup_id];
                    return {
                        _: 'inlineKeyboardButton',
                        type: {
                            _: 'inlineKeyboardButtonTypeCallback',
                            data: b64(post.getChannelMessageId(c.supergroup_id) ? Command.remove : Command.publish, post.id, c.supergroup_id)
                        },
                        text: (
                            state > ChannelPostState.Deleted ? Icons.GREEN : (
                                state === ChannelPostState.Error ? Icons.RED : (
                                    state < ChannelPostState.Error ? Icons.ORANGE : ''
                                )
                            )
                        ) + ((mask & c.mask) ? c.title : '🔸' + c.title),
                    }
                }), this.settings.buttonsPerRow)
            )
        }
    }

    protected async publish(post: Post, supergroup_id: number, mask: number = 0): Promise<message & { is_new?: boolean }> {
        const isAdminChannel = supergroup_id === this.coreSettings.supergroup_id;
        const unlock = isAdminChannel ? await this.updateMarkupMutex.acquire() : null;
        try {
            const caption = await this.getContent(post, isAdminChannel ? null : this.channels.find(c => c.supergroup_id === supergroup_id));
            const reply_markup = isAdminChannel ? this.markup(post, mask) : undefined;
            const chat_id = toChatId(supergroup_id);
            const message_id = post.getChannelMessageId(supergroup_id);

            if (message_id) {
                await this.client.invoke({//sync messages with server
                    _: 'getMessage',
                    chat_id,
                    message_id
                });

                if (post.video._) {
                    return this.client.invoke({
                        _: 'editMessageMedia',
                        chat_id,
                        message_id,
                        reply_markup,
                        input_message_content: post.type === Picture.NONE_OR_VIDEO ? { ...post.video as inputMessageVideo$Input, caption } : {
                            _: 'inputMessagePhoto',
                            caption,
                            photo: { _: 'inputFileLocal', path: post.previewLocal }
                        }
                    });
                } else {
                    return await this.client.invoke({
                        _: 'editMessageText',
                        chat_id,
                        message_id,
                        reply_markup,
                        input_message_content: {
                            _: 'inputMessageText',
                            disable_web_page_preview: !post.canDisplayPreview,
                            text: caption
                        }
                    });
                }
            } else {
                const m = post.video._ ? //todo для админа ttl выставить === this.settings.removeAfter
                    await this.sendMessage(chat_id, post.type === Picture.NONE_OR_VIDEO ? { ...post.video as inputMessageVideo$Input, caption } : {
                        _: 'inputMessagePhoto',
                        caption,
                        photo: { _: 'inputFileLocal', path: post.previewLocal }
                    }, reply_markup) :
                    await this.sendMessage(chat_id, {
                        _: 'inputMessageText',
                        text: caption,
                        clear_draft: false,
                        disable_web_page_preview: !post.canDisplayPreview
                    }, reply_markup);
                post.channels[supergroup_id] = m.id;

                if (isAdminChannel) {
                    kfs.last_post_published_at = new Date();

                    this.refreshFailedPosts(2);
                }

                return Object.assign(m, { is_new: true });
            }
        } finally {
            unlock?.();
        }
    }

    protected refreshing = false;
    protected async refreshFailedPosts(take?: number) {
        try {
            if (this.refreshing) return;
            this.refreshing = true;
            const posts = await this.postRepository.find({
                select: ['id', 'owner_id', 'fails'],
                where: { fails: Between(1, this.settings.maxParseTries - 1) },
                order: { date: 'DESC' },
                take
            });
            for (const post of posts) {
                if (post.tg) {
                    await this.emit('refresh_tg', post)
                } else {
                    await this.emit('refresh_vk', post)
                }
            }
        } catch (err) {
            LOG_ERROR(err);
        } finally {
            this.refreshing = false
        }
    }

    incPostFails(id: string, fails?: number) {
        return this.postRepository.update(
            { id },
            { fails: fails >= this.settings.maxParseTries ? fails + 1 : this.settings.maxParseTries }
        )
    }

    dropOldPosts() {
        return this.settings.removeAfter > 0 && this.postRepository.delete({
            edited: LessThan(Math.round(Date.now() / 1000) - this.settings.removeAfterSeconds),
        }).catch(LOG_ERROR);
    }
}