import Emittery from 'emittery';
import { Context } from 'koa';
import playwright, { Page } from 'playwright-firefox';
import {
    FormattedText, inputMessageAnimation$Input, inputMessageVideo$Input, inputThumbnail$Input
} from 'tdlib-types';
import { Inject, Singleton } from 'typescript-ioc';
import { URL } from 'url';
import { PhotoAttachment, VK } from 'vk-io';

import { ClientSettings } from '@entities/ClientSettings';
import { Photo, Picture, Post } from '@entities/Post';
import { Settings, SettingsRepository } from '@entities/Settings';
import { fromCtx } from '@rpc/ServerRPC';
import { AsyncUniqQueue } from '@utils/AsyncUniqQueue';
import delay from '@utils/delay';
import { ExtendableError } from '@utils/extendable-error';

import { AriaClient } from './AriaClient';
import { BotClient } from './BotClient';
import { loadCookies, saveCookies } from './cookie-parser';
import { loadPhotos, PSIZE } from './photos-utils';
import { PostTransformer } from './PostTransformer';
import { DeepMerge, ffprobe, findTypeOf, last, sortByKey } from './utils';

import type { ElMessageBox } from 'element-ui/types/message-box';
import type { IDisposable } from 'xterm';
import type { BaseImage, DocsDoc, VideoVideoImage, WallWallpostAttachment, WallWallpostFull } from 'vk-io/lib/api/schemas/objects';

const officialAppCredentials = {
    android: {
        clientId: '2274003',
        clientSecret: 'hHbZxrka2uZ6jB1inYsH'
    },
    windows: {
        clientId: '3697615',
        clientSecret: 'AlVXZFMUqyrnABp8ncuU'
    },
    windowsPhone: {
        clientId: '3502557',
        clientSecret: 'PEObAuQi6KloPM4T30DV'
    },
    iphone: {
        clientId: '3140623',
        clientSecret: 'VeWdmVclDCtn6ihuP1nt'
    },
    ipad: {
        clientId: '3682744',
        clientSecret: 'mY6CDUswIVdJLCD3j15n'
    },
    vkMe: {
        clientId: '6146827',
        clientSecret: 'qVxWRF1CwHERuIrKBnqe'
    }
};

const enum Scopes {
    notify = 1,
    friends = 2,
    photos = 4,
    audio = 8,
    video = 16,
    pages = 128,
    link = 256,
    status = 1024,
    notes = 2048,
    messages = 4096,
    wall = 8192,
    ads = 32768,
    offline = 65536,
    docs = 131072,
    groups = 262144,
    notifications = 524288,
    stats = 1048576,
    email = 4194304,
    market = 134217728
}

function merge(post: Post, v: { url?: string, src?: string, width?: number, height?: number }): Photo {
    const url = v?.url || v?.src;
    return url && {
        width: v.width,
        height: v.height,
        ...post.source_photos.find(s => s.url === url),
        url
    };
}

function areaCompare(a: BaseImage, b: BaseImage) {
    a['_area'] ||= a.width * a.height;
    b['_area'] ||= b.width * b.height;
    return a['_area'] - b['_area'];
}

function isValidVideoPreview(p: Photo) {
    return p && p.size && /\.jpe?g$/i.test(p.local) && p.width <= 320 && p.height <= 320 && p.size < 200 * 1024;
}

class VKAuthError extends ExtendableError { }

const PREVIEW_SIZES = ['y', 'x', 'm', 's'] as const;
const NAV_OPTIONS = { timeout: 10000, waitUntil: 'domcontentloaded' } as const;
const VK_LINK_RE = /\b(?:https?:\/{2})?(?:[a-z\d][a-z\d-]*\.)+[a-z]{2,}(?::\d{2,5})?(?:\/[-\w.~:/?[\]@!$&'()*+,;=%]*\w\/?)?/g;
const VK_NAMED_LINK_RE = new RegExp('\\[' + VK_LINK_RE.source + '\\|[^\\]]+\\]', VK_LINK_RE.flags);

async function processPage(page: Page, phone: string, password: string, twoFaHandler: (payload: string) => Promise<string>) {
    const $phone = await page.$('input[name="email"]');
    if ($phone) {
        await $phone.fill(phone);
    }
    const $password = await page.$('input[name="pass"]');
    if ($password) {
        await $password.fill(password);
    }
    const $code = await page.$('input[name="code"]');
    if ($code) {
        const code = await twoFaHandler('```\n' + await page.innerText('.fi_row') + '\n```');
        await $code.fill(code);
    } else if (!$password || !$phone) {
        throw new VKAuthError(await page.innerText('body'));
    }
    await Promise.all([page.waitForNavigation(NAV_OPTIONS), page.click('input[type="submit"]')]);
}

async function getAccess(phone: string, password: string, page: Page, twoFaHandler: (payload: string) => Promise<string>): Promise<string> {
    let lastError = '';
    for (const params: NodeJS.Dict<string> = {
        grant_type: 'password',
        client_id: officialAppCredentials.android.clientId,
        client_secret: officialAppCredentials.android.clientSecret,
        username: phone,
        password,
        scope: (Scopes.offline | Scopes.groups | Scopes.photos | Scopes.video | Scopes.wall | Scopes.docs).toString(),
        //test_redirect_uri: '1',
        v: '5.126',
        '2fa_supported': '1'
    }; ;) {
        const resp = await page.goto('https://oauth.vk.com/token?' + new URLSearchParams(params).toString());
        delete params.code;
        let { access_token, error, redirect_uri, validation_type, phone_mask, error_description } = (await resp.json()) as NodeJS.ReadOnlyDict<string>;

        if (error === 'need_validation' && validation_type) {
            const code = await twoFaHandler((lastError && '```\n' + lastError + '\n```\n') + 'Пожалуйста, введите код двуфакторной авторизации из ' + (validation_type === "2fa_app" ? "личного сообщения от Администрации или из приложения для генерации кодов или воспользуйтесь резервными кодами.\nДля принудительной отправки кода по sms вместо приложения введите /sms (не рекомендуется, так как по sms коды часто не доходят)" : `sms (отправлено на номер \`${phone_mask}\`) или воспользуйтесь резервными кодами. Рекомендуется включить в [настройках безопасности](https://vk.com/settings?act=security) вход через приложение для генерации кодов, так как по sms коды часто не доходят.`));
            if (code === '/sms') {
                params.force_sms = '1';
            } else {
                params.code = code;
            }
        } else if (error === 'need_validation') {
            await page.goto(redirect_uri, NAV_OPTIONS);
            while (!page.url().includes('success=1')) {
                await processPage(page, phone, password, twoFaHandler);
                if (page.url().includes('fail=1')) {
                    throw new VKAuthError(await page.innerText('body'));
                }
            }
            access_token = new URLSearchParams(new URL(page.url()).hash.slice(1)).get('access_token');
        } else if (error) {
            lastError = `[${error}]: ` + error_description;
        }
        if (access_token) {
            return access_token;
        }
    }
}

type media$Input = DeepMerge<inputMessageVideo$Input, inputMessageAnimation$Input>;

function toFormattedText(text: string): FormattedText {
    VK_LINK_RE.lastIndex = 0;
    return {
        _: 'formattedText',
        text,
        entities: Array.from(text.matchAll(VK_LINK_RE), ({ 0: s, index }) => ({
            _: 'textEntity',
            offset: index,
            length: s.length,
            type: { _: 'textEntityTypeUrl' },
        })),
    }
}

@Singleton
export class VKClient extends Emittery<{ media: media$Input }> implements IDisposable {
    protected client: VK;
    protected readonly queue = new AsyncUniqQueue<number>();

    constructor(
        @Inject protected readonly ptransformer: PostTransformer,
        @Inject protected readonly aria: AriaClient,
        @Inject protected readonly bot: BotClient,
        @Inject protected readonly coreSettings: Settings,
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly coreSettingsRepository: SettingsRepository
    ) {
        super();
    }

    dispose() {
        this.bot.clearListeners('refresh_vk');
        this.client?.updates.stop();
        this.client = null;
    }

    remove(id: number) {
        this.queue.delete(id);
    }

    add(id: number) {
        if (id) {
            this.queue.add(id, true);
        }
    }

    async getGroupInfo(screen_name: string) {
        screen_name = screen_name.replace(/^(https?:\/\/)?(m\.)?vk\.com\//i, '');
        const { object_id } = await this.client.api.utils.resolveScreenName({ screen_name });
        return (await this.client.api.groups.getById({ group_id: object_id.toString() }))?.[0];
    }

    fillQueue() {
        if (this.client) {
            for (const v of this.bot.sources.values()) {
                if (!v.tg) {
                    this.queue.add(v.id);
                }
            }
        }
    }

    public async processMessageById({ id, fails }: { id: string, fails: number }) {
        try {
            const [message] = await this.client.api.wall.getById({ posts: id });
            if (message) {
                await this.processMessage(message, true);
            } else {
                await this.bot.incPostFails(id, fails);
            }
        } catch (err) {
            LOG_ERROR('https://vk.com/wall' + id, err);
        }
    }

    public async processMessage(message: WallWallpostFull, force?: boolean, picture?: Picture) {
        if ((message.edited || message.date) + this.settings.removeAfterSeconds < Date.now() / 1000) {
            return;
        }
        const id = message.owner_id.toString() + '_' + message.id.toString();
        try {
            let post = await this.bot.findPost(id);
            if (
                !post ||
                force ||
                message.edited > post.edited ||
                (post.fails && post.fails < this.settings.maxParseTries)
            ) {
                post ||= new Post();
                post.id = id;
                post.date = message.date;
                post.owner_id = -message.owner_id;
                post.from_id = -message.from_id;

                VK_NAMED_LINK_RE.lastIndex = 0;
                message.text = message.text
                    .replace(VK_NAMED_LINK_RE, '')
                    .replace(/#[\p{L}\p{N}_]+/ugi, () => (post.had_tags = true, ''));

                let protectedVideos = new Map<string, Photo>();
                let videos: { url: string, poster?: Photo }[] = [];

                if (await this.ptransformer.process(
                    post,
                    toFormattedText(message.text),
                    picture || post.type,
                    message.attachments || [],
                    (att: WallWallpostAttachment) => {
                        switch (att?.type) {
                            case 'photo': return merge(post, findTypeOf(PREVIEW_SIZES, (att.photo as PhotoAttachment).sizes));
                            case 'doc': {
                                const { type, preview, url } = att.doc as DocsDoc;
                                const photo = preview.photo as PhotoAttachment;
                                switch (type) {
                                    // image
                                    case 4: return merge(post, findTypeOf(PREVIEW_SIZES, photo.sizes) || { url });
                                    // video
                                    case 6: {
                                        videos.unshift({ url });
                                        return null;
                                    }
                                    // gif
                                    case 3: {
                                        videos.unshift({
                                            url: preview.video?.src || url,
                                            // used 'src' instead of 'url'
                                            poster: merge(post, findTypeOf<any>(['q', 'p', 'm', 's'], photo.sizes))
                                        });
                                        return merge(post, findTypeOf<any>(PREVIEW_SIZES, photo.sizes))
                                    }
                                }
                            }
                            case 'video': {
                                const images = (att.video.image as VideoVideoImage[]).filter(i => !i.with_padding).sort(areaCompare);

                                protectedVideos.set(att.video.owner_id + '_' + att.video.id, merge(post,
                                    images.reverse().find(i => i.width <= 320 && i.height <= 320)
                                ));

                                return merge(post, images.find(i => i.width >= PSIZE.WIDTH || i.height >= PSIZE.HEIGHT) || last(images));
                            }
                        }
                    })) {

                    post.edited = message.edited || message.date;

                    if (videos.length || protectedVideos.size) {
                        const vs = await this.client.api.video.get({ videos: Array.from(protectedVideos.keys()), count: protectedVideos.size });

                        for (const item of vs.items) {

                            if (item.live) continue;

                            const { files, converting } = item;
                            let url = files?.mp4_720;
                            if (converting && !url) {
                                return;
                            }
                            if (url ||= files && (files.mp4_480 || files.mp4_360 || files.mp4_240)) {
                                videos.push({ url, poster: protectedVideos.get(item.owner_id + '_' + item.id) });
                            }
                        }

                        if (videos.some(v => v.url === post.video.src)) {
                            videos.length = 0;
                        }

                        for (let v of sortByKey(videos, 'url')) {
                            const path = await this.aria.download(v.url);
                            if (path) {
                                const info = await ffprobe(path);
                                const videoParams = info?.streams?.find(s => s.codec_type === 'video');
                                if (!videoParams) continue;

                                post.video.src = v.url;
                                post.actual = true;

                                const { width, height } = videoParams;
                                const hasAudio = info.streams.some(s => s.codec_type === 'audio');

                                const thumbnail = await this.loadPoster(post, v.poster);
                                await this.bot.savePost(post);
                                await this.emit('media', {
                                    _: hasAudio ? 'inputMessageVideo' : 'inputMessageAnimation',
                                    supports_streaming: hasAudio ? true : undefined,
                                    video: hasAudio ? { _: 'inputFileLocal', path } : undefined,
                                    animation: hasAudio ? undefined : { _: 'inputFileLocal', path },
                                    thumbnail,
                                    width,
                                    height,
                                    caption: post.idCaption
                                });
                                return;
                            }
                        }
                    }
                    return await this.bot.savePost(post, true);
                }
            }
            if (force && post?.db) {
                await this.bot.updateMarkup(post);
            }
        } catch (err) {
            LOG_ERROR('https://vk.com/wall' + id, err);
        }
    }

    protected async thread() {
        this.fillQueue();

        const needExtendedCheck = new Set(Array.from(this.bot.sources.values(), v => v.tg ? 0 : v.id));
        needExtendedCheck.delete(0);

        for await (let id of this.queue) {
            const source = this.bot.sources.get(id);
            try {
                if (!source) {
                    continue;
                }
                const { items } = await this.client.api.wall.get({
                    owner_id: -id,
                    offset: 0,
                    count: needExtendedCheck.has(id) ? this.settings.vkPostCountAfterRestart : this.settings.vkPostCount
                });
                needExtendedCheck.delete(id);
                for (let message of items) {
                    if (!message.marked_as_ads) {
                        await this.processMessage(message, false, source.picture);
                    }
                }
            } catch (err) {
                LOG_ERROR(source.url, err);
            } finally {
                await delay(this.settings.vkRequestTimeout * 1000);
                if (this.queue.size < 1) {
                    this.fillQueue();
                }
            }
        }
    }

    protected async loadPoster(post: Readonly<Post>, poster: Photo): Promise<inputThumbnail$Input> {
        if (poster) {
            poster = (await loadPhotos([poster], this.settings)).find(isValidVideoPreview);
        }
        poster ||= (await loadPhotos(post.source_photos, this.settings)).find(isValidVideoPreview);
        return poster ? {
            _: 'inputThumbnail',
            thumbnail: { _: 'inputFileLocal', path: poster.local },
            width: poster.width,
            height: poster.height
        } : undefined
    }

    protected async getToken(phone: string, password: string, ctx?: Context) {

        if (
            this.coreSettings.vk.token &&
            phone === this.coreSettings.vk.phone &&
            password === this.coreSettings.vk.password
        ) {
            return this.coreSettings.vk.token;
        }

        const twoFaHandler = ctx ? async (payload: string) => {
            const { value } = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt(payload, {
                showCancelButton: false,
                roundButton: true
            }, 'Vk Auth');
            return value as string;
        } : (payload: string) => this.bot.promt('*Vk Auth*\n' + payload);

        const browser = await playwright.firefox.launch(/*{ headless: false }*/);
        const context = await browser.newContext({
            ...playwright.devices['Pixel 2'],
            userAgent: AriaClient.userAgent,
            isMobile: false,
            javaScriptEnabled: false,
            locale: 'ru-RU',
            ignoreHTTPSErrors: true,
            permissions: [],
        });
        try {
            await context.addCookies(await loadCookies(AriaClient.cookiePath));
            await context.route('**', (route, request) => {
                const t = request.resourceType();
                if (('document' !== t && 'script' !== t)) {
                    route.fulfill({ status: 201 });
                } else {
                    route.continue();
                }
            });
            const page = await context.newPage();

            await page.goto('https://m.vk.com/docs', NAV_OPTIONS);
            while (page.url().startsWith('https://m.vk.com/login')) {
                await processPage(page, phone, password, twoFaHandler);
            }

            const token = await getAccess(phone, password, page, twoFaHandler);

            this.coreSettings.vk = { phone, password, token };
            await this.coreSettingsRepository.save(this.coreSettings);
            return token;
        } finally {
            await saveCookies(await context.cookies(), AriaClient.cookiePath);
            browser.close().catch(LOG_ERROR);
        }
    }

    protected threadPromise = null;
    async update(settings: ClientSettings, ctx?: Context) {
        if (
            !this.client ||
            !this.coreSettings.vk.token ||
            settings.vkPhone !== this.coreSettings.vk.phone ||
            settings.vkPassword !== this.coreSettings.vk.password
        ) {
            const token = await this.getToken(settings.vkPhone, settings.vkPassword, ctx);

            this.dispose();

            this.client = new VK({
                token,
                apiHeaders: {
                    'User-Agent': AriaClient.userAgent
                }
            });

            this.threadPromise ||= this.thread().catch(LOG_ERROR);

            this.bot.on('refresh_vk', id => this.processMessageById(id));
        }
    }
}