import { Column, Entity, PrimaryColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';

import { tgLink, toChatId } from '../utils';

@Access({ GET: 0, DELETE: 0, PATCH: 0 }, { display: 'table', icon: '_telegram-plane', order: 5 })
@Entity()
export class Channel {
    @v({ description: 'Название' })
    @v({ state: RTTIItemState.READONLY })
    @Column({ default: '' })
    title: string;

    get slug() {
        return '$' + this.title.replace(/[^\p{L}\p{Nl}$\p{Mn}\p{Mc}\p{Nd}\p{Pc}]+/ug, '_');
    }

    get dtypes() {
        return `const ${this.slug} = ${this.mask}`;
    }

    @v({ description: 'Порядок кнопок' })
    @Column({ default: 0, type: 'int' })
    order: number;

    @v({ state: RTTIItemState.HIDDEN })
    @PrimaryColumn({ type: 'bigint' })
    supergroup_id: number;

    @v({ state: RTTIItemState.HIDDEN })
    @Column({ default: 0, type: 'int', unique: true })
    mask: number;

    @v({ pattern: /^\w{1,31}$/, description: 'Tracking ID [отсюда](https://portals.aliexpress.com/trackIdManage.htm)' })
    @Column({ default: 'aligod_xyz_ru' })
    trackingId: string;

    @v({ description: 'Повторять сообщение каждые' })
    @Column({ default: 0, type: 'int', unsigned: true })
    repeate_message_after: number;

    @v({ type: 'md-tgbotlegacy', state: RTTIItemState.FULLWIDTH, description: 'Повторяемое сообщение (markdown текст)' })
    @Column({ default: '' })
    additional_text: string;

    @v({ empty: false, state: RTTIItemState.FULLWIDTH, description: 'Шаблон преобразования ссылок (для обновления в старых постах требуется перепубликация). Пример _https://clck.ru/--?url=${encodeURIComponent(link)}_' })
    @Column({ default: '${link}' })
    urlTemplate: string;

    get url() {
        return tgLink(toChatId(this.supergroup_id));
    }
}

export abstract class ChannelRepository extends Repository<Channel>{ }