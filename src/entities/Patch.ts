import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';

import { maybe, maybeOneof, oneof, PatchParam } from '../patch-utils';
import { regTransform } from '../utils';

export type PatchInfo = { re: RegExp, arg: string };

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'tools', order: 6 })
@SingletonEntity()
export class Patch {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ description: 'Emoji для удаления' })
    @Column({ default: '♠️💙💚🔥❤️✅❗️💥↩️↪️⚡⁠🌎' })
    emoji: string;

    @v({ description: 'Js скрипт патчей (_CTRL+D_ - автоформатирование кода) [документация](/static/docs/modules/_src_patch_utils_.html) (_CTRL+D_ - автоформатирование кода)', type: 'javascript' })
    @Column({ default: '' })
    script: string;

    run(): PatchInfo[] {
        const patches: PatchInfo[] = [];
        function replace(...s: readonly PatchParam[]) {
            const re = new RegExp(s.map(regTransform).flat().join(''), 'ugis');
            return (literals: TemplateStringsArray, ...placeholders: any[]) => {
                let arg = literals.raw[0];
                for (let i = 0; i < placeholders.length; i++) {
                    arg += placeholders[i];
                    arg += literals.raw[i + 1];
                }
                patches.push({ re, arg });
            }
        }
        new Function('maybeOneof', 'oneof', 'maybe', 'replace', 'del', 'delToEnd', 'append', 'prepend', this.script)(
            maybeOneof,
            oneof,
            maybe,
            replace,
            function del(...s: readonly PatchParam[]) {
                replace(...s)``;
            },
            function delToEnd(...s: readonly PatchParam[]) {
                replace(...s, true)``;
            },
            function append(arg: string) {
                patches.push({ re: /$/, arg });
            },
            function prepend(arg: string) {
                patches.push({ re: /^/, arg });
            },
        );
        return patches;
    }
}

export abstract class PatchRepository extends Repository<Patch>{ }