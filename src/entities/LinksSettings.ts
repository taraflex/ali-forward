import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';
import { SingletonEntity } from '@ioc';

export enum AdPlatforms {
    None = 'None',
    Admitad = 'Admitad',
    AEPlatform = 'AEPlatform',
    Backit = 'Backit',
}

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'external-link-square-alt', order: 3 })
@SingletonEntity()
export class LinksSettings {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @v({ description: 'Рекламная платформа', enum: [AdPlatforms.Admitad, AdPlatforms.AEPlatform, AdPlatforms.Backit, AdPlatforms.None] })
    @Column({ default: AdPlatforms.None })
    readonly adplatform: AdPlatforms;

    @v({ pattern: /^https?:\/\/.+[^/]$/, description: 'Url сокращалки' })
    @Column({ default: 'http://alik.cf' })
    readonly shorbyUrl: string;

    @v({ min: 1, description: 'Таймаут сокращения ссылки (ссылки в посте обрабатываются ПОСЛЕДОВАТЕЛЬНО)' })
    @Column({ unsigned: true, default: 10 })
    readonly maxLinkShortTime: number;

    @v({ min: 1, description: 'Таймаут получения оригинальной ссылки из сокращенной (ссылки в посте обрабатываются ПАРАЛЛЕЛЬНО)' })
    @Column({ unsigned: true, default: 25 })
    readonly maxLinkRestoreTime: number;

    //AEPlatforms

    @v({ description: '**AEPlatforms** user id [взять тут](https://console.aliexpress.com/)' })
    @Column({ unsigned: true, type: 'int', default: 611 })
    readonly aeUserId: number;

    @v({ description: '**AEPlatforms** client id [взять там же](https://aeplatform.ru/ru/profile/clients)' })
    @Column({ unsigned: true, type: 'int', default: 123645 })
    readonly aeClientId: number;

    @v({ pattern: /[a-zA-Z0-9]+/, description: '**AEPlatforms** client secret [взять там же](https://aeplatform.ru/ru/profile/clients)' })
    @Column({ default: '4qkT23MQoaI1UrEizBPOE0WIsEkV4hRD643aHGRiJ4EAtblhK888io0ayou54KGUMIXzmjuJFlc1lF5N6qshRP' })
    readonly aeClientSecret: string;

    //Admitad
    @v({ pattern: /^https?:\/\/.+[^/]$/, description: '**Admitad** ссылка [(скопировать после подключения программы по клику на иконку скрепки)](https://store.admitad.com/ru/catalog/)' })
    @Column({ default: 'https://alitems.co/g/vv3q4oey1vbe13cbb72db6d1781017' })
    readonly admitad: string;

    @v({ description: 'Разрешенные url параметры', state: RTTIItemState.ALLOW_CREATE })
    @Column({ type: "simple-array", default: 'wh_pid,pagePath,shopId,srcSns,sellerAdminSeq,sellerId,image' })
    readonly allowedQueryStrings: string[]

    @v({ description: 'Домены (включая поддомены), которые требуют поиска ссылки редиректа в теле страницы', state: RTTIItemState.ALLOW_CREATE })
    @Column({ type: "simple-array", default: 'go9.ru,69v.ru' })
    readonly hostsFindLinkInBody: string[]

    @v({ description: 'Домены (включая поддомены), которые не требуют развертывания редиректов', state: RTTIItemState.ALLOW_CREATE })
    @Column({ type: "simple-array", default: 'time100.ru' })
    readonly hostsWhiteList: string[]

    @v({ description: 'Домены (включая поддомены), которые точно не указывают на aliexpress', state: RTTIItemState.ALLOW_CREATE })
    @Column({ type: "simple-array", default: 'prfl.me,1sota.ru,220-volt.ru,asos.com,beeline.ru,bit.ly,castorama.ru,citilink.ru,divizion.com,dixis-ul.ru,dochkisinochki.ru,drovosek-profi.ru,eldorado.ru,epicgames.com,euroset.ru,frontime.ru,gloria-jeans.ru,holodilnik.ru,honor.ru,instagram.com,kazanexpress.ru,kcentr.ru,kfc.ru,kuvalda.ru,lamoda.ru,leran.pro,leroymerlin.ru,matrix12.ru,maxidom.ru,megastroy.com,mi.com,multivarka.pro,mvideo.ru,obi.ru,onlinetrade.ru,ozon.ru,rbt.ru,re-store.ru,reebok.ru,sbermegamarket.ru,sc-store.ru,huawei.ru,megafon.ru,mts.ru,samsung.com,tele2.ru,skillbox.ru,store.sony.ru,svyaznoy.ru,t.me,technopark.ru,techport.ru,techprom.ru,tpko.ru,twitter.com,vseinstrumenti.ru,wildberries.ru,ya.cc,yandex.ru,youtu.be,youtube.com,zeon18.ru,zurmarket.ru,auchan.ru,galamart.ru,adidas.ru,tinkoff.promo,akusherstvo.ru,sbermarket.ru,letu.ru,metro-cc.ru,delivery-club.ru' })
    readonly hostsBlackList: string[]
}

export abstract class ShortsSettingsRepository extends Repository<LinksSettings>{ }