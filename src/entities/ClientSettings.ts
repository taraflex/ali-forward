import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';
import { SingletonEntity } from '@ioc';
import { API_HASH_RE, BOT_TOKEN_RE } from '@utils/tg-utils';

const TIMEZONE_OFFSET = new Date().getTimezoneOffset() / 60;

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'cog', order: 1 }, { PATCH: 'tgclient_save' })
@SingletonEntity()
export class ClientSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ notEqual: 0, description: 'Telegram Api id [взять тут](https://my.telegram.org/apps)' })
    @Column({ type: 'int', default: 0 })
    apiId: number;

    @v({ pattern: API_HASH_RE, description: 'Telegram Api hash [взять там же](https://my.telegram.org/apps)' })
    @Column({ default: '', length: 32 })
    apiHash: string;

    @v({ min: 5, description: 'Tg телефон' })
    @Column({ default: '' })
    phone: string;

    @v({ pattern: BOT_TOKEN_RE, description: 'Токен телеграм бота, получить у [@BotFather](tg://resolve?domain=BotFather)' })
    @Column({ default: '' })
    token: string;

    @v({ min: 5, description: 'VK телефон' })
    @Column({ default: '' })
    vkPhone: string;

    @v({ empty: false, description: 'VK пароль' })
    @Column({ default: '' })
    vkPassword: string;

    @v({ min: 18, description: 'Пауза между запросами к VK в секундах (если у нас будет **x** источников, то полная проверка произойдет не быстрее чем за _{ x * пауза }_ секунд + дополнительное время на скачивание картинок)' })
    @Column({ unsigned: true, default: 18 })
    vkRequestTimeout: number;

    @v({ min: 0, max: 24, description: 'В каком часовом поясе отображать дату публикации поста' })
    @Column({ unsigned: true, default: 3, type: 'tinyint' })
    protected timezone: number;

    get timezoneOffset() {
        return this.timezone + TIMEZONE_OFFSET;
    }

    @v({ min: 1, max: 100, description: 'Максимальное число последних постов подгружаемых из VK групп' })
    @Column({ unsigned: true, default: 3, type: 'tinyint' })
    vkPostCount: number;

    @v({ min: 1, max: 100, description: 'Максимальное число последних постов подгружаемых из VK групп после рестарта' })
    @Column({ unsigned: true, default: 20, type: 'tinyint' })
    vkPostCountAfterRestart: number;

    @v({ min: 1, max: 255, description: 'Максимальное число попыток парсинга поста' })
    @Column({ unsigned: true, default: 4, type: 'tinyint' })
    maxParseTries: number;

    @v({ min: 1, max: 24 * 3600, description: 'Перезапускать бота после **x** секунд без новых постов в канале админки' })
    @Column({ unsigned: true, default: 9000, type: 'int' })
    restartWithoutPostsAfter: number;

    @v({ description: 'Часы, в которые требуется проверять бота на перезапуск (UTC+0)', enum: Array(24).fill(0).map((_, i) => i), state: RTTIItemState.ALLOW_CREATE, type: 'array' })
    @Column({ default: '[0,3,12,15,18,21]', type: 'simple-json' })
    checkRestartAt: number[];

    @v({ min: 1, description: 'Таймаут получения превью товара с aliexpress' })
    @Column({ unsigned: true, default: 25 })
    maxParseAliPreviewTime: number;

    @v({ min: 1, max: 255, description: 'Максимальное число фото в коллаже' })
    @Column({ unsigned: true, default: 25, type: 'tinyint' })
    maxPhotosCount: number;

    @v({ min: 1, max: 255, description: 'Максимальное число параллельно загружаемых фото' })
    @Column({ unsigned: true, default: 32, type: 'tinyint' })
    maxPhotosConcurrency: number;

    @v({ min: 1, description: 'Таймаут скачивания одного фото' })
    @Column({ unsigned: true, default: 30 })
    maxPhotoDownloadTime: number;

    @v({ min: 1, description: 'Таймаут скачивания одного видео/анимации' })
    @Column({ unsigned: true, default: 120 })
    maxMediaDownloadTime: number;

    @v({ min: 1, max: 100, description: 'Качество генерируемого превью в формате jpeg' })
    @Column({ unsigned: true, default: 85, type: 'tinyint' })
    previewQuality: number;

    @v({ description: 'Таймаут добавления/обновления тг источника (не забывайте сначала подписываться на источник). **0** - ждать бесконечно' })
    @Column({ unsigned: true, default: 60 })
    maxTgSourceAddTime: number;

    @v({ min: 1, max: 8, description: 'Максимальное число управляющих кнопок в строке у постов в группе-админке' })
    @Column({ unsigned: true, default: 3, type: 'tinyint' })
    buttonsPerRow: number;

    @v({ min: 1, max: 255, description: 'Удалять из базы посты/медиа старше **х** дней. Проверка запускается раз в день. **0** - отключить' })
    @Column({ unsigned: true, default: 7, type: 'tinyint' })
    removeAfter: number;

    get removeAfterSeconds() {
        return this.removeAfter * 86400;
    }
}

export abstract class ClientSettingsRepository extends Repository<ClientSettings>{ }