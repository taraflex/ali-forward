import { Column, Entity, PrimaryColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';

import { tgLink } from '../utils';
import { Channel } from './Channel';
import { Picture, Post } from './Post';

/**
* @hidden
*/
const PROXY = new Proxy(globalThis, {
    has(_, key) {
        return key.constructor === String && key.startsWith('$');
    },
    get() {
        return 0;
    }
});
/**
* @hidden
*/
const POST_PROPS = require('post-props-loader!@entities/types.defs');

@Access({ GET: 0, DELETE: 0, PATCH: 0 }, { display: 'table', icon: '_vk', order: 7 })
@Entity()
export class Source {
    @v({ state: RTTIItemState.HIDDEN })
    @PrimaryColumn({ readonly: true, type: 'bigint', default: 0 })
    id: number;

    get tg() {
        return this.id < 0;
    }

    @v({ state: RTTIItemState.HIDDEN })
    @Column({ default: '' })
    domain: string;

    get url() {
        return this.tg ?
            (this.domain ? 'tg://resolve?domain=' + this.domain : tgLink(this.id)) :
            (this.domain ? 'https://vk.com/' + this.domain : 'https://vk.com/public' + this.id);
    }

    @v({ enum: [Picture.SOURCE, Picture.ALI], description: 'Предпочитаемое превью' })
    @Column({ default: Picture.SOURCE, type: 'varchar', length: Math.max(Picture.SOURCE.length, Picture.ALI.length) })
    picture: Picture;

    @v({ state: RTTIItemState.READONLY, description: 'Название' })
    @Column({ default: '' })
    name: string;

    /**
     * @hidden
     */
    @v({ description: 'Js скрипт расставления весов [доступные переменные](/static/docs/classes/_src_entities_post_.post.html) (_CTRL+D_ - автоформатирование кода)', type: 'javascript', state: RTTIItemState.FULLWIDTH })
    @Column({ default: '' })
    weightCalcScript: string;

    /**
    * @hidden
    */
    masker?: (post: Post, include: (text: string) => boolean) => number;
    /**
    * @hidden
    */
    buildMasker(channels: readonly Channel[]) {
        this.masker ||= new Function('__proxy__', `with(__proxy__) return function({ ${POST_PROPS}, is_owner, tg }, includes) { let channels = 0; ${channels.map(c => c.dtypes).join(';')}; ${this.weightCalcScript};return channels | 0; }`)(PROXY);
    }
}

export abstract class SourceRepository extends Repository<Source>{ }