import lastMatch from 'last-match';
import { Column, Entity, Index, PrimaryColumn, Repository } from 'typeorm';

import { concatPhotos } from '../photos-utils';
import { last, regTransform, tgLink, toChatId, toSupergroupId } from '../utils';

import type { formattedText, inputMessageAnimation$Input, inputMessageVideo$Input } from 'tdlib-types';
import type { ClientSettings } from './ClientSettings';

export const enum Picture {
	NONE_OR_VIDEO = 'None',
	SOURCE = 'Source',
	ALI = 'Ali'
}

export const enum ChannelPostState {
	Error = -1,
	Deleted = 1
}

/**
 * проверяет наличие строки в посте (фонетическое сравнение)
 * 
 * @param {string|RegExp} s что ищем
 */
export function includes(s: string | RegExp): boolean {
	return new RegExp(regTransform(s, 0).join(), 'ugis').test(this as string);
}

export type LocalPhoto = {
	readonly size: number,
	readonly width: number,
	readonly height: number,
	readonly local: string,
	readonly url?: string
}

/**
 * @hidden
 */
export type ResizedLocalPhoto = { readonly aspect: number, cellWidth?: number } & LocalPhoto;

export type Photo = {
	size?: number,
	width?: number,
	height?: number,
	local?: string,
	readonly url?: string
};

@Entity()
export class Post {
	/**
	 * @hidden
	 */
	static id(message: { chat_id: number, id: number }) {
		return (-message.chat_id) + '_' + message.id;
	}
	/**
	 * @hidden
	 */
	@PrimaryColumn()
	id: string;

	/**
	 * @hidden
	 */
	get message_id() {
		return +last(this.id.split('_'));
	}

	/**
	 * источник - телеграм канал
	 */
	get tg() {
		return this.owner_id < 0;
	}

	/**
	 * @hidden
	 */
	get sourceTag() {
		return this.tg ? 'tg' + toSupergroupId(this.owner_id) : 'vk' + this.owner_id;
	}

	/**
	 * состояние поста в чате-админке НЕ требует обновления
	 */
	@Index()
	@Column({ default: false })
	actual: boolean;

	/**
	 * число раз парсинга поста с ошибками ссылок
	 */
	@Index()
	@Column({ default: 0, type: 'tinyint' })
	fails: number;

	/**
	 * были ли у записи хештеги
	 */
	@Column({ default: false })
	had_tags: boolean;

	/**
	 * текст записи.
	 */
	@Column({ default: '' })
	text: string;

	/**
	 * идентификатор автора записи (от чьего имени опубликована запись).
	 */
	@Column({ default: 0, type: 'bigint' })
	from_id: number;

	/**
	 * идентификатор владельца стены, на которой размещена запись.
	 */
	@Column({ default: 0, type: 'bigint' })
	owner_id: number;

	/**
	 * запись опубликована владельцем стены
	 */
	get is_owner(): boolean {
		return this.from_id === this.owner_id;
	}

	/**
	 * время публикации записи в формате unixtime.
	 */
	@Column({ default: 0, type: 'bigint' })
	date: number;

	/**
	 * время изменения записи в формате unixtime.
	 */
	@Column({ default: 0, type: 'bigint' })
	edited: number;

	/**
	 * тип активного фото
	 */
	@Column({ default: Picture.SOURCE, type: 'varchar', length: 4 })
	readonly type: Picture;

	/**
	 * фотографии c источника парсинга
	 */
	@Column({ default: '[]', type: 'simple-json' })
	source_photos: readonly Photo[] = [];

	/**
	 * ali фотографии
	 */
	@Column({ default: '[]', type: 'simple-json' })
	ali_photos: readonly Photo[] = [];

	/**
	 * ссылка на коллаж
	 */
	@Column({ default: '' })
	preview: string;

	/**
	 * @hidden
	 */
	@Column({ default: true })
	readonly db: boolean;

	/**
	 * @hidden
	 */
	@Column({ default: '{}', type: 'simple-json' })
	video: Partial<inputMessageVideo$Input | inputMessageAnimation$Input> & { src?: string } = {};

	/**
	 * @hidden
	 */
	@Column({ default: '{}', type: 'simple-json' })
	readonly channels: Record<string, number> = {};

	/**
	 * @hidden
	 */
	async concatPreviews(picture: Picture, settings: ClientSettings): Promise<[Picture, string?]> {
		if (picture !== Picture.NONE_OR_VIDEO) {
			for (let { photos, type } of (
				picture === Picture.ALI ?
					[
						{ type: Picture.ALI, photos: this.ali_photos },
						{ type: Picture.SOURCE, photos: this.source_photos }
					] : [
						{ type: Picture.SOURCE, photos: this.source_photos },
						{ type: Picture.ALI, photos: this.ali_photos },
					]
			)) {
				const url = await concatPhotos(photos, settings, !!(this.video._ || this.video.src));
				if (url) {
					return [type, url];
				}
			}
		}
		return [Picture.NONE_OR_VIDEO, ''];
	}

	/**
	 * @hidden
	 */
	async makeCollage(picture: Picture, settings: ClientSettings) {
		const [type, url] = await this.concatPreviews(picture, settings);
		try {
			return this.type != type || this.preview != url;
		} finally {
			//@ts-ignore
			this.type = type;
			this.preview = url;
		}
	}

	/**
	 * @hidden
	 */
	get canDisplayPreview() {
		return !!(this.type !== Picture.NONE_OR_VIDEO && this.preview);
	}

	/**
	 * @hidden
	 */
	get previewsCount() {
		return this.preview ? (+lastMatch(this.preview, /\/(\d+)\$/) | 0) || 1 : 0;
	}

	/**
	 * @hidden
	 */
	get previewLocal(): string {
		return this.preview.replace(/^.+?\/static\/files\//, __dirname + '/static/files/');
	}

	/**
	 * @hidden
	 */
	get url() {
		return this.tg ? tgLink(this.owner_id, this.message_id) : 'https://vk.com/wall' + this.id;
	}

	/**
	 * @hidden
	 */
	getChannelMessageId(channel: number) {
		const i = Math.abs(this.channels[channel]);
		return i > ChannelPostState.Deleted ? i : 0
	}

	/**
	 * @hidden
	 */
	changeChannelState(channel: number, messageIdOrState: number | ChannelPostState) {
		return this.channels[channel] === messageIdOrState ? false : (this.channels[channel] = messageIdOrState, true);
	}

	/**
	 * @hidden
	 */
	setError(loc: string, channel: number, err: any): boolean {
		if (err) {
			if (err.message === 'MESSAGE_NOT_MODIFIED') {
				return this.changeChannelState(channel, this.getChannelMessageId(channel));
			} else if (err.message === 'MESSAGE_ID_INVALID' /*|| err.message === 'Message not found'*/) {
				return this.changeChannelState(channel, ChannelPostState.Deleted);
			}
		}
		console.error(loc, tgLink(toChatId(channel), this.getChannelMessageId(channel)), err);
		return this.changeChannelState(channel, -this.getChannelMessageId(channel) || ChannelPostState.Error);
	}

	/**
	 * @hidden
	 */
	get idCaption(): formattedText {
		return {
			_: 'formattedText',
			text: this.id,
			entities: [{ _: 'textEntity', offset: 0, length: this.id.length, type: { _: 'textEntityTypeTextUrl', url: this.url } }]
		};
	}
}

/**
 * @hidden
 */
export abstract class PostRepository extends Repository<Post>{ }