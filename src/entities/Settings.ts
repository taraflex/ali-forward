import { Column, Repository } from 'typeorm';

import { SingletonEntity } from '@ioc';

import { Settings as CoreSettings } from '../core/entities/Settings';
import { toChatId } from '../utils';

@SingletonEntity()
export class Settings extends CoreSettings {
    @Column({ default: 0, type: 'bigint' })
    supergroup_id: number;

    get supergroup_chat() {
        return toChatId(this.supergroup_id);
    }

    @Column({ default: 0, type: 'bigint' })
    admin_id: number;

    @Column({ default: '{}', type: 'simple-json' })
    vk: { phone?: string, password?: string, token?: string };

    @Column({ default: '{}', type: 'simple-json' })
    readonly ae: { token?: string, expires_at?: number };

    @Column({ default: '{}', type: 'simple-json' })
    readonly admitad: { token?: string, expires_at?: number };

    setToken(name: 'ae' | 'admitad', access_token: string, expires_in: number) {
        this[name].token = access_token;
        this[name].expires_at = Math.floor((Date.now() + expires_in * 900) / 1000);
    }

    getToken(name: 'ae' | 'admitad') {
        const { token, expires_at } = this[name];
        if (expires_at < Date.now() / 1000) return '';
        return token || '';
    }
}

export abstract class SettingsRepository extends Repository<Settings>{ }