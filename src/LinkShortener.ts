import { Inject, Singleton } from 'typescript-ioc';

import { isIP } from 'net';
import { Channel } from '@entities/Channel';
import { AdPlatforms, LinksSettings } from '@entities/LinksSettings';
import { fit2size } from '@utils/trim-map2size';

import { AEPlatform } from './AEplatform';
import { asyncMemoizeLast, dnsResolver, escHtml, http, kfs } from './utils';

class TUrl {
    constructor(
        protected readonly _shortable: boolean,
        public url: string,
        readonly title: string
    ) { }

    get shortable() {
        return this.url && this._shortable;
    }

    toString() {
        let { title, url } = this;//todo without title try send raw url
        return url ? (url = escHtml(url), `<a href="${url}">${title ? escHtml(title) : url}</a>`) : '';
    }
}

function wrapLink(url: string, title?: string) {
    return new TUrl(false, url, title);
}

const patchLink = asyncMemoizeLast(async link => {
    const u = new URL(link);
    if (!isIP(u.hostname)) {
        let ip = kfs[u.hostname];
        if (!ip) {
            try {
                kfs[u.hostname] = ip = (await dnsResolver.resolve4(u.hostname)).find(Boolean);
            } catch (e) {
                LOG_ERROR(e);
            }
        }
        if (ip) {
            u.hostname = ip;
        }
    }
    return u.toString();
})

const DUMMY_OBJECT = Object.create(null);
const ALI_RE = /^https?:\/\/(?:[a-z0-9\-]+\.)*aliexpress\.(?:ru|com)\//i;

@Singleton
export class LinkShortener {

    constructor(
        @Inject protected readonly settings: LinksSettings,
        @Inject protected readonly aeplatform: AEPlatform
    ) { }

    protected readonly scache = new Map<string, string>();

    protected async shorby(urls: string[]): Promise<Record<string, string>> {
        const result = Object.create(null) as Record<string, string>;
        try {
            const requestedUrls = [];
            for (const u of urls) {
                const s = this.scache.get(u);
                if (s) {
                    result[u] = s;
                } else {
                    requestedUrls.push(u)
                }
            }
            if (requestedUrls.length) {
                const s = await patchLink(this.settings.shorbyUrl);
                Object.assign(result, await http.put(s, {
                    responseType: 'json',
                    resolveBodyOnly: true,
                    json: requestedUrls,
                    timeout: this.settings.maxLinkShortTime * 1000
                }));
            }
        } catch (e) {
            LOG_ERROR(e);
        }
        for (let url in result) {
            this.scache.set(url, result[url]);
        }
        fit2size(this.scache, 800);
        return result;
    }

    protected admitad(track: string, url: string) {
        return this.settings.admitad + `/?ulp=${encodeURIComponent(url)}&subid=` + encodeURIComponent(track);
    }

    protected async a(track: string, urlTpl: string, url: string, title?: string): Promise<TUrl> {
        return ALI_RE.test(url) && (track || urlTpl) ? new TUrl(
            true,
            (track && (
                (this.settings.adplatform === AdPlatforms.AEPlatform && await this.aeplatform.process(track, url)) ||
                (this.settings.adplatform === AdPlatforms.Admitad && this.admitad(track, url))
            )) ||
            (urlTpl && new Function('link', 'return `' + urlTpl + '`')(url)),
            title
        ) : wrapLink(url, title);
    }

    protected readonly waitAll = async (literals: TemplateStringsArray, ...placeholders: Promise<string | TUrl>[]) => {
        const pp = await Promise.all(placeholders);
        const shortable = pp.filter((p: TUrl) => p.shortable).map((p: TUrl) => p.url);
        const processed = shortable.length ? await this.shorby(shortable) : DUMMY_OBJECT;
        let result = literals[0];
        for (let i = 0; i < pp.length; i++) {
            const p = pp[i];
            if (p instanceof TUrl) {
                const n = processed[p.url];
                if (n) {
                    p.url = this.settings.shorbyUrl + '/' + n;
                }
            }
            result += p.toString();
            result += literals[i + 1];
        }
        return result;
    }

    async processLinks(content: string, channel?: Channel) {
        const a = channel && this.settings.adplatform !== AdPlatforms.None ? this.a.bind(this, channel.trackingId, channel.urlTemplate) : wrapLink;
        return await new Function('waitAll', 'a', 'return waitAll`' + content + '`')(this.waitAll, a);
    }
}
