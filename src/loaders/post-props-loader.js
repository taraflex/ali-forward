/**
 * @param {string} content
 */
module.exports = function (content) {
    //@ts-ignore
    if (this.cacheable) this.cacheable();
    return 'module.exports=`' + JSON.parse(content).children[0].children.find(c => c.name == "Post").children
        .filter(o => o.kindString == 'Property')
        .map(o => o.name).join(',') + '`';
};