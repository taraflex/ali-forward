function extractType({ type }) {
    if (type.type == 'array') {
        return type.elementType.name + '[]';
    }
    return type.name;
}

/**
 * @param {string} content
 */
module.exports = function (content) {
    //@ts-ignore
    if (this.cacheable) this.cacheable();
    return 'module.exports=`' + JSON.parse(content).children[0].children.find(c => c.name == "Post").children
        .filter(o => o.kindString == 'Property' || o.kindString == 'Accessor')
        .concat([{
            vartype: 'let',
            name: 'channels',
            type: { name: 'number' },
            comment: { shortText: 'каналы, в которые будет опубликован пост' }
        }])
        .map(p => `
/** ${p.comment ? p.comment.shortText : ''} */
declare ${p.vartype || 'const'} ${p.name}:${extractType(p.getSignature?.[0] || p)};`).join('\n') + '`';
};