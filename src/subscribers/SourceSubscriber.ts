import {
    EntitySubscriberInterface, EventSubscriber, InsertEvent, RemoveEvent, UpdateEvent
} from 'typeorm';
import { Container } from 'typescript-ioc';

import { Source } from '@entities/Source';

import { AdminClient } from '../AdminClient';
import { BotClient } from '../BotClient';
import { toSupergroupId } from '../utils';
import { VKClient } from '../VKClient';

@EventSubscriber()
export class SourceSubscriber implements EntitySubscriberInterface<Source> {

    listenTo() {
        return Source;
    }

    beforeRemove({ entityId }: RemoveEvent<Source>) {
        const vk = Container.get(VKClient)
        if (Array.isArray(entityId)) {
            entityId.forEach(id => vk.remove(id));
        } else if (entityId) {
            vk.remove(entityId);
        }
    }

    async beforeInsert({ entity }: InsertEvent<Source>) {
        if (entity.tg) {
            entity.domain = await Container.get(AdminClient).getSupergroupDomain(toSupergroupId(entity.id));
        }
    }

    async beforeUpdate({ entity }: UpdateEvent<Source>) {
        if (entity.tg) {
            const admin = Container.get(AdminClient);
            entity.name = (await admin.getChat(entity.id)).title;
            entity.domain = await admin.getSupergroupDomain(toSupergroupId(entity.id));
        } else {
            const info = await Container.get(VKClient).getGroupInfo('https://vk.com/public' + entity.id);
            if (info) {
                entity.name = info.name || entity.name;
                entity.domain = info.screen_name || '';
            }
        }
    }

    afterInsert({ entity }: InsertEvent<Source>) {
        if (!entity.tg) {
            Container.get(VKClient).add(entity.id);
        }
        return Container.get(BotClient).loadSources();
    }

    afterUpdate(e: UpdateEvent<Source>) {
        return this.afterInsert(e);
    }

    afterRemove(_: RemoveEvent<Source>) {
        return Container.get(BotClient).loadSources();
    }
}