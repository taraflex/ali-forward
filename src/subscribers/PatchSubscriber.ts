import { BotClient } from 'src/BotClient';
import { EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { Patch } from '@entities/Patch';

@EventSubscriber()
export class PatchSubscriber implements EntitySubscriberInterface<Patch> {

    listenTo() {
        return Patch;
    }

    afterUpdate(_: UpdateEvent<Patch>) {
        return Container.get(BotClient).loadPatches();
    }

    afterInsert(_: InsertEvent<Patch>) {
        return Container.get(BotClient).loadPatches();
    }
}