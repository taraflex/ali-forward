import { BotClient } from 'src/BotClient';
import {
    EntitySubscriberInterface, EventSubscriber, InsertEvent, RemoveEvent, UpdateEvent
} from 'typeorm';
import { Container } from 'typescript-ioc';

import { Channel } from '@entities/Channel';

@EventSubscriber()
export class ChannelSubscriber implements EntitySubscriberInterface<Channel> {

    listenTo() {
        return Channel;
    }

    afterRemove(_: RemoveEvent<Channel>) {
        return Container.get(BotClient).loadChannels();
    }

    afterUpdate(_: UpdateEvent<Channel>) {
        return Container.get(BotClient).loadChannels();
    }

    afterInsert(_: InsertEvent<Channel>) {
        return Container.get(BotClient).loadChannels();
    }
}