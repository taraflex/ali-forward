import { TextEntityType } from 'tdlib-types';

import { dropHidden } from './utils';

export const SUPPORTED_ENTITIES = new Set([
    'textEntityTypeHashtag',
    'textEntityTypeBold',
    'textEntityTypeItalic',
    'textEntityTypeUnderline',
    'textEntityTypeStrikethrough',
    'textEntityTypeCode',
    'textEntityTypePreCode',
    'textEntityTypePre',
    'textEntityTypeUrl',
    'textEntityTypeTextUrl'
]);

export class ETree {
    parent: ETree;
    readonly children: ETree[] = [];
    readonly end = this.start + this.length;

    protected readonly begin: string;
    protected readonly finish: string;

    constructor(
        protected readonly source: string,
        readonly start: number,
        readonly length: number,
        readonly type?: TextEntityType
    ) {
        if (type) {
            switch (type._) {
                case 'textEntityTypeHashtag': break;
                case 'textEntityTypeBold': this.begin = '${"<b>"}'; this.finish = '${"</b>"}'; break;
                case 'textEntityTypeItalic': this.begin = '${"<i>"}'; this.finish = '${"</i>"}'; break;
                case 'textEntityTypeUnderline': this.begin = '${"<u>"}'; this.finish = '${"</u>"}'; break;
                case 'textEntityTypeStrikethrough': this.begin = '${"<s>"}'; this.finish = '${"</s>"}'; break;
                case 'textEntityTypeCode': this.begin = '${"<code>"}'; this.finish = '${"</code>"}'; break;
                case 'textEntityTypePreCode':
                case 'textEntityTypePre': this.begin = '${"<pre>"}'; this.finish = '${"</pre>"}'; break;
                case 'textEntityTypeUrl': this.begin = '${resolve(`' + source.substr(start, length) + '`)}'; break;
                case 'textEntityTypeTextUrl': {
                    const title = dropHidden(source.substr(start, length)).trim();
                    if (title) {
                        this.begin = '${resolve(`' + type.url + '`, `' + title + '`)}';
                    }
                    break;
                }
            }
        }
    }

    includes(l: ETree) {
        return l != this && this.parent != l && l.start >= this.start && l.end <= this.end;
    }

    *content(from: number) {
        if (from != this.start) {
            yield this.source.substring(from, this.start);
            from = this.start;
        }
        if (this.begin) {
            yield this.begin;
        }
        if (this.finish || !this.type) {
            for (const c of this.children.sort((a, b) => a.start - b.start)) {
                yield* c.content(from);
                from = c.end;
            }
            if (from != this.end) {
                yield this.source.substring(from, this.end);
            }
        }
        if (this.finish) {
            yield this.finish;
        }
    }
}
