//todo publish types to github as types for node-mdbx
declare module 'node-mdbx' {
    type mode = 'string' | 'buffer';

    class DBI<K, V> {
        put(key: K, value: V): void;
        get(key: K): V
        has(key: string): boolean;
        del(key: string): void;
    }

    class TXN<K, V> {
        getDbi(name?: string): DBI<K extends 'string' ? string : Buffer, V extends 'string' ? string : Buffer>;
        clearDbi(name: string, remove: boolean): void;
    }

    class MDBX<K extends mode = 'string', V extends mode = 'buffer'> {
        constructor(options: {
            path: string
            readOnly?: boolean,
            maxDbs?: number,
            pageSize?: number,
            keyMode?: K,
            valueMode?: V,
            syncMode?: 'durable' | 'noMetaSync' | 'safeNoSync' | 'unsafe'
        });

        transact<T>(fn: (txn: TXN<K, V>) => T): T;
        asyncTransact<T>(fn: (txn: TXN<K, V>) => PromiseLike<T>): Promise<T>;
        close(): void;
        protected readonly closed: boolean;
        hasTransaction(): boolean;
        clearDb(): void;
    }
    export = MDBX;
}