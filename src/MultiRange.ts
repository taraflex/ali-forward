import type { formattedText, textEntity } from 'tdlib-types';

function cut(s: string, offset: number, length: number) {
    return s.substr(0, offset) + s.substr(offset + length);
}

function rshift(r: textEntity, from: number, dist: number) {
    let start = r.offset;
    let end = r.offset + r.length;

    if (start >= from) {
        start = Math.max(from, start + dist);
    }
    if (end >= from) {
        end = Math.max(from, end + dist);
    }

    r.length = end - start;
    r.offset = start;

    return r.length > 0;
}

function trimTo(text: string, length: number) {
    return text.slice(0, text.lastIndexOf(' ', length - 1)).trimEnd();
}

export class MultiRange {
    constructor(protected text: string, protected readonly entities: textEntity[]) { }
    prepend(text: string, entity: textEntity) {
        this.insert(0, text.length);
        this.text = text + this.text;
        this.entities.unshift(entity);
    }
    prependTag(hashtag: string, suffix: string = '\n') {
        this.prepend('#' + hashtag + suffix, {
            _: 'textEntity',
            type: { _: 'textEntityTypeHashtag' },
            offset: 0,
            length: hashtag.length + 1
        })
    }
    applyPatch(re: RegExp, arg: string) {
        re.lastIndex = 0;
        this.text = this.text.replace(re, (whatReplace, ...groups) => {
            const offset = groups[groups.length - 2] as number;
            if (whatReplace.length) {
                this.remove(offset, whatReplace.length);
            }
            if (arg.length) {
                this.insert(offset, arg.length);
            }
            return arg;
        });
    }
    cut(offset: number, length: number) {
        this.text = cut(this.text, offset, length);
        this.remove(offset, length);
    }
    protected remove(offset: number, length: number) {
        this.entities.splice(0, this.entities.length, ...this.entities.filter(r => rshift(r, offset, -length)));
    }
    protected insert(offset: number, length: number) {
        this.entities.every(r => rshift(r, offset, length));
    }
    get size() {
        return this.text.length;
    }
    toText(size: number): formattedText {
        const text = this.text.length > size ? trimTo(this.text, size) : this.text;
        return {
            _: 'formattedText',
            text,
            entities: this.entities.filter(r => rshift(r, text.length, -2147483647))
        }
    }
}