import type { Context } from 'koa';
import { exec } from 'child_process';
import makeDir from 'make-dir';
import { Inject, Singleton } from 'typescript-ioc';
import { promisify } from 'util';

import { EntityClass, has, RTTIItemState } from '@crud/types';
import { ClientSettings, ClientSettingsRepository } from '@entities/ClientSettings';
import bodyParseMsgpack from '@middlewares/body-parse-msgpack';
import checkEntityAccess from '@middlewares/check-entity-access';
import smartRedirect from '@middlewares/smart-redirect';
import io from '@pm2/io';
import { RPC } from '@rpc/RPC';
import { stringify } from '@taraflex/string-tools';
import dataPath from '@utils/data-path';
import { sendErrorEmail } from '@utils/send-email';

import { AdminClient } from './AdminClient';
import { BotClient } from './BotClient';
import { AdminRouter } from './core/AdminRouter';
import { ApiRouter, msgpack } from './core/ApiRouter';
import { DBConnection } from './core/DBConnection';
import { kfs } from './utils';
import { VKClient } from './VKClient';

const execAsync = promisify(exec);
makeDir.sync(dataPath('caches'));

@Singleton
export class App {
    constructor(
        @Inject protected readonly bot: BotClient,
        @Inject protected readonly admin: AdminClient,
        @Inject protected readonly vkClient: VKClient,
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly db: DBConnection,
        @Inject adminRouter: AdminRouter,
        @Inject settingsRepository: ClientSettingsRepository,
        @Inject apiRouter: ApiRouter
    ) {
        adminRouter.events.on('restart', this.dispose);
        bot.on('restart', this.dispose);
        process.on('SIGINT', this.dispose);

        if (!kfs.last_post_published_at) {
            kfs.last_post_published_at = new Date();
        }
        if (!kfs.restart_without_posts_after) {
            kfs.restart_without_posts_after = settings.restartWithoutPostsAfter;
        }
        if (!kfs.check_restart_at) {
            kfs.check_restart_at = settings.checkRestartAt;
        }

        RPC.register('channels', {
            dtypes() {
                return [
                    require('@entities/types.shell').default,
                    require('post-vars-loader!@entities/types.defs'),
                    ...bot.channels.map(c => 'declare ' + c.dtypes)
                ].join(';');
            }
        });

        apiRouter.patch('tgclient_save', '/tgclient-save/:id', checkEntityAccess(ClientSettings), bodyParseMsgpack, smartRedirect, async (ctx: Context) => {
            const body: ClientSettings = <any>ctx.request.body;

            (ClientSettings as EntityClass<ClientSettings>).insertValidate(body);

            try {
                await this.bot.update({ ...body, type: 'bot' }, ctx);
            } catch (err) {
                LOG_ERROR(body, err);
                throw [
                    { field: 'token', message: stringify(err) }
                ];
            }

            try {
                await this.admin.update({ ...body, type: 'user' }, ctx);
            } catch (err) {
                LOG_ERROR(body, err);
                throw [
                    { field: 'phone', message: stringify(err) }
                ];
            }

            const { vkPhone, vkPassword } = body;
            try {
                await this.vkClient.update(body, ctx);
            } catch (err) {
                LOG_ERROR(vkPhone, vkPassword, err);
                const message = stringify(err);
                throw [
                    { field: 'vkPhone', message },
                    { field: 'vkPassword', message }
                ];
            }

            const { props } = (ClientSettings as EntityClass).rtti;
            for (let k in body) {
                const p = props[k];
                if (p && !has(p, RTTIItemState.HIDDEN)) {
                    settings[k] = body[k];
                }
            }

            await settingsRepository.save(settings);

            kfs.restart_without_posts_after = body.restartWithoutPostsAfter;
            kfs.check_restart_at = body.checkRestartAt;

            ctx.status = 201;
            msgpack(ctx, settings);
        });
    }

    async init() {

        const { settings } = this;

        if (settings.phone && settings.token) {
            try {
                await this.bot.update({ type: 'bot', ...settings });
                await this.admin.update({ type: 'user', ...settings });
            } catch (err) {
                LOG_ERROR(err);
                sendErrorEmail('Ошибка запуска telegram бота');
            } finally {
                delete kfs.terminated_by;
            }

            if (settings.vkPassword && settings.vkPhone) {
                try {
                    await this.vkClient.update(settings);
                } catch (err) {
                    LOG_ERROR(err);
                    sendErrorEmail('Ошибка запуска vk клиента. Проверьте телефон и пароль. По возможности НЕ используйте двухфакторную авторизацию.');
                }
            }

            io.action('cleanup', (cb: Function) => {
                this.clean().then(_ => cb({ _ }));
            });
        }
    }

    clean() {
        return Promise.allSettled(this.settings.removeAfter ? [
            this.admin.optimizeStorate(this.settings.removeAfterSeconds),
            this.bot.optimizeStorate(this.settings.removeAfterSeconds),
            this.bot.dropOldPosts(),
            process.platform !== "win32" && execAsync(`find ${__dirname}/static/files/* -ctime +${this.settings.removeAfter} -exec rm -rf {} \\;`)
        ] : []);
    }

    protected dispose = (signal: string) => {
        LOG_INFO('Terminate requested by ' + signal);
        try { kfs.terminated_by = signal; } catch { }
        this.admin.dispose();
        this.vkClient.dispose();
        this.bot.dispose();
        this.db.close().catch(LOG_ERROR).finally(() => process.exit(2));
    }
}