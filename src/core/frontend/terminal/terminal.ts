import './index.css';

import ResizeObserver from 'resize-observer-polyfill';

import baseTextSettings from '@frontend/base-text-settings';
import { notify } from '@frontend/error-handler-mixin';

export default {
    name: 'Terminal',
    icon: 'terminal',
    data() {
        return { loading: true };
    },
    async mounted() {
        try {
            if (!this.t) {
                const { Terminal, WebLinksAddon, FitAddon } = await import(/* webpackChunkName: "xterm"*/  './xterm')
                const t = this.t = new Terminal({
                    ...baseTextSettings,
                    cols: 1,
                    rows: 1,
                    cursorBlink: true,
                    cursorStyle: 'bar',
                });

                const fitAddon = this.fitAddon = new FitAddon();
                t.loadAddon(fitAddon);
                t.loadAddon(new WebLinksAddon());
                t.open(this.$refs.terminal);
                fitAddon.fit();

                this.obs = this.obs || new ResizeObserver(() => fitAddon.fit());
                this.obs.observe(this.$refs.terminal);

                await this.$rpc.enableTerminal(t);
            }
            this.loading = false;
        } catch (err) {
            this.fitAddon && this.fitAddon.dispose();
            this.t && this.t.dispose();
            notify(err);
            this.loading = false;
        }
    },
    beforeDestroy() {
        this.obs && this.obs.disconnect();
        this.fitAddon && this.fitAddon.dispose();
        this.t && this.t.dispose();
    }
}