import ResizeObserver from 'resize-observer-polyfill';

import baseTextSettings from '@frontend/base-text-settings';
import { notify } from '@frontend/error-handler-mixin';
import { ClientRPC } from '@rpc/ClientRPC';

import type { editor, IDisposable } from 'monaco-editor';

function createWorker(url: string) {
    const blob = new Blob([`importScripts('${url}')`], { type: 'application/javascript' });
    return new Worker(window.URL.createObjectURL(blob));
}

(self as any).MonacoEnvironment = {
    getWorker(_: any, label: string) {
        const path = window['LAST_SCRIPT_SRC'] || __webpack_require__.p;
        if (label === 'typescript' || label === 'javascript') {
            return createWorker(path + TYPESCRIPT_WORKER);
        }
        return createWorker(path + EDITOR_WORKER);
    }
}

let extra: IDisposable = null;

export default {
    name: 'monaco',
    props: {
        fullscreen: Boolean,
        value: String,
        readonly: Boolean,
        mode: String
    },
    data() {
        return {
            linesCount: 1,
            loading: true
        };
    },
    watch: {
        readonly(v: boolean) {
            this.editor && (this.editor as editor.IStandaloneCodeEditor).updateOptions({ readOnly: v });
        },
        value(v: string) {
            v = v || '';
            if (this.editor && v != this._lastv) {
                const e = this.editor as editor.IStandaloneCodeEditor;
                this._listener.dispose();
                const model = e.getModel();
                model.setValue(v);
                this.linesCount = model.getLineCount();
                this._listener = model.onDidChangeContent(_ => this.onChange());
            }
        }
    },
    methods: {
        onChange() {
            const model = (this.editor as editor.IStandaloneCodeEditor).getModel();
            this.linesCount = model.getLineCount();
            this.$emit('input', this._lastv = model.getValue());
        }
    },
    async mounted() {
        try {
            const { editor, languages, KeyCode, KeyMod } = require('monaco-editor') as typeof import('monaco-editor');
            if (extra == null) {
                editor.defineTheme('cyr', {
                    base: 'vs-dark',
                    inherit: true,
                    rules: [
                        { token: 'comment', foreground: '7F848E', fontStyle: 'italic' },
                        { token: 'number', foreground: 'E5C07B' },
                        { token: 'identifier', foreground: 'ABB2BF' },
                        { token: 'invalid', foreground: 'ABB2BF' },
                        { token: 'delimiter.bracket.js', foreground: 'ABB2BF' },
                        { token: 'delimiter.parenthesis.js', foreground: 'ABB2BF' },
                        { token: 'keyword.js', foreground: 'C678DD' },
                        { token: 'regexp.js', foreground: 'D19A66' },
                        { token: 'string', foreground: '98C379' },
                        { token: 'delimiter', foreground: '61AFEF' },
                        { token: 'keyword.other.js', foreground: '61AFEF' }
                    ],
                    colors: {
                        'activityBar.background': '#282c34',
                        'activityBar.foreground': '#d7dae0',
                        'activityBarBadge.background': '#4d78cc',
                        'activityBarBadge.foreground': '#f8fafd',
                        'badge.background': '#282c34',
                        'button.background': '#404754',
                        'debugToolBar.background': '#21252b',
                        'diffEditor.insertedTextBackground': '#00809b33',
                        'dropdown.background': '#21252b',
                        'dropdown.border': '#21252b',
                        'editor.background': '#282c34',
                        'editor.findMatchBackground': '#42557b',
                        'editor.findMatchBorder': '#457dff',
                        'editor.findMatchHighlightBackground': '#6199ff2f',
                        'editor.foreground': '#abb2bf',
                        'editor.lineHighlightBackground': '#2c313c',
                        'editorLineNumber.activeForeground': '#abb2bf',
                        'editor.selectionBackground': '#67769660',
                        'editor.selectionHighlightBackground': '#ffffff10',
                        'editor.selectionHighlightBorder': '#dddddd',
                        'editor.wordHighlightBackground': '#d2e0ff2f',
                        'editor.wordHighlightBorder': '#7f848e',
                        'editor.wordHighlightStrongBackground': '#abb2bf26',
                        'editor.wordHighlightStrongBorder': '#7f848e',
                        'editorActiveLineNumber.foreground': '#737984',
                        'editorBracketMatch.background': '#515a6b',
                        'editorBracketMatch.border': '#515a6b',
                        'editorCursor.background': '#ffffffc9',
                        'editorCursor.foreground': '#528bff',
                        'editorError.foreground': '#c24038',
                        'editorGroup.background': '#181a1f',
                        'editorGroup.border': '#181a1f',
                        'editorGroupHeader.tabsBackground': '#21252b',
                        'editorHoverWidget.background': '#21252b',
                        'editorHoverWidget.border': '#181a1f',
                        'editorIndentGuide.activeBackground': '#c8c8c859',
                        'editorIndentGuide.background': '#3b4048',
                        'editorLineNumber.foreground': '#495162',
                        'editorMarkerNavigation.background': '#21252b',
                        'editorRuler.foreground': '#abb2bf26',
                        'editorSuggestWidget.background': '#21252b',
                        'editorSuggestWidget.border': '#181a1f',
                        'editorSuggestWidget.selectedBackground': '#2c313a',
                        'editorWarning.foreground': '#d19a66',
                        'editorWhitespace.foreground': '#3b4048',
                        'editorWidget.background': '#21252b',
                        'focusBorder': '#464646',
                        'input.background': '#1d1f23',
                        'list.activeSelectionBackground': '#2c313a',
                        'list.activeSelectionForeground': '#d7dae0',
                        'list.focusBackground': '#383e4a',
                        'list.highlightForeground': '#c5c5c5',
                        'list.hoverBackground': '#292d35',
                        'list.inactiveSelectionBackground': '#2c313a',
                        'list.inactiveSelectionForeground': '#d7dae0',
                        'list.warningForeground': '#d19a66',
                        'menu.foreground': '#c8c8c8',
                        'panelSectionHeader.background': '#21252b',
                        'peekViewEditor.background': '#1b1d23',
                        'peekViewEditor.matchHighlightBackground': '#29244b',
                        'peekViewResult.background': '#22262b',
                        'scrollbarSlider.activeBackground': '#747d9180',
                        'scrollbarSlider.background': '#4e566660',
                        'scrollbarSlider.hoverBackground': '#5a637580',
                        'textLink.foreground': '#61afef',
                    }
                });

                extra = languages.typescript.javascriptDefaults.addExtraLib(await (await (this.$rpc as ClientRPC).waitConnect()).remote('channels').dtypes(), 'backend.d.ts');
            }

            languages.typescript.javascriptDefaults.setDiagnosticsOptions({
                noSemanticValidation: false,
                noSyntaxValidation: false
            });

            languages.typescript.javascriptDefaults.setCompilerOptions({
                target: languages.typescript.ScriptTarget.Latest,
                allowNonTsExtensions: true,
                module: languages.typescript.ModuleKind.CommonJS,
                moduleResolution: languages.typescript.ModuleResolutionKind.NodeJs,
                strictBindCallApply: true,
                resolveJsonModule: true,
                allowSyntheticDefaultImports: true
            });

            const e = editor.create(this.$refs.playground, {
                ...baseTextSettings,
                value: this._lastv = this.value || '',
                lineHeight: 16,
                language: this.mode || 'javascript',
                accessibilitySupport: 'off',
                autoClosingBrackets: 'never',
                minimap: { enabled: false },
                quickSuggestionsDelay: 170,
                renderIndentGuides: false,
                renderLineHighlight: 'gutter',
                renderWhitespace: 'none',
                roundedSelection: false,
                wordSeparators: '`~!@#%^&*()=-+[{]}\\|;:\'",.<>/?',
                wordWrap: 'on',
                lightbulb: { enabled: false },
                readOnly: this.readonly,
                scrollBeyondLastLine: false,
                theme: 'cyr'
            });
            e.addCommand(KeyMod.CtrlCmd | KeyMod.Shift | KeyCode.KEY_P, () => e.getAction('editor.action.quickCommand').run());
            e.addCommand(KeyMod.CtrlCmd | KeyCode.KEY_D, () => e.getAction('editor.action.formatDocument').run());
            this.linesCount = e.getModel().getLineCount();
            this._listener = e.getModel().onDidChangeContent(_ => this.onChange());

            this.editor = e;

            this.obs = this.obs || new ResizeObserver(() => {
                const dim = (this.$refs.playground as HTMLParagraphElement).getBoundingClientRect();
                e.layout({ width: dim.width - 2, height: Math.max(16, dim.height - 20) });
            });

            this.obs.observe(this.$refs.playground);

            await languages.typescript.getJavaScriptWorker();

            this.loading = false;
        } catch (err) {
            if (this.obs) {
                this.obs.disconnect();
            }
            if (this._listener) {
                this._listener.dispose();
                this._listener = null;
            }
            if (this.editor) {
                this.editor.dispose();
                this.editor = null;
            }
            notify(err);
            this.loading = false;
        }
    },
    beforeDestroy() {
        if (this.obs) {
            this.obs.disconnect();
        }
        if (this.editor) {
            this.editor.dispose();
            this.editor = null;
        }
    }
}