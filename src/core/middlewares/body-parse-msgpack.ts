import { Context } from 'koa';
import rawBody from 'raw-body';

import { decode } from '@utils/msgpack';

export const MAX_PAYLOAD_SIZE = 1024 * 1024 * 5;

export default async (ctx: Context, next: () => Promise<any>) => {
    const { method } = ctx.request
    if (method === 'PATCH' || method === 'PUT' || method === 'POST') {
        const body = await rawBody(ctx.req, { limit: MAX_PAYLOAD_SIZE });
        ctx.request.body = Object.assign((body && body.length > 0 ? decode(body) : null), ctx.request.query);
    }
    return next();
}