import { Context } from 'koa';
import Router from 'koa-router';

import resolveUrl from '@utils/resolve-url';

import { setNoStoreHeader } from './no-store';

declare module 'koa' {
    interface Context {
        router: Router;
        namedRedirect: (routName: string, delay?: number, params?: { [_: string]: any }) => void;
        resolve: (routName: string, params?: { [_: string]: any }) => string;
    }
}

const routsCache = Object.create(null);
const secureRoutsCache = Object.create(null);

export default (ctx: Context, next: () => Promise<any>) => {

    const rcache = ctx.secure ? secureRoutsCache : routsCache;

    ctx.resolve = (routeName, params) => {
        if (!params && ctx.router.stack.find(r => r.name === routeName).paramNames.length < 1) {
            return rcache[routeName] || (rcache[routeName] = resolveUrl(ctx, ctx.router.url(routeName, null)));
        }
        return resolveUrl(ctx, ctx.router.url(routeName, Object.assign({}, ctx.params, params)));
    };

    ctx.namedRedirect = (routName, delay, params) => {
        setNoStoreHeader(ctx);
        const target = ctx.resolve(routName || 'root', params);
        if (delay > 0) {
            ctx.type = 'html';
            //в chrome баг, который не дает установить куку при редиректе через location или refresh приходится извращаться
            //todo таймер обратного отсчета
            ctx.body = `<html><head><meta http-equiv="refresh" content="${delay};url=${target}" /></head></html>`;
        } else {
            ctx.redirect(target);
        }
    };

    return next();
}