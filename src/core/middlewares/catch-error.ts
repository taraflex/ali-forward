import { STATUS_CODES } from 'http';
import { Context } from 'koa';

import { isJsonable, stringify } from '@taraflex/string-tools';
import isAjax from '@utils/is-ajax';

function convertError(err: any) {
    return isJsonable(err) ? err : stringify(err);
}

export default async (ctx: Context, next: () => Promise<any>) => {
    try {
        await next();
    } catch (err) {
        const ajax = isAjax(ctx);
        ctx.type = ajax ? 'json' : 'text';
        if (Array.isArray(err)) {
            ctx.status = 400;
            ctx.body = ajax ?
                JSON.stringify({ errors: err.map(convertError) }) :
                err.map(stringify).join('\n');
        } else {
            if (STATUS_CODES[err]) {
                ctx.status = +err;
                err = STATUS_CODES[err];
            } else {
                const s = (+err.status | 0) || 500
                ctx.status = s;
                if (s >= 500) {
                    LOG_ERROR(err);
                    err = STATUS_CODES[s] || STATUS_CODES[500];
                } else {
                    delete err.status;
                }
            }
            ctx.body = ajax ? JSON.stringify({ errors: [convertError(err)] }) : stringify(err);
        }
    }
}