import { EntitySubscriberInterface } from 'typeorm';

//@ts-ignore
import subscribers from '@subscribers/generated';

export default subscribers as EntitySubscriberInterface[];