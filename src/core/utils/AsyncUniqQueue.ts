export class AsyncUniqQueue<T = any> {
    [Symbol.asyncIterator]() {
        return this;
    }
    protected readonly objects: T[] = [];
    protected readonly resolvers: ((o: {
        value: T;
        done: false;
    }) => void)[] = [];
    get size() {
        return this.objects.length;
    }
    add(value: T, first?: boolean) {
        if (this.resolvers.length > 0) {
            this.resolvers.shift()({ done: false, value });
        } else if (!first) {
            if (!this.objects.includes(value)) {
                this.objects.push(value);
            }
        } else {
            this.delete(value);
            this.objects.unshift(value);
        }
    }
    delete(value: T) {
        const i = this.objects.indexOf(value);
        if (i > -1) {
            this.objects.splice(i, 1);
        }
    }
    next(): Promise<{
        value: T;
        done: false;
    }> {
        return new Promise(resolve => {
            if (this.objects.length > 0) {
                resolve({ done: false, value: this.objects.shift() });
            }
            else {
                this.resolvers.push(resolve);
            }
        });
    }
}
