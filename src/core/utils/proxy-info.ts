import { execSync } from 'child_process';

import { PEM, REAL_HOSTNAME } from './config';

export const DEV_NULL = process.platform.startsWith('win') ? 'nul' : '/dev/null';

const cmd = "grep -i '^Port\\|^BasicAuth' /etc/tinyproxy/tinyproxy.conf";

export default function (remote: boolean) {
    const { Port, BasicAuth } = JSON.parse('{' + execSync(remote ? `ssh -o UserKnownHostsFile=${DEV_NULL} -o StrictHostKeyChecking=no -o BatchMode=yes -i "${PEM}" root@${REAL_HOSTNAME} "${cmd}"` : cmd, { encoding: 'utf-8', windowsHide: true }).trim().split('\n').map(v => v.replace(/\s+/, ':').replace(/([^:]+)/g, '"$1"')) + '}');
    return `${BasicAuth.replace(/\s+/, ':')}@${REAL_HOSTNAME}:${Port}`;
}