import { isIP } from 'net';

import { get } from '@taraflex/http-client';

export default async function () {
    let ip = '';
    try {
        const { body } = await get('http://ipecho.net/plain');
        ip = body.toString().trim();
        if (!isIP(ip)) {
            throw null;
        }
    } catch {
        try {
            const { body } = await get('http://ident.me');
            ip = body.toString().trim();
            if (!isIP(ip)) {
                throw null;
            }
        } catch {
            const { body } = await get('http://icanhazip.com');
            ip = body.toString().trim();
            if (!isIP(ip)) {
                throw 'Invalid ip: ' + ip;
            }
        }
    }
    return ip;
}