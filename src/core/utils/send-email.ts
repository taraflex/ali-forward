import { Container } from 'typescript-ioc';

import { UserRepository } from '@entities/User';
import { post } from '@taraflex/http-client';

import { HOSTNAME } from './config';

export default async function sendEmail(email: string, subject: string, message: string) {
    try {
        const { body } = await post({
            url: 'https://script.google.com/macros/s/AKfycbyau7ecNID-Y9M6s067panGIAdkcUS31BCtoa73K4GURJh_76Q/exec',
            form: {
                email,
                body: message,
                subject
            }
        });
        if (JSON.parse(body).success !== 'ok') {
            throw null;
        }
    } catch (err) {
        err && LOG_ERROR(err);
        throw 'Email sending error. Try again later.';
    }
}

export async function sendErrorEmail(message: string) {
    if (!DEBUG) {
        try {
            const ur = Container.get(UserRepository);
            await sendEmail((await ur.findOne()).email, 'Ошибка в работе бота http://' + HOSTNAME, message);
        } catch { }
    }
}