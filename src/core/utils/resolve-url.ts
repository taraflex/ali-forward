import { Context } from 'koa';
import { resolve } from 'url';

import { PORT } from './config';

export default function (ctx: Context, target: string, localhost?: boolean) {
    return resolve(ctx.protocol + '://' + (localhost && !ctx.secure ? '127.0.0.1:' + PORT : ctx.host), target.replace(/[\/\\]{2,}/g, '/'));
}