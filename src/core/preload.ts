import { spawn } from 'child_process';
import { bootstrap } from 'global-agent';

import { PEM, PORT, REAL_HOSTNAME } from '@utils/config';
import proxyInfo, { DEV_NULL } from '@utils/proxy-info';

if (!(DEBUG || typeof v8debug === 'object' || /--debug|--inspect/.test(process.execArgv.join(' ')))) {
    const PrettyError = require('pretty-error');
    const pe = new PrettyError();
    pe.appendStyle({
        'pretty-error > trace > item > header > pointer': {
            display: 'none'
        }
    });
    pe.skipNodeFiles();
    pe.withoutColors();//в логи попадают управляющие символы отвечающие за цвет - становится нечитабельно
    pe.skip(traceLine => traceLine?.dir && (traceLine.dir.startsWith('node:internal/') || traceLine.dir.startsWith('internal/')));
    pe.start();
}

if (PEM) {
    (global as any).SSH_TUNNEL = spawn('ssh', ['-o', 'UserKnownHostsFile=' + DEV_NULL, '-o', 'StrictHostKeyChecking=no', '-o', 'BatchMode=yes', '-gnNT', '-R', PORT + ':localhost:' + PORT, '-i', PEM, 'root@' + REAL_HOSTNAME], {
        windowsHide: true,
        stdio: 'inherit'
    }).once('exit', code => process.exit(code));

    const proxy = proxyInfo(true);
    if (proxy) {
        bootstrap();
        //@ts-ignore
        global.GLOBAL_AGENT.NO_PROXY = 'localhost,127.0.0.1,oauth.vk.com,ipecho.net,ident.me,icanhazip.com';
        //@ts-ignore
        console.log('Active proxy: ' + (global.GLOBAL_AGENT.HTTP_PROXY = `http://${proxy}/`));
    }
}