import MDBX from 'node-mdbx';
import { Inject, Singleton } from 'typescript-ioc';

import { LinksSettings } from '@entities/LinksSettings';
import { Settings, SettingsRepository } from '@entities/Settings';
import dataPath from '@utils/data-path';

import { http } from './utils';

@Singleton
export class AEPlatform {

    constructor(
        @Inject protected readonly settings: LinksSettings,
        @Inject protected readonly coreSettings: Settings,
        @Inject protected readonly coreSettingsRepository: SettingsRepository
    ) { }

    protected readonly cache = new MDBX({
        path: dataPath('caches/aeplatform'),
        maxDbs: 16,
        syncMode: 'safeNoSync',
        valueMode: 'string',
    });

    protected async api(url: string, json?: any) {
        const { errors, data } = await http(url, {
            method: json ? 'POST' : 'GET',
            responseType: 'json',
            resolveBodyOnly: true,
            throwHttpErrors: false,
            timeout: this.settings.maxLinkShortTime * 1000,
            headers: {
                'Content-Type': 'application/json',
                'X-Client-ID': this.settings.aeClientId.toString(),
                'Authorization': this.coreSettings.ae.token
            },
            json
        });
        if (errors) {
            const exists = errors.find(e => e.code === 409002);
            if (exists) {
                const { creative_id } = exists.meta;
                return this.api(url + '/' + creative_id);
            } else {
                throw errors;
            }
        }
        return data.attributes;
    }

    process(tracking_id: string, url: string) {
        return this.cache.asyncTransact(async txn => {
            const cache = txn.getDbi(this.settings.aeClientId.toString(32));
            let link = cache.get(url);
            if (!link) {
                try {
                    let token = this.coreSettings.getToken('ae');

                    if (!token) {
                        const { error, access_token, expires_in } = await http.post('https://oauth2.aeplatform.ru/token', {
                            responseType: 'json',
                            resolveBodyOnly: true,
                            timeout: this.settings.maxLinkShortTime * 1000,
                            form: {
                                grant_type: 'client_credentials',
                                client_id: this.settings.aeClientId,
                                client_secret: this.settings.aeClientSecret
                            }
                        });
                        if (error) {
                            throw error;
                        }
                        this.coreSettings.setToken('ae', token = access_token, expires_in);
                        await this.coreSettingsRepository.save(this.coreSettings);
                    }

                    const { targetLink } = await this.api(`https://api.aeplatform.ru/api/v1/users/${this.settings.aeUserId}/creatives`, {
                        title: APP_TITLE,
                        link: url
                    });

                    if (link = targetLink) {
                        cache.put(url, link);
                    }
                } catch (err) {
                    LOG_ERROR(url, err);
                }
            }
            if (link) {
                link += "?sub=" + tracking_id;
            }
            return link;
        });
    }
}
