import type { Cookie } from 'playwright-firefox';
import { promises as fs } from 'fs';

import notEmptyFile from '@utils/not-empty-file';

function toNetscapeCookies(cookies: readonly Cookie[]) {
    return cookies.map(c => {
        return [
            c.domain,
            c.domain.startsWith('.') ? 'TRUE' : 'FALSE',
            c.path,
            c.secure ? 'TRUE' : 'FALSE',
            c.expires,
            c.name,
            c.value
        ].join('\t');
    }).join('\n');
}

export function saveCookies(cookies: readonly Cookie[], path: string) {
    return fs.writeFile(path, toNetscapeCookies(cookies));
}

function fromNetscapeCookies(cookies: string): Cookie[] {
    return cookies.split('\n').filter(s => s && !s.startsWith('#')).map(s => {
        const [domain, , path, secure, expires, name, value] = s.split('\t');
        return {
            name,
            value,
            domain,
            path,
            expires: +expires | 0,
            httpOnly: false,
            secure: secure === 'TRUE',
            sameSite: "Lax"
        };
    });
}

export async function loadCookies(path: string): Promise<Cookie[]> {
    return notEmptyFile(path) ? fromNetscapeCookies(await fs.readFile(path, 'utf-8')) : [];
}