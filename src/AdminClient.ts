import { Mutex } from 'async-mutex';
import dayjs from 'dayjs';
import lastMatch from 'last-match';
import { resolve } from 'path';
import { Inject, Singleton } from 'typescript-ioc';

import { Channel, ChannelRepository } from '@entities/Channel';
import { ClientSettings } from '@entities/ClientSettings';
import { LocalPhoto, Post, PostRepository } from '@entities/Post';
import { Settings, SettingsRepository } from '@entities/Settings';
import { Source, SourceRepository } from '@entities/Source';
import { stringify } from '@taraflex/string-tools';
import { escMdForce } from '@utils/tg-utils';

import { BaseTelegramClient } from './BaseTelegramClient';
import { BotClient } from './BotClient';
import { makeDeffer } from './Deffered';
import { MultiMap } from './MultiMap';
import { PostTransformer } from './PostTransformer';
import { findTypeOf, kfs, last, tgLink, toChatId, toSupergroupId } from './utils';
import { VKClient } from './VKClient';

import type {
    chat, formattedText, message, messageAnimation, messagePhoto,
    messageText, messageVideo, photoSize, thumbnail, Update
} from 'tdlib-types';

@Singleton
export class AdminClient extends BaseTelegramClient {

    protected readonly queue = new MultiMap<number>(async (key, message_ids) => {
        try {
            const chat_id = +key.split('_', 1)[0];

            let { messages } = await this.client.invoke({
                _: 'getChatHistory',
                chat_id,
                from_message_id: Math.max(...message_ids),
                offset: -1,
                limit: message_ids.length,
            });
            if (messages) {
                messages = messages.filter(m => message_ids.includes(m.id))
                if (messages.length) {
                    this.processContent(messages);
                }
            }
        } catch (err) {
            LOG_ERROR(key, err);
        }
    });

    constructor(
        @Inject protected readonly ptransformer: PostTransformer,
        @Inject protected readonly bot: BotClient,
        @Inject protected readonly vk: VKClient,
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly coreSettings: Settings,
        @Inject protected readonly coreSettingsRepository: SettingsRepository,
        @Inject protected readonly postRepository: PostRepository,
        @Inject protected readonly sourceRepository: SourceRepository,
        @Inject protected readonly channelRepository: ChannelRepository
    ) {
        super();
    }

    protected initialized = makeDeffer<void>();
    dispose() {
        Promise.allSettled([this.initialized.promise, this.initialized.reject(undefined)]);
        this.initialized = makeDeffer<void>();
        this.vk.clearListeners('media');
        this.bot.clearListeners('refresh_tg');
        this.requestedChats.clear();
        super.dispose();
    }

    protected authHandler(text: string, _: boolean) {
        return this.bot.promtNum(text);
    }

    protected passwordHandler(text: string, _: boolean) {
        return this.bot.promt(text);
    }

    protected async addAdminToSupergroup(user_id: number, chat_id: number, isChannel?: boolean) {
        if (!isChannel) {
            await this.client.invoke({
                _: 'addChatMember',
                user_id,
                chat_id,
            });
        }
        const t = await this.client.invoke({
            _: 'searchChatMembers',
            chat_id,
            query: '',
            filter: { _: 'chatMembersFilterAdministrators' }
        });
        if (!t.members.some(u => u.member_id['user_id'] === this.bot.id)) {
            await this.client.invoke({
                _: 'setChatMemberStatus',
                chat_id,
                member_id: { _: 'messageSenderUser', user_id },
                status: {
                    _: 'chatMemberStatusAdministrator',
                    rights: {
                        _: 'chatAdministratorRights',
                        can_delete_messages: true,
                        can_edit_messages: true,
                        can_post_messages: true
                    }
                }
            });
        }
    }

    protected async searchGroup(query: string): Promise<chat> {
        for (const command of ['searchChats', 'searchChatsOnServer']) {
            const { chat_ids } = await this.client.invoke({ _: command as 'searchChats', query, limit: 1 });
            for (const id of chat_ids) {
                const c = await this.getChat(id);
                if (c?.title === query && c.type._ === 'chatTypeSupergroup' && !c.type.is_channel) {
                    return c;
                }
            }
        }
    }

    protected async createGroup(title: string, description: string): Promise<chat> {
        const c = await this.client.invoke({ _: 'createNewSupergroupChat', title, is_channel: false, description });
        await this.client.invoke({
            _: 'setChatPhoto',
            chat_id: c.id,
            photo: {
                _: 'inputChatPhotoStatic',
                photo: { _: 'inputFileLocal', path: resolve(__dirname + '/../chat_photo.png') }
            }
        });
        return c;
    }

    protected requestedChats = new Set<number>();

    protected async syncChats(timeout: number) {
        for (const c of this.requestedChats) {
            if (await this.getChat(c).catch(_ => false)) {
                this.requestedChats.delete(c);
            }
        }
        timeout *= 1000;
        const startAt = Date.now();
        while (this.requestedChats.size) {
            try {
                await this.client.invoke({ _: 'loadChats', limit: 2147483647 });
                if (timeout > 0 && startAt + timeout > Date.now()) {
                    break;
                }
            } catch (e) {
                if (e?.code === 404) break;
                LOG_ERROR(e);
            }
        }
    }

    protected readonly afterUpdate = async () => {
        const description = 'Restart at ' + dayjs().add(this.settings.timezoneOffset, 'hour').format('DD.MM H:mm:ss') + `\n[${kfs.terminated_by || ''}]`;

        await this.client.invoke({
            _: 'searchPublicChat',
            username: (await this.bot.getMe()).username,
        });
        await this.client.invoke({ _: 'createPrivateChat', user_id: this.bot.id });

        this.requestedChats.clear();
        const lastPosts = new Map<number, number>();
        for (const s of this.bot.sources.values()) {
            if (s.tg) {
                this.requestedChats.add(s.id);
                const m = await this.postRepository.findOne(
                    { owner_id: s.id },
                    { select: ['id'], order: { date: 'DESC' } }
                );
                if (m) {
                    lastPosts.set(s.id, m.message_id);
                }
            }
        }
        await this.syncChats(0);

        try {
            const { supergroup_id } = this.coreSettings;
            if (!supergroup_id) {
                throw 'system_supergroup_id required';
            }
            await this.client.invoke({ _: "createSupergroupChat", supergroup_id });
            await this.client.invoke({ _: 'setChatDescription', chat_id: this.coreSettings.supergroup_chat, description });
        } catch (err) {
            LOG_WARN(err);
            const c = (await this.searchGroup('Ali forward bot channel')) || await this.createGroup('Ali forward bot channel', description);
            this.coreSettings.supergroup_id = toSupergroupId(c.id);
            await this.addAdminToSupergroup(this.bot.id, c.id);
        }
        this.coreSettings.admin_id = this.id;
        await this.coreSettingsRepository.save(this.coreSettings);

        await this.bot.setCommands();

        this.bot.on('refresh_tg', post => this.processMessageById({
            fails: post.fails,
            chat_id: post.owner_id,
            message_id: post.message_id
        }));
        this.vk.on('media', i => this.sendMessage(this.bot.id, i) as any);

        for (const [chat_id, message_id] of lastPosts) {
            await this.processMessageById({ chat_id, message_id }, true);
        }

        this.initialized.fulfill();
        this.bot.chain.add(true);
    }

    protected readonly sourceMutex = new Mutex();
    protected async addSource(supergroup_id: number) {
        const unlock = await this.sourceMutex.acquire();
        try {
            if (this.bot.channels.some(c => c.supergroup_id === supergroup_id)) {
                throw 'Нельзя добавлять в качестве источника целевой канал';
            }
            this.requestedChats.clear();
            this.requestedChats.add(toChatId(supergroup_id));
            await this.syncChats(this.settings.maxTgSourceAddTime);

            const chat = await this.client.invoke({ _: "createSupergroupChat", supergroup_id });
            const s = this.bot.sources.get(chat.id) || new Source();
            s.id = chat.id;
            s.name = chat.title;
            await this.sourceRepository.save(s);

            this.bot.sendMessage(this.coreSettings.supergroup_chat, `Источник обновлен [${escMdForce(s.name)}](${s.url})`).catch(LOG_ERROR);
        } catch (err) {
            LOG_ERROR(err);
            this.bot.sendMessage(this.coreSettings.supergroup_chat, `Ошибка обновления источника [${supergroup_id}](${tgLink(toChatId(supergroup_id))})\n${escMdForce(stringify(err))}`).catch(LOG_ERROR);
        } finally {
            unlock();
        }
    }

    protected readonly onUpdate = async (update: Update) => {
        if (update._ === 'updateNewChat') {
            this.requestedChats.delete(update.chat.id);
        } else if (update._ === 'updateMessageEdited') {
            await this.initialized.promise;
            await this.processMessageById(update);
        } else if (update._ === 'updateNewMessage') {
            await this.initialized.promise;
            const { message } = update;
            if (this.bot.sources.has(message.chat_id)) {
                await this.processMessage(message);
            } else if (message.is_outgoing) {
                let markForRemove = false;
                if (
                    message.chat_id === this.bot.id ||
                    message.chat_id === this.coreSettings.supergroup_chat
                ) {
                    if (message.forward_info?.origin?._ === 'messageForwardOriginChannel') {
                        //register new tg source over forward message
                        await this.addSource(toSupergroupId(message.forward_info.origin.chat_id));
                        markForRemove = true;
                    } else if (message.content._ === 'messageText') {
                        //register new tg/vk source channel
                        const ft = message.content.text;
                        for (const link of ft.entities.filter(e => e.type._ === 'textEntityTypeUrl').map(e => ft.text.substr(e.offset, e.length))) {
                            const username = lastMatch(link, /^https:\/\/t\.me\/(c\/\d+|\w+)(?:$|\/\d+)/);
                            if (username) {
                                let supergroup_id = username.startsWith('c/') ?
                                    parseInt(username.substr(2)) :
                                    toSupergroupId((await this.client.invoke({ _: 'searchPublicChat', username })).id);
                                await this.addSource(supergroup_id);
                                markForRemove = true;
                            } else {
                                const group = lastMatch(link, /^https:\/\/vk\.com\/(\w+)(?:\/|$)/);
                                if (group) {
                                    const info = await this.vk.getGroupInfo(group);
                                    if (info) {
                                        const s = this.bot.sources.get(info.id) || new Source();
                                        s.id = info.id;
                                        s.name = info.name || '';
                                        s.domain = info.screen_name || '';
                                        await this.sourceRepository.save(s);
                                        markForRemove = true;
                                    }
                                }
                            }
                        }
                    }
                } else if (
                    //register new target channel
                    message.is_channel_post &&
                    message.content._ === 'messageText' &&
                    /^\s*register[_\s-]channel\s*$/.test(message.content.text.text)
                ) {
                    const chat = await this.getChat(message.chat_id);
                    if (chat.type._ === 'chatTypeSupergroup') {

                        const { supergroup_id } = chat.type;

                        const c = this.bot.channels.find(c => c.supergroup_id === supergroup_id) ||
                            (await this.channelRepository.findOne(supergroup_id)) ||
                            new Channel();
                        c.supergroup_id = supergroup_id;
                        c.title = chat.title;
                        if (!c.mask) {
                            const bitPos = this.bot.channels.reduce((m, c) => m ^ c.mask, 2147483647).toString(2).lastIndexOf('1');
                            if (bitPos < 0) {
                                await this.bot.sendMessage(this.coreSettings.supergroup_chat, `Не удалось добавить канал [${escMdForce(c.title)}](${c.url})\nВозможно добавить не более 31 канала.`);
                            } else {
                                c.mask = 2 ** (30 - bitPos);
                            }
                            c.order = 1 + Math.max(...this.bot.channels.map(c => c.order));
                        }
                        if (c.mask) {
                            await Promise.all([
                                this.channelRepository.save(c),
                                this.addAdminToSupergroup(this.bot.id, chat.id, true)
                            ]);
                            await Promise.all([
                                this.bot.getSupergroupDomain(supergroup_id),
                                this.bot.sendMessage(this.coreSettings.supergroup_chat, `Канал обновлен [${escMdForce(c.title)}](${c.url})`)
                            ]).catch(LOG_ERROR);
                        }
                        markForRemove = true;
                    }
                }
                if (markForRemove) {
                    this.deleteMessage(message.id, message.chat_id).catch(LOG_ERROR);
                }
            }
        }
    }

    protected async processMessageById({ chat_id, message_id, fails }: { chat_id: number, message_id: number, fails?: number }, loadHistory?: boolean) {
        try {
            if (this.bot.sources.has(chat_id)) {
                const { messages } = await this.client.invoke({
                    _: 'getChatHistory',
                    chat_id,
                    from_message_id: message_id,
                    offset: loadHistory ? -99 : -1,
                    limit: loadHistory ? 99 : 1,
                });
                if (!loadHistory) {
                    const m = messages?.[0];
                    if (m) {
                        if (m.id != message_id) {
                            await this.bot.incPostFails(Post.id({ chat_id, id: message_id }), fails);
                            throw `Got invalid message from history ${tgLink(chat_id, m.id)}`;
                        }
                    }
                    await this.processMessage(m);
                } else if (messages?.length) {
                    const errors = [];

                    const ids = new Set((
                        await this.postRepository
                            .createQueryBuilder()
                            .select('id')
                            .whereInIds(messages.map(m => -chat_id + '_' + m.id))
                            .getRawMany()
                    ).map(v => +last(v.id.split('_'))));

                    ids.add(message_id);

                    for (const m of messages.reverse()) {
                        if (!ids.has(m.id)) {
                            await this.processMessage(m).catch(e => errors.push(e));
                        }
                    }

                    switch (errors.length) {
                        case 0: break;
                        case 1: throw errors[0];//todo throw link to message that throw error
                        default: throw errors;
                    }
                }
            }
        } catch (err) {
            LOG_ERROR(tgLink(chat_id, message_id), err);
        }
    }

    protected readonly downloadPhoto = async (m: message, signal: AbortSignal): Promise<LocalPhoto> => {
        try {
            let size = findTypeOf<photoSize>(
                ['x', 'm', 's'],
                ((m.content as messageText).web_page?.type === 'photo' && (m.content as messageText).web_page?.photo?.sizes) ||
                (m.content as messagePhoto).photo?.sizes
            ) ||
                (m.content as messageAnimation).animation?.thumbnail ||
                (m.content as messageVideo).video?.thumbnail;

            if (size) {
                const f = await this.downloadFile(
                    (size as photoSize).photo || (size as thumbnail).file,
                    signal,
                    this.settings.maxPhotoDownloadTime * 1000
                );
                return {
                    size: f.local.downloaded_size,
                    width: size.width,
                    height: size.height,
                    local: f.local.path
                };
            }
        } catch (e) {
            if (e !== 'abort') {
                LOG_ERROR(tgLink(m.chat_id, m.id), e);
            }
        }
    }

    protected async processContent(messages: message[]) {

        const source = this.bot.sources.get(messages[0].chat_id);
        if (!source) return;

        const message = messages.find(m => m.content['caption']?.text) || messages[0];
        const id = Post.id(message);
        let post = await this.bot.findPost(id);
        const ftext: formattedText = message.content['text'] || message.content['caption'];
        if (ftext?.text) {

            post ||= new Post();
            post.id = id;
            post.date = message.date;
            post.from_id =
                message.forward_info?.from_chat_id ||
                message.forward_info?.origin['sender_user_id'] ||
                message.forward_info?.origin['sender_chat_id'] ||
                message.forward_info?.origin['chat_id'] ||
                message.sender_id['chat_id'] ||
                message.sender_id['user_id'];
            post.owner_id = message.chat_id;

            if (await this.ptransformer.process(post, ftext, source.picture, messages, this.downloadPhoto)) {

                post.edited = message.edit_date || message.date;

                const mediaMessage = messages.find(m => m.content._ === 'messageVideo' || m.content._ === 'messageAnimation');
                if (mediaMessage) {
                    const media = await this.messageToMediaInput(mediaMessage, this.settings.maxMediaDownloadTime, post.idCaption);
                    post.actual = true;
                    await this.bot.savePost(post);
                    await this.sendMessage(this.bot.id, media);
                    return;
                } else {
                    return await this.bot.savePost(post, true);
                }
            }
        }
        if (post?.db) {
            await this.bot.updateMarkup(post);
        }
    }

    protected processMessage(message: message) {
        if (
            message.content._ === 'messageText' ||
            message.content._ === 'messagePhoto' ||
            message.content._ === 'messageVideo' ||
            message.content._ === 'messageAnimation'
        ) {
            if (+message.media_album_id) {
                this.queue.push(message.chat_id + '_' + message.media_album_id.toString(), message.id);
            } else {
                return this.processContent([message]);
            }
        }
    }
}