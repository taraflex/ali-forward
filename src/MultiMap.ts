import MDBX from 'node-mdbx';

import dataPath from '@utils/data-path';
import { decode, encode } from '@utils/msgpack';

export class MultiMap<V> extends Map<string, NodeJS.Timeout>{
    protected readonly cache = new MDBX({
        path: dataPath('caches/tg_albums'),
        maxDbs: 1
    });
    constructor(protected readonly cb: (key: string, v: V[]) => any) {
        super();
    }
    push(key: string, v: V) {
        this.cache.transact(txn => {
            const dbi = txn.getDbi();
            if (dbi.has(key)) {
                const a: V[] = decode(dbi.get(key));
                if (!a.includes(v)) {
                    a.push(v);
                    dbi.put(key, encode(a));
                }
            } else {
                dbi.put(key, encode([v]));
            }
        });
        if (!this.has(key)) {
            this.set(key, setTimeout(() => this.cb(key, this.pop(key)), 4000).unref());
        }
    }
    pop(key: string): V[] {
        clearTimeout(this.get(key));
        this.delete(key);
        return this.cache.transact(txn => {
            const buf = txn.getDbi().get(key);
            return buf && decode(buf) || [];
        });
    }
}