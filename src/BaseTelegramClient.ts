import type { ElMessageBox } from 'element-ui/types/message-box';
import type { Context } from 'koa';
import child_process from 'child_process';
import Emittery from 'emittery';
import { existsSync, promises as fs } from 'fs';
import makeDir from 'make-dir';
import sanitize from 'sanitize-filename';
import { Client } from 'tdl';
import { TDLib } from 'tdl-tdlib-ffi';
import { promisify } from 'util';

import { fromCtx } from '@rpc/ServerRPC';
import { PORT } from '@utils/config';
import dataPath from '@utils/data-path';
import { sendErrorEmail } from '@utils/send-email';
import { API_HASH_RE, BOT_TOKEN_RE, getBotInfo } from '@utils/tg-utils';

import { DeferredMap } from './Deffered';
import { DeepMerge } from './utils';

import type {
    file, formattedText, InputFile$Input, inputMessageAnimation$Input, inputMessagePhoto$Input, inputMessageText$Input, inputMessageVideo$Input, message, video, animation, ReplyMarkup$Input, TdlibParameters, Update
} from 'tdlib-types';

import type { IDisposable } from 'xterm';

const exec = promisify(child_process.exec);

export interface AuthInfo {
    readonly apiHash: string;
    readonly apiId: number;
}

export interface UserAuthInfo extends AuthInfo {
    readonly type: 'user';
    readonly phone: string;
}

export interface BotAuthInfo extends AuthInfo {
    readonly type: 'bot';
    readonly token: string;
}

const tdlibOptions = {
    online: false,
    prefer_ipv6: false,
    use_storage_optimizer: true,
    always_parse_markdown: false,
    ignore_inline_thumbnails: true,
} as const;

const tdlibPostOptions = {
    disable_top_chats: true,
    disable_time_adjustment_protection: true,
    disable_persistent_network_statistics: true,
    ignore_sensitive_content_restrictions: true,
    disable_sent_scheduled_message_notifications: true,
} as const;

export abstract class BaseTelegramClient<Events = Record<string, any>> extends Emittery<Events> implements IDisposable {
    protected readonly messagePendings = new DeferredMap<message>();

    protected client: Client;

    protected apiHash: string;
    protected apiId: number;
    protected phoneOrToken: string;
    protected _id: number = 0;

    get id() {
        return this._id;
    }

    private readonly tdl = new TDLib('./libtdjson');

    protected readonly tdParams: Partial<TdlibParameters> = {
        _: 'tdlibParameters',
        use_secret_chats: false,
        device_model: APP_NAME + ' ' + PORT,
        enable_storage_optimizer: true,
        use_file_database: true,
        use_chat_info_database: true,
        use_message_database: true,
        ignore_file_names: false,
        use_test_dc: false
    };

    getChat(chat_id: number) {
        return this.client.invoke({ _: 'getChat', chat_id });
    }

    async getSupergroupDomain(supergroup_id: number) {
        const group = await this.client.invoke({ _: 'getSupergroup', supergroup_id });
        return group?.username || '';
    }

    deleteMessage(id: number, chat_id: number) {
        return this.client.invoke({
            _: 'deleteMessages',
            message_ids: [id],
            chat_id,
            revoke: true
        });
    }

    sendErrorEmail() {
        return sendErrorEmail(`Сбой в авторизации телеграм клиента. Пересохраните настройки для получения нового авторизационного кода.`);
    }

    async update(authInfo: Readonly<UserAuthInfo | BotAuthInfo>, ctx?: Context) {
        const { type, apiHash, apiId } = authInfo;
        if (!apiId) {
            throw 'Invalid apiId: ' + apiId;
        }
        if (type != 'user' && type != 'bot') {
            throw 'Invalid tdlib client type: ' + type;
        }
        if (!API_HASH_RE.test(apiHash)) {
            throw 'Invalid apiHash: ' + apiHash;
        }
        if (authInfo.type === 'user' && !authInfo.phone) {
            throw 'Invalid phone: ' + authInfo.phone;
        }
        if (authInfo.type === 'bot' && !BOT_TOKEN_RE.test(authInfo.token)) {
            throw 'Invalid bot token: ' + authInfo.token;
        }
        if (
            !this.client ||
            authInfo.apiHash != this.apiHash ||
            authInfo.apiId != this.apiId ||
            (authInfo.type === 'user' && authInfo.phone != this.phoneOrToken) ||
            (authInfo.type === 'bot' && authInfo.token != this.phoneOrToken)
        ) {
            const phoneOrToken = authInfo.type === 'bot' ? authInfo.token : authInfo.phone;
            const profile = sanitize(type === 'bot' ? (await getBotInfo(phoneOrToken)).id.toString() : phoneOrToken);
            try {
                this.dispose();
                const filesDir = dataPath(profile + '/files');
                const client = this.client = new Client(this.tdl, {
                    apiId,
                    apiHash,
                    databaseDirectory: dataPath(profile + '/database'),
                    filesDirectory: filesDir,
                    verbosityLevel: 1,
                    skipOldUpdates: false,
                    useTestDc: false,
                    useMutableRename: true,
                    tdlibParameters: this.tdParams
                });

                if (!existsSync(filesDir)) {
                    await makeDir(filesDir);
                }
                if (process.platform === 'win32') {
                    await fs.symlink(filesDir, filesDir + '_unprotect', 'junction').catch(Boolean);
                } else {
                    if (!existsSync(filesDir + '_unprotect')) {
                        await makeDir(filesDir + '_unprotect');
                    }
                    await exec(`mountpoint -q "${filesDir}_unprotect" || mount --bind "${filesDir}" "${filesDir}_unprotect"`);
                }

                if (this.onUpdate) {
                    client.on('update', async (u: Update) => {
                        try {
                            if (u._ === 'updateMessageSendSucceeded') {
                                this.messagePendings.fulfill(u.message.chat_id + '_' + u.old_message_id, u.message);
                            } else if (u._ === 'updateMessageSendFailed') {
                                this.messagePendings.reject(u.message.chat_id + '_' + u.old_message_id, { code: u.error_code, message: u.error_message });
                            } else {
                                await this.onUpdate(u);
                            }
                        } catch (err) {
                            LOG_ERROR(u?._, err);
                        }
                    });
                }
                client.on('error', LOG_ERROR);
                await client.connect();

                const logsDir = dataPath(profile + '/logs');
                await makeDir(logsDir);
                await client.invoke({
                    _: 'setLogStream',
                    log_stream: {
                        _: 'logStreamFile',
                        path: logsDir + '/error.log',
                        max_file_size: 10 * 1024 * 1024
                    }
                });

                await client.invoke({
                    _: 'setLogVerbosityLevel',
                    new_verbosity_level: 1
                });
                for (let name in tdlibOptions) {
                    await client.invoke({
                        _: 'setOption',
                        name,
                        value: { _: 'optionValueBoolean', value: tdlibOptions[name] }
                    });
                }
                this.apiHash = apiHash;
                this.apiId = apiId;
                this.phoneOrToken = phoneOrToken;
                await client.login(() => type === 'bot' ?
                    {
                        type: 'bot',
                        getToken: async () => phoneOrToken
                    } : {
                        type: 'user',
                        getPassword: async (passwordHint: string, retry?: boolean) => {
                            const message = 'Введите пароль двухэтапной аутентификации телеграм' + (passwordHint ? '\nподсказка: ```\n' + passwordHint + '\n```' : '');
                            if (ctx) {
                                const code = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt(message, {
                                    showCancelButton: false,
                                    roundButton: true
                                });
                                return code.value || '';
                            }
                            return this.passwordHandler(message, retry);
                        },
                        getPhoneNumber: async _ => phoneOrToken,
                        getAuthCode: async (retry?: boolean) => {
                            const message = 'Введите код авторизации телеграм клиента (придет в sms или в уже авторизованный клиент):'
                            if (ctx) {
                                const code = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt(message, {
                                    showCancelButton: false,
                                    inputType: 'number',
                                    roundButton: true
                                });
                                return code.value || '';
                            }
                            return this.authHandler(message, retry);
                        },
                    }
                );
                const u = await this.getMe();
                this._id = u.id;
                if (u.type._ !== "userTypeBot") {
                    for (let name in tdlibPostOptions) {
                        try {
                            await this.client.invoke({
                                _: 'setOption',
                                name,
                                value: { _: 'optionValueBoolean', value: tdlibPostOptions[name] }
                            });
                        } catch (err) {
                            LOG_WARN(err);
                        }
                    }
                }
                await this.afterUpdate?.();
                return true;
            } catch (err) {
                this.dispose();
                throw err;
            }
        }
        return false;
    }

    protected async authHandler(text: string, retry: boolean): Promise<string> {
        this.dispose();
        if (!retry) {
            await this.sendErrorEmail();
        }
        throw 'Telegram client auth required. ' + text;
    }

    protected async passwordHandler(text: string, retry: boolean): Promise<string> {
        this.dispose();
        if (!retry) {
            await this.sendErrorEmail();
        }
        throw 'Telegram client password required. ' + text;
    }

    protected readonly afterUpdate: () => Promise<any | void>;
    protected readonly onUpdate: (_: Update) => Promise<any | void>;

    getMe() {
        return this.client ? this.client.invoke({ _: 'getMe' }) : null;
    }

    dispose() {
        //this.clearListeners();
        if (this.client) {
            this.client.destroy();
            this.client = null;
            this.phoneOrToken = null;
            this.apiHash = null;
            this.apiId = null;
            this._id = 0;
        }
    }

    async logout() {
        if (this.client) {
            await this.client.invoke({ _: 'logOut' });
        }
        this.dispose();
    }

    async sendMessage(chat_id: number, content: string | inputMessageVideo$Input | inputMessageAnimation$Input | inputMessageText$Input | inputMessagePhoto$Input, reply_markup?: ReplyMarkup$Input, disable_notification?: boolean) {
        const message = await this.client.invoke({
            _: 'sendMessage',
            chat_id,
            reply_markup,
            options: disable_notification ? {
                _: 'messageSendOptions',
                disable_notification
            } : undefined,
            input_message_content: content.constructor === String ? {
                _: 'inputMessageText',
                text: this.client.execute({
                    _: 'parseTextEntities',
                    text: content as string,
                    parse_mode: { _: 'textParseModeMarkdown' }
                }) as formattedText,
                disable_web_page_preview: true
            } : content as inputMessageVideo$Input | inputMessageAnimation$Input | inputMessageText$Input | inputMessagePhoto$Input
        });
        return this.messagePendings.make(message.chat_id + '_' + message.id);//todo timeout from settings увеличить для видео
    }

    async downloadFile(f: file, signal?: AbortSignal, timeout?: number) {

        f = f.local?.path ? f : await new Promise<file>((resolve, reject) => {

            const cancel = e => {
                reject(e.type || e);
                if (f.local?.is_downloading_active) {
                    this.client.invoke({ _: 'cancelDownloadFile', file_id: f.id, }).catch(LOG_ERROR);
                }
            }

            if (signal) {
                if (signal.aborted) {
                    return reject('abort');
                }
                signal.addEventListener('abort', cancel);
            }

            const p = this.client.invoke({
                _: 'downloadFile',
                file_id: f.id,
                priority: 1,
                synchronous: true
            }).then(resolve, reject);

            if (timeout) {
                const t = setTimeout(cancel, timeout, 'timeout').unref();
                p.finally(() => {
                    signal?.removeEventListener('abort', cancel);
                    clearTimeout(t);
                });
            } else if (signal) {
                p.finally(() => signal.removeEventListener('abort', cancel));
            }
        });
        f.local.path = f.local.path.replace(/[\\\/]files[\\\/]/, '/files_unprotect/');
        return f;
    }

    protected async copyFile(source: file, remote: boolean, timeout: number): Promise<InputFile$Input> {
        return remote ?
            {
                _: 'inputFileRemote',
                id: source.remote.id
            } : {
                _: 'inputFileLocal',
                path: (await this.downloadFile(source, null, timeout)).local.path
            }
    }

    async messageToMediaInput(message: message, timeout: number, caption?: formattedText): Promise<inputMessageVideo$Input | inputMessageAnimation$Input> {

        const { thumbnail, width, height, duration, video, supports_streaming, animation } = (message.content['video'] || message.content['animation']) as DeepMerge<video, animation>;

        const [media, thumb] = await Promise.all([
            this.copyFile(video || animation, message.can_be_forwarded, timeout),
            thumbnail && this.copyFile(thumbnail['photo'] as file || thumbnail.file, message.can_be_forwarded, timeout)
        ]);

        return {
            _: video ? 'inputMessageVideo' : 'inputMessageAnimation',
            video: video ? media : undefined,
            animation: animation ? media : undefined,
            thumbnail: thumbnail ? {
                _: 'inputThumbnail',
                thumbnail: thumb,
                width: thumbnail.width,
                height: thumbnail.height,
            } : undefined,
            duration, width, height, supports_streaming, caption
        }
    }

    optimizeStorate(seconds: number) {
        return this.client?.invoke({
            _: 'optimizeStorage',
            size: -1,
            ttl: seconds,
            count: -1,
            immunity_delay: -1
        })
    }
}