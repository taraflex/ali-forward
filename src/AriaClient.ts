import { basename } from 'path';
import { Singleton } from 'typescript-ioc';
import WebSocket from 'ws';
import xxhash, { XXHashAPI } from 'xxhash-wasm';

import { Repeater } from '@repeaterjs/repeater';
import dataPath from '@utils/data-path';

import { DeferredMap } from './Deffered';
import { http } from './utils';

let xhash: XXHashAPI;

@Singleton
export class AriaClient {
    static readonly userAgent = 'Dalvik/1.4.0 (Linux; U; Android 2.3.5; HTC Desire HD A9191 Build/GRJ90)';
    static readonly cookiePath = dataPath('cookies.txt');
    protected static readonly pendings = new DeferredMap<boolean>();

    constructor() {
        this.thread();
    }

    async download(url: string) {
        try {
            xhash ||= await xxhash();
            const { pathname, searchParams } = new URL(url);
            const out = (basename(pathname, '.mp4') || searchParams.get('id')) + '.mp4';
            const gid = xhash.h64ToString(url);
            const dir = __dirname + '/static/files/';

            return (await AriaClient.pendings.make(gid, undefined, () => http.post('http://localhost:6800/jsonrpc', {
                dnsCache: false,
                json: {
                    jsonrpc: '2.0', id: gid,
                    method: 'aria2.addUri',
                    params: [[url], {
                        gid, out, dir,
                        'user-agent': AriaClient.userAgent,
                        'load-cookies': AriaClient.cookiePath,
                        'save-cookies': AriaClient.cookiePath,

                        'force-save': 'true',
                        'remove-control-file': 'false',
                        'allow-overwrite': 'true',
                        continue: 'true',
                        header: [
                            'Origin: https://vk.com',
                            'Referer: https://vk.com/',
                            'User-Agent: ' + AriaClient.userAgent
                        ]
                    }]
                }
            }))) ? dir + out : null;
        } catch (e) {
            LOG_ERROR(url, e);
            return null;
        }
    }

    checkOld(socket: WebSocket) {
        if (!AriaClient.pendings.empty && socket.readyState === WebSocket.OPEN) {
            socket.send(JSON.stringify({
                jsonrpc: '2.0', id: Date.now(),
                method: 'system.multicall',
                params: [AriaClient.pendings.keys().map(gid => ({
                    methodName: 'aria2.tellStatus',
                    params: [gid, ['gid', 'status']]
                }))]
            }), err => err && LOG_ERROR(err));
        }
    }

    async thread() {
        for (; ;) {
            try {
                for await (const { method, params, result } of new Repeater<{
                    method: string,
                    result?: {
                        gid: string,
                        status: 'active' | 'waiting' | 'paused' | 'error' | 'complete' | 'removed'
                    }[]
                    params?: { gid: string }[]
                }>(async (push, stop) => {
                    const socket = new WebSocket("ws://localhost:6800/jsonrpc");
                    socket.on('message', (e: string) => push(JSON.parse(e)));
                    socket.once('error', stop);
                    socket.once('stop', stop);
                    socket.once('open', () => this.checkOld(socket));
                    //const t = setInterval(() => this.checkOld(socket), 2000).unref();
                    await stop;
                    //clearInterval(t);
                    socket.close();
                })) {
                    if (result) {
                        for (const { status, gid } of result) {
                            if (status === 'error' || status === 'removed') {
                                AriaClient.pendings.fulfill(gid, false);
                            } else if (status === 'complete') {
                                AriaClient.pendings.fulfill(gid, true);
                            }
                        }
                    } else {
                        switch (method) {
                            case 'aria2.onDownloadComplete': params.forEach(({ gid }) => AriaClient.pendings.fulfill(gid, true)); break;
                            case 'aria2.onDownloadStop':
                            case 'aria2.onDownloadError': params.forEach(({ gid }) => AriaClient.pendings.fulfill(gid, false)); break;
                        }
                    }
                }
            } catch (err) {
                console.error(err);
                LOG_ERROR(err);
            }
        }
    }
}