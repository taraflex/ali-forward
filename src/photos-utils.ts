import { exec } from 'child_process';
import { promises as fs, statSync } from 'fs';
import { imageSize } from 'image-size';
import path from 'path';
import { Readable } from 'stream';

import { ClientSettings } from '@entities/ClientSettings';
import slug from '@sindresorhus/slugify';
import { HOSTNAME } from '@utils/config';

import { comap, hashArray, hashString, http, split } from './utils';

import type { Photo, ResizedLocalPhoto } from '@entities/Post';
import type { ISize } from 'image-size/dist/types/interface';

function fileSize(filename: string) {
    try {
        return statSync(filename).size | 0;
    } catch { }
    return 0;
}

function shuffle<T>(array: T[]) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

const braces = (s: string) => ` ( ${s} ) `;

function prow(photo: ResizedLocalPhoto, rowHeight: number) {
    return braces(`"${photo.local}" -resize "${photo.cellWidth}x${rowHeight}^" -gravity center -extent ${photo.cellWidth}x${rowHeight} -strokewidth 4 -fill none -stroke white -draw "rectangle 0,0 ${photo.cellWidth},${rowHeight}"`);
}

const CWD = __dirname + '/static/files/';

export function loadPhotos(photos: readonly Photo[], settings: ClientSettings): Promise<ResizedLocalPhoto[]> {
    const minS = maxLayout(photos.length);
    return comap(photos, async (photo: Photo) => {
        let { url, local, size, width, height } = photo;
        try {
            if (!(size = fileSize(local))) {
                if (!url) return;
                const U = new URL(url);
                local = CWD + (U.hostname.endsWith('alicdn.com') ?
                    slug(U.pathname.slice(1), { lowercase: false, decamelize: false }).replace(/-(jpe?g|png|webp|gif)$/i, '.$1') :
                    hashString(url)
                );

                let body: Buffer = null;
                if (url.includes('.alicdn.com/')) {
                    for (let s of [100, 120, 200, 250, 300, 350, 640]) {
                        if (s >= minS) {
                            try {
                                //q < q50 - ни на что не влияет (q только кратно 10)
                                body = (await http(url + `_${s}x${s}q100.jpg_.webp`, { timeout: settings.maxPhotoDownloadTime * 1000 })).rawBody;
                                break;
                            } catch { }
                        }
                    }
                }
                body ||= (await http(url, { timeout: settings.maxPhotoDownloadTime * 1000 })).rawBody;
                const is = imageSize(body);
                if (!is?.type) {
                    throw 'Unsuppotred type';
                }
                if (is) {
                    width = is.width;
                    height = is.height;
                    local = path.format({ ...path.parse(local), base: undefined, ext: '.' + is.type });
                }
                size = body.length;
                await fs.writeFile(local, body);
            }

            const isize: ISize = width && height ? { width, height } : imageSize(local);
            return Object.assign(photo, {
                size,
                width: isize.width,
                height: isize.height,
                local,
                aspect: isize.width / isize.height
            });
        } catch (e) {
            LOG_ERROR(url || local, e);
        }
    }, { required: settings.maxPhotosCount, concurrency: settings.maxPhotosConcurrency });
}

const smallLayouts = [/*0*/1,/*1*/ 1, /*2*/2, /*3*/2, /*4*/2, /*5*/2,/*6*/2, /*7*/3, /*8*/3];
function ll(length: number) {
    const rowsCount = smallLayouts[length] || (Math.sqrt(length) | 0)
    return [rowsCount,  /* rowSize */ (length / rowsCount) | 0];
}

function calcLayout<T>(ps: readonly T[]): T[][] {
    const [rowsCount, rowSize] = ll(ps.length);
    const bigRowCounts = ps.length % rowsCount;
    const c = bigRowCounts * (rowSize + 1);
    return [
        ...split(shuffle(ps.slice(0, c)), rowSize + 1),
        ...split(shuffle(ps.slice(c)), rowSize)
    ];
}

export const enum PSIZE {
    WIDTH = 800,//788,
    HEIGHT = 800//788
}

function maxLayout(length: number) {
    const [rowsCount, rowSize] = ll(length);
    return Math.max(Math.floor(PSIZE.WIDTH / rowSize), (PSIZE.HEIGHT / rowsCount) | 0);
}

const MAGICK_BIN = process.platform === 'win32' ? 'magick -script -' : 'magick-script -'

export async function concatPhotos(photos: readonly Photo[], settings: ClientSettings, requestLocal: boolean): Promise<string> {

    if (!photos?.length) return null;
    if (photos.length === 1 && !requestLocal && photos[0].url) return photos[0].url;

    const locals = await loadPhotos(photos, settings);
    if (!locals.length) return null;

    const filename = locals.length + '$' + hashArray(locals, 'local', 'size') + (locals.length === 1 ? path.extname(locals[0].local) : '.jpg');

    if (!fileSize(CWD + filename)) {

        if (locals.length === 1) {// todo force convert if not jpeg ?
            //www-data должен иметь +x доступ к папке с файлом
            await fs.symlink(locals[0].local.replace(/[\\\/]files_unprotect[\\\/]/, '/files/'), CWD + filename);
            await fs.chmod(CWD + filename, 0o644);

        } else {

            const table = calcLayout(locals.sort((a, b) => a.aspect - b.aspect));

            const rowHeight = (PSIZE.HEIGHT / table.length) | 0;

            const command = braces(table.map(row => {
                const sa = row.reduce((s, p) => s += p.aspect, 0);
                row.forEach(p => p.cellWidth = (PSIZE.WIDTH * p.aspect / sa) | 0);
                row[row.length - 1].cellWidth += PSIZE.WIDTH - row.reduce((s, p) => s += p.cellWidth, 0);
                return row.length === 1 ? prow(row[0], rowHeight) : braces(row.map(p => prow(p, rowHeight)).join('') + ' +append');
            }).join('')) + `-append -alpha Deactivate -define webp:image-hint=photo -define webp:alpha-compression=0 -quality ${settings.previewQuality} -write "./${filename}"`;

            const { stderr } = await new Promise((resolve, reject) => {
                const cp = exec(MAGICK_BIN, { cwd: CWD, windowsHide: true, encoding: 'utf-8' }, (err, _, stderr) => {
                    if (err) {
                        reject({ err, stderr });
                    } else {
                        resolve({ stderr });
                    }
                });
                Readable.from([command]).pipe(cp.stdin);
            });

            if (!fileSize(CWD + filename)) {
                throw `Fail generate ${filename}: ` + stderr;
            }
        }
    }
    return 'https://' + HOSTNAME + '/static/files/' + filename;
}