import CacheableLookup from 'cacheable-lookup';
import { execFile } from 'child_process';
import { createHash } from 'crypto';
import equal from 'fast-deep-equal';
import got from 'got';
import makeKfs from 'key-file-storage';
import { promises as dns } from 'dns';
import dataPath from '@utils/data-path';

import type { Response } from 'got/dist/source';
import type Request from 'got/dist/source/core';

export const kfs = makeKfs(dataPath('kfs')) as any as {
    restart_without_posts_after?: number;
    check_restart_at?: number[];
    last_post_published_at?: Date | string;
    terminated_by?: string;
    [_: string]: any;
};

export type DeepMerge<A, B> =
    (A | B) extends Record<string, unknown> ?
    & Partial<Omit<A, keyof B>>
    & Partial<Omit<B, keyof A>>
    & { [key in keyof (A | B)]: DeepMerge<A[key], B[key]> }
    : (A | B);

export function last<T>(a: readonly T[]) {
    return a && a[a.length - 1];
}

const taggedMap = {
    '{': '｛',
    '}': '｝',
    '`': 'ᐠ',
    '\\': '⧷',
    '\u2028': '\n',
    '\u2029': '\n'
};

export function escTaggedString(s: string) {
    return s ? s.replace(/[\{\}`\\\u{2028}\u{2029}]/gu, m => taggedMap[m]) : '';
}

const htmlMap = {
    ...taggedMap,
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};

export function escHtml(s: string) {
    return s ? s.replace(/[\{\}`\\\u{2028}\u{2029}&<>]/gu, m => htmlMap[m]) : '';
}

export async function html(literals: TemplateStringsArray, ...placeholders: (string | Promise<string>)[]) {
    placeholders = await Promise.all(placeholders);
    let result = escHtml(literals[0]);
    for (let i = 0; i < placeholders.length; i++) {
        result += placeholders[i] || '';
        result += escHtml(literals[i + 1]);
    }
    return result;
}

const NONE_PRINTABLE = /[\u0000-\u0008\u000B-\u001F\u007F-\u009F\u00AD\u0378\u0379\u037F-\u0383\u038B\u038D\u03A2\u0528-\u0530\u0557\u0558\u0560\u0588\u058B-\u058E\u0590\u05C8-\u05CF\u05EB-\u05EF\u05F5-\u0605\u061C\u061D\u06DD\u070E\u070F\u074B\u074C\u07B2-\u07BF\u07FB-\u07FF\u082E\u082F\u083F\u085C\u085D\u085F-\u089F\u08A1\u08AD-\u08E3\u08FF\u0978\u0980\u0984\u098D\u098E\u0991\u0992\u09A9\u09B1\u09B3-\u09B5\u09BA\u09BB\u09C5\u09C6\u09C9\u09CA\u09CF-\u09D6\u09D8-\u09DB\u09DE\u09E4\u09E5\u09FC-\u0A00\u0A04\u0A0B-\u0A0E\u0A11\u0A12\u0A29\u0A31\u0A34\u0A37\u0A3A\u0A3B\u0A3D\u0A43-\u0A46\u0A49\u0A4A\u0A4E-\u0A50\u0A52-\u0A58\u0A5D\u0A5F-\u0A65\u0A76-\u0A80\u0A84\u0A8E\u0A92\u0AA9\u0AB1\u0AB4\u0ABA\u0ABB\u0AC6\u0ACA\u0ACE\u0ACF\u0AD1-\u0ADF\u0AE4\u0AE5\u0AF2-\u0B00\u0B04\u0B0D\u0B0E\u0B11\u0B12\u0B29\u0B31\u0B34\u0B3A\u0B3B\u0B45\u0B46\u0B49\u0B4A\u0B4E-\u0B55\u0B58-\u0B5B\u0B5E\u0B64\u0B65\u0B78-\u0B81\u0B84\u0B8B-\u0B8D\u0B91\u0B96-\u0B98\u0B9B\u0B9D\u0BA0-\u0BA2\u0BA5-\u0BA7\u0BAB-\u0BAD\u0BBA-\u0BBD\u0BC3-\u0BC5\u0BC9\u0BCE\u0BCF\u0BD1-\u0BD6\u0BD8-\u0BE5\u0BFB-\u0C00\u0C04\u0C0D\u0C11\u0C29\u0C34\u0C3A-\u0C3C\u0C45\u0C49\u0C4E-\u0C54\u0C57\u0C5A-\u0C5F\u0C64\u0C65\u0C70-\u0C77\u0C80\u0C81\u0C84\u0C8D\u0C91\u0CA9\u0CB4\u0CBA\u0CBB\u0CC5\u0CC9\u0CCE-\u0CD4\u0CD7-\u0CDD\u0CDF\u0CE4\u0CE5\u0CF0\u0CF3-\u0D01\u0D04\u0D0D\u0D11\u0D3B\u0D3C\u0D45\u0D49\u0D4F-\u0D56\u0D58-\u0D5F\u0D64\u0D65\u0D76-\u0D78\u0D80\u0D81\u0D84\u0D97-\u0D99\u0DB2\u0DBC\u0DBE\u0DBF\u0DC7-\u0DC9\u0DCB-\u0DCE\u0DD5\u0DD7\u0DE0-\u0DF1\u0DF5-\u0E00\u0E3B-\u0E3E\u0E5C-\u0E80\u0E83\u0E85\u0E86\u0E89\u0E8B\u0E8C\u0E8E-\u0E93\u0E98\u0EA0\u0EA4\u0EA6\u0EA8\u0EA9\u0EAC\u0EBA\u0EBE\u0EBF\u0EC5\u0EC7\u0ECE\u0ECF\u0EDA\u0EDB\u0EE0-\u0EFF\u0F48\u0F6D-\u0F70\u0F98\u0FBD\u0FCD\u0FDB-\u0FFF\u10C6\u10C8-\u10CC\u10CE\u10CF\u1249\u124E\u124F\u1257\u1259\u125E\u125F\u1289\u128E\u128F\u12B1\u12B6\u12B7\u12BF\u12C1\u12C6\u12C7\u12D7\u1311\u1316\u1317\u135B\u135C\u137D-\u137F\u139A-\u139F\u13F5-\u13FF\u169D-\u169F\u16F1-\u16FF\u170D\u1715-\u171F\u1737-\u173F\u1754-\u175F\u176D\u1771\u1774-\u177F\u17DE\u17DF\u17EA-\u17EF\u17FA-\u17FF\u180F\u181A-\u181F\u1878-\u187F\u18AB-\u18AF\u18F6-\u18FF\u191D-\u191F\u192C-\u192F\u193C-\u193F\u1941-\u1943\u196E\u196F\u1975-\u197F\u19AC-\u19AF\u19CA-\u19CF\u19DB-\u19DD\u1A1C\u1A1D\u1A5F\u1A7D\u1A7E\u1A8A-\u1A8F\u1A9A-\u1A9F\u1AAE-\u1AFF\u1B4C-\u1B4F\u1B7D-\u1B7F\u1BF4-\u1BFB\u1C38-\u1C3A\u1C4A-\u1C4C\u1C80-\u1CBF\u1CC8-\u1CCF\u1CF7-\u1CFF\u1DE7-\u1DFB\u1F16\u1F17\u1F1E\u1F1F\u1F46\u1F47\u1F4E\u1F4F\u1F58\u1F5A\u1F5C\u1F5E\u1F7E\u1F7F\u1FB5\u1FC5\u1FD4\u1FD5\u1FDC\u1FF0\u1FF1\u1FF5\u1FFF\u200B\u200C\u200E\u200F\u202A-\u202E\u2060-\u206F\u2072\u2073\u208F\u209D-\u209F\u20BB-\u20CF\u20F1-\u20FF\u218A-\u218F\u23F4-\u23F7\u23FB-\u23FF\u2427-\u243F\u244B-\u245F\u2700\u2B4D-\u2B4F\u2B5A-\u2BFF\u2C2F\u2C5F\u2CF4-\u2CF8\u2D26\u2D28-\u2D2C\u2D2E\u2D2F\u2D68-\u2D6E\u2D71-\u2D7E\u2D97-\u2D9F\u2DA7\u2DAF\u2DB7\u2DBF\u2DC7\u2DCF\u2DD7\u2DDF\u2E3C-\u2E7F\u2E9A\u2EF4-\u2EFF\u2FD6-\u2FEF\u2FFC-\u2FFF\u3040\u3097\u3098\u3100-\u3104\u312E-\u3130\u318F\u31BB-\u31BF\u31E4-\u31EF\u321F\u32FF\u4DB6-\u4DBF\u9FCD-\u9FFF\uA48D-\uA48F\uA4C7-\uA4CF\uA62C-\uA63F\uA698-\uA69E\uA6F8-\uA6FF\uA78F\uA794-\uA79F\uA7AB-\uA7F7\uA82C-\uA82F\uA83A-\uA83F\uA878-\uA87F\uA8C5-\uA8CD\uA8DA-\uA8DF\uA8FC-\uA8FF\uA954-\uA95E\uA97D-\uA97F\uA9CE\uA9DA-\uA9DD\uA9E0-\uA9FF\uAA37-\uAA3F\uAA4E\uAA4F\uAA5A\uAA5B\uAA7C-\uAA7F\uAAC3-\uAADA\uAAF7-\uAB00\uAB07\uAB08\uAB0F\uAB10\uAB17-\uAB1F\uAB27\uAB2F-\uABBF\uABEE\uABEF\uABFA-\uABFF\uD7A4-\uD7AF\uD7C7-\uD7CA\uD7FC-\uD83B\uD83F-\uDB3F\uDB41-\uDBFF\uDCFE\uDD46\uDDCC\uDE52-\uDE6F\uDE75-\uDE77\uDE7B-\uDE7F\uDEC6-\uDECA\uDED8-\uDEDF\uDEE6-\uDEE8\uDEEA\uDEED-\uDEEF\uDEF1\uDEF2\uDEFD-\uDEFF\uDF22\uDF23\uDF94\uDF95\uDF98\uDF9C\uDF9D\uDFF1\uDFF2\uDFF6\uE000-\uF8FF\uFA6E\uFA6F\uFADA-\uFAFF\uFB07-\uFB12\uFB18-\uFB1C\uFB37\uFB3D\uFB3F\uFB42\uFB45\uFBC2-\uFBD2\uFD40-\uFD4F\uFD90\uFD91\uFDC8-\uFDEF\uFDFE\uFDFF\uFE0F\uFE1A-\uFE1F\uFE27-\uFE2F\uFE53\uFE67\uFE6C-\uFE6F\uFE75\uFEFD-\uFF00\uFFBF-\uFFC1\uFFC8\uFFC9\uFFD0\uFFD1\uFFD8\uFFD9\uFFDD-\uFFDF\uFFE7\uFFEF-\uFFFB\uFFFE\uFFFF]/g;
///[\u{0000}-\u{0008}\u{000B}-\u{0019}\u{001b}\u{009b}\u{00ad}\u{200b}-\u{200f}\u{2028}\u{2029}\u{2060}\u{feff}\u{fe00}-\u{fe0f}]+/gu;
export function dropHidden(s: string) {
    NONE_PRINTABLE.lastIndex = 0;
    return s ? s.normalize().replace(NONE_PRINTABLE, '') : '';
}

export function waitResponse(request: Request) {
    return new Promise<Response>((resolve, reject) => request.once('close', resolve).once('error', reject).once('response', resolve)).finally(() => { request.destroy() });
}

export function findTypeOf<T extends { type: string }>(props: readonly string[], a?: readonly T[]): T {
    if (a?.length) {
        for (let p of props) {
            const v = a.find(e => e.type === p);
            if (v) {
                return v;
            }
        }
    }
}

export const dnsResolver = new dns.Resolver({ timeout: 10000 });

export const http = got.extend({
    method: 'GET',
    responseType: 'buffer',
    timeout: 30000,
    maxRedirects: 16,
    dnsCache: new CacheableLookup({
        resolver: dnsResolver,
        fallbackDuration: 0,
        lookup: null
    }),
    dnsLookupIpVersion: 'ipv4',
    headers: {
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',
        Referer: 'https://vk.com/',
        'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
    },
    https: {
        rejectUnauthorized: false
    },
    retry: 0
});


export function comap<T, R, Args extends any[]>(
    a: readonly T[],
    map: (v: T, ...args: Args) => PromiseLike<R> | R,
    options?: {
        filter?: (v: R) => boolean,
        required?: number,
        concurrency?: number,
        breakOnError?: boolean
    },
    ...args: Args
): Promise<R[]> {

    const breakOnError = !!options?.breakOnError;
    const filter = options?.filter || Boolean;
    const required = Math.min(Math.max(0, options?.required) || a.length, a.length);
    let concurrency = Math.min(Math.max(0, options?.concurrency) || required, required);

    async function _map(v: T) {
        return map(v, ...args);
    }

    let i = 0;
    const s = new Set<PromiseLike<R>>();

    function next() {
        if (s.size < required && i < a.length) {
            const p = _map(a[i++]);
            s.add(p);
            return p.then(r => { if (!filter(r)) { s.delete(p); } next(); }, breakOnError ? _ => { i = a.length } : next)
        }
    }

    const threads = [];
    while (concurrency--) {
        threads.push(next());
    }

    return Promise.allSettled(threads).then(_ => Promise.all(s));
}

export function toChatId(supergroup_id: number) {
    return +('-100' + supergroup_id);
}

export function toSupergroupId(chat_id: number) {
    return +chat_id.toString().replace(/^-100/, '');
}

export function tgLink(chat_id: number, message_id: number = 1048576) {
    return `tg://privatepost?channel=${toSupergroupId(chat_id)}&post=${message_id / 1048576}`;
}

export function* split<T extends string | Buffer | ReadonlyArray<any>>(s: T, size: number): Generator<T, void> {
    for (let i = 0; i < s.length; i += size) {
        yield s.slice(i, i + size) as T;
    }
}

export type KeysMatching<T, V> = { [K in keyof T]: T[K] extends V ? K : never }[keyof T];
export function sortByKey<T>(a: T[], key: KeysMatching<T, string>) {
    //@ts-ignore
    return a.sort((a, b) => a[key].localeCompare(b[key]));
}

export function hashString(s: string) {
    return createHash('sha1').update(s, 'utf-8').digest('base64url');
}

export function hashArray<T>(a: readonly T[], ...keys: (keyof T)[]) {
    return hashString(a.map(v => keys.map(k => v[k]).join('?')).sort().join('|'));
}

export function equalArrays<T>(a: readonly T[], b: readonly T[], key: KeysMatching<T, string>) {
    return a.map(v => v[key]).sort().join('|') === b.map(v => v[key]).sort().join('|')
}

export function ffprobe(path: string): Promise<{ streams: any[] }> {
    return new Promise(resolve => {
        execFile('ffprobe', ['-hide_banner', '-show_streams', '-print_format', 'json', path], (err, stdout, stderr) => {
            try {
                if (err) {
                    throw (err.code === 'ENOENT' ? err : stderr);
                }
                return resolve(JSON.parse(stdout));
            } catch (e) {
                LOG_ERROR(e);
            }
            resolve(null);
        });
    });
}

export const regTransform = (() => {
    /*
    ЙО, ИО, ЙЕ, ИЕ → И
    О, Ы, Я → А
    Е, Ё, Э → И
    Ю → У
    */
    const lgroups = [
        'а - a' + 'я - a' + 'о - o0 ѳө' + 'ы - y',
        'б - b ',
        'в - vb ѵ',
        'г - g ԍ',
        'д - d ԁ',
        'ж - zj ',
        'з - z3 ӡ',
        'и - i ӏіĭ' + 'й - j ѝӣ' + 'е - e ҽ' + 'ё - o' + 'э - e',
        'к - k',
        'л - l ᴫ',
        'м - m ',
        'н - nh',
        'п - pn',
        'р - rp',
        'с - sc',
        'т - t',
        'у - uy' + 'ю - u',
        'ф - f',
        'х - hx',
        'ц - ct',
        'ч - c',
        'шщ - s',
        //'ъь - b ꙏ',
        'q - кԛ',
        'h - һ',
        'w - вԝѡ',
        'j - ј',
        'x - к',
        '@'
    ]

    const LETTERS = Array.from(new Set(lgroups.join('').replace(/[\-\s]+/g, ''))).join('');

    const LR: Record<string, string> = Object.create(null);
    for (const l of LETTERS) {
        LR[l] = Array.from(new Set(lgroups.filter(g => g.includes(l)).join('').replace(/[\-\s]+/g, ''))).join('');
    }
    LR[' '] = ' ';

    const NOT_LETTERS = new RegExp('[^' + LETTERS + ']+', 'ugis');

    return (s: string | Function | RegExp | true, notFist: number | boolean): string[] => {
        if (s instanceof Function) {
            return s(notFist);
        } else if (s instanceof RegExp) {
            return [dropHidden(s.source)];
        } else if (s.constructor === String) {
            s = dropHidden(s).replace(NOT_LETTERS, ' ').trim().toLowerCase();
            const a = Array.from(s, l => LR[l])
                .filter((l, i, a) => a[i - 1] != l)
                .map((l, i, a) => l === ' ' ? [] : [`[^${l}]{${!a[i - 1] || a[i - 1] === ' ' ? '1,6' : '0,3'}}`, `[${l}]+`])
                .flat();
            return notFist ? a : a.slice(1);
        } else if (s) {
            return ['.*'];
        }
        return [];
    }
})();


export function asyncMemoizeLast<Args extends any[], R>(fn: (...args: Args) => Promise<R>) {

    let prevArgs: Args;
    let lastResult: ReturnType<typeof fn>;

    return (...args: Args) => {

        if (equal(args, prevArgs)) {
            return lastResult;
        }

        prevArgs = undefined;

        lastResult = fn(...args);
        lastResult.catch(() => prevArgs = undefined);

        prevArgs = args;

        return lastResult;
    }
}