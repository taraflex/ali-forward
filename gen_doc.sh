typedoc --disableSources --excludeNotExported --ignoreCompilerErrors --hideGenerator --excludeExternals ./src/entities/Post.ts ./src/patch-utils.ts --out ./build/static/docs 

typedoc --disableSources --excludeNotExported --ignoreCompilerErrors --hideGenerator --excludeExternals ./src/entities/Post.ts --json ./src/entities/types.defs

npx tsc --skipLibCheck --removeComments --noResolve --declaration --emitDeclarationOnly --module none ./src/patch-utils.ts ./src/entities/Post.ts --outFile ./src/entities/types
sed -i "/export class Post/Q" ./src/entities/types.d.ts
sed -i "/module\|import\|^}/d" ./src/entities/types.d.ts
sed -i 's/export/declare/g' ./src/entities/types.d.ts
sed -i 's/^    //g' ./src/entities/types.d.ts
mv ./src/entities/types.d.ts ./src/entities/types.shell